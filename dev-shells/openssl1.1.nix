{
  nixpkgs,
  system,
  ...
}:
let
  pkgs = import nixpkgs {
    inherit system;
    config = {
      allowUnfree = true;
      permittedInsecurePackages = import ../resources/security/permitted-insecure-packages.nix;
    };
  };
  opensslCnf = builtins.readFile ../resources/configs/openssl_tls10.cnf;
  opensslConfFile = builtins.toFile "openssl-tls11.cnf" opensslCnf;

  netCore = import ../pkgs/dotnet/dotnet.nix pkgs;
in
{
  openssl_1_1Shell = pkgs.mkShell {
    nativeBuildInputs = [
      pkgs.openssl_3_4
      netCore
    ];

    shellHook = ''
      export OPENSSL_CONF=${opensslConfFile}
      export CLR_OPENSSL_VERSION_OVERRIDE="1.1"
      echo "TLS 1.0 environment activated."
    '';
  };
}
