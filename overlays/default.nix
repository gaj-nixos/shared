# This file defines overlays
{ inputs }:
{
  # This one brings our custom packages from the 'pkgs' directory
  additions = final: prev: import ../pkgs final.pkgs;

  # This one contains whatever you want to overlay
  # You can change versions, add patches, set compilation flags, anything really.
  # https://nixos.wiki/wiki/Overlays
  modifications = final: prev: {
    # example = prev.example.overrideAttrs (oldAttrs: rec {
    # ...
    # });
  };

  gaelj-packages = final: prev: {
    gaelj = import inputs.nixpkgs-gaelj {
      system = final.system;
      config = {
        permittedInsecurePackages = import ../resources/security/permitted-insecure-packages.nix;
        allowUnfree = true;
      };
    };
  };

  master-packages = final: prev: {
    master = import inputs.nixpkgs-master {
      system = final.system;
      config = {
        permittedInsecurePackages = import ../resources/security/permitted-insecure-packages.nix;
        allowUnfree = true;
      };
    };
  };

  stable-packages = final: prev: {
    stable = import inputs.nixpkgs-stable {
      system = final.system;
      config = {
        permittedInsecurePackages = import ../resources/security/permitted-insecure-packages.nix;
        allowUnfree = true;
      };
    };
  };
}
