{
  description = "Common library for Nix flakes";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:NixOS/nixpkgs/master";
    nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-gaelj.url = "github:gaelj/nixpkgs/gaelj-pkgs";

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    flake-utils.url = "github:numtide/flake-utils";

    stylix.url = "github:danth/stylix";
    stylix.inputs.nixpkgs.follows = "nixpkgs";
    stylix.inputs.home-manager.follows = "home-manager";
    stylix.inputs.flake-utils.follows = "flake-utils";

    #nvf.url = "github:notashelf/nvf";
    #nvf.inputs.nixpkgs.follows = "nixpkgs";
    #nvf.inputs.flake-utils.follows = "flake-utils";

    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";
    agenix.inputs.home-manager.follows = "home-manager";

    ragenix.url = "github:yaxitech/ragenix";
    ragenix.inputs.nixpkgs.follows = "nixpkgs";
    ragenix.inputs.agenix.follows = "agenix";
    ragenix.inputs.flake-utils.follows = "flake-utils";

    agenix-rekey.url = "github:oddlama/agenix-rekey";
    agenix-rekey.inputs.nixpkgs.follows = "nixpkgs";

    hyprpanel.url = "github:Jas-SinghFSU/HyprPanel";
    hyprpanel.inputs.nixpkgs.follows = "nixpkgs";

    crowdsec.url = "git+https://codeberg.org/kampka/nix-flake-crowdsec?ref=main";
    crowdsec.inputs.nixpkgs.follows = "nixpkgs";

  };

  outputs =
    {
      self,
      nixpkgs,
      nixpkgs-gaelj,
      flake-utils,
      ...
    }@inputs:
    let
      system = flake-utils.lib.system.x86_64-linux;
      pkgs = nixpkgs.legacyPackages.${system};
      nixpkgsGaelj = nixpkgs-gaelj.legacyPackages.${system};
      ourPackages = import ./pkgs pkgs;
      inherit (pkgs) lib;

      inherit (import ./lib/nixos/make-system.nix { inherit inputs; }) makeSystem;
      inherit
        (import ./lib/nixos/make-iso.nix {
          inherit inputs;
          sharedConfig = self;
        })
        makeIso
        ;
      inherit (import ./lib/sudo-no-password.nix { }) sudoNoPassword;
      make-podman-services = import ./lib/containers/make-podman-services.nix { inherit pkgs lib; };
      container-db-backup = import ./lib/containers/container-db-backup.nix { sharedConfig = self; };
      make-agenix-generator-prompt = import ./lib/agenix/make-agenix-generator-prompt.nix;
      make-agenix-container-environment-file = import ./lib/agenix/make-agenix-container-environment-file.nix;
      make-agenix-container-environment-file-matrix = import ./lib/agenix/make-agenix-container-environment-file-matrix.nix;
      make-agenix-container-environment-file-sync-v3 = import ./lib/agenix/make-agenix-container-environment-file-sync-v3.nix;
      make-nginx-hosts = import ./lib/nginx/make-nginx-hosts.nix;
      nginx-config-redirect = import ./lib/nginx/nginx-config-redirect.nix;
      nginx-config = import ./lib/nginx/nginx-config.nix;
      geo-ip-config = import ./lib/nginx/geo-ip-config.nix;
      cloudflare = import ./lib/nginx/cloudflare.nix { inherit lib pkgs; };
      make-samba-client = import ./lib/make-samba-client.nix { inherit pkgs; };
      stylixCommon = import ./modules/stylix.nix { stylixSettings = null; };
      generatePythonPackages = import ./lib/scripts/generate-python-packages.nix { inherit pkgs; };
      generateScriptPackages = import ./lib/scripts/generate-script-packages.nix { inherit pkgs; };

      nixosModules = {
        shared-config =
          { ... }:
          {
            imports = [
              stylixCommon
              ./options
              ./options/dump.nix
              ./modules/nixos
              ./python
              ./modules/users.nix
            ];
          };
      };

      gitlabDl = import ./python/gitlab-dl { inherit pkgs; };
      riskMetric = import ./python/risk-metric { inherit pkgs; };
      coinInfo = import ./python/coin-info { pkgs = nixpkgsGaelj; };
    in
    {
      inherit nixosModules;

      lib = {
        inherit
          sudoNoPassword
          makeSystem
          makeIso
          make-agenix-generator-prompt
          make-agenix-container-environment-file
          make-agenix-container-environment-file-matrix
          make-agenix-container-environment-file-sync-v3
          make-nginx-hosts
          nginx-config-redirect
          nginx-config
          geo-ip-config
          cloudflare
          make-samba-client
          make-podman-services
          container-db-backup
          generatePythonPackages
          generateScriptPackages
          ;
        sharedAllowedSystemCommandsList = import ./resources/security/sudo-no-password-commands.nix;
        commonAliases = ./resources/users/common-aliases.nix;
        sshPublicKeys = {
          gaj = builtins.readFile ./resources/ssh/ssh-gaj.pub;
        };
        agePublicKeys = {
          aero-server = builtins.readFile ./resources/age/aero-server.age.pub;
          dev-server = builtins.readFile ./resources/age/dev-server.age.pub;
          iso = builtins.readFile ./resources/age/iso.age.pub;
          mjolnir = builtins.readFile ./resources/age/mjolnir.age.pub;
          pi4-server = builtins.readFile ./resources/age/pi4-server.age.pub;
          router = builtins.readFile ./resources/age/router.age.pub;
          vmware-workstation = builtins.readFile ./resources/age/vmware-workstation.age.pub;
        };
      };

      formatter."${system}" = nixpkgs.legacyPackages.${system}.nixfmt-rfc-style;

      packages."${system}" = with ourPackages; {
        inherit
          libvirt-dbus
          dotnet-symbol
          nugethierarchy
          nugetupdater
          dotnet-outdated
          options-doc
          ;
      };

      agenix-rekey = inputs.agenix-rekey.configure {
        userFlake = self;
        nixosConfigurations = self.nixosConfigurations;
      };

      devShells."${system}" = {
        openssl_1_1 = (import ./dev-shells/openssl1.1.nix { inherit nixpkgs system; }).openssl_1_1Shell;
        risk-metric = (generatePythonPackages riskMetric).devShell;
        gitlabDl = (generatePythonPackages gitlabDl).devShell;
        coin-info = (generatePythonPackages coinInfo).devShell;
      };

      nixosConfigurations = {
        default = makeSystem {
          inherit system;
          inputs = inputs // {
            sharedConfig = self;
          };
          modules = [
            nixosModules.shared-config
            ./resources/test-config.nix
          ];
        };

        iso = makeIso {
          inherit system;
          inputs = inputs // {
            sharedConfig = self;
          };
          rootPath = ./.;
          targetSystem = self.nixosConfigurations.default;
          modules = [
            #nixosModules.disko
            #nixosModules.shared-config
            #./resources/test-config.nix
          ];
          packages = [ ];
        };
      };
    };
}
