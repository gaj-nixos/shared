#!/usr/bin/env bash

# This script is used to generate any missing secrets and rekey the existing ones.
# These commands can be run with the -f flag to force the generation of new secrets.
# Secrets can be either generated automatically, or the user will be prompted to enter them manually.

# usage: ./update-secrets.sh

echo "******************************"
echo "** 🔒️  Updating secrets 🔒️ **"
echo "******************************"

agenix generate -a
agenix rekey -a
