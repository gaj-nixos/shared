{ lib, ... }:
let
  inherit (lib) mkOption types;
  inherit (types) str;
in
{ ... }:
{
  options = {
    hostName = mkOption {
      type = str;
      default = "";
      description = "Host name";
    };
    description = mkOption {
      type = str;
      default = "";
      description = "Description";
    };
    ip = mkOption {
      type = str;
      default = "";
      description = "IP address";
    };
    mac = mkOption {
      type = str;
      default = "";
      description = "MAC address";
    };
  };
}
