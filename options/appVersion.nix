{ lib, ... }:
let
  inherit (lib) mkOption types;
  inherit (types) nullOr str;
in
{ ... }:
{
  options = {
    version = mkOption {
      type = str;
      default = "";
      description = "Application version";
    };
    sha = mkOption {
      type = nullOr str;
      default = null;
      description = "Sha signature";
    };
  };
}
