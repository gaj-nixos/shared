{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  inherit (lib)
    mkOption
    types
    mkEnableOption
    optional
    any
    ;
  inherit (types)
    nullOr
    str
    enum
    int
    listOf
    attrsOf
    bool
    submodule
    path
    anything
    ;

  defaultVaultwardenPort = 8081;
  defaultHomeAssistantPort = 8123; # don't change (network=host)
  defaultZwaveJsPort1 = 8091; # don't change (network=host)
  defaultZwaveJsPort2 = 3000; # don't change (network=host)
  defaultMosquittoPort1 = 9001; # don't change (network=host)
  defaultMosquittoPort2 = 1883; # don't change (network=host)
  defaultEspHomePort1 = 6123; # don't change (network=host)
  defaultEspHomePort2 = 6052; # don't change (network=host)
  defaultTikaPort = 9998;
  defaultSearxPort = 8888;
  defaultOllamaPort = 11434; # don't change (network=host)
  defaultOpenwebuiPort = 8080;
  defaultOpenwebuiPipelinesPort = 9099;
  defaultDashyPort = 4100;
  defaultGlancesPort = 61208; # don't change (network=host)
  defaultCockpitPort = 9090;
  defaultSilverBulletPort = 8082;
  defaultGotifyPort = 8083;
  defaultImmichPort = 8084;
  defaultNavidromePort = 4533;
  defaultFail2restPort = 5000;
  defaultFail2webPort = 8085;
  defaultAdGuardHomePort = 8086;
  defaultGonicPort = 8087;
  defaultAirsonicPort = 8088;
  defaultPaperlessNgxPort = 8089;
  defaultFireflyIiiPort = 8090;
  defaultFireflyIiiImporterPort = 8092;
  defaultChangeDetectionIoPort = 8093;
  defaultChangeDetectionIoChromePort = 8094;
  defaultBinaryCachePort = 8095;
  defaultWallabagPort = 8096;
  defaultTabbyPort = 8097;
  defaultElasticSearchPort = 9200;
  defaultCrowdsecPort = 8098;
  defaultJupyterPort = 8000;
  defaultDendritePort1 = 8008;
  defaultDendritePort2 = 18448;
  defaultSyncV3Port = 8009;
  defaultDockerRegistryPort = 8099;
  defaultSynapseAdminPort = 8100;

  defaultBinaryCacheServerName = "binarycache";

  # Define the per-user options as a submodule
  appVersion = import ./appVersion.nix { inherit lib; };
  userSettings = import ./userSettings.nix { inherit lib pkgs; };
  stylixSettings = import ./stylixSettings.nix { inherit lib pkgs; };
  dashySettings = import ./dashySettings.nix { inherit lib; };
  dockerRegistrySettings = import ./dockerRegistrySettings.nix { inherit lib; };
  dashySectionItem = import ./dashySectionItem.nix { inherit lib; };
  getProxySettings = import ./proxySettings.nix { inherit lib config; };

  inherit (sharedConfig.lib.cloudflare) cfConfigRealIpV4 cfConfigRealIpV6;

  wa = config.sharedConfig.webApps;
  sv = config.sharedConfig.services;
  nw = config.sharedConfig.network;
  agh = nw.adGuardHome;
  host = config.networking.hostName;
  fqdn = nw.fullyQualifiedDomainName;
  wm = config.sharedConfig.windowManagers;

  anyUserHasI3 = any (u: (u ? windowManagers.i3.enable) && u.windowManagers.i3.enable) (
    lib.attrValues config.sharedConfig.users
  );
  anyUserHasHypr = any (u: (u ? windowManagers.hyprland.enable) && u.windowManagers.hyprland.enable) (
    lib.attrValues config.sharedConfig.users
  );
  anyUserHasWin10 = any (u: (u ? windowManagers.win10.enable) && u.windowManagers.win10.enable) (
    lib.attrValues config.sharedConfig.users
  );
in
{
  options = {
    sharedConfig = {
      appVersions = mkOption {
        description = "Containerized and derivation application versions";
        default = { };
        type = attrsOf (submodule appVersion);
      };

      windowManagers = {
        i3 = mkOption {
          type = bool;
          default = anyUserHasI3;
          description = "i3 window manager";
        };
        hyprland = mkOption {
          type = bool;
          default = anyUserHasHypr;
          description = "Hyprland window manager";
        };
        win10 = mkOption {
          type = bool;
          default = false;
          description = "Windows 10 style window manager";
        };
        disableAnimations = mkEnableOption "Disable window manager animation effects";
        disableShadows = mkEnableOption "Disable window manager shadow effect";
        disableBlur = mkEnableOption "Disable window manager blur effect";
        wayland = mkOption {
          type = bool;
          default = (wm.hyprland || wm.win10) && (anyUserHasHypr || anyUserHasWin10);
          description = "Wayland window manager";
        };
        x11 = mkOption {
          type = bool;
          default = wm.i3 && anyUserHasI3;
          description = "X11 window manager";
        };
      };

      disks = {
        partitioning = mkOption {
          type = nullOr (enum [
            "simple"
            "split"
            "disko"
          ]);
          default = null;
          description = "Partitioning scheme: null, simple, split or with disko";
        };
      };

      boot = {
        videoResolution = mkOption {
          type = str;
          default = "1920x1080@60";
          description = "Video resolution to use for the boot screen";
        };
        enablePlymouth = mkEnableOption "Plymouth: boot screen theming";
        autologinUser = mkOption {
          type = nullOr str;
          default = null;
          description = "User to autologin into kmscon (optional)";
        };
        loader = mkOption {
          type = enum [
            "systemd-boot"
            "extlinux"
          ];
          default = "systemd-boot";
          description = "Boot loader: systemd-boot (standard case) or extlinux (for Raspberry Pi)";
        };
      };

      network = {
        interface = mkOption {
          type = str;
          default = "eth0";
          description = "The network interface to use for samba";
        };
        networkd = {
          enable = mkEnableOption "systemd-networkd";
        };
        domain = mkOption {
          type = str;
          default = "home.arpa";
          description = "The domain name for the machine";
        };
        fullyQualifiedDomainName = mkOption {
          type = str;
          default = "${config.networking.hostName}.${nw.domain}";
          description = "The fully qualified domain name of the machine";
        };
        publicHost = mkOption {
          type = str;
          default = "";
          description = "The public host name";
        };
        ssl = {
          allowSsl11 = mkEnableOption "Allow deprecated SSL v1.1 (to connect to old SQL servers)";
          additionalCertificates = mkOption {
            type = listOf path;
            default = [ ];
            description = "Additional certificates to trust";
          };
          rootCACertPath = mkOption {
            type = nullOr path;
            default = null;
            description = "Path to the root CA certificate";
          };
        };
        firewall = {
          enable = mkEnableOption "Firewall";
          enableOpenSnitch = mkEnableOption "OpenSnitch application firewall";
        };
        fail2ban = {
          enable = mkEnableOption "Fail2ban";
          ignoreIP = mkOption {
            type = listOf str;
            default = [
              "127.0.0.1/8"
              "192.168.0.0/16"
              "10.0.0.0/8"
            ];
            description = "IP addresses to ignore";
          };
        };
        adGuardHome =
          getProxySettings {
            serviceName = "AdGuard Home";
            defaultPort = defaultAdGuardHomePort;
            defaultServerName = "adguard";
            parent = nw.adGuardHome;
          }
          // {
            # Docs: https://github.com/AdguardTeam/AdGuardHome/wiki/Configuration#configuration-file (latest revision commit: 3e1436e)
            userName = mkOption {
              type = str;
              default = "admin";
              description = "The user name to use for AdGuard Home";
            };
            userPassword = mkOption {
              type = str;
              default = "";
              description = "The Bcrypt encrypted password to use for AdGuard Home";
            };
            parentalEnabled = mkEnableOption "Parental control enabled";
            safeSearchEnabled = mkEnableOption "Safe search enabled";
            safeBrowsingEnabled = mkEnableOption "Safe browsing enabled";
            isPublic = mkEnableOption "Public AdGuard Home";
            upstreamDnsServers = mkOption {
              type = listOf str;
              default = [
                "https://unfiltered.adguard-dns.com/dns-query"
                "https://dns.cloudflare.com/dns-query"
                "https://dns10.quad9.net/dns-query"
              ];
              description = "Upstream DNS servers";
            };
            externalDnsServerIps = mkOption {
              type = listOf str;
              default = [
                "9.9.9.10"
                "149.112.112.10"
                "2620:fe::10"
                "2620:fe::fe:10"
              ];
              description = "External DNS servers";
            };
            rewrites = mkOption {
              type = listOf (submodule {
                options = {
                  domain = mkOption {
                    type = str;
                    description = "domain";
                  };
                  answer = mkOption {
                    type = str;
                    description = "answer";
                  };
                };
              });
              default = [ ];
              description = "Rewrites";
            };
            blockedServiceIds = mkOption {
              type = listOf str;
              default = [ ];
              description = "Blocked service IDs";
            };
            dhcpServerEnabled = mkEnableOption "DHCPv4 server";
            dhcpv4 = mkOption {
              type = nullOr (submodule {
                options = {
                  gateway_ip = mkOption {
                    type = str;
                    default = "";
                    description = "Gateway IP";
                  };
                  subnet_mask = mkOption {
                    type = str;
                    default = "";
                    description = "Subnet mask";
                  };
                  range_start = mkOption {
                    type = str;
                    default = "";
                    description = "Range start";
                  };
                  range_end = mkOption {
                    type = str;
                    default = "";
                    description = "Range end";
                  };
                  lease_duration = mkOption {
                    type = int;
                    default = 86400;
                    description = "Lease duration";
                  };
                  icmp_timeout_msec = mkOption {
                    type = int;
                    default = 1000;
                    description = "ICMP timeout msec";
                  };
                  options = mkOption {
                    type = listOf anything;
                    default = [ ];
                    description = "DHCPv4 options";
                  };
                };
              });
              default = null;
              description = "DHCPv4 options";
            };
            dhcpv6RangeStart = mkOption {
              type = str;
              default = "2001::1";
              description = "DHCPv6 range start";
            };
            userRules = mkOption {
              type = listOf str;
              default = [ ];
              description = "User rules";
            };
            dnsOverHttps = mkEnableOption "DNS over HTTPS";
            trustedProxies = mkOption {
              type = listOf str;
              default = [ ];
              example = [ "10.0.0.1/24" ];
              description = "Trusted proxies";
            };
            blockedHosts = mkOption {
              type = listOf str;
              default = [ ];
              example = [ "version.bind" ];
              description = "Blocked hosts";
            };
            persistentClients = mkOption {
              type = listOf (submodule {
                options = {
                  name = mkOption {
                    type = str;
                    description = "Host Name";
                  };
                  ids = mkOption {
                    type = listOf str;
                    description = "IP addresses";
                  };
                  tags = mkOption {
                    type = listOf str;
                    default = [ ];
                    description = "Tags";
                  };
                  safe_search = mkOption {
                    description = "Safe search";
                    type = submodule {
                      options = {
                        enabled = mkEnableOption "Safe search enabled";
                        bing = mkEnableOption "Bing";
                        duckduckgo = mkEnableOption "DuckDuckGo";
                        google = mkEnableOption "Google";
                        pixabay = mkEnableOption "Pixabay";
                        yandex = mkEnableOption "Yandex";
                        youtube = mkEnableOption "YouTube";
                      };
                    };
                  };
                  blocked_services = mkOption {
                    description = "Blocked services";
                    type = submodule {
                      options = {
                        schedule = {
                          time_zone = mkOption {
                            type = str;
                            default = "Local";
                            description = "Time zone";
                          };
                        };
                        ids = mkOption {
                          type = listOf str;
                          default = [ ];
                          description = "IDs";
                        };
                      };
                    };
                  };
                  upstreams = mkOption {
                    description = "Upstreams";
                    type = listOf str;
                    default = [ ];
                  };
                  use_global_settings = mkEnableOption "Use global settings";
                  filtering_enabled = mkEnableOption "Filtering enabled";
                  parental_enabled = mkEnableOption "Parental enabled";
                  safebrowsing_enabled = mkEnableOption "Safebrowsing enabled";
                  use_global_blocked_services = mkEnableOption "Use global blocked services";
                  ignore_querylog = mkEnableOption "Ignore query log";
                  ignore_statistics = mkEnableOption "Ignore statistics";

                  #safe_search = {
                  #  enabled = false;
                  #  bing = true;
                  #  duckduckgo = true;
                  #  google = true;
                  #  pixabay = true;
                  #  yandex = true;
                  #  youtube = true;
                  #};
                  #blocked_services = {
                  #  schedule = {
                  #    time_zone = "Local";
                  #  };
                  #  ids = [];
                  #};
                  #upstreams = [];
                  #use_global_settings = true;
                  #filtering_enabled = false;
                  #parental_enabled = false;
                  #safebrowsing_enabled = false;
                  #use_global_blocked_services = true;
                  #ignore_querylog = false;
                  #ignore_statistics = false;
                  #};
                };
              });
              default = [ ];
              description = "Persistent clients";
            };
          };
      };

      devices = {
        bluetooth = {
          enable = mkEnableOption "Enable bluetooth";
        };

        printing = {
          enable = mkEnableOption "CUPS to print documents";
        };

        sound = {
          enable = mkEnableOption "Sound support";
          fixAudioStutterInVmWare1 = mkEnableOption "Fix Stuttering in VM Ware host (pulseaudio)";
          fixAudioStutterInVmWare2 = mkEnableOption "Fix Stuttering in VM Ware host (pipewire)";
          pipewireMinQuantum = mkOption {
            description = "Pipewire minimum quantum (related to audio stutter)";
            default = null;
            type = nullOr int;
          };
        };

        touchpad = {
          enable = mkEnableOption "Touchpad support in x server";
        };

        yubiKey = mkOption {
          description = "YubiKey";
          default = null;
          type = nullOr (submodule {
            options = {
              enable = mkEnableOption "Enable YubiKey authentication";
              enableForLogin = mkEnableOption "Enable YubiKey for login";
              enableForSudo = mkEnableOption "Enable YubiKey for sudo";
            };
          });
        };

        trezor = {
          enable = mkEnableOption "Trezor";
        };

        fingerprint = {
          enable = mkEnableOption "Fingerprint authentication";
        };

        kmscon = {
          enable = mkOption {
            type = bool;
            default = true;
            description = "Enable kmscon";
          };
        };

        tpm2 = {
          enable = mkEnableOption "TPM2";
        };

        amdCPU = {
          enable = mkEnableOption "AMD CPU optimizations";
        };

        CPU = {
          sensor = mkOption {
            type = str;
            default = "/sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon3/temp1_input";
            description = "Path to the CPU temperature sensor";
          };
        };
      };

      localisation = {
        kbLayout = mkOption {
          type = str;
          default = "be";
          description = "Keyboard layout";
        };
        weatherLocation = mkOption {
          type = str;
          default = "Brussels, BE";
          description = "Weather location";
        };
      };

      virtualisation = {
        isHost = mkEnableOption "Virtual machine host with libvirtd and virt-manager";
        isQemuGuest = mkEnableOption "QEMU guest support";
        isVmwareGuest = mkEnableOption "VMware guest support";
        installOpenVMToolsHeadless = mkEnableOption "Open VM Tools Headless";
      };

      software = {
        guiPrograms = {
          enable = mkEnableOption "Enable GUI programs";
        };
        swDevPrograms = {
          enable = mkEnableOption "Software development tools";
        };
        cliImageTools = {
          enable = mkEnableOption "CLI image processing tools";
        };
        advancedMonitoringTools = {
          enable = mkEnableOption "Advanced monitoring tools";
        };
        zsh = {
          enable = mkEnableOption "Global zsh with autosuggestions";
        };
        docker = {
          enable = mkEnableOption "Docker";
        };
        podman = {
          enable = mkEnableOption "Podman";
        };
        dotnet = {
          enable = mkEnableOption ".Net 8 SDK";
        };
        nodejs = {
          enable = mkEnableOption "Node JS / NPM";
        };
        wireshark = {
          enable = mkEnableOption "Wireshark";
        };
        btcRiskMetric = {
          enable = mkEnableOption "BTC Risk Metric";
        };
        shellGpt = {
          enable = mkEnableOption "Shell GPT";
          ollamaServerUrl = mkOption {
            type = str;
            default = "http://localhost:${toString defaultOllamaPort}";
            description = "Ollama server URL";
          };
          model = mkOption {
            type = str;
            default = "ollama/mistral:7b-instruct";
            description = "Model for Ollama";
          };
        };
      };

      services = {
        auditing = {
          enable = mkEnableOption "Enable audit and auditd, which is a Linux security feature that logs system events";
        };
        sslServer = {
          selfSignedSSLCerts = mkEnableOption "Generate self-signed SSL certificates";

          pkiPath = mkOption {
            type = path;
            default = "/srv/pki";
            description = "Path to store SSL certificates";
          };
          pkiCertsSubPath = mkOption {
            type = str;
            default = "certs";
            description = "Sub-path to store SSL certificates";
          };
          pkiKeysSubPath = mkOption {
            type = str;
            default = "keys";
            description = "Sub-path to store SSL keys";
          };
          pkiCert = mkOption {
            type = path;
            #default = "${pkiPath}/${pkiCertsSubPath}/${hostName}-cert.pem";
            description = "Path to the SSL certificate";
          };
          pkiKey = mkOption {
            type = path;
            #default = "${pkiPath}/${pkiKeysSubPath}/${hostName}-key.pem";
            description = "Path to the SSL key";
          };
        };
        letsEncrypt = {
          enable = mkEnableOption "Enable Let's Encrypt";
          email = mkOption {
            type = str;
            default = "admin@${fqdn}";
            description = "Email for Let's Encrypt";
          };
        };
        smtpHost = mkOption {
          type = str;
          default = "smtp-relay.brevo.com";
          description = "SMTP host for Vaultwarden";
        };
        smbServer = {
          enable = mkEnableOption "Share a directory using samba";
          openFirewall = mkOption {
            type = bool;
            default = true;
            description = "Open firewall for samba";
          };
          userName = mkOption {
            type = str;
            default = "nixos";
            description = "The user name to use for mounting the shared drives";
          };
          password = mkOption {
            type = str;
            default = "nixos";
            description = "The password to use for mounting the shared drives";
          };
          sharedPath = mkOption {
            type = str;
            default = "/srv/Share";
            description = "The path to the shared directory";
          };
          workgroup = mkOption {
            type = str;
            default = "WORKGROUP";
            description = "Samba workgroup";
          };
        };
        ollama = mkOption {
          description = "Ollama AI engine";
          default = null;
          type = nullOr (submodule {
            options = {
              enable = mkEnableOption "Ollama AI engine";
              port = mkOption {
                type = int;
                default = defaultOllamaPort;
                description = "Port for Ollama";
              };
              acceleration = mkOption {
                type = nullOr (enum [
                  "cuda"
                  "rocm"
                ]);
                default = null;
                description = "Acceleration method: NVidia=cuda, AMD=rocm, CPU=null";
              };
              openFirewall = mkEnableOption "Open firewall for Ollama";
            };
          });
        };
        elastic-search = mkOption {
          description = "Elastic search";
          default = null;
          type = nullOr (submodule {
            options = {
              enable = mkEnableOption "Elastic search";
              port = mkOption {
                type = int;
                default = defaultElasticSearchPort;
                description = "Port for Elastic search";
              };
              openFirewall = mkEnableOption "Open firewall for Elastic search";
            };
          });
        };
        nginx = {
          enable = mkEnableOption "Nginx web server";
          cfConfig = mkOption {
            type = str;
            default = ''
              ${cfConfigRealIpV4}
              ${cfConfigRealIpV6}
              real_ip_header CF-Connecting-IP;
              #real_ip_recursive on;
            '';
            description = "Cloudflare configuration";
          };
          geoBlocking = {
            enable = mkEnableOption "Geo blocking";
            databaseDir = mkOption {
              type = path;
              default = "/var/lib/geoip-databases";
              description = "Directory to store GeoIP database";
            };
            allowedCountries = mkOption {
              type = listOf str;
              default = [ ];
              description = "Countries to allow";
            };
          };
        };
        gitlab-runner = mkOption {
          description = "Gitlab runner";
          default = null;
          type = nullOr (submodule {
            options = {
              enable = mkEnableOption "Enable gitlab-runner";
              id = mkOption {
                type = nullOr int;
                description = "ID of the gitlab-runner";
                default = null;
              };
              name = mkOption {
                type = str;
                default = "";
                description = "Name of the gitlab-runner";
              };
              executor = mkOption {
                type = str;
                default = "";
                description = "Executor of the gitlab-runner";
              };
              url = mkOption {
                type = str;
                default = "";
                description = "URL of the gitlab-runner";
              };
              concurrent = mkOption {
                type = int;
                default = 1;
                description = "Concurrent number of gitlab-runners";
              };
            };
          });
        };
        binary-cache = mkOption {
          default = null;
          description = "Binary cache server settings";
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Nix binary package cache server";
              defaultPort = defaultBinaryCachePort;
              defaultServerName = defaultBinaryCacheServerName;
              parent = sv.binary-cache;
            };
          });
        };
        binary-cache-client = {
          substituter = mkOption {
            type = nullOr str;
            default = "https://${defaultBinaryCacheServerName}.${fqdn}";
            description = "Binary cache server to use";
          };
          trustedPublicKey = mkOption {
            type = nullOr str;
            default = "${defaultBinaryCacheServerName}.${fqdn}:CUJdWIxImw7/4hut3Jdsc6SXE6/R1XNYVYui1GCLf5w=";
            description = "Binary cache server trusted public key";
          };
        };
        dockerRegistry = mkOption {
          default = null;
          description = "Docker registry server settings";
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Docker registry server";
              defaultPort = defaultDockerRegistryPort;
              defaultServerName = "docker";
              parent = sv.dockerRegistry;
            };
          });
        };
      };

      defaultRegistrySettings = mkOption {
        type = submodule dockerRegistrySettings;
        default = { };
        description = "Default docker registry settings";
      };

      webApps = {
        dashyAppLinks = mkOption {
          type = listOf (submodule dashySectionItem);
          default =
            optional (if wa.vaultwarden ? enable then wa.vaultwarden.enable else false) {
              title = "Vaultwarden";
              url = "https://${wa.vaultwarden.mainInternalHost}";
              icon = "fas fa-key";
              statusCheck = true;
              statusCheckMaxRedirects = 3;
              statusCheckAllowInsecure = true;
              statusCheckUrl = "${wa.vaultwarden.urlFromContainer}";
            }
            ++ optional (if wa.home-assistant ? enable then wa.home-assistant.enable else false) {
              title = "Home Assistant";
              url = "https://${wa.home-assistant.mainInternalHost}";
              icon = "fas fa-home";
              statusCheckUrl = "${wa.home-assistant.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++
              optional
                (
                  if wa.home-assistant ? enable && wa.home-assistant.zWaveJs ? enable then
                    wa.home-assistant.zWaveJs.enable
                  else
                    false
                )
                {
                  title = "ZWave JS";
                  url = "https://${wa.home-assistant.zWaveJs.mainInternalHost}";
                  icon = "fas fa-home";
                  statusCheckUrl = "${wa.home-assistant.zWaveJs.urlFromContainer}";
                  statusCheckAllowInsecure = true;
                  statusCheck = true;
                  statusCheckMaxRedirects = 3;
                }
            ++
              optional
                (
                  if wa.home-assistant ? enable && wa.home-assistant.espHome ? enable then
                    wa.home-assistant.espHome.enable
                  else
                    false
                )
                {
                  title = "ESP Home";
                  url = "https://${wa.home-assistant.espHome.mainInternalHost}";
                  icon = "fas fa-microchip";
                  statusCheckUrl = "${wa.home-assistant.espHome.urlFromContainer}";
                  statusCheckAllowInsecure = true;
                  statusCheck = true;
                  statusCheckMaxRedirects = 3;
                }
            ++
              optional
                (
                  if wa.home-assistant ? enable && wa.home-assistant.mosquitto ? enable then
                    wa.home-assistant.mosquitto.enable
                  else
                    false
                )
                {
                  title = "Mosquitto";
                  url = "https://${wa.home-assistant.mosquitto.mainInternalHost}";
                  icon = "fas fa-envelope";
                  statusCheckUrl = "${wa.home-assistant.mosquitto.urlFromContainer}";
                  statusCheckAllowInsecure = true;
                  statusCheck = true;
                  statusCheckMaxRedirects = 3;
                }
            ++ optional (if wa.openWebUi ? enable then wa.openWebUi.enable else false) {
              title = "Open Web UI";
              url = "https://${wa.openWebUi.mainInternalHost}";
              icon = "fas fa-comments";
              statusCheckUrl = "${wa.openWebUi.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.glances ? enable then wa.glances.enable else false) {
              title = "Glances";
              url = "https://${wa.glances.mainInternalHost}";
              icon = "fas fa-gauge";
              statusCheckUrl = "${wa.glances.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.searx ? enable then wa.searx.enable else false) {
              title = "Searx";
              url = "https://${wa.searx.mainInternalHost}";
              icon = "fas fa-search";
              statusCheckUrl = "${wa.searx.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.silverbullet ? enable then wa.silverbullet.enable else false) {
              title = "Silverbullet";
              url = "https://${wa.silverbullet.mainInternalHost}";
              icon = "fas fa-book";
              statusCheckUrl = "${wa.silverbullet.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.immich ? enable then wa.immich.enable else false) {
              title = "Immich";
              url = "https://${wa.immich.mainInternalHost}";
              icon = "fas fa-images";
              statusCheckUrl = "${wa.immich.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.navidrome ? enable then wa.navidrome.enable else false) {
              title = "Navidrome";
              url = "https://${wa.navidrome.mainInternalHost}";
              icon = "fas fa-spotify";
              statusCheckUrl = "${wa.navidrome.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.gonic ? enable then wa.gonic.enable else false) {
              title = "Gonic";
              url = "https://${wa.gonic.mainInternalHost}";
              icon = "fas fa-music";
              statusCheckUrl = "${wa.gonic.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.airsonic ? enable then wa.airsonic.enable else false) {
              title = "Airsonic";
              url = "https://${wa.airsonic.mainInternalHost}";
              icon = "fas fa-music";
              statusCheckUrl = "${wa.airsonic.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.gotify ? enable then wa.gotify.enable else false) {
              title = "Gotify";
              url = "https://${wa.gotify.mainInternalHost}";
              icon = "fas fa-bell";
              statusCheckUrl = "${wa.gotify.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.fail2web ? enable then wa.fail2web.enable else false) {
              title = "Fail2web";
              url = "https://${wa.fail2web.mainInternalHost}";
              icon = "fas fa-padlock";
              statusCheckUrl = "${wa.fail2web.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
              statusCheckAcceptCodes = "401";
            }
            ++ optional (if agh ? enable then agh.enable else false) {
              title = "AdGuard Home";
              url = "https://${agh.mainInternalHost}";
              icon = "fas fa-shield-alt";
              statusCheckUrl = "${agh.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.paperless-ngx ? enable then wa.paperless-ngx.enable else false) {
              title = "Paperless";
              url = "https://${wa.paperless-ngx.externalHost}";
              icon = "fas fa-file-alt";
              statusCheckUrl = "${wa.paperless-ngx.externalHost}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.fireflyiii ? enable then wa.fireflyiii.enable else false) {
              title = "Firefly III";
              url = "https://${wa.fireflyiii.mainInternalHost}";
              icon = "fas fa-money-bill-wave";
              statusCheckUrl = "${wa.fireflyiii.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
              statusCheckAcceptCodes = "401";
            }
            ++ optional (if wa.changeDetectionIo ? enable then wa.changeDetectionIo.enable else false) {
              title = "Change Detection.io";
              url = "https://${wa.changeDetectionIo.mainInternalHost}";
              icon = "fas fa-eye";
              statusCheckUrl =
                if (wa.changeDetectionIo ? urlFromContainer) then
                  "${wa.changeDetectionIo.urlFromContainer}"
                else
                  "";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.wallabag ? enable then wa.wallabag.enable else false) {
              title = "Wallabag bookmark manager";
              url = "https://${wa.wallabag.mainInternalHost}";
              icon = "fas fa-bookmark";
              statusCheckUrl = "${wa.wallabag.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if wa.dendrite ? enable then wa.dendrite.enable else false) {
              title = "Dendrite Matrix server";
              url = "https://${wa.dendrite.mainInternalHost}";
              icon = "fas fa-comments";
              statusCheckUrl = "${wa.dendrite.urlFromContainer}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (if sv.dockerRegistry ? enable then sv.dockerRegistry.enable else false) {
              title = "Docker registry";
              url = "https://${sv.dockerRegistry.mainInternalHost}";
              icon = "fas fa-docker";
              statusCheckUrl = "https://${sv.dockerRegistry.mainInternalHost}";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ optional (config.sharedConfig.devices.printing.enable) {
              title = "CUPS";
              url = "https://${host}:631";
              icon = "fas fa-print";
              statusCheckAllowInsecure = true;
              statusCheck = true;
              statusCheckMaxRedirects = 3;
            }
            ++ [
              {
                title = "Cockpit";
                url = "https://${wa.cockpit.mainInternalHost}";
                icon = "fas fa-server";
                statusCheckUrl = "${wa.cockpit.mainInternalHost}";
                statusCheckAllowInsecure = true;
                statusCheck = true;
                statusCheckMaxRedirects = 3;
              }
            ];
          description = "Dashy app links";
        };
        cockpit = mkOption {
          description = "Cockpit server manager";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Cockpit";
              defaultPort = defaultCockpitPort;
              defaultServerName = "cockpit";
              defaultSubUrl = "/cpit";
              parent = wa.cockpit;
            };
          });
        };
        vaultwarden = mkOption {
          description = "Vaultwarden password manager";
          default = null;
          type = nullOr (submodule {
            options =
              {
                orgName = mkOption {
                  type = str;
                  default = "Vaultwarden";
                  description = "Organization name for Vaultwarden";
                };
                allowSignups = mkEnableOption "Allow signups";
              }
              // getProxySettings {
                serviceName = "Vaultwarden";
                defaultPort = defaultVaultwardenPort;
                defaultServerName = "vaultwarden";
                parent = wa.vaultwarden;
              };
          });
        };
        home-assistant = mkOption {
          description = "Home Assistant";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Home Assistant";
                defaultPort = defaultHomeAssistantPort;
                defaultServerName = "hass";
                parent = wa.home-assistant;
              }
              // {
                espHome =
                  getProxySettings {
                    serviceName = "ESP Home";
                    defaultPort = defaultEspHomePort1;
                    defaultServerName = "esp-home";
                    parent = wa.home-assistant.espHome;
                  }
                  // {
                    port2 = mkOption {
                      type = int;
                      default = defaultEspHomePort2;
                      description = "Port 2 for ESP Home";
                    };
                    relayConfigFile = mkOption {
                      type = nullOr path;
                      default = null;
                      description = "Relay configuration file";
                    };
                  };
                zWaveJs =
                  getProxySettings {
                    serviceName = "Z-Wave JS";
                    defaultPort = defaultZwaveJsPort1;
                    defaultServerName = "zwave-js";
                    parent = wa.home-assistant.zWaveJs;
                  }
                  // {
                    port2 = mkOption {
                      type = int;
                      default = defaultZwaveJsPort2;
                      description = "Port 2 for Z-Wave JS";
                    };
                  };
                mosquitto =
                  getProxySettings {
                    serviceName = "Mosquitto MQTT broker";
                    defaultPort = defaultMosquittoPort1;
                    defaultServerName = "mosquitto";
                    parent = wa.home-assistant.mosquitto;
                  }
                  // {
                    port2 = mkOption {
                      type = int;
                      default = defaultMosquittoPort2;
                      description = "Port 2 for Mosquitto";
                    };
                    mosquittoConfigFile = mkOption {
                      type = nullOr path;
                      default = null;
                      description = "Mosquitto configuration file";
                    };
                  };
              };
          });
        };
        dashy = mkOption {
          description = "Dashy dashboard";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Dashy dashboard";
                defaultPort = defaultDashyPort;
                defaultServerName = "home";
                parent = wa.dashy;
              }
              // {
                configuration = mkOption {
                  type = nullOr (submodule dashySettings);
                  default = null;
                  description = "Dashy configuration";
                };
              };
          });
        };
        glances = mkOption {
          description = "Glances system monitor";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Glances system monitor";
                defaultPort = defaultGlancesPort;
                defaultServerName = "glances";
                parent = wa.glances;
              }
              // {
                podmanSockUserId = mkOption {
                  type = int;
                  default = 1000;
                  description = "User ID for podman socket";
                };
                urlFromContainer = mkOption {
                  type = str;
                  default = "http://localhost:${toString wa.glances.port}/";
                  description = "URL from container for Glances";
                };
              };
          });
        };
        searx = mkOption {
          description = "Searx meta search engine";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Searx meta search engine";
              defaultPort = defaultSearxPort;
              defaultServerName = "searx";
              parent = wa.searx;
            };
          });
        };
        crowdsec = mkOption {
          description = "Crowdsec security engine";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Crowdsec security engine";
              defaultPort = defaultCrowdsecPort;
              defaultServerName = "crowdsec";
              parent = wa.crowdsec;
            };
          });
        };
        openWebUi = mkOption {
          description = "Open Web UI";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Open Web UI";
                defaultPort = defaultOpenwebuiPort;
                defaultServerName = "ai";
                parent = wa.openWebUi;
              }
              // {
                pipelines = {
                  port = mkOption {
                    type = types.int;
                    default = defaultOpenwebuiPipelinesPort;
                    description = "Port for Open Web Ui Pipelines";
                  };
                };
                tika = {
                  port = mkOption {
                    type = types.int;
                    default = defaultTikaPort;
                    description = "Port for Tika";
                  };
                };
                volumes = mkOption {
                  type = types.listOf types.str;
                  default = [ ];
                  description = "Volumes mappings for Open Web Ui";
                };
              };
          });
        };
        silverbullet = mkOption {
          description = "Silverbullet notes manager";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Silverbullet notes manager";
              defaultPort = defaultSilverBulletPort;
              defaultServerName = "notes";
              parent = wa.silverbullet;
            };
          });
        };
        immich = mkOption {
          description = "Immich photo manager";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Immich photo manager";
                defaultPort = defaultImmichPort;
                defaultServerName = "photos";
                parent = wa.immich;
              }
              // {
                cacheLocation = mkOption {
                  type = str;
                  default = "/srv/immich/cache";
                  description = "Cache location for Immich";
                };
                libraryLocation = mkOption {
                  type = str;
                  default = "/srv/immich/media";
                  description = "Library location for Immich";
                };
                dbDataLocation = mkOption {
                  type = str;
                  default = "/srv/immich/db";
                  description = "Database data location for Immich";
                };
                dbDatabaseName = mkOption {
                  type = str;
                  default = "immich";
                  description = "Database name for Immich";
                };
                dbUserName = mkOption {
                  type = str;
                  default = "immich";
                  description = "Database user name for Immich";
                };
                dbPassword = mkOption {
                  type = str;
                  default = "immich";
                  description = "Database password for Immich";
                };
                configFile = mkOption {
                  type = str;
                  default = "/etc/immich/immich.json";
                  description = "Configuration file for Immich";
                };
              };
          });
        };
        navidrome = mkOption {
          description = "Navidrome music server";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Navidrome music server";
                defaultPort = defaultNavidromePort;
                defaultServerName = "music-server";
                parent = wa.navidrome;
              }
              // {
                libraryLocation = mkOption {
                  type = str;
                  default = "/srv/music";
                  description = "Library location for Navidrome";
                };
                cacheLocation = mkOption {
                  type = str;
                  default = "/srv/navidrome/cache";
                  description = "Cache location for Navidrome";
                };
                dbDataLocation = mkOption {
                  type = str;
                  default = "/srv/navidrome/db";
                  description = "Database data location for Navidrome";
                };
              };
          });
        };
        gonic = mkOption {
          description = "Gonic music server";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Gonic music server";
                defaultPort = defaultGonicPort;
                defaultServerName = "music-server";
                parent = wa.gonic;
              }
              // {
                musicLocation = mkOption {
                  type = str;
                  default = "/srv/music";
                  description = "Music location for gonic";
                };
                podcastsLocation = mkOption {
                  type = str;
                  default = "/srv/podcasts";
                  description = "Podcasts location for Gonic";
                };
                playlistsLocation = mkOption {
                  type = str;
                  default = "/srv/playlists";
                  description = "Playlists location for Gonic";
                };
                cacheLocation = mkOption {
                  type = str;
                  default = "/srv/gonic/cache";
                  description = "Cache location for Gonic";
                };
                dbDataLocation = mkOption {
                  type = str;
                  default = "/srv/gonic/db";
                  description = "Database data location for Gonic";
                };
              };
          });
        };
        gotify = mkOption {
          description = "Gotify notification server";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Gotify notification server";
                defaultPort = defaultGotifyPort;
                defaultServerName = "push";
                parent = wa.gotify;
              }
              // {
                allowSignups = mkEnableOption "Allow signups";
                dataDirectory = mkOption {
                  type = str;
                  default = "/var/lib/private/gotify";
                  description = "Watchtower data directory";
                };
                fail2banToken = mkOption {
                  type = str;
                  default = "";
                  description = "Fail2ban token";
                };
              };
          });
        };
        fail2web = mkOption {
          description = "Fail2web: admin UI for fail2ban";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Fail2web: admin UI for fail2ban";
                defaultPort = defaultFail2webPort;
                defaultServerName = "fail2web";
                parent = wa.fail2web;
              }
              // {
                restPort = mkOption {
                  type = int;
                  default = defaultFail2restPort;
                  description = "Port for Fail2rest";
                };
              };
          });
        };
        airsonic = mkOption {
          description = "Airsonic music server";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Airsonic music client";
                defaultPort = defaultAirsonicPort;
                defaultServerName = "music";
                parent = wa.airsonic;
              }
              // {
                musicServerUrl = mkOption {
                  type = str;
                  default = if (wa.gonic ? externalHost) then wa.gonic.externalHost else "";
                  description = "Music server URL for Airsonic";
                };
              };
          });
        };
        paperless-ngx = mkOption {
          description = "Paperless-ngx document manager";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Paperless-ngx document manager";
              defaultPort = defaultPaperlessNgxPort;
              defaultServerName = "paperless";
              parent = wa.paperless-ngx;
            };
          });
        };
        changeDetectionIo = mkOption {
          description = "Change Detection.io web site monitor";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Change Detection.io web site monitor";
                defaultPort = defaultChangeDetectionIoPort;
                defaultServerName = "change-detection";
                parent = wa.changeDetectionIo;
              }
              // {
                urlFromContainer = mkOption {
                  type = str;
                  default = "http://localhost:${toString wa.changeDetectionIo.port}/";
                  description = "URL from container for Change Detection.io";
                };
                chromePort = mkOption {
                  type = int;
                  default = defaultChangeDetectionIoChromePort;
                  description = "Port for Chrome";
                };
              };
          });
        };
        fireflyiii = mkOption {
          description = "Firefly III personal finance manager";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Firefly III personal finance manager";
                defaultPort = defaultFireflyIiiPort;
                defaultServerName = "firefly";
                parent = wa.fireflyiii;
              }
              // {
                importerPort = mkOption {
                  type = int;
                  default = defaultFireflyIiiImporterPort;
                  description = "Port for Firefly III importer";
                };
              };
          });
        };
        wallabag = mkOption {
          description = "Wallabag bookmark manager";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "wallabag bookmark manager";
              defaultPort = defaultWallabagPort;
              defaultServerName = "wallabag";
              parent = wa.wallabag;
            };
          });
        };
        tabby = mkOption {
          description = "Tabby AI coding assistant";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Tabby AI coding assistant";
                defaultPort = defaultTabbyPort;
                defaultServerName = "tabby";
                parent = wa.tabby;
              }
              // {
                acceleration = lib.mkOption {
                  type = nullOr (enum [
                    "cpu"
                    "rocm"
                    "cuda"
                    "metal"
                  ]);
                  default = null;
                  description = "Acceleration mode for Tabby";
                };
                model = lib.mkOption {
                  type = lib.types.str;
                  default = "TabbyML/DeepseekCoder-6.7B";
                  description = "Tabby model to use";
                };
              };
          });
        };
        jupyter = {
          enable = lib.mkEnableOption "Jupyter hub (python)";
          port = lib.mkOption {
            type = lib.types.int;
            default = defaultJupyterPort;
            description = "Port for Vaultwarden";
          };
          openFirewall = lib.mkEnableOption "Open firewall for Jupyter hub";
        };
        llama-cpp = {
          enable = lib.mkEnableOption "Llama CPP";
        };
        dendrite = mkOption {
          description = "Dendrite Matrix server";
          default = null;
          type = nullOr (submodule {
            options =
              getProxySettings {
                serviceName = "Dendrite Matrix server";
                defaultPort = defaultDendritePort1;
                defaultServerName = "matrix";
                parent = wa.dendrite;
              }
              // {
                port2 = mkOption {
                  type = int;
                  default = defaultDendritePort2;
                  description = "Port 2 for Dendrite";
                };
                allowRegistration = mkEnableOption "Allow registration";
              };
          });
        };
        sync-v3 = mkOption {
          description = "Dendrite Sync V3 server";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Dendrite Sync V3 server";
              defaultPort = defaultSyncV3Port;
              defaultServerName = "sync-v3";
              parent = wa.sync-v3;
            };
          });
        };
        synapse-admin = mkOption {
          description = "Synapse admin";
          default = null;
          type = nullOr (submodule {
            options = getProxySettings {
              serviceName = "Synapse admin";
              defaultPort = defaultSynapseAdminPort;
              defaultServerName = "synapse-admin";
              parent = wa.synapse-admin;
            };
          });
        };
      };

      stylixGlobal = mkOption {
        type = submodule stylixSettings;
        default = { };
        description = "Stylix settings";
      };

      users = mkOption {
        type = attrsOf (submodule userSettings);
        default = { };
        description = "Per-user settings";
      };

      flakeRoot = mkOption {
        type = path;
        description = "Path to the flake root (./.)";
      };

      homeStateVersion = mkOption {
        type = str;
        default = "24.05";
        description = "Home state version";
      };
    };
  };
}
