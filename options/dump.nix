{
  lib,
  config,
  ...
}:
let
  inherit (lib)
    mkOption
    ;
  pkgNameFromPath =
    pkg:
    let
      matchResult = builtins.match "/nix/store/[a-z0-9]+-(.+)" (toString pkg);
    in
    if matchResult == null then pkg else builtins.head matchResult;
in
{
  options = {
    sharedConfig = {
      dump = {
        systemPackages = mkOption {
          type = lib.types.anything;
          description = "List of installed system packages";
          default = lib.sort lib.lessThan (
            lib.unique (map (pkg: pkgNameFromPath pkg) config.environment.systemPackages)
          );
        };

        containers = mkOption {
          type = lib.types.anything;
          description = "List of installed oci container images";
          default = lib.sort lib.lessThan (
            lib.unique (map (c: c.image) (lib.attrValues config.virtualisation.oci-containers.containers))
          );
        };

        containerConfigs = mkOption {
          type = lib.types.anything;
          description = "List configurations of installed oci container images";
          default = config.virtualisation.oci-containers.containers;
        };

        homeManagerPackages = mkOption {
          type = lib.types.anything;
          description = "List of installed home-manager packages for each user";
          default = lib.mapAttrs (
            _: usr: lib.sort lib.lessThan (lib.unique (map (pkg: pkgNameFromPath pkg) usr.home.packages))
          ) config.home-manager.users;
        };
        userPackages = mkOption {
          type = lib.types.anything;
          description = "List of installed user packages";
          default = lib.mapAttrs (
            _: usr: lib.sort lib.lessThan (lib.unique (map (pkg: pkgNameFromPath pkg) usr.packages))
          ) config.users.users;
        };
      };
    };
  };
}
