{
  config,
  lib,
}:
{
  serviceName,
  defaultPort,
  defaultServerName,
  parent,
  defaultSubUrl ? "",
}:
let
  inherit (lib) mkOption mkEnableOption types;
  inherit (types)
    str
    int
    listOf
    submodule
    ;
  host = config.networking.hostName;
  nw = config.sharedConfig.network;
  fqdn = nw.fullyQualifiedDomainName;
  inherit (parent) serverName port subUrl;
  dockerRegistrySettings = import ./dockerRegistrySettings.nix { inherit lib; };
in
{
  enable = mkEnableOption serviceName;
  port = mkOption {
    type = int;
    default = defaultPort;
    description = "Port for ${serviceName}";
  };
  openFirewall = mkEnableOption "Open firewall for ${serviceName}";
  serverName = mkOption {
    type = str;
    default = defaultServerName;
    description = "Server name for ${serviceName}";
  };
  subUrl = mkOption {
    type = str;
    default = defaultSubUrl;
    description = "Sub URL for ${serviceName}. Must start with / and must not end with /. Leave empty if none";
  };
  mainInternalHost = mkOption {
    type = str;
    default = if serverName != "" then "${serverName}.${fqdn}" else fqdn;
    description = "Main internal host for ${serviceName}";
  };
  externalHost = mkOption {
    type = str;
    default = "";
    description = "External host for ${serviceName}";
  };
  redirectedHosts = mkOption {
    type = listOf str;
    default = [ "${serverName}.${host}" ];
    description = "Hosts that are redirected to the first host";
  };
  proxyPass = mkOption {
    type = str;
    default = "http://localhost:${toString port}${subUrl}/";
    description = "Proxy pass for ${serviceName}";
  };
  urlFromContainer = mkOption {
    type = str;
    default = "http://host.docker.internal:${toString port}${subUrl}/";
    description = "URL from container for ${serviceName}";
  };
  registrySettings = mkOption {
    type = submodule dockerRegistrySettings;
    default = config.sharedConfig.defaultRegistrySettings;
    description = "Registry settings for ${serviceName}";
  };
}
