{ lib, ... }:
let
  inherit (lib) mkOption types;
  inherit (types)
    nullOr
    str
    enum
    int
    listOf
    attrsOf
    bool
    submodule
    ;
in
{ ... }:
{
  options = {
    title = mkOption {
      description = "The text to display/ title of a given item. Max length 18";
      type = str;
    };

    description = mkOption {
      description = "Additional info about an item, which is shown in the tooltip on hover, or visible on large tiles";
      default = null;
      type = nullOr str;
    };

    url = mkOption {
      description = "The URL / location of web address for when the item is clicked";
      type = str;
    };

    icon = mkOption {
      description = "The icon for a given item. Can be a font-awesome icon, favicon, remote URL or local URL";
      default = null;
      type = nullOr str;
    };

    target = mkOption {
      description = "The opening method for when the item is clicked, either newtab, sametab, modal, workspace, clipboard, top or parent. Where newtab will open the link in a new tab, sametab will open it in the current tab, and modal will open a pop-up modal, workspace will open in the Workspace view and clipboard will copy the URL to system clipboard (but not launch app)";
      default = null;
      type = nullOr (enum [
        "newtab"
        "sametab"
        "modal"
        "workspace"
        "clipboard"
        "top"
        "parent"
      ]);
    };

    hotkey = mkOption {
      description = "Give frequently opened applications a numeric hotkey, between 0 - 9. You can then just press that key to launch that application";
      default = null;
      type = nullOr int;
    };

    tags = mkOption {
      description = "A list of tags, which can be used for improved search";
      default = null;
      type = nullOr (listOf str);
    };

    statusCheck = mkOption {
      description = "When set to true, Dashy will ping the URL associated with the current service, and display its status as a dot next to the item. The value here will override appConfig.statusCheck so you can turn off or on checks for a given service. Defaults to appConfig.statusCheck, falls back to false";
      default = null;
      type = nullOr bool;
    };

    statusCheckUrl = mkOption {
      description = "If you've enabled statusCheck, and want to use a different URL to what is defined under the item, then specify it here";
      default = null;
      type = nullOr str;
    };

    statusCheckHeaders = mkOption {
      description = "If you're endpoint requires any specific headers for the status checking, then define them here";
      default = null;
      type = nullOr (attrsOf str);
    };

    statusCheckAllowInsecure = mkOption {
      description = "By default, any request to insecure content will be blocked. Setting this option to true will disable the rejectUnauthorized option, enabling you to ping non-HTTPS services for the current item";
      default = null;
      type = nullOr bool;
    };

    statusCheckAcceptCodes = mkOption {
      description = "If your service's response code is anything other than 2xx, then you can opt to specify an alternative success code. E.g. if you expect your server to return 403, but still want the status indicator to be green, set this value to 403";
      default = null;
      type = nullOr str;
    };

    statusCheckMaxRedirects = mkOption {
      description = "If your service redirects to another page, and you would like status checks to follow redirects, then specify the maximum number of redirects here";
      default = null;
      type = nullOr int;
    };

    color = mkOption {
      description = "An optional color for the text and font-awesome icon to be displayed in. Note that this will override the current theme and so may not display well";
      default = null;
      type = nullOr str;
    };

    rel = mkOption {
      description = "The value of the rel attribute for the link. Useful for specifying the relationship between the target link/document and Dashy";
      default = null;
      type = nullOr str;
    };

    backgroundColor = mkOption {
      description = "An optional background fill color for the that given item. Again, this will override the current theme and so might not display well against the background";
      default = null;
      type = nullOr str;
    };

    provider = mkOption {
      description = "The name of the provider for a given service, useful for when including hosted apps. In some themes, this is visible under the item name";
      default = null;
      type = nullOr str;
    };

    displayData = mkOption {
      description = "Meta-data to optionally override display settings for a given item";
      default = null;
      type = nullOr (submodule {
        options = {
          hideForUsers = mkOption {
            description = "Current item will be visible to all users, except for those specified in this list";
            default = null;
            type = nullOr (listOf str);
          };

          showForUsers = mkOption {
            description = "Current item will be hidden from all users, except for those specified in this list";
            default = null;
            type = nullOr (listOf str);
          };

          hideForGuests = mkOption {
            description = "Current item will be visible for logged in users, but not for guests (see appConfig.enableGuestAccess)";
            default = null;
            type = nullOr bool;
          };

          hideForKeycloakUsers = mkOption {
            description = "Current item will be visible to all keycloak users, except for those configured via these groups and roles";
            default = null;
            type = nullOr (submodule {
              options = {
                groups = mkOption {
                  description = "Current Section or Item will be hidden or shown based on the user having any of the groups in this list";
                  default = null;
                  type = nullOr (listOf str);
                };
                roles = mkOption {
                  description = "Current Section or Item will be hidden or shown based on the user having any of the roles in this list";
                  default = null;
                  type = nullOr (listOf str);
                };
              };
            });
          };

          showForKeycloakUsers = mkOption {
            description = "Current item will be hidden from all keycloak users, except for those configured via these groups and roles";
            default = null;
            type = nullOr (submodule {
              options = {
                groups = mkOption {
                  description = "Current Section or Item will be hidden or shown based on the user having any of the groups in this list";
                  default = null;
                  type = nullOr (listOf str);
                };
                roles = mkOption {
                  description = "Current Section or Item will be hidden or shown based on the user having any of the roles in this list";
                  default = null;
                  type = nullOr (listOf str);
                };
              };
            });
          };
        };
      });
    };
  };
}
