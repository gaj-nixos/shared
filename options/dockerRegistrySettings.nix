{ lib, ... }:
let
  inherit (lib) mkOption types;
  inherit (types) nullOr str;
in
{ ... }:
{
  # Define the options for docker registry login
  options = {
    registry = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        The registry to login to
      '';
    };
    username = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        The username to use when logging into the docker registry.
      '';
    };
    passwordFile = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        The file containing the password to use when logging into the docker registry.
      '';
    };
  };
}
