{ lib, ... }:
let
  inherit (lib) mkOption types;
  inherit (types)
    nullOr
    str
    enum
    int
    listOf
    attrsOf
    bool
    submodule
    either
    anything
    ;
in
{ ... }:
let
  dashySectionItem = import ./dashySectionItem.nix { inherit lib; };
in
{
  # Define the options for the Dashy settings
  # https://dashy.to/docs/configuring/#sectiondisplaydata-optional
  options = {
    pageInfo = mkOption {
      description = "Basic meta data like title, description, nav bar links, footer text";
      type = submodule {
        options = {
          title = mkOption {
            description = "Your dashboard title, displayed in the header and browser tab";
            type = str;
          };

          description = mkOption {
            description = "Description of your dashboard, also displayed as a subtitle";
            default = "";
            type = str;
          };

          navLinks = mkOption {
            description = "Optional list of a maximum of 6 links, which will be displayed in the navigation bar";
            default = null;
            type = nullOr (
              listOf (submodule {
                options = {
                  title = mkOption {
                    description = "The text to display on the link button";
                    type = str;
                  };
                  path = mkOption {
                    description = "The URL to navigate to when clicked. Can be relative (e.g. /about) or absolute (e.g. https://example.com or http://192.168.1.1)";
                    type = str;
                  };
                  target = mkOption {
                    description = "The opening method (external links only). Can be either newtab, sametab, top or parent. Defaults to newtab";
                    default = null;
                    type = nullOr (enum [
                      "newtab"
                      "sametab"
                      "top"
                      "parent"
                    ]);
                  };
                };
              })
            );
          };

          footerText = mkOption {
            description = "Text to display in the footer (note that this will override the default footer content). This can also include HTML and inline CSS";
            default = "";
            type = str;
          };

          logo = mkOption {
            description = "The path to an image to display in the header (to the right of the title). This can be either local, where / is the root of ./public, or any remote image, such as https://i.ibb.co/yhbt6CY/dashy.png. It's recommended to scale your image down, so that it doesn't impact load times";
            default = "";
            type = str;
          };
        };
      };
    };

    appConfig = mkOption {
      description = "Settings related to how the app functions, including API keys and global styles";
      default = null;
      type = nullOr (submodule {
        options = {
          language = mkOption {
            description = "The 2 (or 4-digit) ISO 639-1 code for your language, e.g. en or en-GB. This must be a language that the app has already been translated into. If your language is unavailable, Dashy will fallback to English. By default Dashy will attempt to auto-detect your language, although this may not work on some privacy browsers";
            default = null;
            type = nullOr str;
          };

          defaultOpeningMethod = mkOption {
            description = "The default opening method for items, if no target is specified for a given item. Can be either newtab, sametab, modal, workspace, clipboard, top or parent. Defaults to newtab";
            default = "newtab";
            type = enum [
              "newtab"
              "sametab"
              "modal"
              "workspace"
              "clipboard"
              "top"
              "parent"
            ];
          };

          statusCheck = mkOption {
            description = "When set to true, Dashy will ping each of your services and display their status as a dot next to each item. This can be overridden by setting statusCheck under each item. Defaults to false";
            default = null;
            type = nullOr bool;
          };

          statusCheckInterval = mkOption {
            description = "The number of seconds between checks. If set to 0 then service will only be checked on initial page load, which is usually the desired functionality. If value is less than 10 you may experience a hit in performance. Defaults to 0";
            default = 0;
            type = int;
          };

          webSearch = mkOption {
            description = "Configuration options for the web search feature, set your default search engine, opening method or disable web search";
            default = null;
            type = nullOr (submodule {
              options = {
                disableWebSearch = mkOption {
                  description = "Web search is enabled by default, but can be disabled by setting this property to true";
                  default = null;
                  type = nullOr bool;
                };
                searchEngine = mkOption {
                  description = "Set the key name for your search engine. Can also use a custom engine by setting this property to custom. Currently supported: duckduckgo, google, whoogle, qwant, startpage, searx-bar and searx-info. Defaults to duckduckgo";
                  default = null;
                  type = nullOr (enum [
                    "duckduckgo"
                    "google"
                    "whoogle"
                    "qwant"
                    "startpage"
                    "searx-bar"
                    "searx-info"
                    "github"
                    "gitlab"
                    "arch linux wiki"
                    "reddit"
                    "custom"
                  ]);
                };
                customSearchEngine = mkOption {
                  description = "You can also use a custom search engine, or your own self-hosted instance. This requires searchEngine: custom to be set. Then add the URL of your service, with GET query string included here";
                  default = null;
                  type = nullOr str;
                };
                openingMethod = mkOption {
                  description = "Set your preferred opening method for search results: newtab, sametab, workspace. Defaults to newtab";
                  default = null;
                  type = nullOr (enum [
                    "newtab"
                    "sametab"
                    "workspace"
                  ]);
                };
                searchBangs = mkOption {
                  description = "A key-value-pair set of custom search bangs for redirecting query to a specific app or search engine. The key of each should be the bang you will type (typically starting with /, ! or :), and value is the destination, either as a search engine key (e.g. reddit) or a URL with search parameters (e.g. https://en.wikipedia.org/w/?search=)";
                  default = null;
                  type = nullOr (attrsOf str);
                };
              };
            });
          };

          backgroundImg = mkOption {
            description = "Path to an optional full-screen app background image. This can be either remote (http) or local (relative to /app/public/item-icons/ inside the container). Note that this will slow down initial load";
            default = "";
            type = str;
          };

          enableFontAwesome = mkOption {
            description = "If set to true font-awesome will be loaded, if set to false they will not be. if left blank font-awesome will be enabled only if required by 1 or more icons";
            default = null;
            type = nullOr bool;
          };

          enableMaterialDesignIcons = mkOption {
            description = "If set to true mdi icons will be loaded, if set to false they will not be. Where true is enabled, if left blank material design icons will be enabled only if required by 1 or more icons";
            default = null;
            type = nullOr bool;
          };

          fontAwesomeKey = mkOption {
            description = "If you have a font-awesome key, then you can use it here and make use of premium icons. It is a 10-digit alpha-numeric string from you're FA kit URL (e.g. 13014ae648)";
            default = null;
            type = nullOr str;
          };

          faviconApi = mkOption {
            description = "Only applicable if you are using favicons for item icons. Specifies which service to use to resolve favicons. Set to local to do this locally, without using an API. Services running locally will use this option always. Available options are: local, faviconkit, iconhorse, google, clearbit, webmasterapi and allesedv. Defaults to faviconkit";
            default = "faviconkit";
            type = enum [
              "local"
              "faviconkit"
              "iconhorse"
              "google"
              "clearbit"
              "webmasterapi"
              "allesedv"
            ];
          };

          auth = mkOption {
            description = "All settings relating to user authentication";
            default = null;
            type = nullOr (submodule {
              options = {
                users = mkOption {
                  description = "An array of objects containing usernames and hashed passwords. If this is not provided, then authentication will be off by default, and you will not need any credentials to access the app. See appConfig.auth.users. Note this method of authentication is handled on the client side, so for security critical situations, it is recommended to use an alternate authentication method.";
                  default = null;
                  type = nullOr (
                    listOf (submodule {
                      options = {
                        user = mkOption {
                          description = "Username to log in with";
                          type = str;
                        };
                        hash = mkOption {
                          description = "A SHA-256 hashed password";
                          type = str;
                        };
                        type = mkOption {
                          description = "The user type";
                          default = null;
                          type = nullOr (enum [
                            "admin"
                            "normal"
                          ]);
                        };
                      };
                    })
                  );
                };

                enableKeycloak = mkOption {
                  description = "If set to true, then authentication using Keycloak will be enabled. Note that you need to have an instance running, and have also configured auth.keycloak. Defaults to false";
                  default = null;
                  type = nullOr bool;
                };

                keycloak = mkOption {
                  description = "Config options to point Dashy to your Keycloak server. Requires enableKeycloak: true";
                  default = null;
                  type = nullOr (submodule {
                    options = {
                      serverUrl = mkOption {
                        description = "The URL (or URL/ IP + Port) where your keycloak server is running";
                        type = str;
                      };
                      realm = mkOption {
                        description = "The name of the realm (must already be created) that you want to use";
                        type = str;
                      };
                      clientId = mkOption {
                        description = "The Client ID of the client you created for use with Dashy";
                        type = str;
                      };
                      legacySupport = mkOption {
                        description = "If using Keycloak 17 or older, then set this to true";
                        default = null;
                        type = nullOr bool;
                      };
                    };
                  });
                };

                enableHeaderAuth = mkOption {
                  description = "If set to true, then authentication using HeaderAuth will be enabled. Note that you need to have your web server/reverse proxy running, and have also configured auth.headerAuth. Defaults to false";
                  default = null;
                  type = nullOr bool;
                };

                headerAuth = mkOption {
                  default = null;
                  description = "Config options to point Dashy to your headers for authentication. Requires enableHeaderAuth: true";
                  type = nullOr (submodule {
                    options = {
                      userHeader = mkOption {
                        description = "The Header name which contains username (default: REMOTE_USER). Case insensitive";
                        default = null;
                        type = nullOr str;
                      };
                      proxyWhitelist = mkOption {
                        description = "An array of Upstream proxy servers to expect authencticated requests from";
                        type = listOf str;
                      };
                    };
                  });
                };

                enableOidc = mkOption {
                  description = "If set to true, then authentication using OIDC will be enabled. Note that you need to have a configured OIDC server and configure it with auth.oidc. Defaults to false";
                  default = null;
                  type = nullOr bool;
                };

                oidc = mkOption {
                  description = "Config options to point Dash to your OIDC configuration. Request enableOidc: true";
                  default = null;
                  type = nullOr (submodule {
                    options = {
                      clientId = mkOption {
                        description = "The client id registered in the OIDC server";
                        type = str;
                      };
                      endpoint = mkOption {
                        description = "The URL of the OIDC server that should be used";
                        type = str;
                      };
                    };
                  });
                };

                enableGuestAccess = mkOption {
                  description = "When set to true, an unauthenticated user will be able to access the dashboard, with read-only access, without having to login. Requires auth.users to be configured. Defaults to false";
                  default = null;
                  type = nullOr bool;
                };
              };
            });
          };

          defaultIcon = mkOption {
            description = "An icon to be applied to any items, which don't already have an icon set";
            default = null;
            type = nullOr str;
          };

          layout = mkOption {
            description = "Layout for homepage, either horizontal, vertical or auto. Defaults to auto. This specifies the layout and direction of how sections are positioned on the home screen. This can also be modified and overridden from the UI";
            default = null;
            type = nullOr (enum [
              "horizontal"
              "vertical"
              "auto"
            ]);
          };

          iconSize = mkOption {
            description = "The size of link items / icons. This can also be modified and overridden from the UI";
            default = null;
            type = nullOr (enum [
              "small"
              "medium"
              "large"
            ]);
          };

          colCount = mkOption {
            description = "The number of columns of sections displayed on the homepage, using the default view. Should be in integer between 1 and 8. Note that by default this is applied responsively, based on current screen size, and specifying a value here will override this behavior, which may not be desirable";
            default = null;
            type = nullOr int;
          };

          theme = mkOption {
            default = null;
            description = "The default theme for first load. This can also be modified and overridden from the UI";
            type = nullOr str;
          };

          cssThemes = mkOption {
            description = "An array of custom theme names which can be used in the theme switcher dropdown";
            default = null;
            type = nullOr (listOf str);
          };

          customColors = mkOption {
            description = "Enables you to apply a custom color palette to any given theme. Use the theme name (lowercase) as the key, for an object including key-value-pairs, with the color variable name as keys, and 6-digit hex code as value";
            default = { };
            type = attrsOf str;
          };

          externalStyleSheet = mkOption {
            description = "Either a URL to an external stylesheet or an array or URLs, which can be applied as themes within the UI";
            default = null;
            type = nullOr (either str (listOf str));
          };

          customCss = mkOption {
            description = "Raw CSS that will be applied to the page. This can also be set from the UI. Please minify it first.";
            default = null;
            type = nullOr str;
          };

          hideComponents = mkOption {
            description = "A list of key page components (header, footer, search, settings, etc) that are present by default, but can be removed using this option";
            default = null;
            type = nullOr (attrsOf bool);
          };

          routingMode = mkOption {
            description = "Can be either hash or history. Determines the URL format for sub-pages, hash mode will look like /#/home whereas with history mode available you have nice clean URLs, like /home. For more info, see the Vue docs. If you're hosting Dashy with a custom BASE_URL, you will find that a bit of extra server config is necessary to get history mode working, so here you may want to instead use hash mode";
            default = null;
            type = nullOr (enum [
              "hash"
              "history"
            ]);
          };

          enableMultiTasking = mkOption {
            description = "If set to true, will keep apps open in the background when in the workspace view. Useful for quickly switching between multiple sites, and preserving their state, but comes at the cost of performance";
            default = null;
            type = nullOr bool;
          };

          workspaceLandingUrl = mkOption {
            description = "The URL or an app, service or website to launch when the workspace view is opened, before another service has been launched";
            default = null;
            type = nullOr str;
          };

          preventWriteToDisk = mkOption {
            description = "If set to true, users will be prevented from saving config changes to disk through the UI";
            default = null;
            type = nullOr bool;
          };

          preventLocalSave = mkOption {
            description = "If set to true, users will be prevented from applying config changes to local storage";
            default = null;
            type = nullOr bool;
          };

          disableConfiguration = mkOption {
            description = "If set to true, no users will be able to view or edit the config through the UI";
            default = null;
            type = nullOr bool;
          };

          disableConfigurationForNonAdmin = mkOption {
            description = "If set to true, only admin users will be able to view or edit the config through the UI. disableConfiguration must not be set to true";
            default = null;
            type = nullOr bool;
          };

          widgetsAlwaysUseProxy = mkOption {
            description = "If set to true, requests made by widgets will always be proxied, same as setting useProxy: true on each widget. Note that this may break some widgets";
            default = null;
            type = nullOr bool;
          };

          showSplashScreen = mkOption {
            description = "If set to true, a loading screen will be shown";
            default = null;
            type = nullOr bool;
          };

          enableErrorReporting = mkOption {
            description = "Enable reporting of unexpected errors and crashes. This is off by default, and no data will ever be captured unless you explicitly enable it. Turning on error reporting helps previously unknown bugs get discovered and fixed. Dashy uses Sentry for error reporting";
            default = null;
            type = nullOr bool;
          };

          sentryDsn = mkOption {
            description = "If you need to monitor errors in your instance, then you can use Sentry to collect and process bug reports. Sentry can be self-hosted, or used as SaaS, once your instance is setup, then all you need to do is pass in the DSN here, and enable error reporting. You can learn more on the Sentry DSN Docs. Note that this will only ever be used if enableErrorReporting is explicitly enabled";
            default = null;
            type = nullOr str;
          };

          disableSmartSort = mkOption {
            description = "For the most-used and last-used app sort functions to work, a basic open-count is stored in local storage. If you do not want this to happen, then disable smart sort here, but you wil no longer be able to use these sort options";
            default = null;
            type = nullOr bool;
          };

          disableUpdateChecks = mkOption {
            description = "If set to true, Dashy will not check for updates";
            default = null;
            type = nullOr bool;
          };

          enableServiceWorker = mkOption {
            description = "Service workers cache web applications to improve load times and offer basic offline functionality, and are enabled by default in Dashy. The service worker can sometimes cause older content to be cached, requiring the app to be hard-refreshed. If you do not want SW functionality, or are having issues with caching, set this property to false to disable all service workers";
            default = null;
            type = nullOr bool;
          };

          disableContextMenu = mkOption {
            description = "If set to true, the custom right-click context menu will be disabled";
            default = null;
            type = nullOr bool;
          };
        };
      });
    };

    sections = mkOption {
      description = "An array of sections, each containing an array of items, which will be displayed as links";
      type = listOf (submodule {
        options = {
          name = mkOption {
            description = "The title for the section";
            type = str;
          };

          icon = mkOption {
            description = "An single icon to be displayed next to the title";
            default = null;
            type = nullOr str;
          };

          items = mkOption {
            description = "An array of items to be displayed within the section. See item. Sections must include either 1 or more items, or 1 or more widgets";
            default = null;
            type = nullOr (listOf (submodule dashySectionItem));
          };

          widgets = mkOption {
            description = "An array of widgets to be displayed within the section";
            default = null;
            type = nullOr (
              listOf (submodule {
                options = {
                  type = mkOption {
                    description = "The widget type (see https://dashy.to/docs/widgets for a full list of widgets)";
                    type = str;
                  };

                  options = mkOption {
                    description = "Some widgets accept either optional or required additional options";
                    default = null;
                    type = nullOr anything;
                  };

                  updateInterval = mkOption {
                    description = "You can keep a widget constantly updated by specifying an update interval, in seconds";
                    default = null;
                    type = nullOr int;
                  };

                  useProxy = mkOption {
                    description = "Some widgets make API requests to services that are not CORS-enabled. For these instances, you will need to route requests through a proxy, Dashy has a built in CORS-proxy, which you can use by setting this option to true";
                    default = null;
                    type = nullOr bool;
                  };

                  timeout = mkOption {
                    description = "Request timeout in milliseconds, defaults to ½ a second (500)";
                    default = null;
                    type = nullOr int;
                  };

                  ignoreErrors = mkOption {
                    description = "Prevent an error message being displayed, if a network request or something else fails. Useful for false-positives";
                    default = null;
                    type = nullOr bool;
                  };

                  label = mkOption {
                    description = "Add custom label to a given widget. Useful for identification, if there are multiple of the same type of widget in a single section";
                    default = null;
                    type = nullOr str;
                  };
                };
              })
            );
          };

          displayData = mkOption {
            description = "Meta-data to optionally override display settings for a given section";
            default = null;
            type = nullOr (submodule {
              options = {
                sortBy = mkOption {
                  description = "The sort order for items within the current section. By default items are displayed in the order in which they are listed in within the config";
                  default = null;
                  type = nullOr (enum [
                    "most-used"
                    "last-used"
                    "alphabetical"
                    "reverse-alphabetical"
                    "random"
                    "default"
                  ]);
                };

                collapsed = mkOption {
                  description = "If true, the section will be collapsed initially, and will need to be clicked to open. Useful for less regularly used, or very long sections";
                  default = null;
                  type = nullOr bool;
                };

                cutToHeight = mkOption {
                  description = "By default, sections will fill available space. Set this option to true to match section height with content height";
                  default = null;
                  type = nullOr bool;
                };

                rows = mkOption {
                  description = "Height of the section, specified as the number of rows it should span vertically, e.g. 2. Defaults to 1. Max is 5.";
                  default = null;
                  type = nullOr int;
                };

                cols = mkOption {
                  description = "Width of the section, specified as the number of columns the section should span horizontally, e.g. 2. Defaults to 1. Max is 5";
                  default = null;
                  type = nullOr int;
                };

                itemSize = mkOption {
                  description = "pecify the size for items within this group, either small, medium or large. Note that this will override any settings specified through the UI";
                  default = null;
                  type = nullOr (enum [
                    "small"
                    "medium"
                    "large"
                  ]);
                };

                color = mkOption {
                  description = "A custom accent color for the section, as a hex code or HTML color (e.g. #fff)";
                  default = null;
                  type = nullOr str;
                };

                customStyles = mkOption {
                  description = "Custom CSS properties that should be applied to that section, e.g. border: 2px dashed #ff0000;";
                  default = null;
                  type = nullOr str;
                };

                sectionLayout = mkOption {
                  description = "Specify which CSS layout will be used to responsively place items. Can be either auto (which uses flex layout), or grid. If grid is selected, then itemCountX and itemCountY may also be set. Defaults to auto";
                  default = null;
                  type = nullOr (enum [
                    "auto"
                    "grid"
                  ]);
                };

                itemCountX = mkOption {
                  description = "The number of items to display per row / horizontally. If not set, it will be calculated automatically based on available space. Can only be set if sectionLayout is set to grid. Must be a whole number between 1 and 12";
                  default = null;
                  type = nullOr int;
                };

                itemCountY = mkOption {
                  description = "The number of items to display per column / vertically. If not set, it will be calculated automatically based on available space. If itemCountX is set, then itemCountY can be calculated automatically. Can only be set if sectionLayout is set to grid. Must be a whole number between 1 and 12";
                  default = null;
                  type = nullOr int;
                };

                hideForUsers = mkOption {
                  description = "Current section will be visible to all users, except for those specified in this list";
                  default = null;
                  type = nullOr (listOf str);
                };

                showForUsers = mkOption {
                  description = "Current section will be hidden from all users, except for those specified in this list";
                  default = null;
                  type = nullOr (listOf str);
                };

                hideForGuests = mkOption {
                  description = "Current section will be visible for logged in users, but not for guests (see appConfig.enableGuestAccess). Defaults to false";
                  default = null;
                  type = nullOr bool;
                };

                hideForKeycloakUsers = mkOption {
                  description = "Current section will be visible to all keycloak users, except for those configured via these groups and roles";
                  default = null;
                  type = nullOr (submodule {
                    options = {
                      groups = mkOption {
                        description = "Current Section or Item will be hidden or shown based on the user having any of the groups in this list";
                        default = null;
                        type = nullOr (listOf str);
                      };
                      roles = mkOption {
                        description = "Current Section or Item will be hidden or shown based on the user having any of the roles in this list";
                        default = null;
                        type = nullOr (listOf str);
                      };
                    };
                  });
                };

                showForKeycloakUsers = mkOption {
                  description = "Current section will be hidden from all keycloak users, except for those configured via these groups and roles";
                  default = null;
                  type = nullOr (submodule {
                    options = {
                      groups = mkOption {
                        description = "Current Section or Item will be hidden or shown based on the user having any of the groups in this list";
                        default = null;
                        type = nullOr (listOf str);
                      };
                      roles = mkOption {
                        description = "Current Section or Item will be hidden or shown based on the user having any of the roles in this list";
                        default = null;
                        type = nullOr (listOf str);
                      };
                    };
                  });
                };
              };
            });
          };
        };
      });
    };

    pages = mkOption {
      description = "An array additional config files, used for multi-page dashboards";
      default = [ ];
      type = listOf (submodule {
        options = {
          name = mkOption {
            description = "A unique name for that page";
            type = str;
          };

          path = mkOption {
            description = "The path (local or remote) to the config file to use. For files located within /public, you only need to specify filename, for externally hosted files you must include the full URL";
            type = str;
          };
        };
      });
    };
  };
}
