{
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkOption types mkEnableOption;
  inherit (types)
    nullOr
    str
    listOf
    attrsOf
    submodule
    path
    enum
    ;
  stylixSettings = import ./stylixSettings.nix { inherit lib pkgs; };
in
{ ... }:
{
  options = {
    username = mkOption {
      type = str;
      default = "nixos";
      description = "Username of the user to create";
    };

    name = mkOption {
      type = str;
      default = "NixOS";
      description = "Name of the user to create";
    };

    email = mkOption {
      type = nullOr str;
      default = null;
      description = "Email of the user to create";
    };

    initialPassword = mkOption {
      type = str;
      default = "nixos";
      description = "Initial password of the user to create";
    };

    editor = mkOption {
      type = str;
      default = "nvim";
      description = "Editor of the user to create";
    };

    terminal = mkOption {
      type = str;
      default = "${pkgs.kitty}/bin/kitty";
      description = "Terminal of the user to create";
    };

    sshAuthorizedKeyFiles = mkOption {
      type = listOf path;
      default = [ ];
      description = "SSH public key files authorized for openSSH";
    };

    sshAuthorizedKeys = mkOption {
      type = listOf str;
      default = [ ];
      description = "SSH public keys authorized for openSSH";
    };

    gitAuthorizedKeys = mkOption {
      type = listOf str;
      default = [ ];
      description = "SSH public keys authorized for signing Git commits";
    };

    signGitCommits = mkEnableOption "Sign git commits with ssh";

    windowManagers = {
      i3 = {
        enable = mkEnableOption "Enable i3 window manager";
      };
      hyprland = {
        enable = mkEnableOption "Enable Hyprland window manager";
      };
      win10 = {
        enable = mkEnableOption "Enable Win10 window manager";
      };
    };

    software = {
      guiPrograms = {
        enable = mkEnableOption "Enable GUI programs";
        manageQmmpSettings = mkEnableOption "Manage Qmmp settings with home manager (modifications in the GUI will not be saved)";
      };
      funCliPrograms = {
        enable = mkEnableOption "Non-essential CLI programs";
      };
      swDevPrograms = {
        enable = mkEnableOption "Software development tools";
      };
      zsh = {
        enable = mkEnableOption "Home Manager: zsh with plugins and completions";
        shellAliases = mkOption {
          type = attrsOf str;
          default = { };
          description = "Shell aliases";
        };
        welcomeMessage = mkOption {
          type = str;
          default = "Welcome to NixOS!";
          description = "Welcome message";
        };
      };
      brave = {
        enable = mkEnableOption "Enable Brave browser";
      };
      moc = {
        enable = mkEnableOption "Enable Music On Console";
        theme = mkOption {
          type = str;
          default = "nightly_theme";
          description = "Music On Console theme";
        };
      };
    };

    bar = mkOption {
      description = "Select the bar to use";
      default = "waybar";
      type = enum [
        "waybar"
        "polybar"
        "hyprpanel"
        "win10"
      ];
    };

    dotnet-nuget = {
      enable = mkEnableOption "Enable custom .NET NuGet config";
    };

    ohMyPoshTheme = mkOption {
      type = str;
      default = "tokyonight_storm";
      description = "Oh My Posh theme";
    };

    hyprPanelTheme = mkOption {
      type = str;
      default = "catppuccin_mocha";
      description = "HyprPanel theme";
    };

    stylixUser = mkOption {
      type = submodule stylixSettings;
      default = { };
      description = "Stylix settings";
    };
  };
}
