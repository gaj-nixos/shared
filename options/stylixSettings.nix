{
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkOption types mkEnableOption;
  inherit (types)
    nullOr
    str
    int
    path
    package
    ;
in
{ ... }:
{
  options = {
    enable = mkEnableOption "Enable Stylix";
    theme = mkOption {
      type = str;
      default = "default-light";
      description = "Stylix theme";
    };
    polarity = mkOption {
      type = str;
      default = "light";
      description = "Stylix polarity";
    };
    background = {
      image = mkOption {
        type = nullOr path;
        default = ../resources/wallpapers/wp2811545-retro-apple-wallpaper.jpg;
        #default = pkgs.reflection_by_yuumei;
        description = "Stylix background image";
      };
    };
    fonts = {
      monospace = {
        name = mkOption {
          type = str;
          default = "JetBrainsMono Nerd Font Propo";
          description = "Stylix monospace font";
        };
        package = mkOption {
          type = package;
          default = pkgs.jetbrains-mono;
          description = "Stylix monospace font package";
        };
      };
      emoji = {
        name = mkOption {
          type = str;
          default = "Noto Color Emoji";
          description = "Stylix emoji font";
        };
        package = mkOption {
          type = package;
          default = pkgs.noto-fonts-color-emoji;
          description = "Stylix emoji font package";
        };
      };
      sizes = {
        terminal = mkOption {
          type = int;
          default = 16;
          description = "Stylix terminal font size";
        };
        applications = mkOption {
          type = int;
          default = 12;
          description = "Stylix applications font size";
        };
        popups = mkOption {
          type = int;
          default = 12;
          description = "Stylix popups font size";
        };
        desktop = mkOption {
          type = int;
          default = 12;
          description = "Stylix desktop font size";
        };
      };
    };
  };
}
