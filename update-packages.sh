#!/usr/bin/env bash

# This script is used to update the system packages to the latest versions
# usage: ./update-packages.sh

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

git pull --autostash --rebase=true
nix flake update
git add flake.lock

echo "⬆️ Commit changes"

git commit -m "⬆️ Update packages"
