{
  lib,
  stdenv,
  fetchzip,
  gettext,
}:
let
  v = {
    version = "99"; # https://github.com/cockpit-project/cockpit-podman/releases
    sha = "sha256-rfkGurW2mrqIqtcXe657BWPon8o5wzEqFLr/egGXGpA=";
  };
in
stdenv.mkDerivation rec {
  pname = "cockpit-podman";
  version = v.version;

  src = fetchzip {
    url = "https://github.com/cockpit-project/${pname}/releases/download/${version}/${pname}-${version}.tar.xz";
    sha256 = v.sha;
  };

  nativeBuildInputs = [
    gettext
  ];

  makeFlags = [
    "DESTDIR=$(out)"
    "PREFIX="
  ];

  postPatch = ''
    substituteInPlace Makefile \
      --replace /usr/share $out/share
    touch pkg/lib/cockpit-po-plugin.js
    touch dist/manifest.json
  '';

  dontBuild = true;

  meta = {
    description = "Cockpit UI for podman containers";
    license = lib.licenses.lgpl21;
    homepage = "https://github.com/cockpit-project/cockpit-podman";
    platforms = lib.platforms.linux;
    maintainers = with lib.maintainers; [ ];
  };
}
