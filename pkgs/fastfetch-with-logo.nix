{ pkgs, ... }:

pkgs.writeShellScriptBin "fastfetch-with-logo" ''
  if [[ -n "$TERM_PROGRAM" ]] && [[ "$TERM_PROGRAM" == "vscode" ]]; then
    # Do nothing in VS Code
    :
  elif [[ ( -n "$KITTY_WINDOW_ID" || "$TERM_PROGRAM" = "ghostty" ) && -z "$SSH_CONNECTION" ]]; then
    ${pkgs.fastfetch}/bin/fastfetch \
      --logo "${../resources/wallpapers/nixos3.png}" \
      --logo-height 20 \
      --logo-width 45
  else
    ${pkgs.fastfetch}/bin/fastfetch
  fi
''
