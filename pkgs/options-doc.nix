{ pkgs, ... }:
let
  inherit (pkgs) stdenv mkdocs python310Packages;
  options-doc = pkgs.callPackage ../lib/nixos/options-doc.nix { };
in
stdenv.mkDerivation {
  src = ../.;
  name = "docs";

  # depend on our options doc derivation
  buildInput = [ options-doc ];

  # mkdocs dependencies
  nativeBuildInputs = [
    mkdocs
    python310Packages.mkdocs-material
    python310Packages.pygments
    python310Packages.regex
  ];

  # symlink our generated docs into the correct folder before generating
  buildPhase = ''
    ln -s ${options-doc} "./nixos-options.md"
  '';

  # copy the resulting output to the derivation's $out directory
  installPhase = ''
    cat ./nixos-options.md > $out
  '';
}
