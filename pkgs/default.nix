# Custom packages, that can be defined similarly to ones from nixpkgs
# You can build them using 'nix build .#example'
pkgs: {
  #cockpit-machines = pkgs.callPackage ./cockpit-machines.nix { inherit config lib; };
  #cockpit-podman = pkgs.callPackage ./cockpit-podman.nix { inherit config lib; };
  libvirt-dbus = pkgs.callPackage ./libvirt-dbus.nix { };
  dotnet-symbol = pkgs.callPackage ./dotnet/dotnet-symbol.nix { };
  nugethierarchy = pkgs.callPackage ./dotnet/nugethierarchy.nix { };
  nugetupdater = pkgs.callPackage ./dotnet/nugetupdater.nix { };
  dotnet-outdated = pkgs.callPackage ./dotnet/dotnet-outdated-tool.nix { };
  options-doc = pkgs.callPackage ./options-doc.nix { };
  fastfetch-with-logo = pkgs.callPackage ./fastfetch-with-logo.nix { };
  btrfs-snapshots-diff = pkgs.callPackage ./btrfs-snapshots-diff.nix { };
}
