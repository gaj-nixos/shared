{
  buildDotnetGlobalTool,
  lib,
}:
buildDotnetGlobalTool {
  pname = "dotnet-symbol";
  version = "9.0.553101";

  nugetHash = "sha256-/pFODZt0azL+d1GyTz9BOyQ09ORAHU7LdFM7Rxg1ZFE=";

  meta = {
    homepage = "https://github.com/dotnet/diagnostics";
    changelog = "https://github.com/dotnet/diagnostics/releases";
    license = lib.licenses.mit;
    platforms = lib.platforms.linux;
  };
}
