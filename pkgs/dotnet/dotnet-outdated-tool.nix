{
  buildDotnetGlobalTool,
  lib,
}:
buildDotnetGlobalTool {
  pname = "dotnet-outdated-tool";
  version = "4.6.4";

  nugetHash = "sha256-5IL05jnSkSsb7bJr6JFdudRAPju60d28PnkjEl5sUS0=";
  executables = "dotnet-outdated";

  meta = {
    homepage = "https://github.com/unoplatform/nuget.updater";
    changelog = "https://github.com/unoplatform/nuget.updater/tags";
    license = lib.licenses.mit;
    platforms = lib.platforms.linux;
  };
}
