{
  buildDotnetGlobalTool,
  lib,
}:
buildDotnetGlobalTool {
  pname = "nugethierarchy";
  nugetName = "Uno.NuGet.Hierarchy.Tool";
  version = "1.2.1";

  nugetHash = "sha256-EscakpXj2FE0yDbLJ5CP281xo/T3J+sS3mqDw+w3YYE=";
  executables = "nugethierarchy";

  meta = {
    homepage = "https://github.com/unoplatform/nuget.updater";
    changelog = "https://github.com/unoplatform/nuget.updater/tags";
    license = lib.licenses.mit;
    platforms = lib.platforms.linux;
  };
}
