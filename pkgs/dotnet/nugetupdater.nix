{
  buildDotnetGlobalTool,
  lib,
}:
buildDotnetGlobalTool {
  pname = "nugetupdater";
  nugetName = "Uno.NuGet.Updater.Tool";
  version = "1.2.1";

  nugetHash = "sha256-JI47z0YH7G/xVyq0KhvEOXHK9GTA5eGfu7Uw0dzfVec=";
  executables = "nugetupdater";

  meta = {
    homepage = "https://github.com/unoplatform/nuget.updater";
    changelog = "https://github.com/unoplatform/nuget.updater/tags";
    license = lib.licenses.mit;
    platforms = lib.platforms.linux;
  };
}
