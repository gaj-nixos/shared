pkgs:
let
  netSdk = (with pkgs.dotnetCorePackages; combinePackages [ sdk_9_0 ]).overrideAttrs (
    finalAttrs: previousAttrs: {
      # This is needed to install workload in $HOME
      # https://discourse.nixos.org/t/dotnet-maui-workload/20370/12
      postBuild =
        (previousAttrs.postBuild or '''')
        + ''
          for i in $out/sdk/*
          do
            i=$(basename $i)
            length=$(printf "%s" "$i" | wc -c)
            substring=$(printf "%s" "$i" | cut -c 1-$(expr $length - 2))
            i="$substring""00"
            mkdir -p $out/metadata/workloads/''${i/-*}
            touch $out/metadata/workloads/''${i/-*}/userlocal
          done
        '';
    }
  );
  netCore =
    with pkgs.dotnetCorePackages;
    combinePackages [
      runtime_6_0
      runtime_7_0
      runtime_8_0
      aspnetcore_6_0
      aspnetcore_7_0
      aspnetcore_8_0
      netSdk
      pkgs.openldap
    ];
in
netCore
