{
  lib,
  python3Packages,
  fetchFromGitHub,
}:

python3Packages.buildPythonApplication {
  pname = "btrfs-snapshots-diff";
  version = "1";

  src = fetchFromGitHub {
    owner = "sysnux";
    repo = "btrfs-snapshots-diff";
    rev = "2d46612ff63d9ba982e67fe2320e8de60be8ca3a";
    hash = "sha256-P5AWNARRc9qK4nMTM7BDcrV4ZQLBo3lgR0ITHOsPBlU=";
  };

  format = "other";

  installPhase = ''
    install -Dm755 btrfs-snapshots-diff.py $out/bin/btrfs-snapshots-diff
  '';

  meta = with lib; {
    description = "Displays the differences between two Btrfs snapshots from the same subvolume";
    homepage = "https://github.com/sysnux/btrfs-snapshots-diff";
    changelog = "https://github.com/sysnux/btrfs-snapshots-diff/releases";
    license = licenses.gpl2;
    maintainers = with maintainers; [ gaelj ];
  };
}
