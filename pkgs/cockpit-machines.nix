{
  lib,
  stdenv,
  fetchzip,
  gettext,
}:
let
  v = {
    # https://github.com/cockpit-project/cockpit-machines/releases
    version = "326";
    sha = "sha256-0jP4tcg/cdOwviVlr+sPDXgfKfBQs3nTGlwqkhXbhAc=";
  };
in
stdenv.mkDerivation rec {
  pname = "cockpit-machines";
  version = v.version;
  src = fetchzip {
    url = "https://github.com/cockpit-project/cockpit-machines/releases/download/${version}/cockpit-machines-${version}.tar.xz";
    sha256 = v.sha;
  };

  nativeBuildInputs = [
    gettext
  ];

  makeFlags = [
    "DESTDIR=$(out)"
    "PREFIX="
  ];

  postPatch = ''
    substituteInPlace Makefile \
      --replace /usr/share $out/share
    touch pkg/lib/cockpit.js
    touch pkg/lib/cockpit-po-plugin.js
    touch dist/manifest.json
  '';

  postFixup = ''
    gunzip $out/share/cockpit/machines/index.js.gz
    sed -i "s#/usr/bin/python3#/usr/bin/env python3#ig" $out/share/cockpit/machines/index.js
    sed -i "s#/usr/bin/pwscore#/usr/bin/env pwscore#ig" $out/share/cockpit/machines/index.js
    gzip -9 $out/share/cockpit/machines/index.js
  '';

  dontBuild = true;

  meta = {
    description = "Cockpit UI for virtual machines";
    license = lib.licenses.lgpl21;
    homepage = "https://github.com/cockpit-project/cockpit-machines";
    platforms = lib.platforms.linux;
    maintainers = with lib.maintainers; [ ];
  };
}
