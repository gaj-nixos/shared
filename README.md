# Shared Nixos Configuration

This repo contains shared NixOS configurations.

See also [NixOS README.md](./docs/README-NixOS.md).

## Installation

See [Installation README.md](./docs/README-Installation.md).

## Usage

### update flakes

`sudo nix flake update`

### build

`sudo nixos-rebuild build --flake path:.`
or alternately
`nh os switch ~/.dotfiles/`

### run in vm

`nixos-rebuild build-vm --flake path:.`
`result/bin/run-desktop-vm-vm`

### create live usb image

`nix build path:.#nixosConfigurations.live.config.system.build.isoImage`

### deploy

`sudo nixos-rebuild switch --flake path:.`

### repl (:q to quit)

`nix repl --expr "builtins.getFlake ''$PWD''"`

### pin to specific rev until next update

`nix flake lock --override-input nixpkgs github:NixOS/nixpkgs/revision`

### get last cached rev

`curl -sL "https://monitoring.nixos.org/prometheus/api/v1/query?query=channel_revision" | jq -r ".data.result[] | select(.metric.channel==\"nixos-unstable\") | .metric.revision"`

### Terminal utilities

- `btop` to view resource usage
- `df` to view disk usage
- `ranger` as a terminal file browser
- `bat` to view text files with syntax coloring

### Maintenance scripts

These scripts are run from: `/etc/nixos`. They will use the current git branch.

### General maintenance

```sh
sudo update-config # fetch and apply the latest config defined in this repo
sudo cleanup # frees disk space
```

### Self-signed SSL certificates

To re-generate the SSL certificate in self-signed mode, run `sudo update-self-signed-ssl-cert`.

### Provided SSL certificates

The SMB network share called `pki` can be used to drop SSL certificates.

Then the `pem` / `crt` files must be moved / renamed to:

- caRootCertPath = `/srv/pki/rootCA/rootCA.pem`
- publicKeyPath = `/srv/pki/certs/{hostName}-cert.pem`
- privateKeyPath = `/srv/pki/keys/{hostName}-key.pem` or `/srv/pki/keys/{hostName}-encrypted-key.pem` if encrypted

Then run `sudo update-provided-ssl-cert`.

### Development

```sh
sudo check-config  # fetch and check that the latest config defined in this repo builds without errors
sudo update-packages # update to the latest package versions and commit the modified flake.lock file to git
```

## External documentation
