#!/usr/bin/env bash

# This script is used to test the configuration for errors
# usage: ./test.sh

export NIXPKGS_ALLOW_INSECURE=1

nixos-rebuild dry-activate --accept-flake-config --flake .#default --show-trace |& nom

read -r -p "Press any key to continue..."
echo

nix eval .#nixosConfigurations.default.config.sharedConfig --json | jq
