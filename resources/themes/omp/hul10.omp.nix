{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  palette = {
    white = "#f7f7f7";
    black = "#111111";
  };
  blocks = [
    {
      type = "prompt";
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "transparent";
          foreground = "p:white";
          style = "plain";
          template = "┌";
          type = "text";
        }
        {
          background = "#464646";
          foreground = "p:white";
          properties = {
            windows = "";
            linux = "";
            ubuntu = "";
            macos = "";
          };
          style = "plain";
          template = " {{.Icon}}{{if .WSL}} (WSL){{end}}⠀";
          type = "os";
        }
        {
          background = "p:white";
          foreground = "p:black";
          style = "plain";
          template = "  {{.Name}}⠀";
          type = "shell";
        }
        {
          background = "#ffe093";
          foreground = "p:black";
          style = "plain";
          template = "  {{.HostName}}{{.UserName}}⠀";
          type = "session";
        }
        {
          background = "#ffffd6";
          foreground = "p:black";
          properties = {
            branch_icon = "  ";
          };
          style = "plain";
          template = "{{.HEAD}}⠀";
          type = "git";
        }
      ];
    }
    {
      type = "prompt";
      alignment = "right";
      segments = [
        {
          background = "transparent";
          foreground = "#b3ffde";
          properties = {
            style = "austin";
            threshold = 0;
          };
          style = "plain";
          template = "{{.FormattedMs}}";
          type = "executiontime";
        }
        {
          background = "transparent";
          foreground = "#b3ffde";
          properties = {
            time_format = "02/01/06 15:04";
          };
          style = "plain";
          template = "<p:white> · </>{{.CurrentDate | date .Format}}";
          type = "time";
        }
      ];
    }
    {
      type = "prompt";
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "transparent";
          foreground = "p:white";
          properties = {
            folder_icon = "";
            folder_separator_template = "<#93d0ff> » </>";
            home_icon = "";
            style = "agnoster";
          };
          style = "plain";
          template = "<p:white>└</><#93d0ff>[</> {{.Path}} <#93d0ff>]</>";
          type = "path";
        }
      ];
    }
    {
      type = "prompt";
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "transparent";
          foreground = "#81ff91";
          foreground_templates = [
            "{{if gt .Code 0}}#ff3030{{end}}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯";
          type = "status";
        }
      ];
    }
  ];
  console_title_template = "{{if .Root}}[root] {{end}}{{.Shell}} in <{{.Folder}}>";
  final_space = true;
  version = 3;
}
