#!/usr/bin/env nix-shell
#!nix-shell -i bash -p yq perl
# shellcheck shell=bash

# This script converts unicode characters from their '\uXXXX' representation to the actual character in each nix file of the current directory

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

for nix_file in *.nix; do
  # Skip if no Nix files exist
  [[ -e "$nix_file" ]] || break

  perl -CSDA -i -pe '
    use utf8;
    binmode STDIN, ":encoding(UTF-8)";
    binmode STDOUT, ":encoding(UTF-8)";

    # First process valid surrogate pairs (high + low)
    s/\\u(D[89A-Fa-f]{3})\\u(D[C-Fc-f][0-9A-Fa-f]{2})/
      my $high = hex($1);
      my $low = hex($2);
      my $codepoint = 0x10000 + (($high - 0xD800) << 10) + ($low - 0xDC00);
      chr($codepoint);
    /xige;

    # Then process standalone valid code points
    s/\\u([0-9A-Fa-f]{4})/chr(hex($1))/ige;

    # Replace any remaining invalid surrogates with replacement character
    s/\\u[Dd][89A-Fa-f]{3}/�/g;

    # Fix escaped quotes
    s/\\"/"/g;
  ' "$nix_file"

  echo "Converted: $nix_file"
done
