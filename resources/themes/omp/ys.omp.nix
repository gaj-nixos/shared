{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "white";
          properties = {
            fetch_version = false;
          };
          style = "plain";
          template = "({{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }})";
          type = "python";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "lightBlue";
          style = "plain";
          template = "# ";
          type = "text";
        }
        {
          foreground = "red";
          style = "plain";
          template = " % ";
          type = "root";
        }
        {
          style = "plain";
          template = "<cyan>{{ .UserName }}</> <darkGray>@</> <green>{{ .HostName }}</> ";
          type = "session";
        }
        {
          foreground = "lightYellow";
          properties = {
            style = "full";
          };
          style = "plain";
          template = "<darkGray>in </>{{ .Path }} ";
          type = "path";
        }
        {
          style = "plain";
          template = "<darkGray>on</> <white>git:</><cyan>{{ .HEAD }}</>{{ if .Working.Changed }}<red> x</>{{ end }} ";
          type = "git";
          properties = {
            fetch_status = true;
          };
        }
        {
          foreground = "darkGray";
          style = "plain";
          template = "[{{ .CurrentDate | date .Format }}]";
          type = "time";
        }
        {
          foreground = "red";
          style = "plain";
          template = " C:{{ if gt .Code 0 }}{{ .Code }}{{ end }} ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "lightRed";
          style = "plain";
          template = "$";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
