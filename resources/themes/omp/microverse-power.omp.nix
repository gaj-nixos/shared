{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#3A86FF";
          style = "plain";
          template = "{{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#242424";
          foreground = "#f1184c";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .UserName }}";
          type = "session";
        }
        {
          background = "#242424";
          foreground = "#f1184c";
          powerline_symbol = "";
          style = "powerline";
          template = "- root ";
          type = "root";
        }
        {
          background = "#242424";
          foreground = "#FFBB00";
          powerline_symbol = "";
          properties = {
            time_format = "2006-01-02 15:04:05";
          };
          style = "powerline";
          template = "{{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          background = "#242424";
          foreground = "#33DD2D";
          powerline_symbol = "";
          properties = {
            folder_separator_icon = "/";
            style = "full";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#242424";
          foreground = "#3A86FF";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = "{{ .UpstreamIcon }}{{ .HEAD }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#33DD2D";
          background_templates = [
            "{{ if gt .Code 0 }}#f1184c{{ end }}"
          ];
          foreground = "#242424";
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          template = "  ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#f1184c";
          style = "plain";
          template = "➜ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
