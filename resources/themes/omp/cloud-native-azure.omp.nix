{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#c386f1";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .SSHSession }} {{ end }}{{ .UserName }}@{{ .HostName }} ";
          trailing_diamond = "";
          type = "session";
        }
        {
          background = "#ff479c";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            style = "folder";
          };
          style = "powerline";
          template = "   {{ .Path }} ";
          type = "path";
        }
        {
          background = "#fffb38";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          foreground = "#193549";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#2e9599";
          background_templates = [
            "{{ if gt .Code 0 }}#f1184c{{ end }}"
          ];
          foreground = "#ffffff";
          leading_diamond = "<transparent,background></>";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = "  {{ if gt .Code 0 }} {{ reason .Code }}{{ else }}{{ end }} ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#0077c2";
          foreground = "#ffffff";
          style = "plain";
          template = "<#0077c2,transparent></>  {{ .Name }} <transparent,#0077c2></>";
          type = "shell";
        }
        {
          background = "#f36943";
          background_templates = [
            "{{if eq \"Charging\" .State.String}}#40c4ff{{end}}"
            "{{if eq \"Discharging\" .State.String}}#ff5722{{end}}"
            "{{if eq \"Full\" .State.String}}#4caf50{{end}}"
          ];
          foreground = "#ffffff";
          invert_powerline = true;
          powerline_symbol = "";
          properties = {
            charged_icon = " ";
            charging_icon = " ";
            discharging_icon = " ";
          };
          style = "powerline";
          template = " {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }} ";
          type = "battery";
        }
        {
          background = "#2e9599";
          foreground = "#111111";
          invert_powerline = true;
          leading_diamond = "";
          style = "diamond";
          template = "  {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "#ebcc34";
          foreground = "#000000";
          leading_diamond = "";
          style = "diamond";
          template = "  Kubernetes {{.Context}} cluster {{if .Namespace}}- {{.Namespace}} namespace{{end}} ";
          trailing_diamond = "";
          type = "kubectl";
        }
        {
          background = "#9ec3f0";
          foreground = "#000000";
          powerline_symbol = "";
          style = "powerline";
          template = "  Subscription {{ .Name }} ({{ if .EnvironmentName | contains \"AzureCloud\" }}{{ \"Global\" }}{{ else }}{{ .EnvironmentName }}{{ end }}) ";
          type = "az";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#E06C75";
          style = "plain";
          template = " ~ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Shell }} in {{ .Folder }}";
  final_space = true;
  version = 3;
}
