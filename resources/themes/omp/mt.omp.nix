{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#B4009E";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          background = "#FFFF00";
          foreground = "#000000";
          powerline_symbol = "";
          properties = {
            style = "folder";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#4E44FF";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            branch_icon = "";
            fetch_stash_count = true;
            fetch_status = false;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " ➜ ({{ .UpstreamIcon }}{{ .HEAD }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }}) ";
          type = "git";
        }
        {
          background = "#4e903d";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          background = "#16C60C";
          foreground = "#ffffff";
          properties = {
            time_format = "15:04";
          };
          style = "diamond";
          template = " ♥ {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
