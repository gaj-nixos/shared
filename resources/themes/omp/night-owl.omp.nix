{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
    deepOrange = "#be1818";
    darkRed = "#ce092f";
    spotifyRed = "#ef5350";
    deepOrange2 = "#ca3c34";
    deepOrange3 = "#cc3a3a";
    orange = "#f78c6c";
    yellow = "#ffeb95";
    deepYellow = "#f5bf45";
    lightYellow = "#e4cf6a";
    green = "#addb67";
    darkGreen = "#01a300";
    spotifyGreen = "#1DB954";
    spotifyIconGreen = "#22da6e";
    teal = "#21c7a8";
    darkTeal = "#5a7a94";
    blue = "#82AAFF";
    lightBlue = "#5398c2";
    deepBlue = "#348cba";
    darkBlue = "#011627";
    purple = "#C792EA";
    darkPurple = "#8f43f3";
    lightPurple = "#906cff";
    pink = "#de1f84";
    black = "#000000";
    gray = "#575656";
    darkGray = "#565656";
    lightGray = "#d6deeb";
    white = "#ffffff";
  };

  palette =
    if (config.lib ? stylix) then
      {
        deepOrange = brown;
        darkRed = red;
        spotifyRed = orange;
        deepOrange2 = orange;
        deepOrange3 = orange;
        orange = orange;
        yellow = yellow;
        deepYellow = yellow;
        lightYellow = yellow;
        green = green;
        darkGreen = green;
        spotifyGreen = green;
        spotifyIconGreen = green;
        teal = cyan;
        darkTeal = cyan;
        blue = blue;
        lightBlue = blue;
        deepBlue = blue;
        darkBlue = gray;
        purple = purple;
        darkPurple = purple;
        lightPurple = purple;
        pink = red;
        black = black;
        gray = gray;
        darkGray = darkGray;
        lightGray = lightGray;
        white = white;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  inherit palette;
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "p:teal";
          foreground = "p:darkBlue";
          leading_diamond = "╭─";
          properties = {
            windows = "";
          };
          style = "diamond";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}}  ";
          trailing_diamond = "";
          type = "os";
        }
        {
          background = "p:spotifyRed";
          foreground = "p:yellow";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "root";
        }
        {
          background = "p:blue";
          foreground = "p:darkBlue";
          powerline_symbol = "";
          properties = {
            folder_icon = " ";
            folder_separator_icon = "<p:darkBlue></> ";
            home_icon = "  ";
            style = "agnoster";
          };
          style = "powerline";
          template = "{{ path .Path .Location }}";
          type = "path";
        }
        {
          background = "p:green";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}p:lightYellow{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}p:orange{{ end }}"
            "{{ if gt .Ahead 0 }}p:purple{{ end }}"
            "{{ if gt .Behind 0 }}p:purple{{ end }}"
          ];
          foreground = "p:darkBlue";
          powerline_symbol = "";
          properties = {
            branch_icon = " ";
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
            fetch_worktree_count = true;
          };
          style = "powerline";
          template = " {{ url .UpstreamIcon .UpstreamURL }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "p:gray";
          foreground = "p:lightGray";
          leading_diamond = "<transparent,p:gray></>";
          properties = {
            style = "roundrock";
            threshold = 0;
          };
          style = "diamond";
          template = " {{ .FormattedMs }}";
          trailing_diamond = "";
          type = "executiontime";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      overflow = "break";
      segments = [
        {
          background = "p:lightGray";
          foreground = "p:darkBlue";
          leading_diamond = "";
          style = "diamond";
          template = "  {{ .Name }} ";
          trailing_diamond = "<transparent,p:lightGray></>";
          type = "shell";
        }
        {
          background = "p:darkPurple";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = " {{ round .PhysicalPercentUsed .Precision }}% ";
          trailing_diamond = "<transparent,p:darkPurple></>";
          type = "sysinfo";
        }
        {
          background = "p:white";
          foreground = "p:darkRed";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "angular";
        }
        {
          background = "p:white";
          foreground = "p:pink";
          leading_diamond = "";
          style = "diamond";
          template = "α {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "aurelia";
        }
        {
          background = "p:darkGray";
          foreground = "p:spotifyIconGreen";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .Profile }}{{ if .Region }}@{{ .Region }}{{ end }} ";
          trailing_diamond = "<transparent,p:darkGray></>";
          type = "aws";
        }
        {
          background = "p:white";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .EnvironmentName }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "az";
        }
        {
          background = "p:white";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = "<p:deepYellow></> {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "azfunc";
        }
        {
          background = "p:darkTeal";
          foreground = "p:darkBlue";
          leading_diamond = "";
          style = "diamond";
          template = "  cds {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:darkTeal></>";
          type = "cds";
        }
        {
          background = "p:white";
          foreground = "p:black";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "crystal";
        }
        {
          background = "p:white";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = "  cf {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "cf";
        }
        {
          background = "p:white";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = " {{if .Org }}{{ .Org }}{{ end }}{{ if .Space }}/{{ .Space }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "cftarget";
        }
        {
          background = "p:lightGray";
          foreground = "p:darkGreen";
          leading_diamond = "";
          style = "diamond";
          template = "<p:deepPurple></> <p:deepOrange></>  cmake {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightGray></>";
          type = "cmake";
        }
        {
          background = "p:lightGray";
          foreground = "p:darkBlue";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightGray></>";
          type = "dart";
        }
        {
          background = "p:darkBlue";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = "  {{ if .Unsupported }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:darkBlue></>";
          type = "dotnet";
        }
        {
          background = "p:lightBlue";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightBlue></>";
          type = "flutter";
        }
        {
          background = "p:white";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "go";
        }
        {
          background = "p:darkBlue";
          foreground = "p:lightPurple";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:darkBlue></>";
          type = "haskell";
        }
        {
          background = "p:white";
          foreground = "p:darkRed";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "java";
        }
        {
          background = "p:darkPurple";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = "<p:deepOrange2></> {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:darkPurple></>";
          type = "julia";
        }
        {
          background = "p:lightPurple";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = "K {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightPurple></>";
          type = "kotlin";
        }
        {
          background = "p:lightBlue";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = " {{.Context}} :: {{if .Namespace}}{{.Namespace}}{{else}}default{{end}} ";
          trailing_diamond = "<transparent,p:lightBlue></>";
          type = "kubectl";
        }
        {
          background = "p:white";
          foreground = "p:darkBlue";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "lua";
        }
        {
          background = "p:darkGray";
          foreground = "p:spotifyIconGreen";
          leading_diamond = "";
          properties = {
            fetch_package_manager = true;
            npm_icon = "<p:deepOrange3></> ";
            yarn_icon = "<p:deepBlue></> ";
          };
          style = "diamond";
          template = " {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          trailing_diamond = "<transparent,p:darkGray></>";
          type = "node";
        }
        {
          background = "p:lightBlue";
          foreground = "p:darkBlue";
          leading_diamond = "";
          style = "diamond";
          template = "Nx {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightBlue></>";
          type = "nx";
        }
        {
          background = "p:darkBlue";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:darkBlue></>";
          type = "perl";
        }
        {
          background = "p:lightPurple";
          foreground = "p:black";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightPurple></>";
          type = "php";
        }
        {
          background = "p:lightBlue";
          foreground = "p:yellow";
          leading_diamond = "";
          style = "diamond";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}";
          trailing_diamond = "<transparent,p:lightBlue></>";
          type = "python";
        }
        {
          background = "p:lightGray";
          foreground = "p:lightBlue";
          leading_diamond = "";
          style = "diamond";
          template = "R {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:lightGray></>";
          type = "r";
        }
        {
          background = "p:white";
          foreground = "p:darkRed";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "ruby";
        }
        {
          background = "p:white";
          foreground = "p:black";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:white></>";
          type = "rust";
        }
        {
          background = "p:spotifyRed";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "<transparent,p:spotifyRed></>";
          type = "swift";
        }
        {
          background = "p:darkBlue";
          foreground = "p:lightGray";
          leading_diamond = "";
          properties = {
            time_format = "15:04:05";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }}";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:teal";
          style = "plain";
          template = "╰─";
          type = "text";
        }
        {
          background = "p:spotifyGreen";
          foreground = "p:darkBlue";
          leading_diamond = "";
          properties = {
            playing_icon = " ";
          };
          style = "diamond";
          template = " {{ .Icon }}{{ if ne .Status \"stopped\" }}{{ .Artist }} ~ {{ .Track }}{{ end }} ";
          trailing_diamond = " ";
          type = "spotify";
        }
        {
          foreground = "p:spotifyIconGreen";
          foreground_templates = [
            "{{ if gt .Code 0 }}p:spotifyRed{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Folder }}";
  final_space = true;
  transient_prompt = {
    background = "transparent";
    foreground = "p:lightGray";
    template = " ";
  };
  version = 3;
}
