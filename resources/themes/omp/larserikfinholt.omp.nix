{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#c386f1";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .UserName }} ";
          trailing_diamond = "";
          type = "session";
        }
        {
          background = "#ff479c";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            folder_separator_icon = "  ";
            home_icon = "~";
            style = "folder";
          };
          style = "powerline";
          template = "   {{ .Path }} ";
          type = "path";
        }
        {
          background = "#fffb38";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          foreground = "#193549";
          powerline_symbol = "";
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#83769c";
          foreground = "#ffffff";
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "<transparent></> {{ .FormattedMs }}⠀";
          type = "executiontime";
        }
        {
          background = "#000000";
          background_templates = [
            "{{ if gt .Hours 100 }}#dc3545{{ end }}"
            "{{ if and (lt .Hours 100) (gt .Hours 50) }}#ffc107{{ end }}"
            "{{ if lt .Hours 50 }}#28a745{{ end }}"
          ];
          foreground = "#ffffff";
          foreground_templates = [
            "{{ if gt .Hours 100 }}#FFFFFF{{ end }}"
            "{{ if and (lt .Hours 100) (gt .Hours 50) }}#343a40{{ end }}"
            "{{ if lt .Hours 50 }}#FFFFFF{{ end }}"
          ];
          properties = {
            access_token = "0ccbd2ac1e37a5b84101468df3d367177fe02ab3";
            http_timeout = 1500;
            refresh_token = "111111111111111111111111111111";
          };
          style = "powerline";
          template = " {{.Name}} {{.Ago}} {{.Icon}} ";
          type = "strava";
        }
        {
          background = "#00897b";
          background_templates = [
            "{{ if gt .Code 0 }}#e91e63{{ end }}"
          ];
          foreground = "#ffffff";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = "<parentBackground></>  ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Shell }} in {{ .Folder }}";
  final_space = true;
  version = 3;
}
