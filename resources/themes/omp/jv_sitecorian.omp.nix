{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#ffffff";
          style = "plain";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#185F7B";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          background = "#185F7B";
          foreground = "#185F7B";
          powerline_symbol = "";
          style = "powerline";
          type = "text";
        }
        {
          background = "#185F7B";
          foreground = "#ffffff";
          properties = {
            home_icon = "  ";
            style = "mixed";
          };
          style = "diamond";
          template = "{{ .Path }} ";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#6f42c1";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          leading_diamond = "<transparent,#49404f></>";
          style = "plain";
          trailing_diamond = "";
          type = "text";
        }
        {
          background = "#4caf50";
          background_templates = [
            "{{ if gt .Code 0 }}red{{ end }}"
          ];
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          template = " {{ if gt .Code 0 }}{{ else }} ♥{{ end }} ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#2B2B2B";
          foreground = "#ffffff";
          invert_powerline = true;
          leading_diamond = "";
          properties = {
            time_format = "3:04:05 PM";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }}<#ffffff>  </>";
          trailing_diamond = "";
          type = "time";
        }
        {
          background = "#2B2B2B";
          foreground = "#ffffff";
          invert_powerline = true;
          leading_diamond = "";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = " {{ .FormattedMs }}<#ffffff>  </>";
          trailing_diamond = "";
          type = "executiontime";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#2B2B2B";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          background = "#2B2B2B";
          foreground = "#1CA6A3";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#2B2B2B";
          foreground = "#FBD951";
          powerline_symbol = "";
          style = "powerline";
          template = " ";
          type = "root";
        }
        {
          background = "#DC291E";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .UserName }} ";
          type = "session";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#000000";
          foreground = "#666666";
          style = "plain";
          template = " {{ .Name }}";
          type = "shell";
        }
      ];
      type = "rprompt";
    }
  ];
  console_title_template = "{{if .Root}}Admin: {{end}} {{.Folder}}";
  final_space = true;
  version = 3;
}
