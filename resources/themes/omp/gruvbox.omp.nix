{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#3A3A3A";
          foreground = "#ffffff";
          style = "powerline";
          template = "{{ if .WSL }}WSL at{{ end }} {{.Icon}} ";
          type = "os";
        }
        {
          background = "#458588";
          foreground = "#282828";
          powerline_symbol = "";
          properties = {
            style = "full";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#98971A";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          foreground = "#282828";
          leading_diamond = "";
          powerline_symbol = "";
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            branch_icon = " ";
            branch_identical_icon = "●";
          };
          style = "powerline";
          template = " {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          trailing_diamond = "";
          type = "git";
        }
        {
          background = "#8ED1F7";
          foreground = "#111111";
          powerline_symbol = "";
          properties = {
            fetch_version = true;
          };
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          type = "go";
        }
        {
          background = "#4063D8";
          foreground = "#111111";
          powerline_symbol = "";
          properties = {
            fetch_version = true;
          };
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          type = "julia";
        }
        {
          background = "#FFDE57";
          foreground = "#111111";
          powerline_symbol = "";
          properties = {
            display_mode = "files";
            fetch_virtual_env = false;
          };
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          type = "python";
        }
        {
          background = "#AE1401";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            display_mode = "files";
            fetch_version = true;
          };
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          type = "ruby";
        }
        {
          background = "#FEAC19";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            display_mode = "files";
            fetch_version = false;
          };
          style = "powerline";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          type = "azfunc";
        }
        {
          background_templates = [
            "{{if contains \"default\" .Profile}}#FFA400{{end}}"
            "{{if contains \"jan\" .Profile}}#f1184c{{end}}"
          ];
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            display_default = false;
          };
          style = "powerline";
          template = "  {{ .Profile }}{{ if .Region }}@{{ .Region }}{{ end }} ";
          type = "aws";
        }
        {
          background = "#ffff66";
          foreground = "#111111";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "root";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Folder }}";
  final_space = true;
  version = 3;
}
