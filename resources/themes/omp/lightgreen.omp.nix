{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#000000";
          foreground = "#dd1e1e";
          style = "plain";
          template = " ⚡";
          type = "root";
        }
        {
          background = "transparent";
          foreground = "#ffffff";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
            wsl = "";
          };
          style = "plain";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#272727";
          foreground = "#3EC669";
          leading_diamond = "";
          properties = {
            style = "folder";
          };
          style = "diamond";
          template = " {{ .Path }}";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#272727";
          foreground = "#00ff0d";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
          };
          style = "plain";
          template = "<#000000> </>{{ .HEAD }}{{ if .Staging.Changed }}<#FF6F00>  {{ .Staging.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#306998";
          foreground = "#FFE873";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}";
          trailing_diamond = " ";
          type = "python";
        }
        {
          background = "#ffffff";
          foreground = "#06aad5";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = " ";
          type = "go";
        }
        {
          background = "#565656";
          foreground = "#faa029";
          leading_diamond = "";
          style = "diamond";
          template = " {{.Profile}}{{if .Region}}@{{.Region}}{{end}}";
          trailing_diamond = " ";
          type = "aws";
        }
        {
          type = "terraform";
          style = "powerline";
          powerline_symbol = "";
          foreground = "#000000";
          background = "#ebcc34";
          template = "{{.WorkspaceName}}";
        }
        {
          background = "#316ce4";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = " {{.Context}}{{if .Namespace}} :: {{.Namespace}}{{end}}";
          trailing_diamond = "";
          type = "kubectl";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          properties = {
            style = "roundrock";
            always_enabled = true;
          };
          style = "diamond";
          template = "{{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "#ffffff";
          foreground = "#3c3c3c";
          style = "plain";
          template = " {{ round .PhysicalPercentUsed .Precision }}% ";
          type = "sysinfo";
        }
        {
          background = "#f36943";
          background_templates = [
            "{{if eq \"Charging\" .State.String}}#b8e994{{end}}"
            "{{if eq \"Discharging\" .State.String}}#fff34e{{end}}"
            "{{if eq \"Full\" .State.String}}#33DD2D{{end}}"
          ];
          foreground = "#262626";
          invert_powerline = true;
          leading_diamond = "";
          properties = {
            charged_icon = " ";
            charging_icon = " ";
            discharging_icon = " ";
          };
          style = "plain";
          template = " {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }} <#262626></>";
          type = "battery";
        }
        {
          background = "#000000";
          foreground = "lightGreen";
          style = "plain";
          template = "{{ .CurrentDate | date .Format }}";
          type = "time";
        }
      ];
      type = "rprompt";
    }
  ];
  pwd = "osc99";
  version = 3;
}
