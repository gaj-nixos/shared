{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#3A86FF";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#3A86FF";
          foreground = "#ffffff";
          style = "powerline";
          template = " {{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "#ff479c";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            style = "full";
          };
          style = "powerline";
          template = "  {{ .Path }} ";
          type = "path";
        }
        {
          background = "#4caf50";
          foreground = "#193549";
          powerline_symbol = "";
          properties = {
            time_format = "2006-01-02 15:04:05";
          };
          style = "powerline";
          template = " {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          background = "#fffb38";
          foreground = "#193549";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#6CA35E";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          background = "#ffff66";
          foreground = "#193549";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "root";
        }
        {
          background = "#2e9599";
          background_templates = [
            "{{ if gt .Code 0 }}#f1184c{{ end }}"
          ];
          foreground = "#ffffff";
          leading_diamond = "<transparent,background></>";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = "  ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#f1184c";
          style = "plain";
          template = " ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
