{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#a313a8";
          foreground = "#FFEB3B";
          style = "plain";
          template = "⚡ ";
          type = "root";
        }
        {
          background = "transparent";
          foreground = "#ffffff";
          style = "plain";
          template = "{{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#01579B";
          foreground = "#ffffff";
          leading_diamond = "<transparent,#01579B></>";
          properties = {
            folder_icon = "...";
            folder_separator_icon = "<transparent>  </>";
            home_icon = "";
            style = "agnoster_short";
          };
          style = "diamond";
          template = " {{ .Path }} ";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#00C853";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FFEB3B{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#FFCC80{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          foreground = "#000000";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
          };
          style = "powerline";
          template = " {{ .HEAD }}{{ if .Staging.Changed }}<#FF6F00>  {{ .Staging.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#49404f";
          foreground = "#ffffff";
          leading_diamond = "<transparent,#49404f></>";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "diamond";
          template = " {{ .FormattedMs }}s ";
          trailing_diamond = "";
          type = "executiontime";
        }
        {
          background = "#910000";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "<transparent> </> {{ reason .Code }} ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#29315A";
          foreground = "#43CCEA";
          leading_diamond = "";
          trailing_diamond = "";
          style = "diamond";
          template = "  {{ .UserName }}<transparent> / </>{{ .HostName }} ";
          type = "session";
        }
        {
          background = "#29315A";
          foreground = "#3EC669";
          properties = {
            time_format = "15:04:05";
          };
          leading_diamond = "";
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#ffffff";
          foreground_templates = [
            "{{ if gt .Code 0 }}#ff0000{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}} ⚡ {{end}}{{.Folder | replace \"~\" \"🏚\" }} @ {{.HostName}}";
  version = 3;
}
