{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#E5C07B";
          properties = {
            time_format = "15:04:05";
          };
          style = "plain";
          template = "[{{ .CurrentDate | date .Format }}]";
          type = "time";
        }
        {
          foreground = "#B5B50D";
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          foreground = "#61AFEF";
          properties = {
            style = "agnoster";
          };
          style = "plain";
          template = " {{ .Path }} on";
          type = "path";
        }
        {
          foreground = "#F3C267";
          properties = {
            branch_gone_icon = "❎";
            branch_identical_icon = "";
          };
          style = "plain";
          template = " {{ .HEAD }} ";
          type = "git";
        }
        {
          foreground = "#C94A16";
          style = "plain";
          template = "x{{ reason .Code }} ";
          type = "status";
        }
        {
          foreground = "#E06C75";
          style = "plain";
          template = "❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
