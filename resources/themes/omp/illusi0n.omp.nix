{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      segments = [
        {
          foreground = "#ff8800";
          style = "diamond";
          template = "{{ round .PhysicalPercentUsed .Precision }}% ";
          type = "sysinfo";
        }
        {
          foreground = "#ff8800";
          style = "diamond";
          template = "{{ (div ((sub .PhysicalTotalMemory .PhysicalFreeMemory)|float64) 1073741824.0) }}/{{ (div .PhysicalTotalMemory 1073741824.0) }}GB ";
          type = "sysinfo";
        }
      ];
      type = "rprompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#ff8800";
          properties = {
            macos = "mac";
          };
          style = "plain";
          template = "{{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          foreground = "#ff8800";
          style = "plain";
          template = "$";
          type = "text";
        }
        {
          foreground = "#ff8800";
          style = "plain";
          template = "{{ .UserName }}:";
          type = "session";
        }
        {
          foreground = "#62c0ff";
          properties = {
            folder_separator_icon = "/";
            style = "full";
          };
          style = "plain";
          type = "path";
        }
        {
          foreground = "#62c0ff";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#6287ff{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#7f62ff{{ end }}"
            "{{ if gt .Ahead 0 }}#9962ff{{ end }}"
            "{{ if gt .Behind 0 }}#c062ff{{ end }}"
          ];
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = "<#ff8800>on</> {{.UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          foreground = "#ff8800";
          style = "plain";
          template = "❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
