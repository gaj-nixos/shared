{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  palette = {
    white = "#FFFFFF";
    tan = "#CC3802";
    teal = "#047E84";
    plum = "#9A348E";
    blush = "#DA627D";
    salmon = "#FCA17D";
    sky = "#86BBD8";
    teal_blue = "#33658A";
  };
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          type = "text";
          style = "diamond";
          leading_diamond = "";
          foreground = "p:white";
          background = "p:tan";
          template = "{{ if .Env.PNPPSHOST }}  {{ .Env.PNPPSHOST }} {{ end }}";
        }
        {
          type = "text";
          style = "powerline";
          foreground = "p:white";
          background = "p:teal";
          powerline_symbol = "";
          template = "{{ if .Env.PNPPSSITE }}  {{ .Env.PNPPSSITE }}{{ end }}";
        }
        {
          type = "text";
          style = "diamond";
          trailing_diamond = "";
          foreground = "p:white";
          background = "p:teal";
          template = "{{ if .Env.PNPPSSITE }} {{ end }}";
        }
      ];
      type = "rprompt";
    }
    {
      alignment = "left";
      segments = [
        {
          background = "p:plum";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          background = "p:blush";
          foreground = "p:white";
          powerline_symbol = "";
          properties = {
            style = "folder";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "p:salmon";
          foreground = "p:white";
          powerline_symbol = "";
          properties = {
            branch_icon = "";
            fetch_stash_count = true;
            fetch_status = false;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " ➜ ({{ .UpstreamIcon }}{{ .HEAD }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }}) ";
          type = "git";
        }
        {
          background = "p:sky";
          foreground = "p:white";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          background = "p:teal_blue";
          foreground = "p:white";
          properties = {
            time_format = "15:04";
          };
          style = "diamond";
          template = " ♥ {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
