{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
    white = "#ffffff";
    yellow = "#C2C206";
    lightYellow = "#B5B50D";
    orange = "#C94A16";
    blue = "#007ACC";
  };

  palette =
    if (config.lib ? stylix) then
      {
        white = white;
        yellow = yellow;
        lightYellow = orange;
        orange = brown;
        blue = blue;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  inherit palette;

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "p:white";
          properties = {
            style = "full";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          foreground = "p:yellow";
          style = "plain";
          template = "{{ .HEAD }} ";
          type = "git";
        }
        {
          foreground = "p:lightYellow";
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          foreground = "p:orange";
          style = "plain";
          template = "x{{ reason .Code }} ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:blue";
          style = "plain";
          template = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
