{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "cyan";
          style = "powerline";
          template = "{{ if .WSL }}WSL at {{ end }}{{.Icon}}";
          type = "os";
        }
        {
          foreground = "cyan";
          properties = {
            style = "full";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#F1502F";
          properties = {
            fetch_status = true;
          };
          style = "plain";
          template = ":: {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "red";
          style = "plain";
          template = "| root ";
          type = "root";
        }
        {
          foreground = "#06A4CE";
          style = "powerline";
          template = "|  {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          type = "dart";
        }
        {
          foreground = "#6CA35E";
          style = "powerline";
          template = "|  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          foreground = "#4584b6";
          properties = {
            display_mode = "context";
            fetch_virtual_env = true;
          };
          style = "plain";
          template = "|  {{ .Venv }} ";
          type = "python";
        }
        {
          foreground_templates = [
            "{{if eq \"Charging\" .State.String}}#4caf50{{end}}"
            "{{if eq \"Discharging\" .State.String}}#40c4ff{{end}}"
            "{{if eq \"Full\" .State.String}}#ff0000{{end}}"
          ];
          invert_powerline = true;
          properties = {
            charged_icon = " ";
            charging_icon = " ";
          };
          style = "powerline";
          template = "| {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}  ";
          type = "battery";
        }
        {
          foreground = "lightGreen";
          style = "plain";
          template = "| {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "lightGreen";
          foreground_templates = [
            "{{ if gt .Code 0 }}red{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          template = "➜ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
