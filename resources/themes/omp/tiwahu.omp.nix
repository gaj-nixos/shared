{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#007ACC";
          foreground = "#222222";
          properties = {
            style = "austin";
            threshold = 321;
          };
          style = "plain";
          template = " {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "#880000";
          foreground = "#ff8888";
          style = "plain";
          template = "{{ if gt .Code 0 }} {{ .Code }} {{ end }}";
          type = "status";
        }
        {
          background = "#ffcc88";
          foreground = "#222222";
          style = "plain";
          template = "";
          type = "root";
        }
        {
          background = "#222222";
          foreground = "#666666";
          style = "plain";
          template = " {{ if .WSL }} {{ end }}{{.Icon}}";
          type = "os";
        }
        {
          background = "#222222";
          foreground = "#666666";
          style = "plain";
          template = " {{ if .SSHSession }} {{ end }}{{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "#444444";
          foreground = "#cccccc";
          properties = {
            folder_icon = "…";
            style = "mixed";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#cf432B";
          foreground = "#f1f0e9";
          properties = {
            branch_icon = " ";
            cherry_pick_icon = " ";
            commit_icon = " ";
            fetch_status = false;
            fetch_upstream_icon = false;
            merge_icon = " ";
            no_commits_icon = " ";
            rebase_icon = " ";
            revert_icon = " ";
            tag_icon = " ";
          };
          style = "plain";
          template = " {{ .HEAD }} ";
          type = "git";
        }
        {
          background = "#7014eb";
          foreground = "#ffffff";
          properties = {
            fetch_version = false;
          };
          style = "plain";
          template = "  ";
          type = "dotnet";
        }
        {
          background = "#7FD5EA";
          foreground = "#ffffff";
          properties = {
            fetch_version = false;
          };
          style = "plain";
          template = "  ";
          type = "go";
        }
        {
          background = "#906cff";
          foreground = "#100e23";
          properties = {
            fetch_version = false;
          };
          style = "plain";
          template = "  ";
          type = "python";
        }
        {
          background = "#99908a";
          foreground = "#193549";
          properties = {
            fetch_version = false;
          };
          style = "plain";
          template = "  ";
          type = "rust";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#007ACC";
          style = "plain";
          template = "{{ .Name }} ";
          type = "shell";
          properties = {
            mapped_shell_names = {
              pwsh = "❯";
              shell = "❯";
              cmd = ">";
              lua = ">";
              nu = ":)";
              fish = "~>";
              zsh = "%";
              bash = "$";
            };
          };
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
