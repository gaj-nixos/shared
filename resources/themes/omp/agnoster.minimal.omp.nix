{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
    white = "#ffffff";
    red = "#ff0000";
    blue = "#007ACC";
  };

  palette =
    if (config.lib ? stylix) then
      {
        white = white;
        red = red;
        blue = blue;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  inherit palette;
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "p:white";
          style = "plain";
          template = "{{ reason .Code }}❌ ";
          type = "status";
        }
        {
          foreground = "p:red";
          style = "plain";
          template = "# ";
          type = "root";
        }
        {
          foreground = "p:white";
          style = "plain";
          template = "{{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "p:blue";
          foreground = "p:white";
          properties = {
            folder_icon = "…";
            folder_separator_icon = "  ";
            style = "agnoster_short";
            max_depth = 3;
          };
          style = "plain";
          template = "<transparent></> {{ .Path }} ";
          type = "path";
        }
        {
          background = "p:blue";
          foreground = "p:white";
          properties = {
            cherry_pick_icon = "✓ ";
            commit_icon = "▷ ";
            fetch_status = true;
            merge_icon = "◴ ";
            no_commits_icon = "[no commits]";
            rebase_icon = "Ɫ ";
            tag_icon = "▶ ";
          };
          style = "plain";
          template = "{{ .HEAD }}{{ if and (eq .Ahead 0) (eq .Behind 0) }} ≡{{end}}{{ if gt .Ahead 0 }} ↑{{.Ahead}}{{end}}{{ if gt .Behind 0 }} ↓{{.Behind}}{{end}} {{ if .Working.Changed }}+{{ .Working.Added }} ~{{ .Working.Modified }} -{{ .Working.Deleted }} {{ end }}";
          type = "git";
        }
        {
          foreground = "p:blue";
          style = "plain";
          template = " ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
