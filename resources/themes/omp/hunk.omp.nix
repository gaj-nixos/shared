{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#c44569";
          foreground = "#ffffff";
          leading_diamond = "";
          properties = {
            windows = "";
          };
          style = "diamond";
          template = "{{ if .WSL }}WSL at {{ end }}{{.Icon}}";
          type = "os";
        }
        {
          background = "#c44569";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            style = "full";
          };
          style = "powerline";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          background = "#f78fb3";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          background = "#f78fb3";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            fetch_version = false;
          };
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          type = "python";
        }
        {
          background = "#caffbf";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FCA17D{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#f26d50{{ end }}"
            "{{ if gt .Ahead 0 }}#89d1dc{{ end }}"
            "{{ if gt .Behind 0 }}#f17c37{{ end }}"
          ];
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#83769c";
          foreground = "#ffffff";
          invert_powerline = true;
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          template = " {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "#86BBD8";
          foreground = "#FFD700";
          invert_powerline = true;
          powerline_symbol = "";
          style = "powerline";
          template = " ⚡";
          type = "root";
        }
        {
          background = "#86BBD8";
          foreground = "#ffffff";
          invert_powerline = true;
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .UserName }} ";
          type = "session";
        }
        {
          background = "#33658A";
          foreground = "#ffffff";
          invert_powerline = true;
          style = "diamond";
          template = " {{ .Name }} ";
          trailing_diamond = "";
          type = "shell";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          foreground = "#ffffff";
          style = "plain";
          template = "<#69FF94>❯</>";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
