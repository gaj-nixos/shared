{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#FFE082";
          style = "plain";
          template = "@{{ .UserName }} ➜";
          type = "session";
        }
        {
          foreground = "#56B6C2";
          properties = {
            style = "agnoster_short";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#7FD5EA";
          style = "powerline";
          template = "({{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}) ";
          type = "go";
        }
        {
          foreground = "#9e7eff";
          style = "powerline";
          template = "( {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}) ";
          type = "python";
        }
        {
          foreground = "#56B6C2";
          properties = {
            branch_icon = "";
          };
          style = "plain";
          template = "<#E8CC97>git(</>{{ .HEAD }}<#E8CC97>) </>";
          type = "git";
        }
        {
          foreground = "#FFAB91";
          properties = {
            always_enabled = false;
            style = "austin";
            threshold = 100;
          };
          style = "powerline";
          template = "{{ .FormattedMs }}";
          type = "executiontime";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#193549";
          foreground_templates = [
            "{{if eq \"Charging\" .State.String}}#64B5F6{{end}}"
            "{{if eq \"Discharging\" .State.String}}#E36464{{end}}"
            "{{if eq \"Full\" .State.String}}#66BB6A{{end}}"
          ];
          properties = {
            charged_icon = " ";
            charging_icon = " ";
            discharging_icon = " ";
          };
          style = "powerline";
          template = "[{{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}]";
          type = "battery";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
