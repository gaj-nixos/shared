{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "right";
      segments = [
        {
          background = "#003543";
          foreground = "#ffffff";
          style = "plain";
          template = "  {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          background = "#83769c";
          foreground = "#ffffff";
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "  {{ .FormattedMs }} ";
          type = "executiontime";
        }
      ];
      type = "rprompt";
    }
    {
      alignment = "left";
      segments = [
        {
          background = "#61AFEF";
          foreground = "#ffffff";
          properties = {
            display_host = false;
          };
          style = "diamond";
          template = "{{if .Root}}  {{.UserName}} {{else}} {{.UserName}} {{end}}";
          trailing_diamond = "";
          type = "session";
        }
        {
          background = "#C678DD";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            folder_icon = "";
            folder_separator_icon = "  ";
            max_depth = 2;
            style = "agnoster_short";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#95ffa4";
          foreground = "#193549";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .HEAD }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}} ⚡ {{end}}{{.UserName}} ➔ 📁{{.Folder}}";
  final_space = true;
  version = 3;
}
