{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#cfcfc4";
          background_templates = [
            "{{if .Root}}#ff3026{{else}}#cfcfc4{{end}}"
          ];
          foreground = "#100e23";
          properties = {
            display_host = false;
          };
          style = "powerline";
          template = "{{if .Root}}  {{.UserName}} {{else}} {{.UserName}} {{end}}";
          type = "session";
        }
        {
          background = "#ec9706";
          foreground = "#100e23";
          powerline_symbol = "";
          properties = {
            folder_icon = "";
            folder_separator_icon = "";
            home_icon = "";
            mapped_locations = {
              "C:\\Users" = "";
              "C:\\Windows" = "";
            };
            mixed_threshold = 8;
            style = "mixed";
          };
          style = "powerline";
          template = " {{ path .Path .Location }} ";
          type = "path";
        }
        {
          background = "#3feb56";
          foreground = "#193549";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .HEAD }} ";
          type = "git";
        }
        {
          background = "#906cff";
          foreground = "#100e23";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          type = "python";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#ff4040";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = "  ";
          trailing_diamond = "";
          type = "status";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          leading_diamond = "";
          properties = {
            style = "austin";
          };
          style = "diamond";
          template = " {{ .FormattedMs }} ";
          type = "executiontime";
        }
      ];
      type = "rprompt";
    }
  ];
  console_title_template = "{{if .Root}} ⚡ {{end}}{{.UserName}} ➔ 📁{{.Folder}}";
  final_space = true;
  secondary_prompt = {
    background = "#ec9706";
    foreground = "#100e23";
    template = "  ";
  };
  version = 3;
}
