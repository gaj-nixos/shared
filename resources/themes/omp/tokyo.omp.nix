{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  palette = {
    blue = "#7eb8da";
    purple = "#be9ddf";
    pink = "#ffa5d8";
    green = "#98bfad";
    white = "#ffffff";
  };
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "p:blue";
          style = "plain";
          template = "┏[<p:white></> {{ .UserName }} from <p:white></> {{ .HostName }}]";
          type = "session";
        }
        {
          foreground = "p:purple";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "diamond";
          template = "[<p:white></> {{ .FormattedMs }}s]";
          type = "executiontime";
        }
        {
          properties = {
            root_icon = "";
          };
          style = "diamond";
          template = "";
          type = "root";
        }
        {
          foreground = "p:purple";
          style = "diamond";
          template = "[<p:white></> RAM: {{ (div ((sub .PhysicalTotalMemory .PhysicalFreeMemory)|float64) 1073741824.0) }}/{{ (div .PhysicalTotalMemory 1073741824.0) }}GB]";
          type = "sysinfo";
        }
        {
          foreground = "p:blue";
          properties = {
            time_format = "Monday at 3:04:05 PM";
          };
          style = "diamond";
          template = "[<p:white></> {{ .CurrentDate | date .Format }}]";
          type = "time";
        }
        {
          foreground = "p:pink";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = "[<p:white>{{ .UpstreamIcon }}</>{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }} <p:white></> {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }} <p:white></> {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }} <p:white></> {{ .StashCount }}{{ end }}]";
          type = "git";
        }
        {
          foreground = "p:pink";
          style = "plain";
          template = "[{{ .Profile }}{{if .Region}}@{{ .Region }}{{ end }}]";
          type = "aws";
        }
        {
          foreground = "p:pink";
          style = "plain";
          template = "[{{.Context}}{{if .Namespace}} :: {{.Namespace}}{{end}}]";
          type = "kubectl";
        }
        {
          foreground = "p:pink";
          style = "plain";
          template = "[]";
          type = "root";
        }
        {
          foreground = "p:pink";
          style = "powerline";
          template = "[<p:white></> Error, check your command]";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:blue";
          properties = {
            style = "full";
          };
          style = "plain";
          template = "┖[<p:green>{{ .Path }}</>]";
          type = "path";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:blue";
          style = "plain";
          template = "└─Δ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
