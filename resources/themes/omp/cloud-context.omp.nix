{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  version = 3;
  console_title_template = "{{ if or .Root .Segments.Git.RepoName }}[ {{ if .Root }}Admin{{ end }}{{ if and .Root .Segments.Git.RepoName }}@{{ end }}{{ if .Segments.Git.RepoName }}{{ .Segments.Git.RepoName }}{{ end }} ]{{ end }} {{ .Folder }}";
  palette = {
    background-color = "transparent";
    cloud-text-amazon = "#4285F4";
    cloud-text-azure = "#4285F4";
    cloud-text-gcp = "#4285F4";
    cloud-text-firebase = "#FFA000";
    error-background = "#dd0033";
    error-text = "#242424";
    git-text = "#238636";
    kubernetes-text = "#FFBB00";
    talosctl-text = "#FF6C00";
    symbol-color = "#ffffff";
    timer-text = "#dd0033";
  };
  blocks = [
    {
      alignment = "left";
      newline = false;
      segments = [
        {
          background = "p:background-color";
          foreground = "p:timer-text";
          properties = {
            time_format = "15:04:05";
          };
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{ dateInZone .Format .CurrentDate \"UTC\" }} ";
          type = "time";
        }
        {
          background = "p:background-color";
          foreground = "p:kubernetes-text";
          style = "powerline";
          powerline_symbol = "";
          template = "<p:symbol-color></> {{.Context}}{{if .Namespace}}  {{.Namespace}}{{end}}";
          type = "kubectl";
        }
        {
          background = "p:background-color";
          foreground = "p:talosctl-text";
          style = "powerline";
          powerline_symbol = "";
          template = "<p:symbol-color>������</> {{.Context}}";
          type = "talosctl";
        }
        {
          background = "p:background-color";
          foreground = "p:cloud-text-amazon";
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{.Profile}}{{if .Region}}  {{.Region}}{{end}}";
          type = "aws";
        }
        {
          background = "p:background-color";
          foreground = "p:cloud-text-azure";
          properties = {
            source = "cli";
          };
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{ .Name }}";
          type = "az";
        }
        {
          background = "p:background-color";
          foreground = "p:cloud-text-azure";
          properties = {
            source = "pwsh";
          };
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> (PS) {{ .Name }}";
          type = "az";
        }
        {
          background = "p:background-color";
          foreground = "p:cloud-text-gcp";
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{ .Project }}";
          type = "gcp";
        }
        {
          background = "p:background-color";
          foreground = "p:cloud-text-firebase";
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color>������</> {{ .Project }}";
          type = "firebase";
        }
        {
          background = "p:background-color";
          foreground = "p:git-text";
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{ .RepoName }}";
          type = "git";
        }
        {
          background = "p:background-color";
          foreground = "p:timer-text";
          properties = {
            style = "austin";
            threshold = 1;
          };
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{ .FormattedMs }}";
          type = "executiontime";
        }
        {
          background = "p:background-color";
          foreground = "p:symbol-color";
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          powerline_symbol = "";
          template = "{{ if eq .Code 0 }}  {{ end }}";
          type = "status";
        }
        {
          background = "p:error-background";
          foreground = "p:error-text";
          leading_diamond = " ";
          trailing_diamond = "";
          style = "diamond";
          template = "{{ if ne .Code 0 }}  {{ .Code }}{{ if (ne (reason .Code) (toString .Code)) }} - {{ reason .Code }}{{else}}{{ end }} {{ end }}";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "p:background-color";
          foreground = "p:timer-text";
          properties = {
            time_format = "15:04:05";
          };
          style = "powerline";
          powerline_symbol = "";
          template = " <p:symbol-color></> {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          background = "p:background-color";
          foreground = "p:symbol-color";
          properties = {
            folder_separator_icon = "/";
            style = "folder";
          };
          style = "powerline";
          powerline_symbol = "";
          template = " {{ path .Path .Location }} ";
          type = "path";
        }
        {
          background = "p:background-color";
          foreground = "p:git-text";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
          };
          style = "powerline";
          powerline_symbol = "";
          template = "{{ .HEAD }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "p:error-background";
          foreground = "p:backgrond-color";
          style = "diamond";
          leading_diamond = "";
          trailing_diamond = "";
          template = "  ";
          type = "root";
        }
        {
          background = "p:background-color";
          foreground = "p:git-text";
          style = "plain";
          template = "{{ if .Root }}{{ else }}<p:symbol-color> > </>{{ end }}";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
}
