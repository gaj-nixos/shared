{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  #base00 = "#f8f8f8"; base00-dec-b = "#0.968750"; base00-dec-g = "#0.968750"; base00-dec-r = "#0.968750"; base00-hex = "#f8f8f8"; base00-hex-b = "#f8"; base00-hex-bgr = "#f8f8f8"; base00-hex-g = "#f8"; base00-hex-r = "#f8"; base00-rgb-b = "#248"; base00-rgb-g = "#248"; base00-rgb-r = "#248";
  #base01 = "#e8e8e8"; base01-dec-b = "#0.906250"; base01-dec-g = "#0.906250"; base01-dec-r = "#0.906250"; base01-hex = "#e8e8e8"; base01-hex-b = "#e8"; base01-hex-bgr = "#e8e8e8"; base01-hex-g = "#e8"; base01-hex-r = "#e8"; base01-rgb-b = "#232"; base01-rgb-g = "#232"; base01-rgb-r = "#232";
  #base02 = "#d8d8d8"; base02-dec-b = "#0.843750"; base02-dec-g = "#0.843750"; base02-dec-r = "#0.843750"; base02-hex = "#d8d8d8"; base02-hex-b = "#d8"; base02-hex-bgr = "#d8d8d8"; base02-hex-g = "#d8"; base02-hex-r = "#d8"; base02-rgb-b = "#216"; base02-rgb-g = "#216"; base02-rgb-r = "#216";
  #base03 = "#b8b8b8"; base03-dec-b = "#0.718750"; base03-dec-g = "#0.718750"; base03-dec-r = "#0.718750"; base03-hex = "#b8b8b8"; base03-hex-b = "#b8"; base03-hex-bgr = "#b8b8b8"; base03-hex-g = "#b8"; base03-hex-r = "#b8"; base03-rgb-b = "#184"; base03-rgb-g = "#184"; base03-rgb-r = "#184";
  #base04 = "#585858"; base04-dec-b = "#0.343750"; base04-dec-g = "#0.343750"; base04-dec-r = "#0.343750"; base04-hex = "#585858"; base04-hex-b = "#58"; base04-hex-bgr = "#585858"; base04-hex-g = "#58"; base04-hex-r = "#58"; base04-rgb-b = "#88"; base04-rgb-g = "#88"; base04-rgb-r = "#88";
  #base05 = "#383838"; base05-dec-b = "#0.218750"; base05-dec-g = "#0.218750"; base05-dec-r = "#0.218750"; base05-hex = "#383838"; base05-hex-b = "#38"; base05-hex-bgr = "#383838"; base05-hex-g = "#38"; base05-hex-r = "#38"; base05-rgb-b = "#56"; base05-rgb-g = "#56"; base05-rgb-r = "#56";
  #base06 = "#282828"; base06-dec-b = "#0.156250"; base06-dec-g = "#0.156250"; base06-dec-r = "#0.156250"; base06-hex = "#282828"; base06-hex-b = "#28"; base06-hex-bgr = "#282828"; base06-hex-g = "#28"; base06-hex-r = "#28"; base06-rgb-b = "#40"; base06-rgb-g = "#40"; base06-rgb-r = "#40";
  #base07 = "#181818"; base07-dec-b = "#0.093750"; base07-dec-g = "#0.093750"; base07-dec-r = "#0.093750"; base07-hex = "#181818"; base07-hex-b = "#18"; base07-hex-bgr = "#181818"; base07-hex-g = "#18"; base07-hex-r = "#18"; base07-rgb-b = "#24"; base07-rgb-g = "#24"; base07-rgb-r = "#24";
  #base08 = "#ab4642"; base08-dec-b = "#0.257812"; base08-dec-g = "#0.273438"; base08-dec-r = "#0.667969"; base08-hex = "#ab4642"; base08-hex-b = "#42"; base08-hex-bgr = "#4246ab"; base08-hex-g = "#46"; base08-hex-r = "#ab"; base08-rgb-b = "#66"; base08-rgb-g = "#70"; base08-rgb-r = "#171";
  #base09 = "#dc9656"; base09-dec-b = "#0.335938"; base09-dec-g = "#0.585938"; base09-dec-r = "#0.859375"; base09-hex = "#dc9656"; base09-hex-b = "#56"; base09-hex-bgr = "#5696dc"; base09-hex-g = "#96"; base09-hex-r = "#dc"; base09-rgb-b = "#86"; base09-rgb-g = "#150"; base09-rgb-r = "#220";
  #base0A = "#f7ca88"; base0A-dec-b = "#0.531250"; base0A-dec-g = "#0.789062"; base0A-dec-r = "#0.964844"; base0A-hex = "#f7ca88"; base0A-hex-b = "#88"; base0A-hex-bgr = "#88caf7"; base0A-hex-g = "#ca"; base0A-hex-r = "#f7"; base0A-rgb-b = "#136"; base0A-rgb-g = "#202"; base0A-rgb-r = "#247";
  #base0B = "#a1b56c"; base0B-dec-b = "#0.421875"; base0B-dec-g = "#0.707031"; base0B-dec-r = "#0.628906"; base0B-hex = "#a1b56c"; base0B-hex-b = "#6c"; base0B-hex-bgr = "#6cb5a1"; base0B-hex-g = "#b5"; base0B-hex-r = "#a1"; base0B-rgb-b = "#108"; base0B-rgb-g = "#181"; base0B-rgb-r = "#161";
  #base0C = "#86c1b9"; base0C-dec-b = "#0.722656"; base0C-dec-g = "#0.753906"; base0C-dec-r = "#0.523438"; base0C-hex = "#86c1b9"; base0C-hex-b = "#b9"; base0C-hex-bgr = "#b9c186"; base0C-hex-g = "#c1"; base0C-hex-r = "#86"; base0C-rgb-b = "#185"; base0C-rgb-g = "#193"; base0C-rgb-r = "#134";
  #base0D = "#7cafc2"; base0D-dec-b = "#0.757812"; base0D-dec-g = "#0.683594"; base0D-dec-r = "#0.484375"; base0D-hex = "#7cafc2"; base0D-hex-b = "#c2"; base0D-hex-bgr = "#c2af7c"; base0D-hex-g = "#af"; base0D-hex-r = "#7c"; base0D-rgb-b = "#194"; base0D-rgb-g = "#175"; base0D-rgb-r = "#124";
  #base0E = "#ba8baf"; base0E-dec-b = "#0.683594"; base0E-dec-g = "#0.542969"; base0E-dec-r = "#0.726562"; base0E-hex = "#ba8baf"; base0E-hex-b = "#af"; base0E-hex-bgr = "#af8bba"; base0E-hex-g = "#8b"; base0E-hex-r = "#ba"; base0E-rgb-b = "#175"; base0E-rgb-g = "#139"; base0E-rgb-r = "#186";
  #base0F = "#a16946"; base0F-dec-b = "#0.273438"; base0F-dec-g = "#0.410156"; base0F-dec-r = "#0.628906"; base0F-hex = "#a16946"; base0F-hex-b = "#46"; base0F-hex-bgr = "#4669a1"; base0F-hex-g = "#69"; base0F-hex-r = "#a1"; base0F-rgb-b = "#70"; base0F-rgb-g = "#105"; base0F-rgb-r = "#161";
  #base10 = "#f8f8f8"; base10-dec-b = "#0.968750"; base10-dec-g = "#0.968750"; base10-dec-r = "#0.968750"; base10-hex = "#f8f8f8"; base10-hex-b = "#f8"; base10-hex-bgr = "#f8f8f8"; base10-hex-g = "#f8"; base10-hex-r = "#f8"; base10-rgb-b = "#248"; base10-rgb-g = "#248"; base10-rgb-r = "#248";
  #base11 = "#f8f8f8"; base11-dec-b = "#0.968750"; base11-dec-g = "#0.968750"; base11-dec-r = "#0.968750"; base11-hex = "#f8f8f8"; base11-hex-b = "#f8"; base11-hex-bgr = "#f8f8f8"; base11-hex-g = "#f8"; base11-hex-r = "#f8"; base11-rgb-b = "#248"; base11-rgb-g = "#248"; base11-rgb-r = "#248";
  #base12 = "#ab4642"; base12-dec-b = "#0.257812"; base12-dec-g = "#0.273438"; base12-dec-r = "#0.667969"; base12-hex = "#ab4642"; base12-hex-b = "#42"; base12-hex-bgr = "#4246ab"; base12-hex-g = "#46"; base12-hex-r = "#ab"; base12-rgb-b = "#66"; base12-rgb-g = "#70"; base12-rgb-r = "#171";
  #base13 = "#dc9656"; base13-dec-b = "#0.335938"; base13-dec-g = "#0.585938"; base13-dec-r = "#0.859375"; base13-hex = "#dc9656"; base13-hex-b = "#56"; base13-hex-bgr = "#5696dc"; base13-hex-g = "#96"; base13-hex-r = "#dc"; base13-rgb-b = "#86"; base13-rgb-g = "#150"; base13-rgb-r = "#220";
  #base14 = "#a1b56c"; base14-dec-b = "#0.421875"; base14-dec-g = "#0.707031"; base14-dec-r = "#0.628906"; base14-hex = "#a1b56c"; base14-hex-b = "#6c"; base14-hex-bgr = "#6cb5a1"; base14-hex-g = "#b5"; base14-hex-r = "#a1"; base14-rgb-b = "#108"; base14-rgb-g = "#181"; base14-rgb-r = "#161";
  #base15 = "#86c1b9"; base15-dec-b = "#0.722656"; base15-dec-g = "#0.753906"; base15-dec-r = "#0.523438"; base15-hex = "#86c1b9"; base15-hex-b = "#b9"; base15-hex-bgr = "#b9c186"; base15-hex-g = "#c1"; base15-hex-r = "#86"; base15-rgb-b = "#185"; base15-rgb-g = "#193"; base15-rgb-r = "#134";
  #base16 = "#7cafc2"; base16-dec-b = "#0.757812"; base16-dec-g = "#0.683594"; base16-dec-r = "#0.484375"; base16-hex = "#7cafc2"; base16-hex-b = "#c2"; base16-hex-bgr = "#c2af7c"; base16-hex-g = "#af"; base16-hex-r = "#7c"; base16-rgb-b = "#194"; base16-rgb-g = "#175"; base16-rgb-r = "#124";
  #base17 = "#ba8baf"; base17-dec-b = "#0.683594"; base17-dec-g = "#0.542969"; base17-dec-r = "#0.726562"; base17-hex = "#ba8baf"; base17-hex-b = "#af"; base17-hex-bgr = "#af8bba"; base17-hex-g = "#8b"; base17-hex-r = "#ba"; base17-rgb-b = "#175"; base17-rgb-g = "#139"; base17-rgb-r = "#186";

  #blue = "#7cafc2"; bright-blue = "#7cafc2"; bright-cyan = "#86c1b9"; bright-green = "#a1b56c"; bright-magenta = "#ba8baf"; bright-orange = "#dc9656"; bright-red = "#ab4642"; brown = "#a16946"; cyan = "#86c1b9"; green = "#a1b56c"; magenta = "#ba8baf"; orange = "#dc9656"; red = "#ab4642"; yellow = "#f7ca88";

  #toList = [ "#f8f8f8" "#e8e8e8" "#d8d8d8" "#b8b8b8" "#585858" "#383838" "#282828" "#181818" "#ab4642" "#dc9656" "#f7ca88" "#a1b56c" "#86c1b9" "#7cafc2" "#ba8baf" "#a16946" "#f8f8f8" "#f8f8f8" "#ab4642" "#dc9656" "#a1b56c" "#86c1b9" "#7cafc2" "#ba8baf" ];

  defaultPalette = {
    red = "#ef5350";
    pink = "#ffbebc";
    lightPink2 = "#ffafd2";
    lightPink = "#ff70a6";
    magenta = "#ee79d1";
    purple = "#bc93ff";
    blue = "#00c7fc";
    green = "#a9ffb4";
    lightGreen = "#94ffa2";
    foreground = "#FEF5ED";
    white = "#ffffff";
  };

  palette =
    if (config.lib ? stylix) then
      {
        red = red;
        pink = orange;
        lightPink2 = orange;
        lightPink = orange;
        magenta = yellow;
        purple = purple;
        blue = blue;
        green = green;
        lightGreen = cyan;
        foreground = white;
        white = brightWhite;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";
  inherit palette;
  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:pink";
          leading_diamond = "<p:lightPink>  </>";
          properties = {
            display_host = true;
          };
          style = "diamond";
          template = "{{ .UserName }} <p:white>on</>";
          type = "session";
        }
        {
          foreground = "p:purple";
          properties = {
            time_format = "Monday <p:white>at</> 3:04 PM";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          foreground = "p:magenta";
          properties = {
            branch_icon = " ";
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
            fetch_worktree_count = true;
          };
          style = "diamond";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "p:green";
          style = "plain";
          type = "text";
        }
        {
          foreground = "p:green";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "diamond";
          template = " {{ .FormattedMs }}s <p:white></>";
          type = "executiontime";
        }
        {
          properties = {
            root_icon = " ";
          };
          style = "diamond";
          template = "  ";
          type = "root";
        }
        {
          foreground = "p:lightGreen";
          style = "diamond";
          template = " <p:white>MEM:</> {{ round .PhysicalPercentUsed .Precision }}% ({{ (div ((sub .PhysicalTotalMemory .PhysicalFreeMemory)|float64) 1073741824.0) }}/{{ (div .PhysicalTotalMemory 1073741824.0) }}GB)";
          type = "sysinfo";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:lightPink2";
          leading_diamond = "<p:blue>  </><p:lightPink2>{</>";
          properties = {
            folder_icon = "";
            folder_separator_icon = "  ";
            home_icon = "home";
            style = "agnoster_full";
          };
          style = "diamond";
          template = "  {{ .Path }} ";
          trailing_diamond = "<p:lightPink2>}</>";
          type = "path";
        }
        {
          foreground = "p:green";
          foreground_templates = [
            "{{ if gt .Code 0 }}p:red{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "  ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Folder }}";
  transient_prompt = {
    background = "transparent";
    foreground = "p:foreground";
    template = " ";
  };
  version = 3;
}
