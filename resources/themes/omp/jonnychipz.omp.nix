{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#4707a8";
          foreground = "#000000";
          leading_diamond = "";
          style = "diamond";
          type = "text";
        }
        {
          background = "#4707a8";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .UserName }}@{{ .HostName }} ";
          trailing_diamond = "";
          type = "session";
        }
        {
          background = "#1a1af5";
          foreground = "#ffffff";
          trailing_diamond = "";
          style = "diamond";
          template = "  {{ .Name }} ";
          type = "az";
        }
        {
          background = "#4707a8";
          background_templates = [
            "{{ if gt .Code 0 }}#f1184c{{ end }}"
          ];
          foreground = "#ffffff";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = "  ";
          leading_diamond = "<transparent></>";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "#ffffff";
          foreground = "#000000";
          leading_diamond = "";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
          };
          style = "diamond";
          template = " {{ if .WSL }} on {{ end }}{{ .Icon }}<#000000> </>";
          type = "os";
        }
        {
          background = "#000000";
          foreground = "#ffffff";
          style = "diamond";
          template = " <#ffffff> </>";
          type = "root";
        }
        {
          background = "#ffffff";
          foreground = "#000000";
          properties = {
            folder_icon = "<#000000> </>";
            folder_separator_icon = "<#000000> </>";
            home_icon = " ";
            style = "agnoster_short";
          };
          style = "diamond";
          template = "<#000000>  </>{{ .Path }} ";
          type = "path";
        }
        {
          background = "#ffffff";
          foreground = "#000000";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#ffea00{{ end }}"
            "{{ if gt .Ahead 0 }}#2EC4B6{{ end }}"
            "{{ if gt .Behind 0 }}#8A4FFF{{ end }}"
          ];
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "diamond";
          template = "<#000000> </>{{ .UpstreamIcon }}{{ .HEAD }}{{ if .Staging.Changed }}<#2FDA4E>  {{ .Staging.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}<#E84855>  {{ .Working.String }}</>{{ end }} ";
          type = "git";
        }
        {
          background = "#ffffff";
          foreground = "#000000";
          style = "powerline";
          template = " {{ .WorkspaceName }} ";
          type = "terraform";
        }
        {
          background = "#ffffff";
          foreground = "#000000";
          style = "diamond";
          trailing_diamond = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#1a1af5";
          style = "plain";
          template = " ❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}}root :: {{end}}{{.Shell}} :: {{.Folder}}";
  final_space = true;
  version = 3;
}
