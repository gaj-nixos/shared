{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#ffdd86";
          style = "plain";
          template = "{{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          foreground = "#42a9ff";
          style = "plain";
          properties = {
            style = "full";
          };
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          properties = {
            branch_icon = "";
            fetch_status = true;
          };
          style = "plain";
          template = "git:{{ if or (.Working.Changed) (.Staging.Changed) (gt .StashCount 0) }}<#ffdd86>{{ .HEAD }}</>{{ else }}{{ .HEAD }}{{ end }}{{ if .Staging.Changed }} <#98c379>{{ .Staging.String }}</>{{ end }}{{ if .Working.Changed }} <#d16971>{{ .Working.String }}</>{{ end }}";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#ffdd86";
          foreground_templates = [
            "{{ if gt .Code 0 }}#42a9ff{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "> ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
