{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;

  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  #base0X	    Color	    	ANSI        Terminal	                  Text Editor
  #base00 = "#1e1e2e"  	0	        Black (Background)	      Default Background
  #base01 = "#181825"  	18	      (Darkest Gray)	          Lighter Background (Used for status bars)
  #base02 = "#313244"  	19	      (Dark Gray)	              Selection Background
  #base03 = "#45475a"  	8	        Bright Black (Gray)	      Comments, Invisibles, Line Highlighting
  #base04 = "#585b70"  	20	      (Light Gray)	            Dark Foreground (Used for status bars)
  #base05 = "#cdd6f4"  	21	      Foreground	              Default Foreground, Caret, Delimiters, Operators
  #base06 = "#f5e0dc"  	7	        White	                    Light Foreground
  #base07 = "#b4befe"  	15	      Bright White	            The Lightest Foreground
  #base08 = "#f38ba8"  	1 and 9	  Red and Bright Red	      Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
  #base09 = "#fab387"  	16	      (Orange)	                Integers, Boolean, Constants, XML Attributes, Markup Link Url
  #base0A = "#f9e2af"  	3 and 11	Yellow and Bright Yellow	Classes, Markup Bold, Search Text Background
  #base0B = "#a6e3a1"  	2 and 10	Green and Bright Green	  Strings, Inherited Class, Markup Code, Diff Inserted
  #base0C = "#94e2d5"  	6 and 14	Cyan and Bright Cyan	    Support, Regular Expressions, Escape Characters, Markup Quotes
  #base0D = "#89b4fa"  	4 and 12	Blue and Bright Blue	    Functions, Methods, Attribute IDs, Headings
  #base0E = "#cba6f7"  	5 and 13	Purple and Bright Purple	Keywords, Storage, Selector, Markup Italic, Diff Changed
  #base0F = "#f2cdcd"  	17	      (Dark Red or Brown)       Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>

in
rec
#if (config.lib ? stylix) then
{
  inherit base00;
  inherit base01;
  inherit base02;
  inherit base03;
  inherit base04;
  inherit base05;
  inherit base06;
  inherit base07;
  inherit base08;
  inherit base09;
  inherit base0A;
  inherit base0B;
  inherit base0C;
  inherit base0D;
  inherit base0E;
  inherit base0F;

  background = black;
  foreground = white;

  osBg = blue;
  osFg = darkGray;
  sessionBg = lightGray;
  sessionRootText = red;
  sessionSSHText = orange;
  pathBg = background;
  projectBg = lightGray;

  gitBg = background;
  gitChangedBg = yellow;
  gitUpToDateBg = background;
  gitAheadBg = purple;
  gitText = background;

  promptErrorText = red;
  statusBg = blue;
  statusErrorBg = red;
  statusFg = darkGray;
  execTimeBg = lightGray;
  timeTextColor = blue;

  memBg1 = lightGray;
  memBg2 = gray;
  memTextColor = blue;
  memTextColor2 = cyan;
  secPromptFg = lightGray;
}
