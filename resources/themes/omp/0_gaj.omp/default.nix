{ config, ... }:

let
  palette = import ./palette.nix { inherit config; };

  prompt = "";
  long_prompt = "";
  root = "⚡";

  style = "plain";

  textSeg = foreground: template: {
    type = "text";
    inherit foreground style template;
  };
  osSeg = foreground: {
    type = "os";
    style = "diamond";
    background = "p:osBg";
    foreground = "p:osFg";
    properties = {
      alpine = "";
      arch = "";
      centos = "";
      debian = "";
      elementary = "";
      fedora = "";
      gentoo = "";
      linux = "";
      macos = "";
      manjaro = "";
      mint = "";
      opensuse = "";
      raspbian = "";
      ubuntu = "";
      windows = "";
    };
    template = "{{.Icon}} ";
    leading_diamond = "";
    properties.always_enabled = true;
  };

  sessionSeg = foreground: {
    type = "session";
    style = "diamond";
    background = "p:sessionBg";
    trailing_diamond = "";
    foreground = "p:background";
    template = " {{ if .Root }}<p:sessionRootText>${root}</> {{ end }}{{ if .SSHSession }}<p:sessionSSHText> {{ .UserName }}@{{ .HostName }}</>{{ else }}{{ .UserName }}{{ end }}";
  };

  osIconAndSessionLeftBlock = foreground: {
    alignment = "left";
    type = "prompt";
    segments = [
      (textSeg "p:white" "╭")
      (osSeg "p:white")
      (sessionSeg "p:white")
    ];
  };

  pathSeg = foreground: {
    type = "path";
    background = "p:background";
    #powerline_symbol = "";
    properties = {
      #folder_icon = "";
      #folder_separator_icon = "  ";
      #home_icon = "";
      folder_separator_icon = "/";
      style = "full";
      cycle = [
        "p:base0E"
        "p:base0D"
        "p:base0C"
        "p:base0B"
        "p:base0A"
        "p:base09"
        "p:base08"
      ];
      mapped_locations_enabled = true;
      #mapped_locations = {
      #  "/root" = "👑";
      #  "/home" = "🏠";
      #  "/etc" = "🔧";
      #  "/srv" = "🖥️";
      #  "/run" = "🏃";
      #  "/nix" = "❄️";
      #  "/boot" = "👢";
      #  "/dev" = "🔌";
      #  "/mnt" = "📂";
      #  "/media" = "📀";
      #  "/tmp" = "🗑️";
      #};
    };
    inherit foreground style;
    template = "─  {{ .Path }} ";
  };

  cnd = v: "{{ if ${v} }} {{ ${v} }}{{ end }}";

  projectSeg1 = foreground: {
    type = "project";
    style = "diamond";
    background = "p:osBg";
    foreground = "p:osFg";
    leading_diamond = "";
    template = " ";
  };

  projectSeg2 = foreground: {
    type = "project";
    style = "diamond";
    background = "p:projectBg";
    inherit foreground;
    trailing_diamond = "";
    template = "{{ if .Error }} {{ .Error }}{{ else }}${cnd ".Target"}${cnd ".Version"}${cnd ".Name"}{{ end }}";
  };

  upstream = "{{ url .UpstreamIcon .UpstreamURL }}";
  head = "{{ .HEAD }}";
  branchStatus = "{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}";
  working = "{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}";
  separator = "{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}";
  staging = "{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}";
  stash = "{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }}";

  gitSeg1 = foreground: {
    type = "git";
    properties = {
      branch_icon = " ";
      cherry_pick_icon = " ";
      commit_icon = " ";
      merge_icon = " ";
      no_commits_icon = " ";
      rebase_icon = " ";
      revert_icon = " ";
      tag_icon = " ";
      branch_ahead_icon = "↑";
      branch_behind_icon = "↓";
      branch_gone = "≢";
      branch_identical_icon = "≡";
      fetch_stash_count = true;
      fetch_status = true;
      fetch_upstream_icon = true;
      fetch_worktree_count = true;
      truncate_symbol = "…";
    };
    style = "diamond";
    background = "p:osBg";
    foreground = "p:osFg";
    leading_diamond = "";
    template = "${upstream} ";
  };

  gitSeg2 = foreground: {
    type = "git";
    style = "diamond";
    background = "p:projectBg";
    trailing_diamond = "";
    background_templates = [
      "{{ if or (.Working.Changed) (.Staging.Changed) }}p:gitChangedBg{{ end }}"
      "{{ if and (gt .Ahead 0) (gt .Behind 0) }}p:gitUpToDateBg{{ end }}"
      "{{ if or (gt .Ahead 0) (gt .Behind 0) }}p:gitAheadBg{{ end }}"
    ];
    foreground_templates = [
      "{{ if or (.Working.Changed) (.Staging.Changed) }}p:gitText{{ end }}"
      "{{ if and (gt .Ahead 0) (gt .Behind 0) }}p:gitText{{ end }}"
      "{{ if or (gt .Ahead 0) (gt .Behind 0) }}p:gitText{{ end }}"
    ];
    properties = {
      branch_icon = " ";
      cherry_pick_icon = " ";
      commit_icon = " ";
      merge_icon = " ";
      no_commits_icon = " ";
      rebase_icon = " ";
      revert_icon = " ";
      tag_icon = " ";
      branch_ahead_icon = "↑";
      branch_behind_icon = "↓";
      branch_gone = "≢";
      branch_identical_icon = "≡";
      fetch_stash_count = true;
      fetch_status = true;
      fetch_upstream_icon = true;
      fetch_worktree_count = true;
      truncate_symbol = "…";
    };
    inherit foreground;
    template = " ${head}${branchStatus}${working}${separator}${staging}${stash} ";
  };

  timeSeg1 = iconColor: textColor: {
    type = "time";
    style = "diamond";
    background = "p:osBg";
    foreground = "p:osFg";
    properties = {
      time_format = "15:04:05";
    };
    leading_diamond = "";
    template = " ";
  };

  timeSeg2 = iconColor: textColor: {
    type = "time";
    style = "diamond";
    background = "p:projectBg";
    foreground = textColor;
    properties = {
      time_format = "15:04:05";
    };
    trailing_diamond = "";
    template = " {{ .CurrentDate | date .Format }}";
  };

  memSeg1 = iconColor: textColor: {
    type = "sysinfo";
    style = "diamond";
    background = "p:osBg";
    foreground = "p:osFg";
    leading_diamond = "";
    template = " ";
  };

  memSeg2 = iconColor: textColor: {
    type = "sysinfo";
    style = "diamond";
    background = "p:memBg1";
    foreground = "p:memTextColor";
    template = " {{round .PhysicalPercentUsed .Precision }}% ";
  };

  memSeg3 = iconColor: textColor: {
    type = "sysinfo";
    style = "diamond";
    background = "p:memBg2";
    foreground = "p:memTextColor2";
    trailing_diamond = "";
    template = " {{round .SwapPercentUsed .Precision }}%";
  };

  promptSeg = foreground: template: {
    type = "text";
    background = "p:background";
    foreground_templates = [
      "{{ if gt .Code 0 }}p:promptErrorText{{ end }}"
    ];
    inherit style foreground template;
  };

  execTimeSeg = iconColor: textColor: {
    type = "executiontime";
    foreground = textColor;
    properties = {
      style = "roundrock";
      threshold = 500;
    };
    template = "<${iconColor}></> {{ .FormattedMs }}";
    inherit style;
  };

  errorSeg = {
    type = "status";
    foreground = "p:green";
    foreground_templates = [
      "{{ if gt .Code 0 }}p:red{{ end }}"
    ];
    inherit style;
    #template = "  {{ if gt .Code 0 }} {{ .Code }}{{ else }}{{ end }} ";
    template = "{{ if gt .Code 0 }} {{ reason .Code }}{{ end }}";
  };

  langSeq = type: foreground: template: {
    inherit
      style
      type
      foreground
      template
      ;
    background = "p:background";
    properties = {
      #fetch_version = false;
    };
  };

  statusAndExecTimeRightBlock = foreground: {
    alignment = "right";
    type = "prompt";
    filler = "─";
    segments = [
      {
        type = "status";
        style = "diamond";
        background = "p:statusBg";
        background_templates = [
          "{{ if gt .Code 0 }}p:statusErrorBg{{ end }}"
        ];
        foreground = "p:statusFg";
        foreground_templates = [
          "{{ if gt .Code 0 }}transparent{{ end }}"
        ];
        leading_diamond = "";
        properties.always_enabled = true;
        template = "{{ if gt .Code 0 }} {{ reason .Code }}{{ else }}{{ end }} ";
      }
      {
        type = "executiontime";
        style = "diamond";
        background = "p:execTimeBg";
        foreground = "p:background";
        properties = {
          style = "roundrock";
          threshold = 0;
        };
        template = "  {{ .FormattedMs }}";
        trailing_diamond = "";
      }
      (textSeg "p:white" "─╮")
    ];
  };
in
{
  inherit palette;

  #console_title_template = "{{ .Folder }}";
  console_title_template = "{{if .Root}} ${root} {{end}}{{.UserName}} ➔ 📁{{.Folder}}";
  final_space = true;

  blocks = [
    #{
    #  alignment = "left";
    #  type = "prompt";
    #  segments = [
    #    {
    #      type = "text";
    #      inherit style;
    #      template = "╭";
    #    }
    #  ];
    #}
    #{
    #  alignment = "right";
    #  type = "prompt";
    #  filler = "{{ if .Overflow }}{{ else }}─{{ end }}";
    #  overflow = "hide";
    #  segments = [
    #    {
    #      type = "text";
    #      inherit style;
    #      template = "╮";
    #    }
    #  ];
    #}    }

    # line 1

    (osIconAndSessionLeftBlock "p:white")

    {
      type = "prompt";
      alignment = "left";
      segments = [
        (pathSeg "p:white")
      ];
    }

    (statusAndExecTimeRightBlock "p:white")

    # line 2

    {
      type = "prompt";
      alignment = "left";
      newline = true;
      segments = [
        (textSeg "p:white" "│")
        (projectSeg1 "p:white")
        (projectSeg2 "p:white")
        (gitSeg1 "p:white")
        (gitSeg2 "p:white")
      ];
    }

    {
      type = "prompt";
      alignment = "right";
      overflow = "hide";
      segments = [
        #(textSeg "p:blue" " ❮ ")
        (timeSeg1 "p:white" "p:timeTextColor")
        (timeSeg2 "p:white" "p:timeTextColor")
        (memSeg1 "p:white" "p:memTextColor")
        (memSeg2 "p:white" "p:memTextColor")
        (memSeg3 "p:white" "p:memTextColor")
        (textSeg "p:white" "│")
      ];
    }

    # line 3

    {
      type = "prompt";
      alignment = "left";
      newline = true;
      segments = [
        (textSeg "p:white" "╰─")
        (promptSeg "p:white" long_prompt)
      ];
    }

    {
      alignment = "right";
      type = "rprompt";
      overflow = "hide";
      segments = [
        (langSeq "python" "p:white"
          "{{ if .Error }} {{ .Error }}{{ else }}{{ if .Venv }} {{ .Venv }}{{ end }} {{ .Full }}{{ end }}─"
        )
        #(langSeq "go" "p:white" "{{ if .Error }} {{ .Error }}{{ else }} {{ .Full }}{{ end }}─")
        (langSeq "node" "p:white"
          "{{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}─"
        )
        #(langSeq "ruby" "p:white" "{{ if .Error }} {{ .Error }}{{ else }} {{ .Full }}{{ end }}─")
        #(langSeq "java" "p:white" "{{ if .Error }} {{ .Error }}{{ else }} {{ .Full }}{{ end }}─")
        (langSeq "dotnet" "p:white"
          "{{ if .Unsupported }}{{ else }} {{ .Full }} {{ .BuildMetadata }}{{ end }}─"
        )
        (langSeq "haskell" "p:white" "{{ if .Error }} {{ .Error }}{{ else }} {{ .Full }}{{ end }}─")
        #(execTimeSeg "p:white" "p:blue")
        #errorSeg
        (textSeg "p:white" "─╯")
      ];
    }
  ];

  tooltips = [
  ];

  transient_prompt = {
    background = "transparent";
    foreground = "p:foreground";
    template = "{{if .Root}}${root} {{end}}${prompt} ";
    terminal_background = "p:background";
    enable_cursor_positioning = true;
    newline = true;
  };

  secondary_prompt = {
    foreground = "p:secPromptFg";
    template = "{{if .Root}}${root} {{end}}${prompt} ";
  };

  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  version = 3;
}
