{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#F1FA8C";
          properties = {
            style = "folder";
          };
          style = "diamond";
          template = "<#42E66C> </><b>{{ .Path }}</b>";
          type = "path";
        }
        {
          foreground = "#43CCEA";
          properties = {
            branch_icon = "";
          };
          style = "diamond";
          template = "<#ffffff> ⚡ </><b>{{ .HEAD }}</b>";
          type = "git";
        }
        {
          foreground = "#ff0000";
          style = "diamond";
          template = "<#ff0000> ◉</>";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#FFFFFF";
          style = "plain";
          template = ">";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
