{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#FFFFFF";
          style = "plain";
          template = "<#0377C8># </><#0377C8>{{ .UserName }}</> <#ffffff>in</> <#4A9207>{{ .HostName }}</> ";
          type = "session";
        }
        {
          foreground = "#0973C0";
          properties = {
            folder_icon = "";
            folder_separator_icon = "  ";
            style = "full";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#B8B80A";
          style = "plain";
          template = "<#ffffff>on git:</>{{ .HEAD }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "#ffffff";
          style = "plain";
          template = "[{{ .CurrentDate | date .Format }}]";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#CECE04";
          style = "plain";
          template = " ";
          type = "root";
        }
        {
          foreground = "#100e23";
          style = "plain";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          type = "python";
        }
        {
          foreground = "#CB4B16";
          style = "plain";
          template = " {{ reason .Code }} ";
          type = "status";
        }
        {
          foreground = "#CC4B16";
          style = "plain";
          template = "➜ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
