{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
    blue = "#61AFEF";
    purple = "#C678DD";
    green = "#95ffa4";
    red = "#FF6471";
    white = "#ffffff";
    darkGreen = "#193549";
  };

  palette =
    if (config.lib ? stylix) then
      {
        blue = cyan;
        purple = purple;
        green = green;
        red = red;
        white = white;
        darkGreen = blue;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  inherit palette;
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "p:blue";
          foreground = "p:white";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .UserName }}@{{ .HostName }} ";
          trailing_diamond = "";
          type = "session";
        }
        {
          background = "p:purple";
          foreground = "p:white";
          powerline_symbol = "";
          properties = {
            style = "full";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "p:green";
          foreground = "p:darkGreen";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .HEAD }} ";
          type = "git";
        }
        {
          background = "p:red";
          foreground = "p:white";
          leading_diamond = "<transparent,background></>";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          trailing_diamond = "";
          type = "python";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
