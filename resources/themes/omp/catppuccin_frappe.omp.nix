{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  palette = {
    os = "#ACB0BE";
    closer = "p:os";
    pink = "#F4B8E4";
    lavender = "#BABBF1";
    blue = "#8CAAEE";
  };
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "p:os";
          style = "plain";
          template = "{{.Icon}} ";
          type = "os";
        }
        {
          foreground = "p:blue";
          style = "plain";
          template = "{{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          foreground = "p:pink";
          properties = {
            folder_icon = "....";
            home_icon = "~";
            style = "agnoster_short";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          foreground = "p:lavender";
          properties = {
            branch_icon = " ";
            cherry_pick_icon = " ";
            commit_icon = " ";
            fetch_status = false;
            fetch_upstream_icon = false;
            merge_icon = " ";
            no_commits_icon = " ";
            rebase_icon = " ";
            revert_icon = " ";
            tag_icon = " ";
          };
          template = "{{ .HEAD }} ";
          style = "plain";
          type = "git";
        }
        {
          style = "plain";
          foreground = "p:closer";
          template = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
