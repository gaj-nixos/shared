{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "lightBlue";
          foreground_templates = [
            "{{ if .Root }}lightRed{{ end }}"
          ];
          properties = {
            display_host = true;
          };
          style = "plain";
          template = "<{{ if .Root }}lightBlue{{ else }}green{{ end }}>┌──(</>{{ .UserName }}{{ if .Root }}💀{{ else }}㉿{{ end }}{{ .HostName }}<{{ if .Root }}lightBlue{{ else }}green{{ end }}>)</>";
          type = "session";
        }
        {
          foreground = "yellow";
          properties = {
            fetch_version = false;
            fetch_virtual_env = true;
          };
          style = "plain";
          template = "<{{ if .Root }}lightBlue{{ else }}green{{ end }}>-[</> {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }}{{ end }}{{ .Full }}{{ end }}<{{ if .Root }}lightBlue{{ else }}green{{ end }}>]</>";
          type = "python";
        }
        {
          foreground = "lightWhite";
          properties = {
            folder_separator_icon = "<#c0c0c0>/</>";
            style = "full";
          };
          style = "plain";
          template = "<{{ if .Root }}lightBlue{{ else }}green{{ end }}>-[</>{{ .Path }}<{{ if .Root }}lightBlue{{ else }}green{{ end }}>]</>";
          type = "path";
        }
        {
          foreground = "white";
          style = "plain";
          template = "<{{ if .Root }}lightBlue{{ else }}green{{ end }}>-[</>{{ .HEAD }}<{{ if .Root }}lightBlue{{ else }}green{{ end }}>]</>";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "white";
          properties = {
            always_enabled = true;
            style = "round";
          };
          style = "plain";
          template = " {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          foreground = "green";
          foreground_templates = [
            "{{ if gt .Code 0 }}red{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = " {{ if gt .Code 0 }}{{else}}{{ end }} ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "lightBlue";
          style = "plain";
          template = "<{{ if .Root }}lightBlue{{ else }}green{{ end }}>└─</>{{ if .Root }}<lightRed>#</>{{ else }}\${{ end }} ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
