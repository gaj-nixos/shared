{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  console_title_template = "{{ if .Root }}root @ {{ end }}{{ .Shell }} in {{ .Folder }}";
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#575656";
          foreground = "#D6DEEB";
          leading_diamond = "";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
          };
          style = "diamond";
          template = " {{ if .WSL }} on {{ end }}{{ .Icon }} ";
          type = "os";
        }
        {
          background = "#00C7FC";
          foreground = "#011627";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ .Name }} ";
          type = "shell";
        }
        {
          background = "#EF541C";
          foreground = "#D6DEEB";
          powerline_symbol = "";
          style = "powerline";
          template = "  admin ";
          type = "root";
        }
        {
          type = "cmake";
          style = "powerline";
          powerline_symbol = "";
          foreground = "#E8EAEE";
          background = "#1E9748";
          template = "   cmake {{ .Full }} ";
        }
        {
          type = "python";
          style = "powerline";
          powerline_symbol = "";
          properties = {
            display_mode = "context";
          };
          foreground = "#011627";
          background = "#FFDE57";
          template = "  {{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }} ";
        }
        {
          type = "go";
          style = "powerline";
          powerline_symbol = "";
          foreground = "#ffffff";
          background = "#7FD5EA";
          template = " ‭ {{ .Full }} ";
        }
        {
          type = "rust";
          style = "powerline";
          powerline_symbol = "";
          foreground = "#193549";
          background = "#99908A";
          template = "  {{ .Full }} ";
        }
        {
          background = "#1BD4CD";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#16B1AC{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#16B1AC{{ end }}"
            "{{ if gt .Ahead 0 }}#B787D7{{ end }}"
            "{{ if gt .Behind 0 }}#B787D7{{ end }}"
          ];
          foreground = "#011627";
          powerline_symbol = "";
          properties = {
            branch_icon = " ";
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
            fetch_worktree_count = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}<#CAEBE1>  {{ .Staging.String }}</>{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#03DED3";
          background_templates = [
            "{{ if gt .Code 0 }}#E44141{{ end }}"
          ];
          foreground = "#414141";
          foreground_templates = [
            "{{ if gt .Code 0 }}#D6DEEB{{ end }}"
          ];
          leading_diamond = "";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = " {{ if gt .Code 0 }}{{ else }}{{ end }} ";
          type = "status";
        }
        {
          background = "#575656";
          foreground = "#D6DEEB";
          properties = {
            style = "roundrock";
            threshold = 0;
          };
          style = "diamond";
          template = "  {{ .FormattedMs }} ";
          trailing_diamond = "";
          type = "executiontime";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#D6DEEB";
          style = "plain";
          template = "╭─";
          type = "text";
        }
        {
          foreground = "#F2D3B6";
          properties = {
            time_format = "<#D6DEEB> 15:04:05</> <#79DFE1>|</>  2 Jan, Monday";
          };
          style = "plain";
          template = "{{ .CurrentDate | date .Format }} <#79DFE1>|</>";
          type = "time";
        }
        {
          foreground = "#B6D6F2";
          leading_diamond = "<#00C7FC>  </><#B6D6F2> in </>";
          properties = {
            folder_icon = "  ";
            folder_separator_icon = "  ";
            home_icon = " ";
            style = "agnoster_short";
            max_depth = 3;
          };
          style = "diamond";
          template = " {{ .Path }} ";
          type = "path";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#D6DEEB";
          style = "plain";
          template = "╰─";
          type = "text";
        }
        {
          foreground = "#D6DEEB";
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  osc99 = true;
  transient_prompt = {
    background = "transparent";
    foreground = "#FEF5ED";
    template = " ";
  };
  secondary_prompt = {
    background = "transparent";
    foreground = "#D6DEEB";
    template = "╰─❯ ";
  };
  version = 3;
}
