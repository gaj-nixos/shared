{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#1865f5";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = " ";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = " ";
            windows = " ";
            wsl = "";
          };
          style = "diamond";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}}";
          trailing_diamond = "<transparent,></>";
          type = "os";
        }
        {
          background = "#00c7fc";
          foreground = "#000000";
          style = "diamond";
          leading_diamond = "<transparent,#00c7fc></>";
          properties = {
            template = "  CPU: {{ round .PhysicalPercentUsed .Precision }}% | ";
          };
          type = "sysinfo";
        }
        {
          background = "#00c7fc";
          foreground = "#000000";
          style = "diamond";
          template = "RAM: {{ (div ((sub .PhysicalTotalMemory .PhysicalFreeMemory)|float64) 1073741824.0) }}/{{ (div .PhysicalTotalMemory 1073741824.0) }}GB  ";
          trailing_diamond = "<transparent,#00c7fc></>";
          type = "sysinfo";
        }
        {
          background = "#2343e2";
          foreground = "#ffffff";
          leading_diamond = "";
          properties = {
            style = "roundrock";
            threshold = 0;
          };
          style = "diamond";
          template = " {{ .FormattedMs }} ";
          trailing_diamond = "";
          type = "executiontime";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#ee79d1";
          foreground = "#000000";
          leading_diamond = "";
          properties = {
            branch_icon = " ";
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
            fetch_worktree_count = true;
          };
          style = "diamond";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          trailing_diamond = "";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#91f2ff";
          style = "plain";
          template = "╭─";
          type = "text";
        }
        {
          background = "#91f2ff";
          foreground = "#000000";
          leading_diamond = "";
          style = "diamond";
          template = "  {{ .Name }} ";
          trailing_diamond = "<transparent,#91f2ff></>";
          type = "shell";
        }
        {
          background = "#ff8c94";
          foreground = "#000000";
          leading_diamond = "";
          properties = {
            time_format = "3:04:05 PM | Monday";
          };
          style = "diamond";
          template = "  {{ .CurrentDate | date .Format }}  ";
          trailing_diamond = "";
          type = "time";
        }
        {
          properties = {
            root_icon = " ";
          };
          style = "diamond";
          template = "  ";
          type = "root";
        }
        {
          background = "#f8677b";
          foreground = "#000000";
          leading_diamond = "<transparent,#F8677b></>";
          properties = {
            folder_icon = " ";
            folder_separator_icon = "  ";
            home_icon = "";
            style = "agnoster_full";
            template = "  {{ .Path }} ";
          };
          style = "diamond";
          trailing_diamond = "";
          type = "path";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#00BCF9";
          foreground_templates = [
            "{{ if gt .Code 0 }}#ef5350{{ end }}"
          ];
          properties = {
            always_enabled = true;
            template = "╰─  ";
          };
          style = "plain";
          template = " ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title = true;
  console_title_style = "template";
  console_title_template = "{{ .Folder }}";
  transient_prompt = {
    background = "transparent";
    foreground = "#FEF5ED";
    template = " ";
  };
  version = 3;
}
