{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#CECE04";
          style = "plain";
          template = "# ";
          type = "root";
        }
        {
          style = "plain";
          template = "{{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          foreground = "#7B68EE";
          properties = {
            style = "agnoster_short";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#48D1CC";
          properties = {
            branch_icon = "";
            fetch_upstream_icon = false;
          };
          style = "plain";
          template = "HEAD:{{ .UpstreamIcon }}{{ .HEAD }} ";
          type = "git";
        }
        {
          foreground = "#7FFFD4";
          foreground_templates = [
            "{{ if gt .Code 0 }}#E84855{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "<#66CDAA>❯</><#76EEC6>❯</><foreground>❯</> ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
