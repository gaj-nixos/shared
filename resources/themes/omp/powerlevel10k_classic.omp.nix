{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#546E7A";
          foreground = "#26C6DA";
          style = "plain";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#546E7A";
          foreground = "#26C6DA";
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          background = "#546E7A";
          foreground = "#26C6DA";
          properties = {
            style = "full";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#546E7A";
          foreground = "#D4E157";
          style = "plain";
          template = "<#26C6DA> </>{{ .HEAD }} ";
          type = "git";
        }
        {
          background = "transparent";
          foreground = "#546E7A";
          style = "plain";
          template = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#546E7A";
          foreground = "#D4E157";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .UserName }}@{{ .HostName }} <#26C6DA></> ";
          type = "session";
        }
        {
          background = "#546E7A";
          foreground = "#D4E157";
          properties = {
            time_format = "15:04:05";
          };
          style = "plain";
          template = " {{ .CurrentDate | date .Format }}  ";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#D4E157";
          foreground_templates = [
            "{{ if gt .Code 0 }}#FF5252{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
