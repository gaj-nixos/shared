{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#ffe9aa";
          foreground = "#100e23";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "root";
        }
        {
          background = "#ffffff";
          foreground = "#100e23";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "#91ddff";
          foreground = "#100e23";
          powerline_symbol = "";
          properties = {
            folder_icon = "";
            folder_separator_icon = "  ";
            style = "full";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#95ffa4";
          foreground = "#193549";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .HEAD }} ";
          type = "git";
        }
        {
          background = "#906cff";
          foreground = "#100e23";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          type = "python";
        }
        {
          background = "#ff8080";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#007ACC";
          style = "plain";
          template = "❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
