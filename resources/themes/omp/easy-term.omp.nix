{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "#0c7bbb";
          foreground = "#ffffff";
          leading_diamond = "╭─";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
          };
          style = "diamond";
          template = " {{ if .WSL }} on {{ end }}{{ .Icon }}  ";
          type = "os";
        }
        {
          background = "#DA627D";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "diamond";
          template = " 🏠  ";
          type = "text";
        }
        {
          background = "#8a62da";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            style = "folder";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#191f48";
          foreground = "#43CCEA";
          style = "powerline";
          powerline_symbol = "";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          trailing_diamond = "";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "#81ff91";
          style = "diamond";
          template = "<#cc7eda> | </><#7eb8da>RAM:</> {{ (div ((sub .PhysicalTotalMemory .PhysicalFreeMemory)|float64) 1073741824.0) }}/{{ (div .PhysicalTotalMemory 1073741824.0) }}GB";
          type = "sysinfo";
        }
        {
          foreground = "#81ff91";
          properties = {
            fetch_version = true;
          };
          style = "powerline";
          template = "<#cc7eda> | </><#7eb8da></> {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}";
          type = "node";
        }
        {
          type = "php";
          style = "powerline";
          foreground = "#81ff91";
          template = "<#cc7eda> | </><#7eb8da></> {{ .Full }}";
        }
        {
          type = "npm";
          style = "powerline";
          foreground = "#81ff91";
          template = "<#cc7eda> | </><#7eb8da> </> {{ .Full }}";
        }
        {
          type = "sysinfo";
          style = "powerline";
          foreground = "#81ff91";
          template = "<> </>";
        }
        {
          background = "#cecece";
          foreground = "#4b4b4b";
          leading_diamond = "";
          trailing_diamond = "";
          properties = {
            style = "austin";
            threshold = 150;
          };
          style = "diamond";
          template = "⌛  {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "#cecece";
          foreground = "#4b4b4b";
          leading_diamond = "";
          properties = {
            time_format = "15:04:05";
          };
          style = "diamond";
          template = "⏰  {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#0c7bbb";
          style = "plain";
          template = "╰─";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
