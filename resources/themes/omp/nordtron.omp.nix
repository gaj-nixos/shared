{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#88c0d0";
          style = "plain";
          template = "<#5e81ac>┏[</>{{ .UserName }}<#5e81ac>]</>";
          type = "session";
        }
        {
          foreground = "#b48ead";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = "<#5e81ac>--[</>{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}<#8fbcbb> ● </>{{ end }}{{ if .Staging.Changed }}<#88c0d0> ● </>{{ end }}<#5e81ac>]</>";
          type = "git";
        }
        {
          foreground = "#b48ead";
          style = "plain";
          template = "<#5e81ac>--[</>{{.Profile}}{{if .Region}}@{{.Region}}{{end}}<#5e81ac>]</>";
          type = "aws";
        }
        {
          foreground = "#b48ead";
          style = "plain";
          template = "<#5e81ac>--[</>{{.Context}}{{if .Namespace}} :: {{.Namespace}}{{end}}<#5e81ac>]</>";
          type = "kubectl";
        }
        {
          foreground = "#d8dee9";
          style = "plain";
          template = "<#5e81ac>[</><#5e81ac>]</>";
          type = "root";
        }
        {
          foreground = "#d8dee9";
          style = "plain";
          template = "<#5e81ac>[x</>{{ reason .Code }}<#5e81ac>]</>";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#88c0d0";
          properties = {
            style = "full";
          };
          style = "plain";
          template = "<#5e81ac>┖[</>{{ .Path }}<#5e81ac>]</>";
          type = "path";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#5e81ac";
          style = "plain";
          template = "  ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
