{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#003543";
          foreground = "#fff";
          powerline_symbol = "";
          properties = {
            windows = "";
          };
          style = "powerline";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}}";
          type = "os";
        }
        {
          background = "#003543";
          foreground = "#d2ff5e";
          powerline_symbol = "";
          style = "powerline";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          background = "#0087D8";
          foreground = "#003544";
          powerline_symbol = "";
          properties = {
            folder_separator_icon = "/";
            style = "full";
          };
          style = "powerline";
          template = "  {{ .Path }} ";
          type = "path";
        }
        {
          background = "#d2ff5e";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#ff9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#f26d50{{ end }}"
            "{{ if gt .Ahead 0 }}#89d1dc{{ end }}"
            "{{ if gt .Behind 0 }}#f17c37{{ end }}"
          ];
          foreground = "#193549";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
          };
          style = "powerline";
          template = " {{ .HEAD }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#33DD2D";
          background_templates = [
            "{{ if gt .Code 0 }}#f1184c{{ end }}"
          ];
          foreground = "#242424";
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          template = "  ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#f36943";
          background_templates = [
            "{{if eq \"Charging\" .State.String}}#33DD2D{{end}}"
            "{{if eq \"Discharging\" .State.String}}#FFCD58{{end}}"
            "{{if eq \"Full\" .State.String}}#0476d0{{end}}"
          ];
          foreground = "#242424";
          invert_powerline = true;
          powerline_symbol = "";
          style = "powerline";
          template = " {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}  ";
          type = "battery";
        }
        {
          background = "#0087D8";
          foreground = "#003544";
          invert_powerline = true;
          powerline_symbol = "";
          properties = {
            display_mode = "context";
            fetch_virtual_env = true;
          };
          style = "powerline";
          template = "  {{ .Venv }} ";
          type = "python";
        }
        {
          background = "#003543";
          foreground = "#fff";
          invert_powerline = true;
          powerline_symbol = "";
          style = "powerline";
          template = "<#fff>  </>{{ .CurrentDate | date .Format }} ";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#FFD700";
          style = "plain";
          template = " ⚡ ";
          type = "root";
        }
        {
          foreground = "#f1184c";
          style = "plain";
          template = "🚀 ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Shell }} in {{ .Folder }}";
  final_space = true;
  transient_prompt = {
    background = "transparent";
    foreground = "#FFD700";
    template = "{{if .Root}}⚡ {{end}}🚀 ";
  };
  version = 3;
}
