{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  palette = {
    yellow = "#ffff55";
    red = "#ff5555";
    green = "#55ff55";
    white = "#ffffff";
    orange = "#FF9248";
    darkGreen = "#3C873A";
    lightYellow = "#FFE873";
    darkRed = "#ec2729";
    blue = "#0d6da8";
    lightBlue = "#06aad5";
    brown = "#925837";
    darkBlue = "#055b9c";
    darkPink = "#ce092f";
    pink = "#de1f84";
    darkGray = "#1e293b";
    lightGreen = "#359a25";
    darkBrown = "#9c1006";
    lightBlue2 = "#5398c2";
    lightOrange = "#faa029";
    darkBlue2 = "#316ce4";
    cyan = "#55ffff";
    lightCyan = "#e0f8ff";
    lightGray = "#ecf7fa";
    lightRed = "#ef5350";
    cyan2 = "#40c4ff";
    yellow2 = "#FFFB38";
    green2 = "#33DD2D";
  };

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          style = "plain";
          template = "╭─[<p:yellow>{{ .UserName }}</><p:red>@</><p:green>{{ .HostName }}</>]─";
          type = "session";
        }
        {
          foreground = "p:red";
          style = "plain";
          template = "<p:white>[</>{{ .Path }}<p:white>]</>";
          type = "path";
        }
        {
          foreground = "p:orange";
          style = "plain";
          template = "<p:white>─(</>#<p:white>)</>";
          type = "root";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "p:darkGreen";
          properties = {
            fetch_package_manager = true;
            npm_icon = " <p:red></> ";
            yarn_icon = " <p:blue></>";
          };
          style = "plain";
          template = "<p:white>(</>{{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}<p:white>)</>";
          type = "node";
        }
        {
          foreground = "p:lightYellow";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "python";
        }
        {
          foreground = "p:darkRed";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "java";
        }
        {
          foreground = "p:blue";
          style = "plain";
          template = "<p:white>(</>{{ if .Unsupported }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "dotnet";
        }
        {
          foreground = "p:lightBlue";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "go";
        }
        {
          foreground = "p:brown";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "rust";
        }
        {
          foreground = "p:darkBlue";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "dart";
        }
        {
          foreground = "p:darkPink";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "angular";
        }
        {
          foreground = "p:pink";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "aurelia";
        }
        {
          foreground = "p:white";
          style = "plain";
          template = "<p:darkGray>(</>{{ if .Error }}{{ .Error }}{{ else }}Nx {{ .Full }}{{ end }}<p:darkGray>)</>";
          type = "nx";
        }
        {
          foreground = "p:lightGreen";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "julia";
        }
        {
          foreground = "p:darkBrown";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "ruby";
        }
        {
          foreground = "p:lightBlue2";
          style = "plain";
          template = "<p:white>(</>{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}<p:white>)</>";
          type = "azfunc";
        }
        {
          foreground = "p:lightOrange";
          style = "plain";
          template = "<p:white>(</>{{.Profile}}{{if .Region}}@{{.Region}}{{end}}<p:white>)</>";
          type = "aws";
        }
        {
          foreground = "p:darkBlue2";
          style = "plain";
          template = "<p:white>(</>{{.Context}}{{if .Namespace}} :: {{.Namespace}}{{end}}<p:white>)</>";
          type = "kubectl";
        }
        {
          foreground = "p:white";
          properties = {
            linux = "<p:white></>";
            macos = "<p:white></>";
            windows = "<p:white></>";
          };
          style = "plain";
          template = "<p:white>(</>{{ if .WSL }}WSL at {{ end }}{{.Icon}}<p:white>)─</>";
          type = "os";
        }
        {
          foreground = "p:white";
          foreground_templates = [
            "{{if eq \"Charging\" .State.String}}p:cyan2{{end}}"
            "{{if eq \"Discharging\" .State.String}}p:yellow2{{end}}"
            "{{if eq \"Full\" .State.String}}p:green2{{end}}"
          ];
          properties = {
            charged_icon = " ";
            charging_icon = " ";
            discharging_icon = " ";
          };
          style = "plain";
          template = "<p:white>[</>{{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}%<p:white>]─</>";
          type = "battery";
        }
        {
          foreground = "p:cyan";
          properties = {
            time_format = "_2,15:04";
          };
          style = "plain";
          template = "<p:white>[</>{{ .CurrentDate | date .Format }}<p:white>]</>";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          style = "plain";
          template = "╰─";
          type = "text";
        }
        {
          foreground = "p:lightCyan";
          properties = {
            branch_icon = " ";
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = "<p:white>[</>{{ .HEAD }}{{ if .Staging.Changed }}<p:green> ● {{ .Staging.String }}</>{{ end }}{{ if .Working.Changed }}<p:orange> ● {{ .Working.String }}</>{{ end }}<p:white>]-</>";
          type = "git";
        }
        {
          foreground = "p:lightGray";
          foreground_templates = [
            "{{ if gt .Code 0 }}p:lightRed{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = " ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
