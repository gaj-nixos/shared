{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#ffffff";
          style = "plain";
          template = "<#C591E8>❯</><#69FF94>❯</> ";
          type = "text";
        }
        {
          foreground = "#56B6C2";
          properties = {
            style = "folder";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#D0666F";
          properties = {
            branch_icon = "";
          };
          style = "plain";
          template = "<#5FAAE8>git:(</>{{ .HEAD }}<#5FAAE8>)</>";
          type = "git";
        }
        {
          foreground = "#DCB977";
          style = "plain";
          template = "  ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "#ffffff";
          properties = {
            command = "git log --pretty=format:%cr -1 || date +%H:%M:%S";
            shell = "bash";
          };
          style = "plain";
          template = " {{ .Output }} ";
          type = "command";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#ffffff";
          style = "plain";
          template = " ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
