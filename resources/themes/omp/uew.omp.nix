{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#BBC4B9";
          leading_diamond = "<#BFA8BF>~</> ";
          properties = {
            display_host = true;
          };
          style = "diamond";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          foreground = "#BFA8BF";
          properties = {
            branch_icon = " ";
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
            fetch_worktree_count = true;
          };
          style = "diamond";
          template = "<#fff>{{ .UpstreamIcon }}</>{{ .HEAD }}";
          type = "git";
        }
        {
          foreground = "#BFA8BF";
          style = "powerline";
          template = " <#fff></> {{ .Name }} ";
          type = "shell";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "#fff";
          style = "plain";
          type = "text";
        }
        {
          foreground = "#fff";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "diamond";
          template = " <#BBC4B9>{{ .FormattedMs }}s</> <#fff>������</>";
          type = "executiontime";
        }
        {
          foreground = "#BBC4B9";
          properties = {
            time_format = "Monday <#fff>at</> 3:04 PM";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          properties = {
            root_icon = " ";
          };
          style = "diamond";
          template = " ";
          type = "root";
        }
        {
          foreground = "#BBC4B9";
          style = "diamond";
          template = " <#fff></> {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#B6D6F2";
          leading_diamond = "<#BFA8BF>~</>";
          properties = {
            folder_icon = "/";
            folder_separator_icon = " / ";
            home_icon = "home";
            style = "agnoster_full";
          };
          style = "diamond";
          template = "  {{ .Path }}";
          trailing_diamond = "";
          type = "path";
        }
        {
          foreground = "#BFA8BF";
          foreground_templates = [
            "{{ if gt .Code 0 }}#fff{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = " ~ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Folder }}";
  transient_prompt = {
    background = "transparent";
    foreground = "#FEF5ED";
    template = " ";
  };
  version = 3;
}
