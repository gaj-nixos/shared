{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#333333";
          foreground = "#ff0000";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .UserName }}@{{ .HostName }} ";
          trailing_diamond = "";
          type = "session";
        }
        {
          background = "#1BD760";
          foreground = "#434343";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ .Icon }}{{ if ne .Status \"stopped\" }}{{ .Artist }} - {{ .Track }}{{ end }} ";
          type = "spotify";
        }
        {
          background = "#ff0000";
          foreground = "#000000";
          powerline_symbol = "";
          properties = {
            style = "folder";
          };
          style = "powerline";
          template = "  {{ .Path }} ";
          type = "path";
        }
        {
          background = "#fffb38";
          foreground = "#193549";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#6CA35E";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          background = "#203248";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ .Name }} ";
          type = "shell";
        }
        {
          background = "#ffff66";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "root";
        }
        {
          background = "#2e9599";
          background_templates = [
            "{{ if gt .Code 0 }}#f1184c{{ end }}"
          ];
          foreground = "#ffffff";
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "<transparent></>  <background,transparent></>";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
