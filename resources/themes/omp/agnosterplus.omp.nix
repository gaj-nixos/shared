{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
    blue = "#007ACC";
    white = "#ffffff";
    dark = "#100e23";
    lightBlue = "#91ddff";
    green = "#95ffa4";
    darkGreen = "#193549";
  };

  palette =
    if (config.lib ? stylix) then
      {
        blue = blue;
        white = white;
        dark = darkestGray;
        lightBlue = cyan;
        green = green;
        darkGreen = darkGray;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  inherit palette;
  blocks = [
    {
      type = "rprompt";
      segments = [
        {
          foreground = "p:blue";
          properties = {
            time_format = "15:04:05";
          };
          style = "plain";
          template = " {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
      ];
    }
    {
      alignment = "left";
      segments = [
        {
          background = "p:white";
          foreground = "p:dark";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "p:lightBlue";
          foreground = "p:dark";
          powerline_symbol = "";
          properties = {
            folder_icon = "";
            folder_separator_icon = "  ";
            home_icon = "";
            style = "agnoster";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "p:green";
          foreground = "p:darkGreen";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .HEAD }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
