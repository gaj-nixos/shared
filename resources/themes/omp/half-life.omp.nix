{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#7E46B6";
          style = "plain";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          foreground = "#ffffff";
          style = "plain";
          template = "in ";
          type = "text";
        }
        {
          foreground = "#87FF00";
          properties = {
            style = "full";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#5FD7FF";
          properties = {
            branch_ahead_icon = "";
            branch_behind_icon = "";
            branch_gone_icon = "";
            branch_icon = "";
            branch_identical_icon = "";
            cherry_pick_icon = "";
            commit_icon = "";
            fetch_status = true;
            merge_icon = "";
            rebase_icon = "";
            revert_icon = "";
            tag_icon = "";
          };
          style = "plain";
          template = "<#ffffff>on</> {{ .HEAD }}{{ if .Staging.Changed }}<#87FF00> ● {{ .Staging.String }}</>{{ end }}{{ if .Working.Changed }}<#D75F00> ● {{ .Working.String }}</>{{ end }} ";
          type = "git";
        }
        {
          foreground = "#D75F00";
          style = "plain";
          template = "λ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
