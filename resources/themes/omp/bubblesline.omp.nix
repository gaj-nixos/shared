{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "right";
      segments = [
        {
          background = "#424242";
          foreground = "#3EC669";
          leading_diamond = "";
          properties = {
            style = "folder";
          };
          style = "diamond";
          template = " {{ .Path }}";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#424242";
          foreground = "#43CCEA";
          leading_diamond = " ";
          properties = {
            branch_icon = "";
          };
          style = "diamond";
          template = "{{ .HEAD }}";
          trailing_diamond = "";
          type = "git";
        }
        {
          background = "#424242";
          foreground = "#E4F34A";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "python";
        }
        {
          background = "#424242";
          foreground = "#7FD5EA";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "go";
        }
        {
          background = "#424242";
          foreground = "#42E66C";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}";
          trailing_diamond = "";
          type = "node";
        }
        {
          background = "#424242";
          foreground = "#E64747";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "ruby";
        }
        {
          background = "#424242";
          foreground = "#E64747";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "java";
        }
        {
          background = "#424242";
          foreground = "#9B6BDF";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "julia";
        }
        {
          background = "#424242";
          foreground = "#9B6BDF";
          foreground_templates = [
            "{{if eq \"Charging\" .State.String}}#40c4ff{{end}}"
            "{{if eq \"Discharging\" .State.String}}#ff5722{{end}}"
            "{{if eq \"Full\" .State.String}}#4caf50{{end}}"
          ];
          leading_diamond = " ";
          properties = {
            charged_icon = "● ";
            charging_icon = "⇡ ";
            discharging_icon = "⇣ ";
          };
          style = "diamond";
          template = "{{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}";
          trailing_diamond = "";
          type = "battery";
        }
      ];
      type = "rprompt";
    }
    {
      alignment = "left";
      segments = [
        {
          background = "#424242";
          foreground = "#9B6BDF";
          leading_diamond = "";
          style = "diamond";
          template = "{{ .UserName }} ❯";
          trailing_diamond = " ";
          type = "session";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
