{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#000000";
          foreground = "#dd1e1e";
          style = "plain";
          template = " ⚡ ";
          type = "root";
        }
        {
          background = "transparent";
          foreground = "#ffffff";
          style = "plain";
          template = "{{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#5a3d29";
          foreground = "#43CCEA";
          properties = {
            folder_icon = "...";
            folder_separator_icon = "<transparent>  </>";
            home_icon = "";
            style = "agnoster_short";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#00C853";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FFEB3B{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#FFCC80{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          foreground = "#000000";
          leading_diamond = "<parentBackground,background></>";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
          };
          style = "diamond";
          template = " {{ .HEAD }}{{ if .Staging.Changed }}<#FF6F00>  {{ .Staging.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#2ba1bb";
          foreground = "#5a3d29";
          leading_diamond = "<parentBackground,background></>";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "diamond";
          template = " {{ .FormattedMs }}s ";
          type = "executiontime";
        }
        {
          background = "#910000";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "{{ if gt .Code 0 }}<parentBackground,background></><transparent> </> {{ reason .Code }} <background,transparent></>{{ else }}<parentBackground,transparent></>{{ end }}";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#5a3d29";
          foreground = "#43CCEA";
          leading_diamond = "";
          style = "diamond";
          template = "  {{ if .SSHSession }} {{ end }}{{ .UserName }}<transparent> / </>{{ .HostName }}";
          type = "session";
        }
        {
          background = "#5a3d29";
          foreground = "#43CCEA";
          properties = {
            time_format = "3:04:05 PM";
          };
          style = "plain";
          template = "<transparent>  </>{{ .CurrentDate | date .Format }} ";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#ffffff";
          foreground_templates = [
            "{{ if gt .Code 0 }}#ff0000{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}} ⚡ {{end}}{{.Folder | replace \"~\" \"🏚\" }} @ {{.HostName}}";
  version = 3;
}
