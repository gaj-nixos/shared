{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#BF231D";
          foreground = "#ffffff";
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          background = "#0A703E";
          foreground = "#ffffff";
          style = "plain";
          template = " {{ .Icon }} ";
          type = "os";
        }
        {
          background = "#0A703E";
          foreground = "#ffffff";
          style = "plain";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          background = "#256C9D";
          foreground = "#ffffff";
          properties = {
            folder_icon = "";
            folder_separator_icon = "  ";
            max_depth = 2;
            style = "agnoster_short";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#256C9D";
          foreground = "#ffffff";
          properties = {
            branch_max_length = 30;
            fetch_stash_count = false;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = "[ {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ]";
          type = "git";
        }
        {
          background = "#256C9D";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "plain";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          properties = {
            text = "";
          };
          type = "python";
        }
        {
          foreground = "#256C9D";
          style = "plain";
          template = " ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
