{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
    background = "#292929";
    foreground = "#fb7e14";
    foreground_alt = "#26C6DA";
    git_foreground = "#D4E157";
    blue = "#40c4ff";
  };

  palette =
    if (config.lib ? stylix) then
      {
        background = darkGray;
        foreground = orange;
        foreground_alt = cyan;
        git_foreground = yellow;
        blue = blue;
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  inherit palette;
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "p:background";
          foreground = "p:foreground";
          leading_diamond = "╭─";
          style = "diamond";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "p:background";
          foreground = "p:foreground";
          properties = {
            always_enabled = true;
            style = "austin";
            threshold = 500;
          };
          style = "powerline";
          template = " {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "p:background";
          foreground = "p:foreground_alt";
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          background = "p:background";
          foreground = "p:foreground";
          properties = {
            style = "full";
            trailing_diamond = "";
          };
          style = "diamond";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "p:background";
          foreground = "p:git_foreground";
          style = "plain";
          type = "git";
        }
        {
          background = "p:background";
          foreground = "p:foreground_alt";
          foreground_templates = [
            "{{ if eq \"Full\" .State.String }}p:background{{ end }}"
            "{{ if eq \"Charging\" .State.String }}p:blue{{ end }}"
            "{{ if eq \"Discharging\" .State.String }}p:foreground{{ end }}"
          ];
          properties = {
            charged_icon = " ";
            charging_icon = " ";
            discharging_icon = " ";
            text = "";
          };
          style = "powerline";
          template = " {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }} ";
          type = "battery";
        }
        {
          background = "p:background";
          background_templates = [
            "{{ if gt .Code 0 }}p:background{{ end }}"
          ];
          foreground = "p:foreground";
          properties = {
            always_enabled = true;
            display_exit_code = true;
          };
          style = "diamond";
          template = " {{ if gt .Code 0 }} {{ reason .Code }}{{ else }}{{ end }} ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "p:background";
          foreground = "p:foreground";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .SSHSession }} {{ end }}{{ .UserName }}@{{ .HostName }} <p:foreground></> ";
          type = "session";
        }
        {
          background = "p:background";
          foreground = "p:foreground";
          properties = {
            time_format = "15:04:05, _2";
          };
          style = "diamond";
          template = "{{ .CurrentDate | date .Format }}  ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          properties = {
            always_enabled = true;
          };
          background = "p:foreground";
          foreground = "p:background";
          style = "diamond";
          leading_diamond = "╰─";
          template = "";
          trailing_diamond = "";
          type = "path";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
