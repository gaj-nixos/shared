{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  console_title_template = " {{ .Folder }} :: {{if .Root}}Admin{{end}}";
  palette = {
    main-bg = "#24283b";
    terminal-red = "#f7768e";
    pistachio-green = "#9ece6a";
    terminal-green = "#73daca";
    terminal-yellow = "#e0af68";
    terminal-blue = "#7aa2f7";
    celeste-blue = "#b4f9f8";
    light-sky-blue = "#7dcfff";
    terminal-white = "#c0caf5";
    white-blue = "#a9b1d6";
    blue-bell = "#9aa5ce";
    pastal-grey = "#cfc9c2";
    terminal-magenta = "#bb9af7";
    blue-black = "#565f89";
    terminal-black = "#414868";
    t-background = "p:main-bg";
  };
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          type = "text";
          style = "plain";
          background = "transparent";
          foreground = "p:terminal-blue";
          template = "➜ ";
        }
        {
          type = "path";
          style = "plain";
          foreground = "p:terminal-magenta";
          properties = {
            style = "folder";
          };
          template = "<b>{{ .Path }}</b> <p:light-sky-blue>⚡</>";
        }
        {
          type = "git";
          style = "plain";
          foreground = "p:light-sky-blue";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}p:terminal-red{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0)}}p:light-sky-blue {{ end }}"
            "{{ if gt .Ahead 0 }}p:terminal-blue{{ end }}"
            "{{ if gt .Behind 0 }}p:celeste-blue{{ end }}"
          ];
          template = "({{ .HEAD}})";
          properties = {
            fetch_status = true;
            branch_icon = " ";
          };
        }
        {
          type = "status";
          style = "plain";
          foreground = "p:terminal-red";
          template = " ";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      overflow = "hide";
      segments = [
        {
          type = "node";
          style = "plain";
          foreground = "p:pistachio-green";
          template = " {{ .Full }} ";
        }
        {
          type = "php";
          style = "plain";
          foreground = "p:terminal-blue";
          template = " {{ .Full }} ";
        }
        {
          type = "python";
          style = "plain";
          foreground = "p:terminal-yellow";
          template = " {{ .Full }}";
        }
        {
          type = "julia";
          style = "plain";
          foreground = "p:terminal-magenta";
          template = " {{ .Full }}";
        }
        {
          type = "ruby";
          style = "plain";
          foreground = "p:terminal-red";
          template = " {{ .Full}}";
        }
        {
          type = "go";
          style = "plain";
          foreground = "p:light-sky-blue";
          template = "ﳑ {{ .Full}}";
        }
        {
          type = "command";
          style = "plain";
          foreground = "p:white-blue";
          properties = {
            command = "git log --pretty=format:%cr -1 || date +%H:%M:%S";
            shell = "bash";
          };
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "p:pistachio-green";
          style = "plain";
          template = "▶";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  secondary_prompt = {
    background = "transparent";
    foreground = "p:terminal-blue";
    template = "➜ ";
  };
  transient_prompt = {
    background = "p:t-background";
    foreground = "p:terminal-blue";
    template = "➜ ";
  };
  final_space = true;
  version = 3;
  terminal_background = "p:t-background";
}
