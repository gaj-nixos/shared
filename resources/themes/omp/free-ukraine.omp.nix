{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#ffffff";
          style = "plain";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#0057b7";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          background = "#0057b7";
          foreground = "#FBD951";
          powerline_symbol = "";
          style = "powerline";
          template = "  ";
          type = "text";
        }
        {
          background = "#0057b7";
          foreground = "#FBD951";
          properties = {
            home_icon = "";
            style = "mixed";
          };
          style = "diamond";
          template = "{{ .Path }} ";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#0057b7";
          foreground = "#ffd700";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = "   {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Staging.Changed) (.Working.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0}} {{ .StashCount }}{{ end }}{{ if gt .WorktreeCount 0}}  {{ .WorktreeCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#3379c5";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#3379c5{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#c57f33{{ end }}"
            "{{ if gt .Ahead 0 }}#79c533{{ end }}"
            "{{ if gt .Behind 0 }}#c53379{{ end }}"
          ];
          foreground = "#0057b7";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = false;
            fetch_status = false;
            fetch_upstream_icon = false;
          };
          style = "powerline";
          template = "";
          type = "git";
        }
        {
          leading_diamond = "<transparent,#49404f></>";
          style = "plain";
          trailing_diamond = "";
          type = "text";
        }
        {
          background = "#ffd700";
          foreground = "#0057b7";
          foreground_templates = [
            "{{ if gt .Code 0 }}#a30000{{ end }}"
          ];
          leading_diamond = "<transparent,#ffd700></>";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = " {{ if gt .Code 0 }}  {{ else }} ♥ {{ end }}";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#0057b7";
          foreground = "#FBD951";
          leading_diamond = "";
          style = "diamond";
          template = "  <#111111></>";
          type = "text";
        }
        {
          background = "#0057b7";
          foreground = "#FBD951";
          invert_powerline = true;
          leading_diamond = "";
          properties = {
            charged_icon = " ";
            charging_icon = " ";
            discharging_icon = " ";
          };
          style = "diamond";
          template = " {{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}% <#111111></>";
          type = "battery";
        }
        {
          background = "#0057b7";
          foreground = "#FBD951";
          invert_powerline = true;
          leading_diamond = "";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = "  {{ .FormattedMs }} <#111111></>";
          type = "executiontime";
        }
        {
          background = "#0057b7";
          foreground = "#FBD951";
          invert_powerline = true;
          leading_diamond = "";
          properties = {
            time_format = "_2,15:04";
          };
          style = "diamond";
          template = "  {{ .CurrentDate | date .Format }}";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#0057b7";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          background = "#0057b7";
          foreground = "#ffd700";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          type = "os";
        }
        {
          background = "#0057b7";
          foreground = "#FBD951";
          powerline_symbol = "";
          style = "powerline";
          template = " ";
          type = "root";
        }
        {
          background = "#FBD951";
          foreground = "#0057b7";
          powerline_symbol = "";
          properties = {
            display_host = false;
          };
          style = "powerline";
          template = " {{ if .SSHSession }} {{ end }}{{ .UserName }} ";
          type = "session";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "#0057b7";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          foreground = "#FBD951";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          foreground = "#0057b7";
          style = "plain";
          template = "";
          type = "text";
        }
        {
          foreground = "#FBD951";
          style = "plain";
          template = "  Ukraine";
          type = "text";
        }
      ];
      type = "rprompt";
    }
  ];
  console_title_template = "{{if .Root}}Admin: {{end}} {{.Folder}}";
  final_space = true;
  version = 3;
}
