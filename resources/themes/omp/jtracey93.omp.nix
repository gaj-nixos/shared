{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#EB9654";
          foreground = "#ffffff";
          leading_diamond = "";
          properties = {
            display_host = false;
          };
          style = "diamond";
          template = "{{ if .SSHSession }} {{ end }}{{ .UserName }} ";
          type = "session";
        }
        {
          background = "lightYellow";
          foreground = "#3f3f3f";
          properties = {
            style = "full";
          };
          style = "plain";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#25AFF3";
          foreground = "#ffffff";
          properties = {
            fetch_status = true;
          };
          style = "plain";
          template = " branch ({{ .HEAD }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}) ";
          type = "git";
        }
        {
          background = "#1BD760";
          foreground = "#ffffff";
          properties = {
            paused_icon = "";
            playing_icon = "";
            stopped_icon = "";
            track_separator = " - ";
          };
          style = "plain";
          template = " {{ .Icon }}{{ if ne .Status \"stopped\" }}{{ .Artist }} - {{ .Track }}{{ end }} ";
          type = "spotify";
        }
        {
          background = "#cc00ff";
          foreground = "#ffffff";
          properties = {
            time_format = "15:04:05";
          };
          style = "plain";
          template = " {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          background = "#49404f";
          foreground = "#ffffff";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "plain";
          template = " {{ .FormattedMs }}s ";
          type = "executiontime";
        }
        {
          type = "status";
          style = "diamond";
          foreground = "#ffffff";
          background = "#00897b";
          background_templates = [
            "{{ if .Error }}#e91e63{{ end }}"
          ];
          trailing_diamond = "";
          properties = {
            always_enabled = true;
          };
        }
      ];
      type = "prompt";
    }
    {
      type = "prompt";
      newline = true;
      alignment = "left";
      segments = [
        {
          foreground = "#ffffff";
          foreground_templates = [
            "{{ if gt .Code 0 }}#ff0000{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
    }
  ];
  final_space = true;
  version = 3;
}
