{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#2e9599";
          foreground = "#ffffff";
          leading_diamond = "";
          powerline_symbol = "";
          style = "diamond";
          template = "������ {{ .HostName }} ";
          type = "session";
        }
        {
          background = "#D51111";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            home_icon = "";
            folder_separator_icon = " <transparent></> ";
            folder_icon = "";
            style = "agnoster";
          };
          style = "powerline";
          template = " {{ path .Path .Location }} ";
          type = "path";
        }
        {
          background = "#c19c00";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FFEB3B{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#FFA300{{ end }}"
            "{{ if gt .Ahead 0 }}#FF7070{{ end }}"
            "{{ if gt .Behind 0 }}#90F090{{ end }}"
          ];
          foreground = "#000000";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = "  {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#303030";
          foreground = "#3C873A";
          leading_diamond = "";
          properties = {
            fetch_package_manager = true;
            npm_icon = " <#cc3a3a></> ";
            yarn_icon = " <#348cba></>";
          };
          style = "diamond";
          template = " {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}";
          trailing_diamond = " ";
          type = "node";
        }
        {
          background = "#306998";
          foreground = "#FFE873";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}";
          trailing_diamond = " ";
          type = "python";
        }
        {
          background = "#ffffff";
          foreground = "#06aad5";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = " ";
          type = "go";
        }
        {
          background = "#2e9599";
          foreground = "#fff";
          background_templates = [
            "{{ if gt .Code 0 }}#D51111{{ end }}"
          ];
          leading_diamond = "";
          properties = {
            time_format = "_2, 15:04:05";
          };
          style = "diamond";
          template = "  {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#FFF";
          foreground_templates = [
            "{{ if gt .Code 0 }}#D51111{{ end }}"
          ];
          style = "plain";
          template = "  ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{ .Folder }}";
  final_space = true;
  osc99 = true;
  version = 3;
}
