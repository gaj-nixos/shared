{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#ffea00";
          foreground = "#000000";
          leading_diamond = "▓";
          style = "diamond";
          type = "text";
        }
        {
          background = "#ffea00";
          foreground = "#000000";
          style = "powerline";
          template = " ������ {{.Context}}{{if .Namespace}} :: {{.Namespace}}{{end}} ";
          type = "kubectl";
        }
        {
          background = "#008AD7";
          foreground = "#000000";
          powerline_symbol = "";
          properties = {
            source = "cli";
          };
          style = "powerline";
          template = "  {{ .Name }} [ {{ .Origin }} ] ";
          type = "az";
        }
        {
          background = "#012456";
          foreground = "#FFFFFF";
          powerline_symbol = "";
          properties = {
            source = "pwsh";
          };
          style = "powerline";
          template = "  {{ .Name }} [ {{ .Origin }} ] ";
          type = "az";
        }
        {
          type = "aws";
          style = "powerline";
          powerline_symbol = "";
          properties = {
            display_default = true;
          };
          foreground = "#000000";
          background = "#FFA400";
          template = " ������ {{.Profile}}{{if .Region}}@{{.Region}}{{end}}";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "#2f2f2f";
          foreground = "#26C6DA";
          leading_diamond = "▓";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
          };
          style = "diamond";
          template = " {{ if .WSL }}{{ .Icon }} on {{ end }} <#ffea00></>";
          type = "os";
        }
        {
          background = "#2f2f2f";
          foreground = "#ffea00";
          style = "diamond";
          template = " {{ if ne .Env.POSH_SESSION_DEFAULT_USER .UserName }}{{ .UserName }}{{ end }}<#ffea00> </>";
          type = "session";
        }
        {
          background = "#2f2f2f";
          foreground = "#ffea00";
          style = "diamond";
          template = " <#ffea00> </>";
          type = "root";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          properties = {
            folder_icon = "<#B5B2C2> </>";
            folder_separator_icon = "<#ffea00> </>";
            home_icon = "  ";
            style = "agnoster_short";
          };
          style = "diamond";
          template = "<#f2f200>  </>{{ .Path }} ";
          type = "path";
        }
        {
          background = "#2f2f2f";
          foreground = "#ffea00";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#ffea00{{ end }}"
            "{{ if gt .Ahead 0 }}#2EC4B6{{ end }}"
            "{{ if gt .Behind 0 }}#8A4FFF{{ end }}"
          ];
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "diamond";
          template = "<#ffea00> </>{{ .UpstreamIcon }}{{ .HEAD }}{{ .BranchStatus }}{{ if .Working.Changed }}<#E84855>  {{ .Working.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}<#2FDA4E>  {{ .Staging.String }}</>{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          style = "diamond";
          trailing_diamond = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#ffea00";
          style = "plain";
          template = " ❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}}root :: {{end}}{{.Shell}} :: {{.Folder}}";
  final_space = true;
  version = 3;
}
