#!/usr/bin/env nix-shell
#!nix-shell -i bash -p yq perl
# shellcheck shell=bash

# This script converts unicode characters from their '\uXXXX' representation to the actual character in each nix file of the current directory

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

for nix_file in *.nix; do
  # Skip if no Nix files exist
  [[ -e "$nix_file" ]] || break

  # Get first line (empty if file is empty)
  first_line=$(head -n 1 "$nix_file" 2>/dev/null || true)

  # Only add header if it doesn't already exist
  if [[ "$first_line" != "{ config, ... }:" ]]; then
    # Create temp file with atomic replacement
    tmp_file=$(mktemp)
    {
      echo "{ config, ... }:"
      cat "$nix_file"
    } > "$tmp_file"

    # Preserve original permissions
    mv "$tmp_file" "$nix_file"

    echo "Added header to: $nix_file"
  else
    echo "Header exists in: $nix_file (skipped)"
  fi
done
