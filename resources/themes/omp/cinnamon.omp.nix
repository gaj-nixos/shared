{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  palette = {
    userBackground = "#07585c";
    pathBackground = "#3e9022";
    gitBackground = "#de076f";
    spotifyBackground = "#491545";
    statusBackground = "#491515";
    errorBackground = "#f1184c";
    foreground = "#ffffff";
  };
  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "p:userBackground";
          foreground = "p:foreground";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "p:pathBackground";
          foreground = "p:foreground";
          properties = {
            style = "folder";
          };
          style = "plain";
          template = "  {{ .Path }} ";
          type = "path";
        }
        {
          background = "p:gitBackground";
          foreground = "p:foreground";
          properties = {
            branch_icon = "";
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = " {{ .UpstreamIcon }}{{ .HEAD }} ";
          type = "git";
        }
        {
          background = "p:spotifyBackground";
          foreground = "p:foreground";
          properties = {
            paused_icon = " ";
            playing_icon = " ";
            stopped_icon = " ";
            track_separator = " - ";
          };
          style = "plain";
          template = "  {{ .Icon }}{{ if ne .Status \"stopped\" }}{{ .Artist }} - {{ .Track }}{{ end }} ";
          type = "spotify";
        }
        {
          background = "p:statusBackground";
          background_templates = [
            "{{ if gt .Code 0 }}p:errorBackground{{ end }}"
          ];
          foreground = "p:foreground";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = "  ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
