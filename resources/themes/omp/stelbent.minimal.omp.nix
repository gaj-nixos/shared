{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "right";
      segments = [
        {
          foreground = "#007acc";
          style = "plain";
          template = "{{ .Name }}";
          type = "shell";
        }
        {
          foreground = "#ff8080";
          style = "plain";
          template = " <#757575,>as</> root";
          type = "root";
        }
        {
          foreground = "#689f38";
          properties = {
            time_format = "15:04:05";
          };
          style = "plain";
          template = " <#757575,>|</> {{ .CurrentDate | date .Format }}";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#757575";
          style = "plain";
          template = "┌ {{ .UserName }}@{{ .HostName }} ";
          type = "session";
        }
        {
          background = "#91ddff";
          foreground = "#100e23";
          powerline_symbol = "";
          properties = {
            style = "folder";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#95ffa4";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#ff9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#f26d50{{ end }}"
            "{{ if gt .Ahead 0 }}#89d1dc{{ end }}"
            "{{ if gt .Behind 0 }}#c5b6ad{{ end }}"
          ];
          foreground = "#100e23";
          powerline_symbol = "";
          properties = {
            branch_ahead_icon = "↑";
            branch_behind_icon = "↓";
            branch_gone = "≢";
            branch_icon = " ";
            branch_identical_icon = "≡";
            cherry_pick_icon = "✓ ";
            commit_icon = "▷ ";
            fetch_stash_count = true;
            fetch_status = true;
            merge_icon = "◴ ";
            no_commits_icon = "[no commits]";
            rebase_icon = "Ɫ ";
            tag_icon = "▶ ";
          };
          style = "powerline";
          template = " {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }} {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} │{{ end }}{{ if .Staging.Changed }} {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }} {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#ffee58";
          foreground = "#100e23";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .WorkspaceName }} ";
          type = "terraform";
        }
        {
          background = "#ff8080";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = " error ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#757575";
          style = "plain";
          template = "└ ";
          type = "text";
        }
        {
          foreground = "#ffffff";
          style = "plain";
          template = "$";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
