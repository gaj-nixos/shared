{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#29315A";
          foreground = "#3EC669";
          leading_diamond = "";
          properties = {
            style = "mixed";
          };
          style = "diamond";
          template = " {{ .Path }}";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#29315A";
          foreground = "#43CCEA";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          leading_diamond = " ";
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "diamond";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          trailing_diamond = "";
          type = "git";
        }
        {
          foreground = "#C94A16";
          style = "plain";
          template = "x ";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          type = "node";
          style = "plain";
          foreground = "#88e570";
          properties = {
            display_mode = "files";
            fetch_package_manager = true;
            fetch_version = true;
            npm_icon = "<#e5272d> npm</> ";
            yarn_icon = "<#37aee5> yarn</> ";
            pnpm_icon = "<#e5a100>������ pnpm</> ";
          };
          template = "{{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }} {{ .Full }}";
        }
        {
          foreground = "#4063D8";
          properties = {
            display_mode = "files";
            fetch_version = true;
          };
          style = "plain";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          type = "crystal";
        }
        {
          foreground = "#DE3F24";
          properties = {
            display_mode = "files";
            fetch_version = true;
          };
          style = "plain";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          type = "ruby";
        }
        {
          foreground = "#FED142";
          properties = {
            display_mode = "context";
            fetch_virtual_env = false;
          };
          style = "plain";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          type = "python";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#63F08C";
          style = "plain";
          template = "➜ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
