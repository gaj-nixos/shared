{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#003543";
          foreground = "#00c983";
          leading_diamond = "";
          style = "diamond";
          template = "{{ .Icon }} {{ .HostName }} ";
          type = "os";
        }
        {
          background = "#DA627D";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            folder_icon = "";
            folder_separator_icon = "\\";
            home_icon = "";
            style = "full";
          };
          style = "powerline";
          template = " <#000> </> {{ .Path }} ";
          type = "path";
        }
        {
          background = "#FCA17D";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            branch_icon = " <#ffffff> </>";
            fetch_stash_count = true;
            fetch_status = false;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " ➜ ({{ .UpstreamIcon }}{{ .HEAD }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }}) ";
          type = "git";
        }
        {
          background = "#76b367";
          foreground = "#ffffff";
          powerline_symbol = "";
          style = "powerline";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }} ";
          type = "node";
        }
        {
          background = "#83769c";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "powerline";
          template = "  {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "#33658A";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            time_format = "3:04 PM";
          };
          style = "powerline";
          template = "  {{ .CurrentDate | date .Format }} ";
          type = "time";
        }
        {
          background = "#2e9599";
          background_templates = [
            "{{ if gt .Code 0 }}red{{ end }}"
          ];
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            always_enabled = true;
          };
          style = "diamond";
          template = " {{ if gt .Code 0 }}{{ else }}{{ end }} ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#cd5e42";
          style = "plain";
          template = " ";
          type = "root";
        }
        {
          foreground = "#CD4277";
          style = "plain";
          template = " <#45F1C2><b>⚡</b></><b>{{ .UserName }}</b> <#26C6DA>❯</><#45F1C2>❯</>";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
