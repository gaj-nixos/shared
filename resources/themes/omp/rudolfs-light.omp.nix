{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#E0E0E0";
          foreground = "#BF231D";
          style = "plain";
          template = "  ";
          type = "root";
        }
        {
          background = "#E0E0E0";
          foreground = "#EF7D00";
          style = "plain";
          template = " {{ .Icon }} ";
          type = "os";
        }
        {
          background = "#E0E0E0";
          foreground = "#424242";
          style = "plain";
          template = "{{ .UserName }}";
          type = "session";
        }
        {
          background = "#E0E0E0";
          foreground = "#424242";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#053F22{{ end }}"
            "{{ if or (gt .Ahead 0) (gt .Behind 0) }}#0A703E{{ end }}"
          ];
          properties = {
            branch_max_length = 30;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "plain";
          template = " [{{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}<#BD6200>  {{ .Working.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}<#053F22>  {{ .Staging.String }}</>{{ end }}] ";
          type = "git";
        }
        {
          background = "#E0E0E0";
          foreground = "#424242";
          properties = {
            style = "full";
          };
          style = "plain";
          template = " {{ .Path }}";
          type = "path";
        }
        {
          background = "#E0E0E0";
          foreground = "#424242";
          powerline_symbol = "";
          style = "plain";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} ";
          properties = {
            text = "";
          };
          type = "python";
        }
        {
          foreground = "#E0E0E0";
          style = "plain";
          template = " ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
