{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#0C212F";
          foreground = "#FFFFFF";
          leading_diamond = "";
          style = "diamond";
          template = " {{ .Icon }} ";
          trailing_diamond = "";
          type = "os";
        }
        {
          background = "#DE2121";
          foreground = "#FFFFFF";
          leading_diamond = "<transparent,#DE2121></>";
          style = "diamond";
          template = "  ";
          trailing_diamond = "";
          type = "root";
        }
        {
          background = "#26BDBB";
          foreground = "#0C212F";
          leading_diamond = "<transparent,#26BDBB></>";
          properties = {
            folder_icon = "...";
            folder_separator_icon = "<transparent>  </>";
            home_icon = "";
            style = "agnoster_short";
          };
          style = "diamond";
          template = " {{ .Path }} ";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#280C2E";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#7621DE{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#7621DE{{ end }}"
            "{{ if gt .Ahead 0 }}#7621DE{{ end }}"
            "{{ if gt .Behind 0 }}#7621DE{{ end }}"
          ];
          foreground = "#FFFFFF";
          powerline_symbol = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "powerline";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#0C212F";
          foreground = "#FFFFFF";
          leading_diamond = "<transparent,#0C212F></>";
          properties = {
            time_format = "15:04";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
        {
          background = "#26BDBB";
          foreground = "#0C212F";
          leading_diamond = "<transparent,#26BDBB></>";
          properties = {
            style = "dallas";
            threshold = 0;
          };
          style = "diamond";
          template = " {{ .FormattedMs }}s ";
          trailing_diamond = "";
          type = "executiontime";
        }
        {
          background = "#910000";
          foreground = "#ffffff";
          leading_diamond = "<transparent,background></>";
          style = "diamond";
          template = "<transparent> </> {{ reason .Code }} ";
          trailing_diamond = "";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}} ⚡ {{end}}{{.Folder | replace \"~\" \"🏠\"}} @ {{.HostName}}";
  final_space = true;
  version = 3;
}
