{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "right";
      segments = [
        {
          background = "#29315A";
          foreground = "#3EC669";
          leading_diamond = "";
          properties = {
            style = "folder";
          };
          style = "diamond";
          template = " {{ .Path }}";
          trailing_diamond = "";
          type = "path";
        }
        {
          background = "#29315A";
          foreground = "#43CCEA";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FF9248{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#ff4500{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          leading_diamond = " ";
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "diamond";
          template = " {{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          trailing_diamond = "";
          type = "git";
        }
        {
          background = "#29315A";
          foreground = "#E4F34A";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "python";
        }
        {
          background = "#29315A";
          foreground = "#7FD5EA";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "go";
        }
        {
          background = "#29315A";
          foreground = "#42E66C";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}";
          trailing_diamond = "";
          type = "node";
        }
        {
          background = "#29315A";
          foreground = "#E64747";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "ruby";
        }
        {
          background = "#29315A";
          foreground = "#E64747";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }}";
          trailing_diamond = "";
          type = "java";
        }
        {
          background = "#29315A";
          foreground = "#9B6BDF";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          style = "diamond";
          template = "{{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} ";
          trailing_diamond = "";
          type = "julia";
        }
        {
          type = "php";
          style = "diamond";
          foreground = "#ffffff";
          background = "#4063D8";
          leading_diamond = " ";
          properties = {
            fetch_version = false;
          };
          template = " {{ .Full }} ";
          trailing_diamond = "";
        }
        {
          background = "#29315A";
          foreground = "#9B6BDF";
          foreground_templates = [
            "{{if eq \"Charging\" .State.String}}#40c4ff{{end}}"
            "{{if eq \"Discharging\" .State.String}}#ff5722{{end}}"
            "{{if eq \"Full\" .State.String}}#4caf50{{end}}"
          ];
          leading_diamond = " ";
          properties = {
            charged_icon = " ";
            charging_icon = "⇡ ";
            discharging_icon = "⇣ ";
          };
          style = "diamond";
          template = "{{ if not .Error }}{{ .Icon }}{{ .Percentage }}{{ end }}{{ .Error }}";
          trailing_diamond = "";
          type = "battery";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "#29315A";
          foreground = "#AEA4BF";
          leading_diamond = "";
          properties = {
            style = "austin";
            threshold = 150;
          };
          style = "diamond";
          template = "{{ .FormattedMs }}";
          trailing_diamond = " ";
          type = "executiontime";
        }
        {
          background = "#29315A";
          foreground = "#E64747";
          leading_diamond = "";
          style = "diamond";
          template = "{{ .UserName }} ❯";
          trailing_diamond = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
