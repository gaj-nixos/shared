{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#1478DB";
          foreground = "#000000";
          leading_diamond = "";
          trailing_diamond = "";
          properties = {
            style = "full";
          };
          style = "diamond";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          background = "#3AD900";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#FFC600{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#FFCC80{{ end }}"
            "{{ if gt .Ahead 0 }}#B388FF{{ end }}"
            "{{ if gt .Behind 0 }}#B388FF{{ end }}"
          ];
          foreground = "#000000";
          leading_diamond = "<transparent,background></>";
          trailing_diamond = "";
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
          };
          style = "diamond";
          template = " {{ .HEAD }}{{ if .Staging.Changed }}<#FF6F00>  {{ .Staging.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
