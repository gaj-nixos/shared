{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          background = "#d75f00";
          foreground = "#f2f3f8";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
          };
          style = "diamond";
          leading_diamond = "╭─";
          template = " {{ .Icon }} ";
          type = "os";
        }
        {
          background = "#e4e4e4";
          foreground = "#4e4e4e";
          style = "powerline";
          powerline_symbol = "";
          template = " {{ .UserName }} ";
          type = "session";
        }
        {
          background = "#0087af";
          foreground = "#f2f3f8";
          properties = {
            style = "agnoster_short";
            max_depth = 3;
            folder_icon = "…";
            folder_separator_icon = " <transparent></> ";
          };
          style = "powerline";
          powerline_symbol = "";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#378504";
          foreground = "#f2f3f8";
          background_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#a97400{{ end }}"
            "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#54433a{{ end }}"
            "{{ if gt .Ahead 0 }}#744d89{{ end }}"
            "{{ if gt .Behind 0 }}#744d89{{ end }}"
          ];
          properties = {
            branch_max_length = 25;
            fetch_stash_count = true;
            fetch_status = true;
            branch_icon = " ";
            branch_identical_icon = "";
            branch_gone_icon = "";
          };
          style = "diamond";
          leading_diamond = "<transparent,background></>";
          template = " {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }} <transparent></> <#121318> {{ .Working.String }}</>{{ end }}{{ if .Staging.Changed }} <transparent></> <#121318> {{ .Staging.String }}</>{{ end }}{{ if gt .StashCount 0 }} <transparent></> <#121318> {{ .StashCount }}</>{{ end }} ";
          trailing_diamond = "";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#e4e4e4";
          foreground = "#585858";
          properties = {
            style = "austin";
            always_enabled = true;
          };
          invert_powerline = true;
          style = "powerline";
          powerline_symbol = "";
          template = "  {{ .FormattedMs }} ";
          type = "executiontime";
        }
        {
          background = "#d75f00";
          foreground = "#f2f3f8";
          properties = {
            time_format = "15:04:05";
          };
          invert_powerline = true;
          style = "diamond";
          template = "  {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#d75f00";
          style = "plain";
          template = "╰─ {{ if .Root }}#{{else}}\${{end}}";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
