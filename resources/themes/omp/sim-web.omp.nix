{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      type = "prompt";
      alignment = "right";
      overflow = "hide";
      segments = [
        {
          type = "executiontime";
          style = "powerline";
          foreground = "#a9ffb4";
          template = " {{ .FormattedMs }}s <#ffffff></>";
          properties = {
            threshold = 0;
            style = "dallas";
          };
        }
        {
          type = "node";
          style = "powerline";
          foreground = "#45bf17";
          template = "  {{ .Full }} ";
        }
        {
          type = "npm";
          style = "powerline";
          foreground = "#FE4A49";
          template = "<#F3EFF5>and</>  {{ .Full }} ";
        }
      ];
    }
    {
      type = "prompt";
      alignment = "left";
      newline = true;
      overflow = "break";
      segments = [
        {
          type = "path";
          style = "powerline";
          foreground = "#ffafd2";
          properties = {
            style = "agnoster_full";
            home_icon = "home";
            folder_icon = "";
            folder_separator_icon = " ❯ ";
          };
          template = " {{ .Path }} ";
        }
        {
          type = "git";
          style = "powerline";
          foreground = "#f14e32";
          properties = {
            branch_icon = " ";
          };
          template = "({{ .HEAD }})";
        }
      ];
    }
    {
      alignment = "left";
      newline = true;
      type = "prompt";
      segments = [
        {
          type = "status";
          style = "diamond";
          foreground = "#00c7fc";
          properties = {
            always_enabled = true;
          };
          template = "<#00c7fc>❯</>_: ";
        }
      ];
    }
  ];
  console_title_template = "{{ .Folder }}";
  transient_prompt = {
    background = "transparent";
    foreground = "#FEF5ED";
    template = "{{ .Shell }}";
  };
  version = 3;
}
