{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#ffffff";
          foreground = "#000000";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .WSL }}WSL at {{ end }}{{.Icon}} ";
          trailing_diamond = "";
          type = "os";
        }
        {
          background = "#0000ff";
          foreground = "#000000";
          powerline_symbol = "";
          properties = {
            style = "full";
          };
          style = "powerline";
          template = "  ";
          type = "root";
        }
        {
          background = "#0000ff";
          foreground = "#ffffff";
          powerline_symbol = "";
          properties = {
            style = "full";
          };
          style = "powerline";
          template = " {{ .Path }} ";
          type = "path";
        }
        {
          background = "#D4E157";
          foreground = "#000000";
          powerline_symbol = "";
          style = "powerline";
          template = " {{ .HEAD }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#D4E157";
          foreground = "#000000";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }} <transparent></>";
          type = "python";
        }
        {
          background = "#7FD5EA";
          foreground = "#ffffff";
          leading_diamond = "";
          style = "diamond";
          template = " {{ if .Error }}{{ .Error }}{{ else }}{{ .Full }}{{ end }} <transparent></>";
          type = "go";
        }
        {
          background = "#ffffff";
          foreground = "#000000";
          leading_diamond = "";
          properties = {
            time_format = "15:04:05";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }} ";
          trailing_diamond = "";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#D4E157";
          foreground_templates = [
            "{{ if gt .Code 0 }}#FF5252{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
