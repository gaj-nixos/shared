{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          background = "#2f2f2f";
          foreground = "#26C6DA";
          leading_diamond = "▓";
          properties = {
            alpine = "";
            arch = "";
            centos = "";
            debian = "";
            elementary = "";
            fedora = "";
            gentoo = "";
            linux = "";
            macos = "";
            manjaro = "";
            mint = "";
            opensuse = "";
            raspbian = "";
            ubuntu = "";
            windows = "";
          };
          style = "diamond";
          template = " {{ if .WSL }} on {{ end }}{{ .Icon }}<#7a7a7a> </>";
          type = "os";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          style = "diamond";
          template = " {{ if ne .Env.POSH_SESSION_DEFAULT_USER .UserName }}<#77f5d6>{{ .UserName }}</><#7a7a7a></>{{ end }}<#2EEFBF>{{ .HostName }}</><#7a7a7a> </>";
          type = "session";
        }
        {
          background = "#2f2f2f";
          foreground = "#ffff66";
          style = "diamond";
          template = " <#7a7a7a> </>";
          type = "root";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          properties = {
            folder_icon = "<#B5B2C2> </>";
            style = "full";
          };
          style = "diamond";
          template = "<#f2f200>  </>{{ .Path }} ";
          type = "path";
        }
        {
          background = "#2f2f2f";
          foreground = "#ffeb3b";
          foreground_templates = [
            "{{ if or (.Working.Changed) (.Staging.Changed) }}#ffeb3b{{ end }}"
            "{{ if gt .Ahead 0 }}#2EC4B6{{ end }}"
            "{{ if gt .Behind 0 }}#8A4FFF{{ end }}"
          ];
          properties = {
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
          };
          style = "diamond";
          template = "<#7a7a7a> </>{{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}<#E84855>  {{ .Working.String }}</>{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}<#2FDA4E>  {{ .Staging.String }}</>{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          style = "diamond";
          trailing_diamond = "";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          leading_diamond = "";
          style = "diamond";
          type = "text";
        }
        {
          background = "#2f2f2f";
          foreground = "#6CA35E";
          style = "diamond";
          template = "  {{ if .PackageManagerIcon }}{{ .PackageManagerIcon }} {{ end }}{{ .Full }}<#7a7a7a> </>";
          type = "node";
        }
        {
          background = "#2f2f2f";
          foreground = "#96E072";
          style = "diamond";
          template = "  {{ if .Error }}{{ .Error }}{{ else }}{{ if .Venv }}{{ .Venv }} {{ end }}{{ .Full }}{{ end }}<#7a7a7a> </>";
          type = "python";
        }
        {
          background = "#2f2f2f";
          foreground = "#3891A6";
          style = "diamond";
          template = "  {{ if .Unsupported }}{{ else }}{{ .Full }}{{ end }}<#7a7a7a> </>";
          type = "dotnet";
        }
        {
          background = "#2f2f2f";
          foreground = "#fafafa";
          properties = {
            time_format = "15:04:05";
          };
          style = "diamond";
          template = " {{ .CurrentDate | date .Format }}<#007ACC>  </>";
          trailing_diamond = "▓";
          type = "time";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#7a7a7a";
          style = "plain";
          template = " ~#@❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "right";
      segments = [
        {
          foreground = "#7a7a7a";
          style = "plain";
          template = " ❮ ";
          type = "text";
        }
        {
          foreground = "#FFCE5C";
          properties = {
            style = "roundrock";
            threshold = 500;
          };
          style = "plain";
          template = " {{ .FormattedMs }}<#fafafa>  </>";
          type = "executiontime";
        }
        {
          foreground = "#9FD356";
          foreground_templates = [
            "{{ if gt .Code 0 }}#E84855{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "  {{ if gt .Code 0 }}{{ .Code }}{{ end }} ";
          type = "status";
        }
      ];
      type = "rprompt";
    }
  ];
  console_title_template = "{{if .Root}}root :: {{end}}{{.Shell}} :: {{.Folder}}";
  final_space = true;
  transient_prompt = {
    background = "transparent";
    foreground = "#7a7a7a";
    template = " ~#@❯ ";
  };
  version = 3;
}
