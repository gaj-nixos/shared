{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#E36464";
          style = "plain";
          template = "@{{ .UserName }} ";
          type = "session";
        }
        {
          foreground = "#62ED8B";
          style = "plain";
          template = "➜";
          type = "text";
        }
        {
          foreground = "#56B6C2";
          properties = {
            style = "folder";
          };
          style = "plain";
          template = " {{ .Path }}";
          type = "path";
        }
        {
          foreground = "#D4AAFC";
          properties = {
            branch_icon = "";
          };
          style = "plain";
          template = " <#DDB15F>git(</>{{ .HEAD }}<#DDB15F>)</>";
          type = "git";
        }
        {
          foreground = "#DCB977";
          style = "plain";
          template = " ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  final_space = true;
  version = 3;
}
