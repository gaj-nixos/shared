{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#BF616A";
          style = "plain";
          template = "{{ .UserName }} ";
          type = "session";
        }
        {
          foreground = "#81A1C1";
          properties = {
            style = "full";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#6C6C6C";
          properties = {
            branch_ahead_icon = "<#88C0D0>⇡ </>";
            branch_behind_icon = "<#88C0D0>⇣ </>";
            branch_icon = "";
            fetch_stash_count = true;
            fetch_status = true;
            fetch_upstream_icon = true;
            github_icon = "";
          };
          style = "plain";
          template = "{{ .UpstreamIcon }}{{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}<#FFAFD7>*</>{{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
          type = "git";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      segments = [
        {
          foreground = "#A3BE8C";
          properties = {
            style = "austin";
          };
          style = "plain";
          template = " {{ .FormattedMs }} ";
          type = "executiontime";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#B48EAD";
          foreground_templates = [
            "{{ if gt .Code 0 }}#BF616A{{ end }}"
          ];
          properties = {
            always_enabled = true;
          };
          style = "plain";
          template = "❯ ";
          type = "status";
        }
      ];
      type = "prompt";
    }
  ];
  console_title_template = "{{if .Root}}(Admin){{end}} {{.PWD}}";
  transient_prompt = {
    foreground = "#B48EAD";
    foreground_templates = [
      "{{ if gt .Code 0 }}#BF616A{{ end }}"
    ];
    template = "❯ ";
  };
  version = 3;
}
