{ config, ... }:
let
  inherit (config.lib.stylix.colors.withHashtag)
    base00
    base01
    base02
    base03
    base04
    base05
    base06
    base07
    base08
    base09
    base0A
    base0B
    base0C
    base0D
    base0E
    base0F
    ;
  black = base00;
  darkestGray = base01;
  darkGray = base02;
  gray = base03;
  lightGray = base04;
  lightestGray = base05;
  white = base06;
  brightWhite = base07;
  red = base08;
  orange = base09;
  yellow = base0A;
  green = base0B;
  cyan = base0C;
  blue = base0D;
  purple = base0E;
  brown = base0F;

  defaultPalette = {
  };

  palette =
    if (config.lib ? stylix) then
      {
      }
    else
      defaultPalette;
in
{
  "$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json";

  blocks = [
    {
      alignment = "left";
      segments = [
        {
          foreground = "#15C2CB";
          properties = {
            style = "folder";
          };
          style = "plain";
          template = "{{ .Path }} ";
          type = "path";
        }
        {
          foreground = "#F141A8";
          properties = {
            branch_icon = " ";
          };
          style = "plain";
          template = "<#F3EFF5>on</> {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }} ";
          type = "git";
        }
        {
          foreground = "#5EADF2";
          style = "plain";
          template = "{{if .Version}}<#F3EFF5>is</>  {{.Version}} {{end}}";
          type = "project";
        }
        {
          foreground = "#44FFD2";
          properties = {
            fetch_version = true;
          };
          style = "plain";
          template = "<#F3EFF5>via</>  {{ .Full }} ";
          type = "node";
        }
        {
          foreground = "#FE4A49";
          style = "plain";
          template = "<#F3EFF5>and</>  {{.Full}} ";
          type = "npm";
        }
      ];
      type = "prompt";
    }
    {
      alignment = "left";
      newline = true;
      segments = [
        {
          foreground = "#FFE45E";
          style = "plain";
          template = "❯ ";
          type = "text";
        }
      ];
      type = "prompt";
    }
  ];
  version = 3;
}
