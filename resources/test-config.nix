{ config, ... }:
{
  system.stateVersion = "25.05";
  time.timeZone = "Europe/Paris";
  boot.loader.grub.devices = [ "/dev/sdx" ];

  age = {
    rekey = {
      hostPubkey = builtins.readFile ../resources/age/iso.age.pub;
      localStorageDir = ../. + "/secrets/rekeyed/${config.networking.hostName}";
    };
    secrets = {
      test-secret = {
        rekeyFile = ../secrets/test-secret.age;
        generator.script = "passphrase";
      };
    };
  };

  environment.etc."test-secret/path".source = config.age.secrets.test-secret.path;

  sharedConfig = {
    homeStateVersion = "25.05";
    flakeRoot = ../.;
    windowManagers = {
      i3 = false;
      hyprland = false;
      win10 = true;
    };
    disks.partitioning = "simple";
    network = {
      ssl.allowSsl11 = true;
      fail2ban.enable = true;
    };
    devices.amdCPU.enable = false;

    services.letsEncrypt = {
      enable = true;
      email = "a@b.c";
    };

    users = {
      "user1" = {
        username = "user1";
        email = "a@b.c";
        stylixUser.enable = true;
        ohMyPoshTheme = "0_gaj";
        software.brave.enable = true;
        windowManagers = {
          hyprland.enable = true;
          i3.enable = true;
          win10.enable = true;
        };
        bar = "polybar"; # "hyprpanel";
        dotnet-nuget.enable = true;
        software.guiPrograms.enable = true;
        software.swDevPrograms.enable = true;
      };
    };

    stylixGlobal.enable = true;
    services = {
      ollama.enable = true;
      elastic-search.enable = true;
      smbServer.enable = true;
      binary-cache.enable = true;
      dockerRegistry.enable = true;
      nginx = {
        enable = true;
        geoBlocking = {
          enable = true;
          allowedCountries = [
            "FR"
            "BE"
          ];
        };
      };
    };
    software = {
      dotnet.enable = true;
      advancedMonitoringTools.enable = true;
      btcRiskMetric.enable = true;
      cliImageTools.enable = true;
    };
    webApps = {
      cockpit.enable = true;
      gotify.enable = true;
      fail2web.enable = true;
      searx.enable = true;
      dashy.enable = true;
      airsonic.enable = true;
      glances.enable = true;
      openWebUi.enable = true;
      crowdsec.enable = true;
      vaultwarden = {
        enable = true;
        externalHost = "vaultwarden.example.com";
      };
      home-assistant = {
        enable = true;
        externalHost = "home-assistant.example.com";
        espHome = {
          enable = false;
          relayConfigFile = null;
        };
        zWaveJs.enable = true;
        mosquitto.enable = true;
      };
      silverbullet.enable = true;
      immich.enable = true;
      gonic.enable = true;
      paperless-ngx.enable = true;
      fireflyiii.enable = true;
      wallabag.enable = true;
      dendrite = {
        enable = true;
        externalHost = "dendrite.example.com";
      };
      sync-v3 = {
        enable = true;
        externalHost = "sync-v3.example.com";
      };
    };
    defaultRegistrySettings = {
      registry = "https://docker.pkg.github.com";
    };
  };
}
