{ rootPath, config, ... }:
let
  flakeRoot = rootPath;
in
{
  system.stateVersion = "25.05";
  time.timeZone = "Europe/Paris";

  age = {
    rekey = {
      hostPubkey = builtins.readFile ../resources/age/iso.age.pub;
      #localStorageDir = ../. + "/secrets/rekeyed/${config.networking.hostName}";
      localStorageDir = flakeRoot + "/secrets/rekeyed/${config.networking.hostName}";
    };
    secrets = {
      test-secret = {
        rekeyFile = flakeRoot + "/secrets/test-secret.age";
        generator.script = "passphrase";
      };
    };
  };

  environment.etc."test-secret/path".source = config.age.secrets.test-secret.path;
}
