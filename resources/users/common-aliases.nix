# Return a set of common shell aliases
{
  "cd.." = "cd ..";
  "ssn" = "sudo shutdown now";
  "sr" = "sudo reboot";
  "wget" = "wget -c"; # continue downloads
  "free" = "free -ht"; # human readable with total
  # "df" = "df -h"; # human readable
  "grep" = "grep --color=auto"; # colorize output
  "ls" = "lsd";
  "c" = "clear";
  "calc" = "bc -li";
  "mkdir" = "mkdir -pv"; # create parent directories, verbose mode
  "ping5" = "ping -c 5"; # ping 5 times
  "ports" = "netstat -tulanp"; # show listening ports

  ## shortcut  for iptables and pass it via sudo#
  ipt = "sudo /sbin/iptables";

  # display all rules #
  iptlist = "sudo iptables -L -n -v --line-numbers";
  iptlistin = "sudo iptables -L INPUT -n -v --line-numbers";
  iptlistout = "sudo iptables -L OUTPUT -n -v --line-numbers";
  iptlistfw = "sudo iptables -L FORWARD -n -v --line-numbers";
  firewall = "iptlist";

  "dnc" = "rm -rf **/bin **/obj && dotnet restore --no-cache && dotnet build";
}
