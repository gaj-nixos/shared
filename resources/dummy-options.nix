{ lib, ... }:

let
  inherit (lib) mkOption;
  inherit (lib.types) str;
in
{
  options = {
    networking.hostName = mkOption {
      type = str;
      default = "nixos";
      description = "Dummy value for documentation generation";
    };
  };

  config = {
    sharedConfig = {
      network = {
        adGuardHome.enable = true;
      };
      devices = {
        printing.enable = true;
      };
      services = {
        binary-cache.enable = true;
      };
      webApps = {
        vaultwarden.enable = true;
        home-assistant.enable = true;
        home-assistant.zWaveJs.enable = true;
        home-assistant.espHome.enable = true;
        home-assistant.mosquitto.enable = true;
        openWebUi.enable = true;
        glances.enable = true;
        searx.enable = true;
        silverbullet.enable = true;
        immich.enable = true;
        navidrome.enable = true;
        gonic.enable = true;
        airsonic.enable = true;
        gotify.enable = true;
        fail2web.enable = true;
        paperless-ngx.enable = true;
        fireflyiii.enable = true;
        changeDetectionIo.enable = true;
        wallabag.enable = true;
        cockpit.enable = true;
        crowdsec.enable = true;
        dashy.enable = true;
        tabby.enable = true;
      };
    };
  };
}
