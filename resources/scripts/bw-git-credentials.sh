#!/usr/bin/env bash

# Git-Credentials helper script

# Check if a command is available
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Ensure required commands are installed
for cmd in bw pass curl jq; do
  if ! command_exists $cmd; then
    echo "Error: $cmd is not installed." >&2
    exit 1
  fi
done

# Retrieve the Bitwarden server URL
VAULTWARDEN_URL=$(bw config server)
if [[ -z "$VAULTWARDEN_URL" ]]; then
  echo "Failed to retrieve Bitwarden server URL" >&2
  exit 1
fi

# Retrieve the API key using pass
get_api_key() {
  pass show "Bitwarden API key"
}

# Fetch credentials from Vaultwarden
get_credentials() {
  local api_key
  api_key=$(get_api_key)
  local response
  response=$(curl -S -L -H "Authorization: Bearer $api_key" "$VAULTWARDEN_URL/api/credentials/git") # FIXME: Update the endpoint to retrieve the correct password for the git repository with bw CLI

  if [[ "$(echo "$response" | jq -r '.status_code')" == "200" ]]; then
    local username
    username=$(echo "$response" | jq -r '.username')
    local password
    password=$(echo "$response" | jq -r '.password')
    echo "username=$username"
    echo "password=$password"
  else
    echo "Failed to retrieve credentials" >&2
    echo "$response" | jq
    return 1
  fi
}

# Main
if [[ "$1" == "get" ]]; then
  get_credentials
fi
