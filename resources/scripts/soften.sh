#!/usr/bin/env bash

# This will soften the security of these dotfiles, allowing
# the default unprivileged user with UID/GID of 1000 to edit ALL FILES
# in the dotfiles directory

# This mainly is just here to be used by some scripts

# After running this, YOUR UNPRIVILEGED USER CAN MAKE EDITS TO
# IMPORTANT SYSTEM FILES WHICH MAY COMPROMISE THE SYSTEM AFTER
# RUNNING nixos-rebuild switch!

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

SCRIPT_DIR="${1:-/etc/nixos}"

pushd "$SCRIPT_DIR" &> /dev/null || return;
chown :users . -R;
chmod g=rwX . -R;
popd &> /dev/null || return;
