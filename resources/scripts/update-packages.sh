#!/usr/bin/env bash

# This script is used to update the system packages to the latest versions
# usage: update-packages

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

INPUTS=(
  nixpkgs
  home-manager
  stylix
)

config_name=$(uname --nodename)

echo "***********************************************************"
echo "✨🚀  Updating all packages inputs for $config_name  🚀✨"
echo "***********************************************************"

pushd /etc/nixos || exit
git reset --hard
git pull

# if on master branch, create a new branch called update-packages to force update via merge request
if [ "$(git branch --show-current)" == "master" ]; then
  onMaster=true
else
  onMaster=false
fi

if [ "$onMaster" = true ]; then
  git branch update-packages || true
  git checkout update-packages
  git pull || true
  git push --set-upstream origin update-packages
  git pull || true
  git merge origin/master
fi

for input in "${INPUTS[@]}"; do
  nix flake update "$input"
  if [ ! $(git diff --exit-code --quiet) ]; then
    git add flake.lock || true
    git commit -m "⬆️ Update $input" || true
  fi
done

nix flake update
git add flake.lock || true
git commit -m "⬆️ Update packages" || true
git push

# display the diff
nixos-rebuild build  --flake .# && nvd diff /run/current-system result
rm -rf ./result

if [ "$onMaster" = true ]; then
  dump-config /etc/nixos
  git checkout master
  echo "👆 Inputs updated in the update-packages branch, please create a merge request to apply the changes in a new OS generation"
  popd || exit
  exit 0
fi

set -a  # Automatically export all variables
if [ -f .env ]; then
  source .env
fi
set +a  # Stop automatic exporting

currentGeneration=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

# tolerate errors
set +e
nixos-rebuild switch --accept-flake-config --flake .# |& nom
set -e

newGeneration=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

dump-config /etc/nixos

if [ "$currentGeneration" == "$newGeneration" ]; then
  echo "🚫 No new generation was created"
  popd || exit
  exit 0
fi

tag-generation

popd || exit
