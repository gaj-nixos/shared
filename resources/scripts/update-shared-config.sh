#!/usr/bin/env bash

# This script is used to update the system packages to the latest versions
# usage: update-shared-config

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

config_name=$(uname --nodename)

echo "*********************************************************"
echo "** ✨🚀  Updating our libraries for $config_name  ✨🚀 "
echo "*********************************************************"

pushd /etc/nixos || exit
git reset --hard
git pull

# if current branch is protected, create a new branch update-libraries
if [ "$(git branch --show-current)" == "master" ]; then
  onMaster=true
else
  onMaster=false
fi

if [ "$onMaster" = true ]; then
  git branch update-libraries || true
  git checkout update-libraries
  git pull || true
  git push --set-upstream origin update-libraries
  git pull || true
  git merge origin/master
fi

nix flake update sharedConfig
git add flake.lock || true
git commit -m "⬆️ Update Shared Config" || true

set +e
nix flake update aisinConfig
git add flake.lock || true
git commit -m "⬆️ Update Aisin Config" || true
git push
set -e

if [ "$onMaster" = true ]; then
  dump-config /etc/nixos
  git checkout master
  echo "👆 Inputs updated in the update-libraries branch, please create a merge request to apply the changes in a new OS generation"
  popd || exit
  exit 0
fi

# display the diff
nixos-rebuild build  --flake .# && nvd diff /run/current-system result
rm -rf ./result

set -a  # Automatically export all variables
if [ -f .env ]; then
  source .env
fi
set +a  # Stop automatic exporting

currentGeneration=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

# tolerate errors
set +e
nixos-rebuild switch --accept-flake-config --flake .# |& nom
set -e

newGeneration=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

dump-config /etc/nixos

if [ "$currentGeneration" == "$newGeneration" ]; then
  echo "🚫 No new generation was created"
  popd || exit
  exit 0
fi

tag-generation

popd || exit
