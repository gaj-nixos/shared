#!/usr/bin/env bash

set -e -u -o pipefail -o nounset # Exit immediately if a command exits with a non-zero status, undefined variable, or pipefail

# print error messages
error() {
    echo "❌ $1" >&2
}

# Input argument for snapshot base directory
SNAPSHOT_DIR="${1:-/.snapshots}"

echo "🧹 Pruning old snapshots in $SNAPSHOT_DIR"

# Helper function: Get the first snapshot of a period
get_first_snapshot_of_period() {
    local period="$1"
    find "$SNAPSHOT_DIR" -maxdepth 1 -type d -name "snapshot_*" \
        | sort \
        | awk -F 'snapshot_' -v period="$period" '
        {
            date = $2
            split(date, parts, "-")
            year = parts[1]
            month = parts[2]
            day = parts[3]
            if (period == "week") {
                week = strftime("%U", mktime(year " " month " " day " 0 0 0"))
                key = year "-" week
            } else if (period == "month") {
                key = year "-" month
            }
            if (!(key in seen)) {
                seen[key] = $0
                print $0
            }
        }'
}

# Retention logic
current_date=$(date +"%Y-%m-%d")
current_week=$(date +"%U")
current_year=$(date +"%Y")

echo "************************"
echo "📅 Current date: $current_date"
echo "📅 Current week: $current_week"
echo "📅 Current year: $current_year"

# Gather snapshots
all_snapshots=$(find "$SNAPSHOT_DIR" -maxdepth 1 -type d -name "snapshot_*" | sort)

current_week_snapshots=$(find "$SNAPSHOT_DIR" -maxdepth 1 -type d -name "snapshot_${current_year}-$(date +"%m")-*" | sort | uniq)

# Retain snapshots of the current week
retain_snapshots="$current_week_snapshots"
retain_snapshots=$(echo "$retain_snapshots" | sed '/^\s*$/d')

# Retain the first snapshot of each week for the previous 3 weeks
retain_snapshots+="
$(get_first_snapshot_of_period "week" | tail -n +2 | head -n 3)"
retain_snapshots=$(echo "$retain_snapshots" | sed '/^\s*$/d')

# Retain the first snapshot of each month for the previous 6 months
retain_snapshots+="
$(get_first_snapshot_of_period "month" | tail -n 6)"
retain_snapshots=$(echo "$retain_snapshots" | sed '/^\s*$/d')
retain_snapshots=$(echo "$retain_snapshots" | sort | uniq)

# Clean up the list of snapshots to retain
if [[ -z "$(echo "$retain_snapshots" | sed '/^\s*$/d')" ]]; then
    echo "🚫 No snapshots to retain"
    delete_snapshots="$all_snapshots"
else
    echo "🔍 Filtering and sorting the list of snapshots to retain"
    delete_snapshots=$(comm -23 <(echo "$all_snapshots" | sort) <(echo "$retain_snapshots" | sort))
fi

# exit if there are no snapshots
if [[ -z "$all_snapshots" ]]; then
    echo "⚠️ No snapshots found"
    exit 0
fi

for snapshot in $all_snapshots; do
    # check in delete_snapshots
    if echo "$delete_snapshots" | grep -qx "$snapshot"; then
        echo "❌ $snapshot"
    else
        echo "✅ $snapshot"
    fi
    if ! echo "$retain_snapshots" | grep -qx "$snapshot"; then
        echo "❌ $snapshot"
    else
        echo "✅ $snapshot"
    fi
done

if [[ -z "$delete_snapshots" ]]; then
    echo "⚠️ No snapshots to delete"
    exit 0
fi

# ask user to continue
read -r -p "🚨 Do you want to delete the snapshots? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
    echo "🗑️ Deleting snapshots"
    for snapshot in $delete_snapshots; do
        echo "🗑️ $snapshot"
        sudo btrfs subvolume delete "$snapshot"
    done
else
    echo "🚫 Not deleting snapshots"
fi
