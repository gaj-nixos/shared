#!/usr/bin/env nix-shell
#!nix-shell -i bash -p yq
# shellcheck shell=bash

# This script converts all JSON or YAML files in the current directory to Nix files

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

rm -f *.nix

for yaml_file in *.yaml; do
  # Skip if no YAML files exist
  [[ -e "$yaml_file" ]] || break

  json_file="${yaml_file%.yaml}.json"

  cat $yaml_file | yq > $json_file

  echo "Converted: $yaml_file -> $json_file"
done

for json_file in *.json; do
  # Skip if no JSON files exist
  [[ -e "$json_file" ]] || break

  nix_file="${json_file%.json}.nix"

  printf "%s\n%s\n" "$json_file" "$nix_file" | nix run github:sempruijs/json2nix

  echo "Converted: $json_file -> $nix_file"
done
