#!/usr/bin/env nix-shell
#!nix-shell -i bash -p git
# shellcheck shell=bash

# This script formats a system using a specific flake configuration.
# Usage: sudo format-with-disko <disk> <disko-config.nix> (e.g., sudo format-with-disko sdd disko-config.nix)

set -o errexit  # Exit on error
set -o nounset  # Treat unset variables as an error
set -o pipefail # Consider errors in a pipeline

log() {
  echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

error_exit() {
  log "Error: $1"
  exit 1
}

MAIN_DISK=$1
CONFIG_FILE=$2

# Ensures the script is executed with root privileges.
if [ "$(id -u)" -ne 0 ]; then
  error_exit "This script must be run with sudo or as root."
fi

get_disk_devices() {
  echo ""
  echo "Listing available disks:"
  lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

  read -rp "Main disk: enter the disk device to use (without the /dev/ prefix) (default: sdd, example: sda): /dev/" MAIN_DISK
  echo ""
  if [[ -z "$MAIN_DISK" ]]; then
    MAIN_DISK=sdd # Default disk
    echo "Defaulting to /dev/$MAIN_DISK"
  fi
  MAIN_DISK="/dev/$MAIN_DISK"

  echo "Selected disk(s): $MAIN_DISK"
}

delete_partition_on_disk() {
  disk=$MAIN_DISK
  echo ""
  read -rn 1 -p "Delete all partitions on the disk $disk? (y/N)" DELETE_PARTITIONS
  echo ""
  if [[ $DELETE_PARTITIONS =~ ^[Yy]$ ]]; then
    sudo umount "$disk" || true

    # Erase any previous MBR bootloader and partition table
    sudo dd if=/dev/zero of="$disk" bs=512 count=1
    read -rn 1 -p "Press enter to wipe the file system on $disk..." CONTINUE
    wipefs -a "$disk"
  fi
}

format_mount_partitions() {
  echo ""
  echo "Main disk:"
  echo "1. Format, partition and mount"
  echo "2. Mount existing partition"
  read -rn 1 -p "Enter your choice: " choiceDisk1
  echo ""

  DISKO_CONFIG_MAIN=$CONFIG_FILE
  if [[ ! -f "./$DISKO_CONFIG_MAIN" ]]; then
    error_exit "Disko configuration file not found: $DISKO_CONFIG_MAIN"
  fi

  if [[ $choiceDisk1 == 1 ]]; then
    format_mount_partitions_with_disko "disko" "$DISKO_CONFIG_MAIN"
  else
    read -rn 1 -p "Press enter to mount all partitions..." CONTINUE
    format_mount_partitions_with_disko "mount" "disko-config.nix"
  fi

  echo ""
}

format_mount_partitions_with_disko() {
  DISKO_CMD=$1
  DISKO_FILE=$2
  echo ""
  echo "Partitioning with disko (command = $DISKO_CMD)..."
  echo "sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode $DISKO_CMD ./$DISKO_FILE"

  while true; do
    sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode $DISKO_CMD "./$DISKO_FILE" || true
    echo "Listing available disks:"
    lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

    echo "Disko file used: $DISKO_FILE"
    read -rn 1 -p "Please check all partitions are mounted. If not, press Y to run disko again (y/N)" RETRY_DISKO
    echo ""
    if [[ ! $RETRY_DISKO =~ ^[Yy]$ ]]; then
      break
    fi
  done
}

get_disk_devices
delete_partition_on_disk
format_mount_partitions
