#!/usr/bin/env bash

# This will harden the security of these dotfiles, preventing
# unprivileged users from editing system-level (root configuration)
# files maliciously

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

if [ "$#" = 1 ]; then
  SCRIPT_DIR=$1;
else
  SCRIPT_DIR="/etc/nixos"
fi
pushd "$SCRIPT_DIR" &> /dev/null || return;
chown :0 . -R;
chmod g=rX . -R;
popd &> /dev/null || return;
