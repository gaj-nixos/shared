#!/usr/bin/env bash

# This script is used to open a shell with the OpenSSL 1.1.1 environment
# usage: shell-open-ssl-1_1

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset


BASE_DIR=/etc/nixos
nix develop $BASE_DIR/#openssl_1_1
