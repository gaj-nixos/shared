#!/usr/bin/env bash

# This script is used to modify all instances of a given password in a user's Bitwarden vault.

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

OVERRIDEN_GIT_USER_EMAIL=""
OLD_PASSWORD=""
NEW_PASSWORD=""

########## User ##########

function get_user_email() {
  if [ -n "$OVERRIDEN_GIT_USER_EMAIL" ]; then
    echo "$OVERRIDEN_GIT_USER_EMAIL"
  else
    getent passwd "$USER" | cut -d: -f5 | cut -d, -f2
  fi
}

########## Utilities ##########

function normalize_url() {
  # Converts URL to lowercase and strips protocol and trailing slashes
  echo "$1" | awk '{gsub(/https:\/\//, ""); gsub(/http:\/\//, ""); gsub(/\/$/, ""); print tolower($0)}'
}

function remove_quotes() {
  echo "${1//\"}"
}

########## Bitwarden ##########

function generate_password() {
  length=$1
  bw generate --special --lowercase --uppercase --ambiguous --number --length "$length"
}

function update_bitwarden_password_by_id() {
  itemId=$1
  password=$2
  echo "Updating password in Bitwarden..."
  bw get item "$itemId" | \
    jq --arg password "$password" '.login.password=$password' | \
    bw encode | bw edit item "$itemId" 1&>/dev/null
  echo "Password updated successfully!"
}

function update_all_entries_with_password() {
  read -p "Enter the old password: " -r -s OLD_PASSWORD
  echo ""
  # ask the user if they want to generate a new password or re-use one they will provide
  read -p "Do you want to generate a new password? [y/N]" -n 1 -r generatePassword
  echo ""
  if [[ $generatePassword =~ ^[Yy]$ ]]; then
    read -p "Enter the length of the new password: " -r length
    NEW_PASSWORD=$(generate_password "$length")
    echo "The new password is: $NEW_PASSWORD"
  else
    read -p "Enter the new password: " -r -s NEW_PASSWORD
    echo ""
  fi
  ids=$(bw list items | jq --arg password "$OLD_PASSWORD" '.[] | select(.login.password == $password) | .id')
  for id in $ids; do
    id=$(remove_quotes "$id")
    bw get item "$id" | jq
    read -p "Do you want to update the password for the item with id $id? [y/N]" -n 1 -r updatePassword
    echo ""
    if [[ ! $updatePassword =~ ^[Yy]$ ]]; then
      continue
    fi
    update_bitwarden_password_by_id "$id" "$NEW_PASSWORD"
    bw get item "$id" | jq '.login.password'
  done
}

function initialize_vaultwarden() {
  echo "Setting up Bitwarden server"
  configuredServer=$(bw config server | grep -oP 'https://\K[^"]+')
  configuredServer=$(normalize_url "$configuredServer")
  echo "Configured server: $configuredServer"
  if [ "$configuredServer" = "bitwarden.com" ]; then
    echo "Vaultwarden setup"
    read -p "Enter the Vaultwarden server name (without https://)" -r server
    bw config server "https://$server"
  fi

  # login & sync
  if [ -z "${BW_SESSION:-}" ]; then
    echo "Logging in to Bitwarden with $(get_user_email)"
    bw logout || true # ignore errors if the user is not logged in
    read -p "Please enter Bitwarden password for $(get_user_email): " -r -s BW_PASSWORD
    bwSession=$(bw login "$(get_user_email)" "$BW_PASSWORD" --raw)
    echo "BW_SESSION: $bwSession"
    export BW_SESSION=$bwSession
  fi
  bw sync
}

########## Main ##########

echo "***  Post-install security setup for $USER (Bitwarden login: $(get_user_email))  ***"
echo ""

# Display a menu to choose the security setup
while true; do
  echo "******************************"
  PS3="Please select the security setup: "
  options=("Override BW login" "Bitwarden login" "Update a password in Bitwarden" "Quit")
  select opt in "${options[@]}"
  do
    case $opt in
      "Override BW login")
        read -p "Enter the new BW login email: " -r OVERRIDEN_GIT_USER_EMAIL
        echo ""
        break
        ;;
      "Bitwarden login")
        initialize_vaultwarden
        break
        ;;
      "Update a password in Bitwarden")
        update_all_entries_with_password
        break
        ;;
      "Quit")
        break 2
        ;;
      *) echo "invalid option $REPLY";;
    esac
  done
done
