#!/usr/bin/env bash

# This script is used to update the system packages to the latest versions
# usage: vpn <username> <server>

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

openconnect --protocol=gp --user="$1" --server="$2"
