#!/usr/bin/env bash

# This script is used to update the system with the latest configuration defined in this git repository
# usage: update-config

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

config_name=$(uname --nodename)

echo "**************************************************"
echo "✨🚀  Applying configuration $config_name  ✨🚀 "
echo "**************************************************"

pushd /etc/nixos || exit

git reset --hard
git pull

# display the diff
nixos-rebuild build  --flake .# && nvd diff /run/current-system result
rm -rf ./result

set -a  # Automatically export all variables
if [ -f .env ]; then
  source .env
fi
set +a  # Stop automatic exporting

currentGeneration=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

# tolerate errors
set +e
nixos-rebuild switch --accept-flake-config --flake .# |& nom
set -e

newGeneration=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

dump-config /etc/nixos

if [ "$currentGeneration" == "$newGeneration" ]; then
  echo "🚫 No new generation was created"
  popd || exit
  exit 0
fi

tag-generation

popd || exit
