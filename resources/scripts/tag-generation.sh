#!/usr/bin/env bash

# This script is used to create a git tag with the current generation number

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

echo "******************************************"
echo "**  Creating generation tag 🔖"
echo "******************************************"

pushd /etc/nixos || exit

HOSTNAME=$(hostname)

# Find the highest generation number from the system profiles
GENERATION_NUMBER=$(ls -l /nix/var/nix/profiles/system-* | awk -F'-' '{print $2}' | sort -n | tail -1)

TAG="${HOSTNAME}-${GENERATION_NUMBER}"

# Check if the current commit already has the same tag
if git rev-list --tags --max-count=1 | grep -q "$(git rev-parse HEAD)"; then
  EXISTING_TAG=$(git describe --tags --exact-match "$(git rev-parse HEAD)" 2>/dev/null)

  if [[ "$EXISTING_TAG" == "$TAG" ]]; then
    echo "✅ The current commit already has the tag '$TAG'. No new tag is needed."
    popd || exit 0
    exit 0
  fi
fi

# Check if the tag already exists in Git
if git rev-parse "$TAG" >/dev/null 2>&1; then
  # If the tag exists, append a suffix to ensure uniqueness
  SUFFIX=1
  NEW_TAG="${TAG}-${SUFFIX}"

  # Loop to find a unique tag
  while git rev-parse "$NEW_TAG" >/dev/null 2>&1; do
    SUFFIX=$((SUFFIX + 1))
    NEW_TAG="${TAG}-${SUFFIX}"
  done

  TAG=$NEW_TAG
fi

git tag "$TAG"

git push origin --tags

echo "🔖 Created tag: $TAG"
popd || exit
