#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status
set -e

FLAKE=${1:-"iso"}
rm -rf ./result || true
#nix build .#nixosConfigurations.iso.config.system.build.isoImage

rm -rf ./iso || true
#nixos-generate --flake .#$FLAKE --format raw-efi --system x86_64-linux -o ./iso
#nixos-generate --flake .#$FLAKE --format iso --system x86_64-linux -o ./iso
nixos-generate --flake .#$FLAKE --format install-iso --system x86_64-linux -o ./iso

echo "The ISO for $FLAKE is in ./iso directory. You can flash it to a USB drive with flash-iso /dev/sdX"
ls ./iso
