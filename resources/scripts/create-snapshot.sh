#!/usr/bin/env nix-shell
#!nix-shell -i bash -p realpath tr date btrfs
# shellcheck shell=bash

# Creates a read-only snapshot of a btrfs volume
# Usage: create-snapshot <subvolume-path> [message]
# Example: create-snapshot /srv "Before system update"

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

RP=$(which realpath)
TR=$(which tr)
DT=$(which date)
BTR=$(which btrfs)
MD=$(which mkdir)

SNAPSHOT_DIR="/.snapshots"

PATH=${1:-"/srv"}
SUBVOLUME_PATH=$($RP "$PATH")
MSG=${2:-"Manual snapshot"}

# sanitize message
MSG=$(echo "$MSG" | $TR -cd '[:alnum:]')

TIMESTAMP=$($DT +%Y-%m-%d_%H-%M-%S)

SNAPSHOT_DST="$SNAPSHOT_DIR$SUBVOLUME_PATH"
NAME=$"snapshot_${TIMESTAMP}_${MSG}"

$MD -p "$SNAPSHOT_DST"

echo "📸 Creating snapshot of $SUBVOLUME_PATH at $SNAPSHOT_DST/$NAME"

# Create a read-only snapshot of the subvolume
$BTR subvolume snapshot -r "$SUBVOLUME_PATH" "$SNAPSHOT_DST/$NAME"
# btrfs subvolume snapshot -r /srv /.snapshots/snapshot_2021-08-01_12-00-00_Before_system_update

echo "✅ Snapshot created at $SNAPSHOT_DST/$NAME"
