#!/usr/bin/env bash

# Generate all the secrets, either with their generators or with the user's input.
# Usage: update-secrets
# Display links to any websites to be updated / to retrieve API keys from

# Exit immediately if a command exits with a non-zero status
set -e

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

pushd /etc/nixos || exit

# Generate the secrets

# if current branch is protected, create a new branch update-libraries
if [ "$(git branch --show-current)" == "master" ]; then
  onMaster=true
else
  onMaster=false
fi

if [ "$onMaster" = true ]; then
  git branch update-libraries || true
  git checkout update-libraries
  git pull || true
  git push --set-upstream origin update-libraries
  git pull || true
  git merge origin/master
fi

echo "Generating secrets..."
agenix generate -a -f
agenix rekey

git add ./secrets/* || true
git commit -m "🔒️ Generate secrets" || true

git push || true


if [ "$onMaster" = true ]; then
  git checkout master
  echo "👆 Secrets updated in the update-libraries branch, please create a merge request to apply the changes in a new OS generation"
  popd || exit
  exit 0
fi

popd || exit
