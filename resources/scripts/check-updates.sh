#!/usr/bin/env bash

# This script is used to check for any updates to the system
# usage: check-updates

#This script assumes your flake is in ~/gaj-nixos-config and that your flake's nixosConfigurations is named the same as your $host
pushd ~/gaj-nixos-config || exit

git reset origin/main --hard && \
git pull && \
nix flake update && \
nix build .#nixosConfigurations.$(uname --nodename).config.system.build.toplevel

updates="$( \
    nvd diff /run/current-system ./result | grep -e '\[U'
)"

rm -rf ./result
git reset origin/main --hard

nb_updates=$(echo $updates | wc -l)

echo "🔄 $nb_updates update(s) available"
echo $updates


alt="has-updates"
# if updates contains "Already up to date."
if [[ $nb_updates == *"Already up to date."* ]]; then
    alt="updated"
    tooltip="System updated"
else
	tooltip=$(echo $updates | awk '{ for (i=3; i<NF; i++) printf $i " "; if (NF >= 3) print $NF; }' ORS='\\n' )
fi

popd || exit

echo "{ \"text\":\"$nb_updates\", \"alt\":\"$alt\", \"tooltip\":\"$tooltip\" }"
