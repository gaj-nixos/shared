#!/usr/bin/env bash

# This script is used to build the system configuration to check for errors
# usage: check-config

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

pushd /etc/nixos || exit
git pull

set -a  # Automatically export all variables
if [ -f .env ]; then
  source .env
fi
set +a  # Stop automatic exporting

nixos-rebuild build  --flake .# && nvd diff /run/current-system result
rm -rf ./result

popd || exit
