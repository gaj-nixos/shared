#!/usr/bin/env bash

# This script is used to cleanup the system and rebuild the system configuration
# usage: cleanup

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

echo "🧹 Collecting garbage"
nix-collect-garbage -d

echo "🔧 Optimizing the store"
nix-store --optimise

echo "🧼 Cleaning up podman images"
podman image prune -a -f

echo "🔍 Verifying the system"
nix-store --verify --check-contents --repair
