#!/usr/bin/env bash

# This script is used to create a gitlab merge request
# usage:
# - merge the current branch to a target: glmr <assignee> <target-branch-name>
# - merge any 2 branches: glmr <assignee> <source-branch-name> <target-branch-name>
# - merge develop to beta and beta to master: glmr deploy <assignee_beta> <assignee_master>
# - merge the current branch to develop, develop to beta and beta to master: glmr full <assignee_develop> <assignee_beta> <assignee_master>

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

assignee=$1
source_branch=${2:-""}
target_branch=${3:-""}

# if argument 1 is "deploy", argument 2 and 3 are the users to assign the MRs to beta and master to
if [ "$assignee" == "deploy" ]; then
  assignee1=$2
  assignee2=$3
  glab mr create --assignee "$assignee1" --source-branch develop --target-branch beta --title "Develop to Beta" --description "" --yes
  glab mr create --assignee "$assignee2" --source-branch beta --target-branch master --title "Beta to Production" --description "" --yes
elif [ "$assignee" == "full" ]; then
  assignee1=$2
  assignee2=$3
  assignee3=$4
  glab mr create --assignee "$assignee1" --fill --fill-commit-body --remove-source-branch --yes --target-branch develop
  glab mr create --assignee "$assignee2" --source-branch develop --target-branch beta --title "Develop to Beta" --description "" --yes
  glab mr create --assignee "$assignee3" --source-branch beta --target-branch master --title "Beta to Production" --description "" --yes
else
  if [ -z "$target_branch" ]; then
    if [ -z "$source_branch" ]; then
      target_branch="develop"
    else
      target_branch=$2
    fi
    glab mr create --assignee "$assignee" --fill --fill-commit-body --remove-source-branch --yes --target-branch "$target_branch"
  else
    glab mr create --assignee "$assignee" --source-branch "$source_branch" --target-branch "$target_branch" --title "$source_branch to $target_branch" --description "" --yes
  fi
fi
