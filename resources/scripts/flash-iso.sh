#!/usr/bin/env bash

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "Please run with sudo"
  exit 1
fi

OF=${1:-"/dev/sda"}
ISO=$(find ./result/iso -name "nixos-*.iso" | sort | tail -n 1)

if [ -z "$ISO" ]; then
  echo "ISO not found"
  exit 1
fi

lsblk

read -rn 1 -p "Flash $ISO to $OF? [y/N] " REPLY
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  echo "Aborted"
  exit 1
fi

dd status=progress if=$ISO of=$OF
sync
