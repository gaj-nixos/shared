#!/usr/bin/env bash

# This script is used to dump the shared library config of each config_name to a file,
# and list all installed programs and versions, system wide and per user

# usage: dump-config [DIR] [config_name] [dumpConfig]

DIR=${1:-"/etc/nixos"}
config_name=${2:-$(uname --nodename)}
dumpConfig=${3:-"Yes"}

echo "******************************************"
echo "**  Dumping configuration $config_name"
echo "******************************************"

if [ -d "$DIR" ]; then
  pushd $DIR || exit
fi

export NIXPKGS_ALLOW_INSECURE=1

git fetch origin

# if current branch is protected, create a new branch update-dumps
if [ "$(git branch --show-current)" == "master" ]; then
  onMaster=true
else
  onMaster=false
fi

if [ "$onMaster" = true ]; then
  git branch update-dumps || true
  git checkout update-dumps
  git pull || true
  git push --set-upstream origin update-dumps
  git pull || true
  git merge origin/master
fi

DUMPS_DIR="$DIR/config-dumps"

# delete any subdirectories, but keep any files directly in the directory
#find $DUMPS_DIR -mindepth 1 -maxdepth 1 -type d -exec rm -rf {} \;

echo $DUMPS_DIR

mkdir -p $DUMPS_DIR

echo "📄 Dumping $config_name"

# define the sharedConfig path
sharedConfigPath="nixosConfigurations.$config_name.config.sharedConfig"
aisinConfigPath="nixosConfigurations.$config_name.config.aisinConfig"

D="$DUMPS_DIR"

if [ ! -d "$D" ]; then
  echo "📁 Creating $D"
  mkdir -p "$D"
fi

# dump the sharedConfig and aisinConfigs to a file
echo "📂 Dumping sharedConfig"
nix eval .#$sharedConfigPath --json | jq > $D/$config_name.json

# tolerate errors
set +e
aisinConfig=$(nix eval .#$aisinConfigPath --json 2>/dev/null)
if [ $? -eq 0 ]; then
  echo "📂 Dumping aisinConfig"
  echo "$aisinConfig" | jq >> $D/$config_name.json
else
  echo "❌ Failed to dump aisinConfig"
fi
set -e

# replace any secrets from $config_name.json by their environment variable names
ENV_FILE="$DIR/.env"
JSON_FILE="$D/$config_name.json"

if [ -f "$ENV_FILE" ]; then
  # Read each line of the .env file
  while IFS='=' read -r key value; do
      # Check if the line contains a valid key-value pair
      if [[ ! -z "$key" && ! -z "$value" ]]; then
          # Escape special characters in the value for use in sed
          escaped_value=$(printf '%s\n' "$value" | sed -e 's/[\/&]/\\&/g')

          # Replace occurrences of the value in the text file with $key
          sed -i "s/${escaped_value}/\$$key/g" "$JSON_FILE"
      fi
  done < "$ENV_FILE"
fi

if [[ "$dumpConfig" =~ ^([yY][eE][sS]|[yY])$ ]]; then

  echo "📥 Committing dumps"

  git add "$DUMPS_DIR/$config_name.json" || true
  git commit -m "📝 Update configuration dumps" || true
  git push origin
fi

if [ "$onMaster" = true ]; then
  git checkout master
fi

if [ -d "$DIR" ]; then
  popd || exit
fi
