#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status
set -e

rm -rf ./result || true
rm -rf ./iso || true

nixos-generate --flake .#iso --format install-iso --system x86_64-linux -o ./iso

echo "The ISO is in ./iso directory"
ls ./iso
