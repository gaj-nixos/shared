#!/usr/bin/env bash
# Fetch Cloudflare IP lists

set -e          # Exit on error
set -u          # Exit on unset variable
set -o pipefail # Exit on pipe error
set -o errexit
set -o nounset

echo "🔄 Updating tags"
nix-doc tags >docs/options.md
git add tags || true
git commit -m "📝 Update tags" || true

echo "🔄 Updating ./docs/options.md"
nix build .#options-doc && cat result > ./docs/options.md && rm -f result
git add ./docs/options.md || true
git commit -m "📝 Update options.md" || true
