# ❄️ NixOS

`NixOS` is a GNU/Linux distribution, that is set-up and configured *declaratively* rather than *imperatively*, via `.nix` files and a few `nix*` commands, allowing reproduceability, unbreakability, traceability and auditability.

Every aspect of the OS is exclusively configured in this way. No more package-manager commands, configuration commands, and modifying `/etc/*`, `~/.config/*` and other dotfiles.

User configurations are managed similarly with the `home-manager` add-on.

In addition to using a basic `configuration.nix` file, there is the concept of Nix Flakes, which generates a `flake.lock` file that sets the exact version of the packages in use when the system was last created or updated.

NixOS uses `systemd` as init / system management.

> The project started in 2002 as a PhD thesis and first released in 2006. 2022 marked a sharp rise in its popularity.

## Advantages

- reproduceability: the OS and user environments can be exactly cloned on another machine. Existing configurations can be used as a base for new ones
- unbreakability: most errors will be caught when building the new environment. The boot menu allows booting on a history of prior generations
- traceability: every change is committed to Git, allowing peer-reviews, history management, diff viewing, etc.
- auditability: due to the `declarative` paradigm, every aspect of the configuration can be inspected by reading the repo
- stable and bleeding-edge package sources can be easily used simultaneously
- deployments can be done with CI jobs

## Disadvantages

- the `Linux File Hierarchy Standard` is not respected. Packages are installed in a specific software archive, which can potentially break some programs.
- compiling some software with the 'classic' `./make` won't work.
- steep learning curve (but AI is a great help!)
- Nix build errors are often cryptic / unhelpful (don't modify too much between 2 test builds!)
- higher disk occupation

## Out-of-scope

Files locally written by a user or a process (general data) aren't managed by NixOS.

## Links

- [manual](https://nixos.org/manual)
- [community wiki](https://nixos.wiki/wiki)
- [packages](https://search.nixos.org/packages)
- [home-manager configuration](https://nix-community.github.io/home-manager/options.xhtml)
- [home-manager options](https://home-manager-options.extranix.com/)

## Usual commands

```sh
cd /etc/nixos
sudo git pull
```

### Apply the latest configuration

```sh
sudo nixos-rebuild switch --flake .#<system>
```

> replace `<system>` with the name of the desired system as defined in the `flake.nix` file

### Check a flake for errors

```sh
sudo nixos-rebuild build --flake .#<system>
```

### Update packages & flake lock file

```sh
sudo nix flake update
sudo git add flake.lock
sudo git commit -m "Update packages"
sudo git push
sudo nixos-rebuild switch --flake .#<system>
```

### Cleanup

```sh
sudo nix-collect-garbage -d
sudo nix-store --optimise
```
