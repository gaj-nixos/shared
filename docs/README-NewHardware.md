# New hardware detection

Only if running for the first time on an unknown hardware (for VM Ware workstation install, skip this step) - generate and commit the hardware config:

```sh
sudo commit-hw-config server master # add the auto-generated machine config to the git repository, if not yet defined
```

> Replace the UUID disk references by their label to make the hardware configuration generic and update the configurations in `flake.nix` as needed
