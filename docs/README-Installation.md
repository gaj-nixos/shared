# Installation

## Local setup on VMWare Workstation

This NixOS flake can also be locally installed and run on VMWare Workstation (or QEmu) VMs, for validations before deployment to production.

### Keyboard layout

To set the keyboard layout the `be` during installation, enter this command:

```sh
sudo loadkeys be-latin1
```

On an `azerty` layout, this becomes:

```sh
sudo loqdkeys be)lqtin&
```

### Virtual Machine setup

- prepare a USB stick containing: your `.git-credentials` file, and the age key for the target machine if pre-existing (located in `/etc/age/key.txt` of any pre-existing machine that would need to be re-installed) in directory `age/key.txt` of the USB stick
- generate an ISO image with the target flake's `./build-iso.sh` script
- Open VM Ware Workstation, select `create a new machine`
- Select `installer disc image file (iso)` and browse to the `nixos-minimal` ISO file you downloaded
- Select `Linux` / `Other Linux 6.x kernel 64-bit`
- Enter a name and location for the VM
- disk size: **at least** 50 GiB for the initial OS install + extra space for the data + the swap partition.
- select `customize hardware`
- set memory as desired (51GiB)
- set processor count as desired (12)
- network: NAT
- sound card: check `connect at power on`
- `close` && `finish`
- with `windows explorer`, navigate to where you chose the install the VM
- open the `.vmx` file in a text editor
- add the following lines, then save and close
  - `firmware = "efi"`
  - `mouse.vusb.enable = "TRUE"`
  - `mouse.vusb.useBasicMouse = "FALSE"`
- power on the VM with the installation ISO and the USB stick.
- in the installation ISO's boot menu, press `enter` to select the first option
- make sure that `lsblk` shows the disks in the correct order, and that their names match the definition in the flake's Disko setup (under the `hardware/<hostname>` directory). If not, reboot the VM / adapt the Disko setup
- Mount the USB stick and run `sudo install-creds /dev/sdXY`, replacing `sdXY` with the stick's actual device as shown by `lsblk`
- run `sudo install-??? <flake_name> <flake_branch>` (replace ??? with the actual install script provided by the ISO)
- copy the age key to the target machine (`sudo cp -raf /etc/age/key.txt /mnt/etc/age/key.txt`)
- delete the secrets on the USB stick

> The script will list the available block devices (ex: `sda`) and prompt which one to partition (ex: `/dev/vda`) (default: `/dev/sda`)

Only if running for the first time on an unknown hardware (for VM Ware workstation install, skip this step) - generate and commit the hardware config. [See New Hardware README.md](./README-NewHardware.md).

The repository will be cloned in the target's `/etc/nixos` directory.

> Check for any errors in the output when prompted to continue or break

### Post-installation

#### Passwords

Run `post-install` to install secrets

Users can change their default user account password (`nixos`) with the `passwd` command.

#### Time sync workaround

To ensure correct time, Time-sync must be enabled **while the VM is running**:

- click on `Player` in VMWare workstation
- select `manage`, open `virtual machine settings` then `options` tab, select the `VMWare tools` section
- check `synchronize guest time`
- close the `settings` window
- in the VM's terminal, type `date` to check the correct time

> This only needs to be done once.

#### Convert a docker-compose.yml file to nixos format

`cd` into the directory containing the `docker-compose.yml` file, then run:

`nix run github:aksiksi/compose2nix -- --runtime podman --project xxx`

## External documentation

- [awesome nix](https://github.com/nix-community/awesome-nix)
