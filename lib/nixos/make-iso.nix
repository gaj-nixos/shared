{ sharedConfig, inputs }:
let
  lib = inputs.nixpkgs.lib;

  localOverlays = import ../../overlays { inherit inputs; };
  wallpapers = import ../../resources/wallpapers;

  inputModules =
    [ ]
    #++ (lib.optional (inputs ? stylix) inputs.stylix.nixosModules.stylix)
    ++ (lib.optional (inputs ? ragenix) inputs.ragenix.nixosModules.default)
    ++ (lib.optional (inputs ? agenix-rekey) inputs.agenix-rekey.nixosModules.default)
    ++ (lib.optional (inputs ? home-manager) inputs.home-manager.nixosModules.home-manager)
    #++ (lib.optional (inputs ? crowdsec) inputs.crowdsec.nixosModules.crowdsec)
    #++ (lib.optional (inputs ? crowdsec) inputs.crowdsec.nixosModules.crowdsec-firewall-bouncer)
    #++ (lib.optional (inputs ? nvf) inputs.nvf.nixosModules.default)
    ++ (lib.optional (inputs ? disko) inputs.disko.nixosModules.disko);

  inputOverlays =
    [ ] ++ (lib.optional (inputs ? agenix-rekey) inputs.agenix-rekey.overlays.default)
  #++ (lib.optional (inputs ? hyprpanel) inputs.hyprpanel.overlay)
  #++ (lib.optional (inputs ? crowdsec) inputs.crowdsec.overlays.default)
  ;
in
{
  makeIso =
    {
      inputs,
      targetSystem,
      rootPath,
      system,
      modules ? [ ],
      packages ? [ ],
      overlays ? [ ],
    }:
    lib.nixosSystem {
      inherit system;
      modules =
        modules
        ++ inputModules
        ++ [
          ({ config, ... }: import ../../resources/test-secret.nix { inherit rootPath config; })
          (
            {
              pkgs,
              modulesPath,
              lib,
              config,
              ...
            }:
            let
              generateScriptPackages = sharedConfig.lib.generateScriptPackages;
            in
            {
              imports = [
                (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix")
                ../../modules/nixos/system/scripts.nix
                (import ../../modules/nixos/security/agenix/agenix.nix {
                  inherit config pkgs;
                  flakeRoot = rootPath;
                })
                ../../modules/nixos/security/ssl-client.nix
              ];
              system.stateVersion = "25.05";
              time.timeZone = "Europe/Paris";
              environment.systemPackages =
                (generateScriptPackages ../../install)
                ++ packages
                ++ targetSystem.config.environment.systemPackages
                ++ lib.flatten (
                  builtins.map (usr: usr.home.packages) (lib.attrValues targetSystem.config.home-manager.users)
                );
              nix = {
                extraOptions = targetSystem.config.nix.extraOptions;
                settings = {
                  system-features = targetSystem.config.nix.settings.system-features;
                  substituters = targetSystem.config.nix.settings.substituters;
                  trusted-public-keys = targetSystem.config.nix.settings.trusted-public-keys;
                };
              };

              nixpkgs = {
                overlays =
                  overlays
                  ++ inputOverlays
                  ++ [
                    # Add overlays your own flake exports (from overlays and pkgs dir):
                    localOverlays.additions
                    localOverlays.modifications

                    localOverlays.gaelj-packages
                    localOverlays.master-packages
                    localOverlays.stable-packages
                    wallpapers
                  ];
                # Configure your nixpkgs instance
                config = {
                  # Disable if you don't want unfree packages
                  allowUnfree = true;
                  permittedInsecurePackages = import ../../resources/security/permitted-insecure-packages.nix;
                };
              };

              age.rekey = {
                hostPubkey = builtins.readFile ../../resources/age/iso.age.pub;
                localStorageDir = rootPath + "/secrets/rekeyed/${config.networking.hostName}";
              };

              # Enable OpenSSH service
              services.openssh = {
                enable = true;
                settings = {
                  PermitRootLogin = "yes";
                  PasswordAuthentication = lib.mkForce true;
                };
                listenAddresses = [
                  {
                    addr = "0.0.0.0";
                    port = 22;
                  }
                ];
              };

              # Set SSH keys or password for access
              systemd.services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
              users.users =
                let
                  nixos = "$6$yBMk4T1vomknTqro$YUCxvDD1wIZHpuNLpeHkeSNFtdPMNeCD8FGuNN4cVhYXmU64.SPvTPid5v8oJEllY2TpHgRvjCJO4gBubuon51"; # nixos
                  sshPublicKey = builtins.readFile ../../resources/ssh/ssh-gaj.pub;
                in
                {
                  root = {
                    hashedPassword = nixos;
                    initialPassword = lib.mkForce null;
                    initialHashedPassword = lib.mkForce null;
                    openssh.authorizedKeys.keys = [
                      "ssh-rsa ${sshPublicKey} root@nixos-install"
                    ];
                  };
                  nixos = {
                    hashedPassword = nixos;
                    initialPassword = lib.mkForce null;
                    initialHashedPassword = lib.mkForce null;
                    openssh.authorizedKeys.keys = [
                      "ssh-rsa ${sshPublicKey} nixos@nixos-install"
                    ];
                  };
                };

              # Start networking in the installer (allows SSH)
              networking = {
                hostName = lib.mkForce "nixos-install";
                useDHCP = lib.mkDefault true;
                firewall.enable = lib.mkForce false;
              };

              isoImage.squashfsCompression = "zstd -Xcompression-level 5";
            }
          )
        ];
      specialArgs = inputs // {
        inherit system inputs;
      };
    };
}
