{ inputs }:
let
  lib = inputs.nixpkgs.lib;

  localOverlays = import ../../overlays { inherit inputs; };
  wallpapers = import ../../resources/wallpapers;

  inputModules =
    [ ]
    ++ (lib.optional (inputs ? stylix) inputs.stylix.nixosModules.stylix)
    ++ (lib.optional (inputs ? ragenix) inputs.ragenix.nixosModules.default)
    ++ (lib.optional (inputs ? agenix-rekey) inputs.agenix-rekey.nixosModules.default)
    ++ (lib.optional (inputs ? home-manager) inputs.home-manager.nixosModules.home-manager)
    ++ (lib.optional (inputs ? crowdsec) inputs.crowdsec.nixosModules.crowdsec)
    ++ (lib.optional (inputs ? crowdsec) inputs.crowdsec.nixosModules.crowdsec-firewall-bouncer)
    ++ (lib.optional (inputs ? nvf) inputs.nvf.nixosModules.default)
    ++ (lib.optional (inputs ? disko) inputs.disko.nixosModules.disko);

  inputOverlays =
    [ ]
    ++ (lib.optional (inputs ? agenix-rekey) inputs.agenix-rekey.overlays.default)
    ++ (lib.optional (inputs ? hyprpanel) inputs.hyprpanel.overlay)
    ++ (lib.optional (inputs ? crowdsec) inputs.crowdsec.overlays.default);
in
{
  makeSystem =
    {
      inputs,
      system,
      modules,
      overlays ? [ ],
    }:
    lib.nixosSystem {
      inherit system;
      modules =
        modules
        ++ inputModules
        ++ [
          (
            { config, ... }:
            {
              nixpkgs = {
                overlays =
                  overlays
                  ++ inputOverlays
                  ++ [
                    # Add overlays your own flake exports (from overlays and pkgs dir):
                    localOverlays.additions
                    localOverlays.modifications

                    localOverlays.gaelj-packages
                    localOverlays.master-packages
                    localOverlays.stable-packages
                    wallpapers

                    # You can also add overlays exported from other flakes:
                    # neovim-nightly-overlay.overlays.default

                    # Or define it inline, for example:
                    # (final: prev: {
                    #   hi = final.hello.overrideAttrs (oldAttrs: {
                    #     patches = [ ./change-hello-to-hi.patch ];
                    #   });
                    # })
                  ];
                # Configure your nixpkgs instance
                config = {
                  # Disable if you don't want unfree packages
                  allowUnfree = true;
                  permittedInsecurePackages = import ../../resources/security/permitted-insecure-packages.nix;
                };
              };
              nix = {
                registry = lib.mapAttrs (_: value: { flake = value; }) inputs;
                nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;
              };
            }
          )
        ];
      specialArgs = inputs // {
        inherit system inputs;
      };
    };
}
