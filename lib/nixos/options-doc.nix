{
  lib,
  pkgs,
  runCommand,
  nixosOptionsDoc,
  ...
}:
let
  # evaluate our options
  eval = lib.evalModules {
    modules = [
      ../../options
      ../../resources/dummy-options.nix
    ];
    specialArgs = {
      inherit pkgs;
    };
    #check = false;
  };
  # generate our docs
  optionsDoc = nixosOptionsDoc {
    inherit (eval) options;
  };
in
# create a derivation for capturing the markdown output
runCommand "options-doc.md" { } ''
  cat ${optionsDoc.optionsCommonMark} >> $out
''
