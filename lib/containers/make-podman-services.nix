{
  pkgs,
  lib,
  ...
}:
{ config }:

let
  mkBaseSharedContainerSettings =
    cfg:
    {
      volumes ? [ ],
      environment ? { },
      extraOptions ? [ ],
    }:
    {
      volumes = [
        "/etc/localtime:/etc/localtime:ro"
      ] ++ volumes;
      environment = {
        TZ = config.time.timeZone;
      } // environment;
      log-driver = "journald";
      inherit extraOptions;
      login = cfg.registrySettings;
    };

  mkSharedContainerSettings =
    name: cfg:
    {
      volumes ? [ ],
      environment ? { },
      extraOptions ? [ ],
    }:
    (mkBaseSharedContainerSettings cfg {
      inherit volumes environment;
      extraOptions = [
        "--network=${name}_default"
      ] ++ extraOptions;
    });

  mkSharedContainerSettingsHostNetwork =
    cfg:
    {
      volumes ? [ ],
      environment ? { },
      extraOptions ? [ ],
    }:
    (mkBaseSharedContainerSettings cfg {
      inherit volumes environment;
      extraOptions = [
        "--network=host"
      ] ++ extraOptions;
    });

  mkContainerUser = name: {
    users."${name}" = {
      isSystemUser = true;
      extraGroups = [
        "docker"
      ];
      shell = pkgs.bashInteractive;
      group = "${name}";
    };
    groups."${name}" = { };
  };

  dbDumperSharedContainerConfig =
    name: cfg: POSTGRES_HOST: POSTGRES_USER: POSTGRES_DB: backupPath: environment:
    (mkSharedContainerSettings name cfg {
      environment = {
        POSTGRES_HOST = "${POSTGRES_HOST}";
        POSTGRES_CLUSTER = "TRUE";
        POSTGRES_USER = "${POSTGRES_USER}";
        POSTGRES_DB = "${POSTGRES_DB}";
        SCHEDULE = "@daily";
        POSTGRES_EXTRA_OPTS = "--clean --if-exists";
        BACKUP_DIR = "/db_dumps";
      } // environment;
      volumes = [
        "${backupPath}:/db_dumps:rw"
      ];
      extraOptions = [
        "--network-alias=${name}_db_dumper"
      ];
    });

  restartAlways = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
  };

  partOfRoot = name: {
    partOf = [ "podman-compose-${name}-root.target" ];
    wantedBy = [ "podman-compose-${name}-root.target" ];
  };

  mkAppUnitNoNetwork = name: (partOfRoot name) // restartAlways;

  mkAppUnit =
    name:
    {
      after = [ "podman-network-${name}_default.service" ];
      requires = [ "podman-network-${name}_default.service" ];
    }
    // (mkAppUnitNoNetwork name);

  # Root service
  # When started, this will automatically create all resources and start
  # the containers. When stopped, this will teardown all resources.
  mkRootUnit =
    name:
    {
      unitConfig = {
        Description = "Root target for ${name}";
      };
    }
    // (partOfRoot "all");

  mkDbDumperUnit =
    name:
    {
      after = [ "podman-${name}-db.service" ];
      requires = [ "podman-${name}-db.service" ];
    }
    // (mkAppUnitNoNetwork name);

  mkPostGresDumperContainer =
    name: POSTGRES_HOST: POSTGRES_USER: POSTGRES_PASSWORD: POSTGRES_DB: backupPath: cfg:
    {
      hostname = "${name}_db_dumper";
      image = "prodrigestivill/postgres-backup-local:14";
    }
    // (dbDumperSharedContainerConfig name cfg POSTGRES_HOST POSTGRES_USER POSTGRES_DB backupPath {
      POSTGRES_PASSWORD = "${POSTGRES_PASSWORD}";
    });

  mkPostGresDumperContainerEnv =
    name: POSTGRES_HOST: POSTGRES_USER: envSecretPath: POSTGRES_DB: backupPath: cfg:
    {
      hostname = "${name}_db_dumper";
      image = "prodrigestivill/postgres-backup-local:14";
      environmentFiles = [ envSecretPath ];
    }
    // (dbDumperSharedContainerConfig name cfg POSTGRES_HOST POSTGRES_USER POSTGRES_DB backupPath { });

  mkNetworkUnit =
    name:
    {
      path = [ pkgs.podman ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStop = "podman network rm -f ${name}_default";
      };
      script = ''
        podman network inspect ${name}_default || podman network create ${name}_default
      '';
    }
    // (partOfRoot name);

  mkBridgeNetworkUnit =
    name:
    {
      path = [ pkgs.podman ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStop = "podman network rm -f ${name}_default";
      };
      script = ''
        podman network inspect ${name}_default || podman network create ${name}_default --driver=bridge
      '';
    }
    // (partOfRoot name);
in
{
  inherit
    mkAppUnit
    mkRootUnit
    mkDbDumperUnit
    mkPostGresDumperContainer
    mkPostGresDumperContainerEnv
    mkNetworkUnit
    mkBridgeNetworkUnit
    mkBaseSharedContainerSettings
    mkSharedContainerSettings
    mkSharedContainerSettingsHostNetwork
    mkContainerUser
    mkAppUnitNoNetwork
    ;
}
