{ sharedConfig }:
{ backupConfig }:
{
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf;
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkBaseSharedContainerSettings
    ;
  dbBackupVersion = config.sharedConfig.appVersions.dbBackup.version;

  name = backupConfig.name; # ex: vaultwarden
  dataDir = backupConfig.dataDir; # ex: data => /srv/vaultwarden/data
  dbType = backupConfig.dbType; # ex: sqlite3
  dbHost = backupConfig.dbHost; # ex: db.sqlite3

  cfg = config.sharedConfig.webApps."${name}";
  dataPath = "/srv/${name}/${dataDir}";
  backupPath = "/srv/backups/${name}/db";
  backupLogsPath = "/srv/backups/${name}/backupLogs";
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    virtualisation.oci-containers.containers."${name}-db-backup" =
      {
        image = "tiredofit/db-backup:${dbBackupVersion}";
        dependsOn = [ name ];
      }
      // (mkBaseSharedContainerSettings cfg {
        volumes = [
          "${dataPath}:/data"
          "${backupPath}:/backup"
          "${backupLogsPath}:/logs"
        ];
        environment = {
          DB_TYPE = dbType;
          DB_HOST = "/data/${dbHost}";
          MD_5 = "true";
        };
      });

    system.activationScripts = {
      "create_${name}_backupDir".text = ''
        mkdir -p "${backupPath}"
        chown -R root:root "${backupPath}"
        mkdir -p "${backupLogsPath}"
        chown -R root:root "${backupLogsPath}"
      '';
    };
  };
}
