{ pkgs, ... }:

scriptDir:
let
  # Check if the script directory exists
  dirExists = builtins.pathExists scriptDir;

  # Create a shell script package
  createScriptPackage =
    fileName:
    pkgs.writeShellScriptBin (builtins.replaceStrings [ ".sh" ] [ "" ] fileName) (
      builtins.readFile "${scriptDir}/${fileName}"
    );

  # List all `.sh` files in the directory and create a package for each
  scriptFiles =
    if dirExists then
      builtins.filter (name: builtins.match ".+.sh$" name != null) (
        builtins.attrNames (builtins.readDir scriptDir)
      )
    else
      [ ];

  scriptPackages = map createScriptPackage scriptFiles;
in
scriptPackages
