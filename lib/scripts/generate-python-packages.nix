{ pkgs, ... }:
scriptDef:
let
  scriptDir = scriptDef.scriptDir;
  scriptName = scriptDef.scriptName;
  requirements = scriptDef.requirements;
  pythonEnv = pkgs.python3.buildEnv.override { extraLibs = requirements; };
  scriptBin = pkgs.writeScriptBin scriptName ''
    #!${pkgs.runtimeShell}
    ${pythonEnv}/bin/python ${scriptDir}/${scriptName}.py "$@"
  '';
in
{
  pkg = pkgs.buildFHSEnv {
    name = scriptName;
    targetPkgs = pkgs: [ scriptBin ];
    multiPkgs = pkgs: [ pythonEnv ];
    runScript = scriptName;
  };

  devShell = pkgs.mkShell {
    name = "${scriptName}-dev-shell";
    buildInputs = [ pythonEnv ];
    shellHook = ''
      echo "Development environment for ${scriptName} loaded."
    '';
  };
}
