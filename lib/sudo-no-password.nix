{ ... }:
let
  currentSystem = "/run/current-system/sw/bin";

  createSystemCommand = command: "${currentSystem}/${command}";

  systemCommandsString =
    systemCommands: builtins.concatStringsSep ", " (map createSystemCommand systemCommands);

  homeDir = userId: if userId == "root" then "/root" else "/home/${userId}";

  userCommand = userId: command: "${homeDir userId}/.nix-profile/bin/${command}";

  mapUserCommands = userId: list: map (userCommand userId) list;

  userConfig = userId: commands: (builtins.concatStringsSep ", " (mapUserCommands userId commands));
in
{
  sudoNoPassword =
    userId: commands: systemCommands:
    "${userId} ALL=(ALL) NOPASSWD: ${systemCommandsString systemCommands}, ${(userConfig userId commands)}";

  # User commands must be in ~/.nix-profile/bin
  # System commands must be in /run/current-system/sw/bin
}
