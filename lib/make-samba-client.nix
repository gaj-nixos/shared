{
  pkgs,
  ...
}:
{
  sharedConfig,
  lib,
  config,
}:
{
  mappings,
  smbName,
  userName,
  domain,
}:

# Example for mappings:
#  mappings = {
#    "/mnt/xxx" = "//server.home.arpa/xxx";
#  };

let
  passwordSecret = "${smbName}SmbPassword";
  configSecret = "${smbName}SmbSecrets";

  fileSystems =
    let
      makeFileSystem = name: value: {
        device = value;
        fsType = "cifs";
        options =
          let
            automountOpts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
          in
          [
            "${automountOpts},credentials=${config.age.secrets."${configSecret}".path},uid=1000,gid=${
              toString config.users.groups."samba".gid
            }"
            "nofail"
            "_netdev"
          ];
      };
    in
    builtins.mapAttrs makeFileSystem mappings;

  # create mount directory and change group
  mkDirs =
    let
      mkDir = map (mountPoint: ''
        mkdir -p ${mountPoint}
        chgrp samba ${mountPoint}
        chmod 770 ${mountPoint}
      '') (builtins.attrNames mappings);
    in
    builtins.concatStringsSep "\n" mkDir;

in
{
  age.secrets = {
    "${passwordSecret}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${passwordSecret}.age";
      generator.script = (
        sharedConfig.lib.make-agenix-generator-prompt {
          prompts = [
            "Configuring SMB client '${smbName}'"
            "Please enter the SMB password for user '${userName}'."
          ];
        }
      );
    };
    "${configSecret}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${configSecret}.age";
      mode = "660"; # RW
      group = "samba";
      generator = {
        dependencies = [ config.age.secrets."${passwordSecret}" ];
        script =
          { decrypt, deps, ... }:
          ''
            printf 'username=%s\n' ${userName}
            printf 'domain=%s\n' ${domain}
            printf 'password=%s\n' $(${decrypt} ${lib.escapeShellArg (builtins.head deps).file})
          '';
      };
    };
  };

  environment.systemPackages = [ pkgs.cifs-utils ];

  inherit fileSystems;

  users.groups."samba" = {
    gid = 982;
  };

  system.activationScripts = {
    "mkMountPoints${smbName}Script".text = ''
      ${mkDirs}
    '';
  };
}
