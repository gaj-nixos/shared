{
  config,
  lib,
  POSTGRES_USER,
}:

let
  secretName = "syncV3SecretKey";
  envSecretName = "syncV3SecretEnv";
  envSecretPath = config.age.secrets."${envSecretName}".path;
in
{
  secrets = {
    "${secretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${secretName}.age";
      generator.script =
        {
          name,
          secret,
          lib,
          pkgs,
          file,
          deps,
          decrypt,
          ...
        }:
        ''
          echo -n "$(${pkgs.openssl}/bin/openssl rand -hex 32)"
        '';
    };
    "${envSecretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${envSecretName}.age";
      generator = {
        dependencies = [
          config.age.secrets."${secretName}"
          config.age.secrets."syncV3SecretDb"
        ];
        script =
          { decrypt, deps, ... }:
          ''
            printf '"SYNCV3_SECRET"=%s\n' $(echo $(${decrypt} ${lib.escapeShellArg (builtins.head deps).file}))
            printf 'SYNCV3_DB=user=${POSTGRES_USER} dbname=syncv3 sslmode=disable host=host.docker.internal password=%s\n' $(echo $(${decrypt} ${lib.escapeShellArg (builtins.head (builtins.tail deps)).file}));
          '';
      };
    };
  };
  inherit envSecretPath;
}
