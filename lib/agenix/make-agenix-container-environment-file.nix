{
  config,
  lib,
  secretName,
  envSecretName,
  envKeyName,
  maxLength ? 48,
}:

let
  envSecretPath = config.age.secrets."${envSecretName}".path;
in
{
  secrets = {
    "${secretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${secretName}.age";
      generator.script = "alnum"; # Generates a 48 character alphanumeric password
    };
    "${envSecretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${envSecretName}.age";
      generator = {
        dependencies = [ config.age.secrets."${secretName}" ];
        script =
          { decrypt, deps, ... }:
          ''
            printf '${envKeyName}=%s\n' $(echo $(${decrypt} ${lib.escapeShellArg (builtins.head deps).file}) | cut -c1-${builtins.toString maxLength})
          '';
      };
    };
  };
  inherit envSecretPath;
}
