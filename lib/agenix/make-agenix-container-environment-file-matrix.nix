{
  config,
  secretName,
}:

let
  secretPath = config.age.secrets."${secretName}".path;
in
{
  secrets = {
    "${secretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${secretName}.age";
      generator.script =
        {
          name,
          secret,
          lib,
          pkgs,
          file,
          deps,
          decrypt,
          ...
        }:
        ''
          echo "Please enter the contents of matrix_key.pem." >&2
          echo "Generate with" >&2
          echo "mkdir -p ./config" >&2
          echo "docker run --rm --entrypoint=\"/usr/bin/generate-keys\" \ " >&2
          echo "  -v $(pwd)/config:/mnt \ " >&2
          echo "  matrixdotorg/dendrite-monolith:latest \ " >&2
          echo "  -private-key /mnt/matrix_key.pem " >&2

          echo "-----BEGIN MATRIX PRIVATE KEY-----"

          echo "Enter Key-ID (complete line)" >&2
          read -r keyId
          echo $keyId

          echo ""

          echo "Enter Key (complete line)" >&2
          read -r password
          echo $password

          echo "-----END MATRIX PRIVATE KEY-----"
        '';
    };
  };
  inherit secretPath;
}
