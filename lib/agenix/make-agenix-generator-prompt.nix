{
  prompts ? [ ],
}:
let
  echoPrompts = builtins.map (prompt: "echo ${prompt} >&2") prompts;
  allEchoPrompts = builtins.concatStringsSep "\n" echoPrompts;
in
{
  name,
  secret,
  lib,
  pkgs,
  file,
  deps,
  decrypt,
  ...
}:
''
  ${allEchoPrompts}
  read -r password
  echo $password
''
