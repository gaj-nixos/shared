{
  config,
  enableACME ? true,
}:

if config.sharedConfig.services.nginx.geoBlocking.enable && enableACME then
  ''
    set $allow_access 0;
    if ($allowed_country = 1) {
      set $allow_access 1;
    }
    if ($is_trusted_proxy = 1) {
      set $allow_access 1;
    }
    if ($remote_addr ~ ^192\.168\.1\.) {
      set $allow_access 1;
    }
    if ($remote_addr ~ ^172\.16\.\.) {
      set $allow_access 1;
    }
    if ($remote_addr ~ ^10\.\.\.) {
      set $allow_access 1;
    }
    if ($remote_addr ~ ^127\.\.\.) {
      set $allow_access 1;
    }
    if ($allow_access = 0) {
      return 403;
    }
  ''
else
  ""
