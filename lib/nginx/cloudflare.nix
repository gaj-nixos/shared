{
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) splitString concatStringsSep;

  splitLines = str: builtins.filter (line: line != "") (splitString "\n" str);

  # Read the Cloudflare IP files
  cloudflareIPsV4 = splitLines (
    builtins.readFile (
      pkgs.fetchurl {
        url = "https://www.cloudflare.com/ips-v4";
        sha256 = "0ywy9sg7spafi3gm9q5wb59lbiq0swvf0q3iazl0maq1pj1nsb7h";
      }
    )
  );
  cloudflareIPsV6 = splitLines (
    builtins.readFile (
      pkgs.fetchurl {
        url = "https://www.cloudflare.com/ips-v6";
        sha256 = "1ad09hijignj6zlqvdjxv7rjj8567z357zfavv201b9vx3ikk7cy";
      }
    )
  );

  # For each IP, generate a `set_real_ip_from` line
  generateNginxIPs = ips: concatStringsSep "\n" (map (ip: "set_real_ip_from ${ip};") ips);

  cfConfigRealIpV4 = generateNginxIPs cloudflareIPsV4;
  cfConfigRealIpV6 = generateNginxIPs cloudflareIPsV6;
in
{
  inherit
    cloudflareIPsV4
    cloudflareIPsV6
    cfConfigRealIpV4
    cfConfigRealIpV6
    ;
}
