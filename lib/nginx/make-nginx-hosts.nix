{
  sharedConfig,
  config,
  lib,
  pkgs,
  cfg,
  proxyPass,
  extraConfig ? "",
  subUrl ? "",
}:
let
  inherit (lib) genAttrs;
  enableACME = config.sharedConfig.services.letsEncrypt.enable;
  #extraConfigWithCF = ''
  #  ${config.sharedConfig.services.nginx.cfConfig}
  #  ${extraConfig}
  #'';
  redirectToHost = cfg.mainInternalHost;
in
genAttrs (if cfg.mainInternalHost != "" then [ cfg.mainInternalHost ] else [ ]) (
  host:
  (sharedConfig.lib.nginx-config {
    inherit
      sharedConfig
      config
      lib
      proxyPass
      extraConfig
      subUrl
      ;
  })
)
// genAttrs (if cfg.externalHost != "" then [ cfg.externalHost ] else [ ]) (
  host:
  (sharedConfig.lib.nginx-config {
    inherit
      sharedConfig
      config
      lib
      proxyPass
      extraConfig
      subUrl
      enableACME
      ;
    #extraConfig = extraConfigWithCF;
    blockCommonExploits = true;
  })
)
// genAttrs cfg.redirectedHosts (
  host: (sharedConfig.lib.nginx-config-redirect { inherit config redirectToHost subUrl; })
)
