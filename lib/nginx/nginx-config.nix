{
  sharedConfig,
  config,
  lib,
  proxyPass,
  subUrl ? "",
  extraConfig ? "",
  blockCommonExploits ? false,
  enableACME ? false,
}:
let
  sslCertificate = if !enableACME then config.sharedConfig.services.sslServer.pkiCert else "";
  sslCertificateKey = if !enableACME then config.sharedConfig.services.sslServer.pkiKey else "";

  geoIpConfig = sharedConfig.lib.geo-ip-config { inherit config enableACME; };
in
# defines contents of an item in services.nginx.virtualHosts.<item-name>
{
  forceSSL = true;
  inherit enableACME sslCertificate sslCertificateKey;

  locations = {
    "/.well-known/acme-challenge" = lib.mkIf enableACME {
      root = "/var/lib/acme/acme-challenge";
    };

    "/robots.txt" = lib.mkIf enableACME {
      extraConfig = ''
        rewrite ^/(.*)  $1;
        return 200 "User-agent: *\nDisallow: /";
      '';
    };

    "${subUrl}/" = {
      inherit proxyPass;
      proxyWebsockets = true;
      extraConfig = ''
        ${extraConfig}

        ${geoIpConfig}
      '';
      #  ${extraSecurityConfig}
    };
  };
}

#// (lib.mkIf blockCommonExploits {
#  # Prevent Access to System Files
#  "~* \.(?:bak|config|sql|fla|ini|log|sh|inc|swp|dist)$" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to Specific Files
#  "~ /(\.|data|config|db_structure\.xml|md5sums|\.sh|\.sql|\.tar|info/\.vars)" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to Specific Files
#  "~ /(composer\.(json|lock)|proc/self/environ|etc/passwd)" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to PHP Files
#  "~ \..*/.*\.php$" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to Hidden Files
#  "~ /\.|^\.git|^\.svn|^\.hg|^\.env" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to Backup Files
#  "~* \.(?:bak|old|swp|temp|tmp|~)$" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to Specific System Files
#  "~ /(logs|dump|backups|files|data|db|log)/" = {
#    extraConfig = "deny all;";
#  };
#  # Prevent Access to PHP Files in Specific Directories
#  "~ /(cache|static|assets)/.*\.php$" = {
#    extraConfig = "deny all;";
#  };
#  # Block common web shells
#  "~* (php|cmd|shell|system)_?(exec|cmd|up|down|shell|eval)\.(php|sh|py|pl|cgi)$" = {
#    extraConfig = "deny all;";
#  };
#  # Block Directory Listing
#  "/" = {
#    extraConfig = "autoindex off;";
#  };
#})
