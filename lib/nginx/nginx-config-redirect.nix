{
  config,
  redirectToHost,
  subUrl ? "",
}:
let
  sslCertificate = config.sharedConfig.services.sslServer.pkiCert;
  sslCertificateKey = config.sharedConfig.services.sslServer.pkiKey;
in
# defines contents of an item in services.nginx.virtualHosts.<item-name>
{
  forceSSL = true;
  inherit sslCertificate sslCertificateKey;
  locations = {
    "${subUrl}/" = {
      return = "301 https://${redirectToHost}$request_uri";
    };
  };
}
