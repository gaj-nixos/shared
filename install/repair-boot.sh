#!/usr/bin/env bash

# This script is used to repair the boot partition of a NixOS installation
# usage:
# sudo repair-boot /dev/sda /dev/sda1 <flake-name>

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

# Check if the script is run as root
if [[ $(id -u) -ne 0 ]]; then
  echo "❗ Please run with sudo"
  exit 1
fi

BOOT_DEVICE_WITHOUT_NUMBER=$1 # /dev/sda
DEVICE=$2 # /dev/sda1
FLAKE_NAME=$2 # vmware-workstation

PARTITION_NUMBER=$(echo $DEVICE | sed 's/[^0-9]*//g') # 1

lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINT

if [[ -z "$BOOT_DEVICE_WITHOUT_NUMBER" ]]; then
  echo "Please provide the boot device without the partition number (e.g., /dev/sda). Usage: sudo repair-boot /dev/sda /dev/sda1 <flake-name>"
  exit 1
fi

if [[ -z "$DEVICE" ]]; then
  echo "Please provide the boot partition device (e.g., /dev/sda1) Usage: sudo repair-boot /dev/sda /dev/sda1 <flake-name>"
  exit 1
fi

if [[ -z "$FLAKE_NAME" ]]; then
  echo "Please provide the flake name (e.g., vmware-workstation) Usage: sudo repair-boot /dev/sda /dev/sda1 <flake-name>"
  exit 1
fi

echo "Repairing the boot partition..."

fdisk "$BOOT_DEVICE" <<EOF
p       # Print the current partition table for reference
d       # Delete the existing /boot partition
$PARTITION_NUMBER       # Select partition number
n       # Create a new partition
p       # Select primary partition (typically for BIOS systems)
$PARTITION_NUMBER
        # Accept default starting sector
        # Accept default partition size
t       # Change partition type
1       # Choose partition type 1 (EFI on EFI systems)
w       # Write changes to disk
EOF

mkfs.fat -F32 $DEVICE
fatlabel $DEVICE BOOT

parted "$BOOT_DEVICE" --script <<EOF
name $PARTITION_NUMBER disk-main-esp
quit
EOF

mount $DEVICE /mnt/boot

echo "Entering the NixOS environment..."
echo "if the following command fails, run 'nixos-enter' manually"
echo "and then run in /etc/nixos:"
echo "bootctl --path=/boot install"
echo "nixos-rebuild boot --accept-flake-config --flake .#$FLAKE_NAME"

nixos-enter

cd /etc/nixos
bootctl --path=/boot install
nixos-rebuild boot --accept-flake-config --flake .#$FLAKE_NAME
