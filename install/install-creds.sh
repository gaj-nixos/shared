#!/usr/bin/env bash
# install .git-credentials and ISO Age key from /dev/sdcX/.git-credentials and setup git for root to use these credentials

# Exit immediately if a command exits with a non-zero status
set -e

DEVICE=$1

if [[ -z "$DEVICE" ]]; then
  lsblk
  echo "Please provide the device to mount from the information above (e.g., /dev/sdc3) containing the Ventoy directory. Usage: sudo install-creds /dev/sdc3"
  exit 1
fi

sudo mkdir -p /mnt/Ventoy
sudo mount $DEVICE /mnt/Ventoy

cp /mnt/Ventoy/.git-credentials .
sudo chown 1000:1000 .git-credentials
sudo chmod 600 .git-credentials
sudo cp .git-credentials /root/.git-credentials

git config --global credential.helper store
sudo git config --global credential.helper store
sudo git config --global user.email "gaj@nixos"
sudo git config --global user.name "gaj"

sudo mkdir /etc/age/
sudo cp /mnt/Ventoy/age/key.txt /etc/age/key.txt
