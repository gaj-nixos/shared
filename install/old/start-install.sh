#!/usr/bin/env nix-shell
#!nix-shell -i bash -p git
# shellcheck shell=bash

# This script is used to download the latest version of the installation scripts
# usage:
# curl -S -L https://gitlab.com/gaj-nixos/shared/-/raw/main/install/start-install.sh -o start-install.sh
# chmod +x start-install.sh
# ./start-install.sh <flake-repo> <flake-branch> <flake-name>

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

clear

flake_repo=$1
flake_repo_main_branch="main"

shared_repo="https://gitlab.com/gaj-nixos/shared"
shared_repo_main_branch="main"
shared_repo_install="$shared_repo/-/raw/$shared_repo_main_branch/install"
crt="gitlab.crt"
shared_repo_crt="$shared_repo/-/raw/$shared_repo_main_branch/resources/certificates/$crt"
iso_config="/etc/nixos/configuration.nix"

flake_branch=$2
flake_name=$3
default_flake_name="vmware-workstation"

# if not arguments passed, display the list of remote branche names on origin/ and prompt the user to select one, defaulting to $flake_repo_main_branch
if [ -z "$flake_branch" ]; then
  flake_branches=$(git ls-remote --heads "$flake_repo.git" | awk -F'/' '{print $3}' | sort | uniq)
  # if branches contains master, set flake_repo_main_branch to master
  if echo "$flake_branches" | grep -q "master"; then
    flake_repo_main_branch="master"
  fi
  echo "Available branches in $flake_repo:"
  echo "$flake_branches"
  echo ""
  read -rp "Enter the branch name to use (default: $flake_repo_main_branch): " flake_branch
  if [ -z "$flake_branch" ]; then
    flake_branch=$flake_repo_main_branch
  fi
fi

if [ -z "$flake_name" ]; then
  read -rp "Please enter the name of the configuration to install: (qemu, vmware-workstation, ...) (default=$default_flake_name)" flake_name
  echo ""
  if [ -z "$flake_name" ]; then
    flake_name=$default_flake_name
  fi
fi

echo "Downloading the installation scripts..."
# List of file paths to download
files=(
  "commit-hw-config.sh"
  "install.sh"
  "part-efi-btrfs.sh"
  #"part-efi-btrfs-split.sh"
  "mount-part.sh"
  #"mount-part-split.sh"
  "unmount-part.sh"
  "repair-boot.sh"
)

# Remove existing files
for file in "${files[@]}"; do
  if [ -f "./$file" ]; then
    rm "./$file"
  fi
  curl -S -L "$shared_repo_install/$file" -o "$file"
  chmod +x "./$file"
done

# disko-config.nix
if [ -f "./disko-config.nix" ]; then
  rm "./disko-config.nix"
fi
if [ -f "./disko-config1.nix" ]; then
  rm "./disko-config1.nix"
fi
if [ -f "./disko-config2.nix" ]; then
  rm "./disko-config2.nix"
fi

# git clone the repo and checkout the branch
git clone "$flake_repo.git" /tmp/nixos || true
cd /tmp/nixos
git checkout -f -B "$flake_branch" "origin/$flake_branch"
git pull
cd
cp -f /tmp/nixos/hardwares/$flake_name/disko-config.nix ~/disko-config.nix
cp -f /tmp/nixos/hardwares/$flake_name/disko-config1.nix ~/disko-config1.nix || true
cp -f /tmp/nixos/hardwares/$flake_name/disko-config2.nix ~/disko-config2.nix || true

# sudo su
# curl -S -L https://tinyurl.com/tutorial-nixos-install-rpi4 > /etc/nixos/configuration.nix
# su nixos

read -rp "Do you want to add the custom gitlab SSL certificate to the NixOS configuration? (y/N) " add_cert
echo ""
if [ "$add_cert" = "y" ]; then

  # Install private CA certificate
  if [ ! -f "/etc/nixos/$crt" ]; then
    curl -s "$shared_repo_crt" -o "$crt"
    sudo mv "$crt" /etc/nixos/$crt
  fi

  if [ ! -f "$iso_config.bak" ]; then

    sudo mv $iso_config "$iso_config.bak"
    sudo bash <<EOF
cat $iso_config.bak | head -n 4 > $iso_config
echo "  security.pki.certificateFiles = [ ./$crt ];" >> $iso_config
echo '}' >> $iso_config
nixos-rebuild switch
EOF
  fi
fi

echo "Scripts downloaded successfully."
echo ""

sudo install-nixos "$flake_repo.git" "$flake_name" "$flake_branch"
