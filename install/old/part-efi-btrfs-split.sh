#!/usr/bin/env bash

# This script partitions a disk for NixOS installation with EFI and Btrfs support.
# Usage: sudo part-efi-btrfs

set -o errexit  # Exit on error
set -o nounset  # Treat unset variables as an error
set -o pipefail # Consider errors in a pipeline

# Check if the script is run as root
if [[ $(id -u) -ne 0 ]]; then
  echo "❗ Please run with sudo"
  exit 1
fi

# Check previous command success
check_success() {
  if [[ $? -ne 0 ]]; then
    echo "Error occurred: $1 failed." >&2
    exit 1
  else
    echo "$1 succeeded."
  fi
}

echo "Listing available disks:"
lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

read -rp "Main disk: enter the disk device to partition (without the /dev/ prefix). All data will be erased! (default: nvme0n1, example: sda): /dev/" MAIN_DISK
if [[ -z "$MAIN_DISK" ]]; then
  MAIN_DISK=nvme0n1  # Default disk
  echo "Defaulting to /dev/$MAIN_DISK"
fi
MAIN_DISK="/dev/$MAIN_DISK"

DATA_DISK=
read -rp "Use a separate data disk (for /home, /srv...)? (Y/n)" USE_DATA_DISK
if [[ $USE_DATA_DISK =~ ^[Nn]$ ]]; then
  echo "No data disk selected."
else
  read -rp "Data disk: enter the disk device to partition (without the /dev/ prefix). All data will be erased! (default: nvme1n1, example: sdb). Leave empty to use the main disk as data disk: /dev/" DATA_DISK
  if [[ -z "$DATA_DISK" ]]; then
    DATA_DISK=nvme1n1  # Default disk
    echo "Defaulting to /dev/$DATA_DISK"
  fi
  DATA_DISK="/dev/$DATA_DISK"
fi

echo "Selected disk(s): $MAIN_DISK $DATA_DISK"
read -rp "Press Enter to continue or Ctrl+C to abort."
echo

read -rp "Enter swap size in GB (default 8): " SWAP_SIZE
echo
SWAP_SIZE="${SWAP_SIZE:-8}"

if ! [[ "$SWAP_SIZE" =~ ^[0-9]+$ ]]; then
  echo "Invalid swap size: must be a number (of GiB)."
  exit 1
fi

# Confirmation
read -rp "This will erase all data on $MAIN_DISK $DATA_DISK. Press Enter to continue or Ctrl+C to abort."
echo
echo "Starting partitioning..."

# Erase any previous MBR bootloader and partition table
sudo dd if=/dev/zero of="${MAIN_DISK}" bs=512 count=1; check_success "erase bootloader"
wipefs -a "${MAIN_DISK}"; check_success "wipefs"

if [[ -n "$DATA_DISK" ]]; then
  sudo dd if=/dev/zero of="${DATA_DISK}" bs=512 count=1; check_success "erase bootloader"
  wipefs -a "${DATA_DISK}"; check_success "wipefs"
fi

# Create partitions (with alignment)
DISK_START="1"
ESP_SIZE="512"
BOOT_SIZE="1024"

ESP_START="${DISK_START}"
ESP_END="$((${ESP_START} + ${ESP_SIZE}))"

BOOT_START="${ESP_END}"
BOOT_END="$((${BOOT_START} + ${BOOT_SIZE}))"

DISK_START="${DISK_START}MiB"
ESP_START="${ESP_START}MiB"
ESP_END="${ESP_END}MiB"
ESP_SIZE="${ESP_SIZE}MiB"
BOOT_SIZE="${BOOT_SIZE}MiB"
BOOT_START="${BOOT_START}MiB"
BOOT_END="${BOOT_END}MiB"

read -rp "Press Enter to create partitions or Ctrl+C to abort."
echo

parted "${MAIN_DISK}" --script "mklabel gpt"; check_success "mklabel"

parted "${MAIN_DISK}" --script "mkpart ESP fat32 ${ESP_START} ${ESP_END}"; check_success "mkpart ESP"
parted "${MAIN_DISK}" --script "set 1 esp on"; check_success "set ESP flag"

parted "${MAIN_DISK}" --script "mkpart primary ext4 ${BOOT_START} ${BOOT_END}"; check_success "mkpart primary ext4"
LINUX_EXTENDED_BOOT=bc13c2ff-59e6-4262-a352-b275fd6f7172
sgdisk --typecode="2:${LINUX_EXTENDED_BOOT}" "${MAIN_DISK}"

TOTAL_MAIN_SIZE_MIB=$(parted "${MAIN_DISK}" unit MiB print | grep "^Disk ${MAIN_DISK}" | awk -F: '{print $2}' | sed 's/MiB//')
START_SWAP=$((${TOTAL_MAIN_SIZE_MIB} - ${SWAP_SIZE} * 1024))
START_SWAP_ALIGNED=$(( (START_SWAP / 1) * 1 ))
START_SWAP_ALIGNED="${START_SWAP_ALIGNED}MiB"

echo Start swap: "$START_SWAP"MiB
echo Start swap aligned: $START_SWAP_ALIGNED

read -rp "Press Enter to create swap partition or Ctrl+C to abort."
echo

parted "${MAIN_DISK}" --script "mkpart primary btrfs ${BOOT_END} ${START_SWAP_ALIGNED}"; check_success "mkpart primary btrfs"
parted "${MAIN_DISK}" --script "mkpart primary linux-swap ${START_SWAP_ALIGNED} 100%"; check_success "mkpart swap"

if [[ -n "$DATA_DISK" ]]; then
  parted "${DATA_DISK}" --script "mklabel gpt"; check_success "mklabel"
  parted "${DATA_DISK}" --script "mkpart primary btrfs ${DISK_START} 100%"; check_success "mkpart primary btrfs"
fi

# Create btrfs disk labels
ESP_LABEL="ESP"
BOOT_LABEL="BOOT"
NIXOS_LABEL="nixos"
SWAP_LABEL="swap"
DATA_LABEL="data"

TMP_MOUNT="/mnt"

# if MAIN_DISK starts with nvme, add p to partition names
if [[ $MAIN_DISK == /dev/nvme* ]]; then
  PART_1="${MAIN_DISK}p1"
  PART_2="${MAIN_DISK}p2"
  PART_3="${MAIN_DISK}p3"
  PART_4="${MAIN_DISK}p4"
else
  PART_1="${MAIN_DISK}1"
  PART_2="${MAIN_DISK}2"
  PART_3="${MAIN_DISK}3"
  PART_4="${MAIN_DISK}4"
fi

# if DATA_DISK starts with nvme, add p to partition name
PART_5=
if [[ -n "$DATA_DISK" ]]; then
  if [[ $DATA_DISK == /dev/nvme* ]]; then
    PART_5="${DATA_DISK}p1"
  else
    PART_5="${DATA_DISK}1"
  fi
fi

PART_ESP="${PART_1}"
PART_BOOT="${PART_2}"
PART_NIXOS="${PART_3}"
PART_SWAP="${PART_4}"
PART_DATA="${PART_5}"

read -rp "Press Enter to format partitions or Ctrl+C to abort."
echo

# Format partitions on main disk
mkfs.fat -F 32 -n ${ESP_LABEL} "${PART_ESP}"; check_success "format ${ESP_LABEL}"
fatlabel "${PART_ESP}" ${ESP_LABEL}; check_success "label ${ESP_LABEL}"

mkfs.ext4 -L ${BOOT_LABEL} "${PART_BOOT}"; check_success "format ${BOOT_LABEL}"
e2label "${PART_BOOT}" "${BOOT_LABEL}"

mkfs.btrfs -f -L ${NIXOS_LABEL} "${PART_NIXOS}"; check_success "format ${NIXOS_LABEL}"
btrfs filesystem label "${PART_NIXOS}" ${NIXOS_LABEL}; check_success "label ${NIXOS_LABEL}"

mkswap -L "${SWAP_LABEL}" "${PART_SWAP}"; check_success "format swap"
swapon "${PART_SWAP}"; check_success "enable swap"

# Format partitions on data disk

if [[ -n "$DATA_DISK" ]]; then
  read -rp "Encrypt the data disk? (Y/n)" ENCRYPT_DATA_DISK
  echo
  if [[ $ENCRYPT_DATA_DISK =~ ^[Nn]$ ]]; then
    echo "No encryption selected."
    PART_DATA_DECRYPTED="${PART_DATA}"
  else
    echo "Encrypting the data disk..."
    cryptsetup luksFormat "${PART_DATA}" --type luks2; check_success "format LUKS"
    cryptsetup open "${PART_DATA}" data; check_success "open LUKS"
    PART_DATA_DECRYPTED="/dev/mapper/data"
  fi

  mkfs.btrfs -f -L ${DATA_LABEL} "${PART_DATA_DECRYPTED}"; check_success "format ${DATA_LABEL}"
  btrfs filesystem label "${PART_DATA_DECRYPTED}" ${DATA_LABEL}; check_success "label ${DATA_LABEL}"
fi

read -rp "Formatting and labelling done. Press Enter to create the subvolumes..."
echo

# Mount the root partition and create subvolumes
mount /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}
btrfs subvolume create ${TMP_MOUNT}/@; check_success "create @ subvolume"
btrfs subvolume create ${TMP_MOUNT}/@var; check_success "create @var subvolume"
btrfs subvolume create ${TMP_MOUNT}/@var_cache; check_success "create @var_cache subvolume"
btrfs subvolume create ${TMP_MOUNT}/@var_log; check_success "create @var_log subvolume"
btrfs subvolume create ${TMP_MOUNT}/@var_tmp; check_success "create @var_tmp subvolume"
btrfs subvolume create ${TMP_MOUNT}/@nix; check_success "create @nix subvolume"
btrfs subvolume create ${TMP_MOUNT}/@tmp; check_success "create @tmp subvolume"

NIXOS_OR_DATA_LABEL=${NIXOS_LABEL}

if [[ -n "$DATA_DISK" ]]; then
  umount ${TMP_MOUNT}
  mount ${PART_DATA_DECRYPTED} ${TMP_MOUNT}
  NIXOS_OR_DATA_LABEL=${DATA_LABEL}
fi

btrfs subvolume create ${TMP_MOUNT}/@home; check_success "create @home subvolume"
btrfs subvolume create ${TMP_MOUNT}/@srv; check_success "create @srv subvolume"
btrfs subvolume create ${TMP_MOUNT}/@backups; check_success "create @backups subvolume"
btrfs subvolume create ${TMP_MOUNT}/@db; check_success "create @db subvolume"
btrfs subvolume create ${TMP_MOUNT}/@snapshots_home; check_success "create @snapshots_home subvolume"
btrfs subvolume create ${TMP_MOUNT}/@snapshots_srv; check_success "create @snapshots_srv subvolume"
btrfs subvolume create ${TMP_MOUNT}/@snapshots_db; check_success "create @snapshots_db subvolume"

umount ${TMP_MOUNT}

read -rp "Press enter to mount the subvolumes..."
echo

mount-part-split
