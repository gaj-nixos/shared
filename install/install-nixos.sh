#!/usr/bin/env nix-shell
#!nix-shell -i bash -p git
# shellcheck shell=bash

# This script installs NixOS on a new system using a specific flake configuration.
# Usage: sudo ./install-nixos <repo_url> [flake-name] [branch-name]

set -o errexit  # Exit on error
set -o nounset  # Treat unset variables as an error
set -o pipefail # Consider errors in a pipeline

log() {
  echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

error_exit() {
  log "Error: $1"
  exit 1
}

# Default values for arguments
default_flake="vmware-workstation"
default_branch="master"

# Checks if any arguments are provided and sets them accordingly
repo_url=$1
flake_name=${2:-$default_flake}
branch_name=${3:-$default_branch}

MAIN_DISK=
DATA_DISK=

# Ensures the script is executed with root privileges.
if [ "$(id -u)" -ne 0 ]; then
  error_exit "This script must be run with sudo or as root."
fi

get_disk_devices() {
  echo ""
  echo "Listing available disks:"
  lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

  read -rp "Main disk: enter the disk device to use (without the /dev/ prefix) (default: nvme0n1, example: sda): /dev/" MAIN_DISK
  echo ""
  if [[ -z "$MAIN_DISK" ]]; then
    MAIN_DISK=nvme0n1 # Default disk
    echo "Defaulting to /dev/$MAIN_DISK"
  fi
  MAIN_DISK="/dev/$MAIN_DISK"

  DATA_DISK=
  read -rn 1 -p "Use a separate data disk (for /home, /srv...)? (Y/n)" USE_DATA_DISK
  echo ""
  if [[ $USE_DATA_DISK =~ ^[Nn]$ ]]; then
    echo "No data disk selected."
  else
    read -rp "Data disk: enter the disk device to partition (without the /dev/ prefix) (default: nvme1n1, example: sdb). Leave empty to use the main disk as data disk: /dev/" DATA_DISK
    echo ""
    if [[ -z "$DATA_DISK" ]]; then
      DATA_DISK=nvme1n1 # Default disk
      echo "Defaulting to /dev/$DATA_DISK"
    fi
    DATA_DISK="/dev/$DATA_DISK"
  fi

  echo "Selected disk(s): $MAIN_DISK $DATA_DISK"
}

delete_partition_on_disk() {
  disk=$1
  echo ""
  read -rn 1 -p "Delete all partitions on the disk $disk? (y/N)" DELETE_PARTITIONS
  echo ""
  if [[ $DELETE_PARTITIONS =~ ^[Yy]$ ]]; then
    sudo unmount-part "$disk"

    # Erase any previous MBR bootloader and partition table
    sudo dd if=/dev/zero of="$disk" bs=512 count=1
    read -rn 1 -p "Press enter to wipe the file system on $disk..." CONTINUE
    wipefs -a "$disk"
  fi
}

delete_partitions() {
  delete_partition_on_disk "$MAIN_DISK"
  if [[ -n "$DATA_DISK" ]]; then
    delete_partition_on_disk "$DATA_DISK"
  fi
}

format_mount_partitions() {
  echo ""
  echo "Disk format and partitioning / mount:"
  echo "1. Use the old custom format and partition script (without using Disko)"
  echo "2. Use Disko"

  read -rn 1 -p "Enter your choice: " choice
  echo ""

  if [[ $choice == 2 ]]; then
    echo "Main disk:"
    echo "1. Format, partition and mount"
    echo "2. Mount existing partition"
    read -rn 1 -p "Enter your choice: " choiceDisk1
    echo ""

    HAS_DISKO_CONFIG_2=false
    if [[ -f "./disko-config2.nix" ]]; then
      HAS_DISKO_CONFIG_2=true
      echo "Data disk:"
      echo "1. Format, partition and mount"
      echo "2. Mount existing partition"
      read -rn 1 -p "Enter your choice: " choiceDisk2
      echo ""
    fi
  fi

  DISKO_CONFIG_MAIN="disko-config1.nix"
  if [[ ! -f "./$DISKO_CONFIG_MAIN" ]]; then
    DISKO_CONFIG_MAIN="disko-config.nix"
  fi

  case $choice in
  1)
    sudo part-efi-btrfs "$MAIN_DISK" "$DATA_DISK"
    ;;
  2)
    MUST_UNMOUNT=0
    if [[ $choiceDisk1 == 1 ]]; then
      format_mount_partitions_with_disko "disko" "$DISKO_CONFIG_MAIN"
    fi

    if [[ $HAS_DISKO_CONFIG_2 == true ]]; then
      if [[ $choiceDisk1 == 1 ]]; then
        MUST_UNMOUNT=1
      fi
      if [[ $choiceDisk2 == 1 ]]; then
        format_mount_partitions_with_disko "disko" "disko-config2.nix"
        MUST_UNMOUNT=1
      fi
    fi

    if [[ $MUST_UNMOUNT == 1 ]]; then
      sudo unmount-part
    fi

    read -rn 1 -p "Press enter to mount all partitions..." CONTINUE
    format_mount_partitions_with_disko "mount" "disko-config.nix"

    ;;
  *)
    echo "Invalid choice '$choice'. Exiting..."
    exit 1
    ;;
  esac

  echo ""
  read -rp "Enter the swap partition or leave empty to ignore (e.g. /dev/sda2): /dev/" SWAP_PARTITION
  # use swap partition
  if [[ -n "$SWAP_PARTITION" ]]; then
    SWAP_PARTITION="/dev/$SWAP_PARTITION"
    sudo swapoff "$SWAP_PARTITION" || true
    sudo mkswap "$SWAP_PARTITION"
    sudo swapon "$SWAP_PARTITION"
  fi
}

format_mount_partitions_with_disko() {
  DISKO_CMD=$1
  DISKO_FILE=$2
  echo ""
  echo "Partitioning with disko (command = $DISKO_CMD)..."
  echo "sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode $DISKO_CMD ./$DISKO_FILE"

  while true; do
    sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode $DISKO_CMD "./$DISKO_FILE" || true
    echo "Listing available disks:"
    lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

    echo "Disko file used: $DISKO_FILE"
    read -rn 1 -p "Please check all partitions are mounted. If not, press Y to run disko again (y/N)" RETRY_DISKO
    echo ""
    if [[ ! $RETRY_DISKO =~ ^[Yy]$ ]]; then
      break
    fi
  done
}

probe_hw() {
  echo ""
  read -rn 1 -p "Probe and commit the hardware configuration to the repository? (y/N)" COMMIT_HW_CONFIG
  echo ""
  if [[ $COMMIT_HW_CONFIG =~ ^[Yy]$ ]]; then
    sudo commit-hw-config "$flake_repo.git" "$flake_name" "$flake_branch"
  fi
}

# Function to cleanup directory and clone the repo
clone_repo() {
  echo ""
  mkdir -p /mnt/etc
  cd "/mnt/etc"
  if [ -d "./nixos" ]; then
    log "Cleaning up the existing NixOS configuration directory..."
    rm -rf ./nixos || error_exit "Failed to remove old NixOS configuration directory."
  fi

  log "Cloning the flake repository..."
  if ! git clone "$repo_url" ./nixos; then
    cd
    error_exit "Failed to clone the repository."
  fi

  cd "/mnt/etc/nixos"
  log "Setting up the branch: $branch_name..."
  git fetch || error_exit "Failed to fetch latest branches."
  git checkout -f -B "$branch_name" "origin/$branch_name"
  git pull || error_exit "Failed to pull latest changes."
}

# Main installation function
install_nixos() {
  nixos-install --flake ".#$flake_name"

  # Cleanup configuration files.
  # rm configuration.nix hardware-configuration.nix || log "Note: Failed to clean up configuration files."

  log "Installation complete. Press enter to reboot or CTRL+C to cancel."
  read -r
  reboot
}

# Start script execution
log "Installing NixOS configuration: $flake_name (branch $branch_name)..."

get_disk_devices
delete_partitions
format_mount_partitions
probe_hw
clone_repo
install_nixos

cd
