#!/usr/bin/env bash

# This script mounts our Btrfs volumes in /mnt.
# Usage: sudo mount-part

set -o errexit  # Exit on error
set -o nounset  # Treat unset variables as an error
set -o pipefail # Consider errors in a pipeline

# Check if the script is run as root
if [[ $(id -u) -ne 0 ]]; then
  echo "❗ Please run with sudo"
  exit 1
fi

# Check previous command success
check_success() {
  if [[ $? -ne 0 ]]; then
    echo "❌ Error occurred: $1 failed." >&2
    exit 1
  else
    echo "✅ $1 succeeded."
  fi
}

echo "Listing available disks:"
lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

read -rp "Use a separate data disk (for /home, /srv...)? (Y/n)" USE_DATA_DISK
echo

# Create btrfs disk labels
BOOT_LABEL="BOOT"
NIXOS_LABEL="nixos"
SWAP_LABEL="swap"
DATA_LABEL="data"

TMP_MOUNT="/mnt"

# Mount the root subvolume and other subvolumes in their respective directories

mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@ /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}

mkdir -p ${TMP_MOUNT}/boot
mount -o umask=077 /dev/disk/by-label/${BOOT_LABEL} ${TMP_MOUNT}/boot; check_success "mount ${BOOT_LABEL}"

mkdir -p ${TMP_MOUNT}/var
mkdir -p ${TMP_MOUNT}/nix
mkdir -p ${TMP_MOUNT}/tmp
mkdir -p ${TMP_MOUNT}/etc

mkdir -p ${TMP_MOUNT}/home
mkdir -p ${TMP_MOUNT}/srv

mkdir -p ${TMP_MOUNT}/.snapshots
mkdir -p ${TMP_MOUNT}/.snapshots/home
mkdir -p ${TMP_MOUNT}/.snapshots/srv
mkdir -p ${TMP_MOUNT}/.snapshots/db

mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@var       /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}/var

mkdir -p ${TMP_MOUNT}/var/log
mkdir -p ${TMP_MOUNT}/var/cache
mkdir -p ${TMP_MOUNT}/var/tmp

mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@var_log   /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}/var/log
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@var_cache /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}/var/cache
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@var_tmp   /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}/var/tmp
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@nix       /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}/nix
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,subvol=@tmp       /dev/disk/by-label/${NIXOS_LABEL} ${TMP_MOUNT}/tmp

if [[ ! $USE_DATA_DISK =~ ^[Nn]$ ]]; then
  NIXOS_OR_DATA_LABEL=${DATA_LABEL}
else
  NIXOS_OR_DATA_LABEL=${NIXOS_LABEL}
fi

mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,autodefrag,subvol=@home           /dev/disk/by-label/${NIXOS_OR_DATA_LABEL} ${TMP_MOUNT}/home
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,autodefrag,subvol=@srv            /dev/disk/by-label/${NIXOS_OR_DATA_LABEL} ${TMP_MOUNT}/srv

mkdir -p ${TMP_MOUNT}/srv/db

mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,autodefrag,subvol=@db             /dev/disk/by-label/${NIXOS_OR_DATA_LABEL} ${TMP_MOUNT}/srv/db
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,autodefrag,subvol=@snapshots_home /dev/disk/by-label/${NIXOS_OR_DATA_LABEL} ${TMP_MOUNT}/.snapshots/home
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,autodefrag,subvol=@snapshots_srv  /dev/disk/by-label/${NIXOS_OR_DATA_LABEL} ${TMP_MOUNT}/.snapshots/srv
mount -o compress=zstd,space_cache=v2,noatime,ssd,discard=async,autodefrag,subvol=@snapshots_db   /dev/disk/by-label/${NIXOS_OR_DATA_LABEL} ${TMP_MOUNT}/.snapshots/db

lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS

# Generate NixOS config
# Ask user confirmation before generating the config
read -rp "Generate default NixOS configuration? (Y/n)" GENERATE_CONFIG
echo
if [[ ! $GENERATE_CONFIG =~ ^[Nn]$ ]]; then
  nixos-generate-config --root ${TMP_MOUNT}
fi
