#!/usr/bin/env nix-shell
#!nix-shell -i bash -p git
# shellcheck shell=bash

# This script is used to download the latest version of the installation scripts
# usage:
# start-install <flake-repo> <flake-branch> <flake-name>

# Exit immediately if a command exits with a non-zero status
set -e -o pipefail -o nounset

clear

flake_repo=$1
flake_branch=$2
flake_name=$3

flake_repo_main_branch="main"
default_flake_name="vmware-workstation"

# if not arguments passed, display the list of remote branch names on origin/ and prompt the user to select one, defaulting to $flake_repo_main_branch
if [ -z "$flake_branch" ]; then
  flake_branches=$(git ls-remote --heads "$flake_repo.git" | awk -F'/' '{print $3}' | sort | uniq)
  # if branches contains master, set flake_repo_main_branch to master
  if echo "$flake_branches" | grep -q "master"; then
    flake_repo_main_branch="master"
  fi
  echo "Available branches in $flake_repo:"
  echo "$flake_branches"
  echo ""
  read -rp "Enter the branch name to use (default: $flake_repo_main_branch): " flake_branch
  if [ -z "$flake_branch" ]; then
    flake_branch=$flake_repo_main_branch
  fi
fi

if [ -z "$flake_name" ]; then
  read -rp "Please enter the name of the configuration to install: (qemu, vmware-workstation, ...) (default=$default_flake_name)" flake_name
  echo ""
  if [ -z "$flake_name" ]; then
    flake_name=$default_flake_name
  fi
fi

# disko-config.nix
rm "./disko-config*" || true

# git clone the repo and checkout the branch
git clone "$flake_repo.git" /tmp/nixos || true
cd /tmp/nixos
git checkout -f -B "$flake_branch" "origin/$flake_branch"
git pull
cd
cp -f /tmp/nixos/hardwares/$flake_name/disko-config* ~/ || true

echo "Scripts downloaded successfully."
echo ""

sudo install-nixos "$flake_repo.git" "$flake_name" "$flake_branch"
