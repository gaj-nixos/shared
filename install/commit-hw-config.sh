#!/usr/bin/env nix-shell
#!nix-shell -i bash -p git
# shellcheck shell=bash

# This script is used to commit a new auto-generated hardware configuration to the repository
# usage: commit-hw-config <repo-url> <flake_name> <branch-name>

# Check the script is run with sudo
if [ "$(id -u)" -ne 0 ]; then
  echo "❗ Please run with sudo"
  exit 1
fi

repo_url=$1
flake_name=${2:-$default_flake}
branch_name=${3:-$default_branch}

pushd /mnt/etc/nixos || exit
  if [ -d "./tmp" ]; then
      rm -rf ./tmp
  fi
  git clone "$repo_url" ./tmp
  pushd ./tmp || exit
  git fetch
  git checkout -f -b "$branch_name" "origin/$branch_name"
  git pull
  popd || exit

  nixos-generate-config --root /mnt --force

  # create a new directory under ./hardwares with the name of the hardware configuration
  mkdir -p "./tmp/hardwares/$flake_name"
  # Move the current hardware-configuration.nix to the new directory
  cp ./hardware-configuration.nix "./tmp/hardwares/$flake_name"
  # Configure git to allow pushing to the repository
  echo "Enter your Git username: "
  read -r git_username
  echo "Enter your Git email: "
  read -r git_email
  if [ -z "$git_username" ] || [ -z "$git_email" ]; then
      git_username="Gaël James"
  fi
  if [ -z "$git_email" ]; then
      git_email="gaeljames@gmail.com"
  fi
  git config --global user.name "$git_username"
  git config --global user.email "$git_email"
  # Add the new hardware configuration to the repository
  pushd ./tmp || exit
  git pull
  git add "./hardwares/$flake_name/hardware-configuration.nix"
  git commit -m "Add hardware configuration for $flake_name"
  git push
  echo "Please update the flake to install with this configuration"
  read -r
popd || exit

rm -rf ./tmp
popd || exit
