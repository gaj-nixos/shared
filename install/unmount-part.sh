#!/usr/bin/env bash

# This script partitions a disk for NixOS installation with EFI and Btrfs support.
# Usage: sudo unmount-part

# Check if the script is run as root
if [[ $(id -u) -ne 0 ]]; then
  echo "❗ Please run with sudo"
  exit 1
fi

# Check previous command success
check_success() {
  if [[ $? -ne 0 ]]; then
    echo "Error occurred: $1 failed." >&2
    exit 1
  else
    echo "$1 succeeded."
  fi
}

TMP_MOUNT="/mnt"

# Mount the root subvolume and other subvolumes in their respective directories

umount ${TMP_MOUNT}/.snapshots/home
umount ${TMP_MOUNT}/.snapshots/srv
umount ${TMP_MOUNT}/.snapshots/db
umount ${TMP_MOUNT}/var/log
umount ${TMP_MOUNT}/var/cache
umount ${TMP_MOUNT}/var/tmp
umount ${TMP_MOUNT}/var
umount ${TMP_MOUNT}/srv/db
umount ${TMP_MOUNT}/srv
umount ${TMP_MOUNT}/boot
umount ${TMP_MOUNT}/efi
umount ${TMP_MOUNT}/nix
umount ${TMP_MOUNT}/tmp
umount ${TMP_MOUNT}/home
umount ${TMP_MOUNT}

cryptsetup close rootCrypted
cryptsetup close dataCrypted

lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINTS
