{
  pkgs,
  lib,
  osConfig,
  ...
}:
{
  config = lib.mkIf osConfig.sharedConfig.software.advancedMonitoringTools.enable {
    home.packages = with pkgs; [
      dnstop # dns monitoring
      kmon
      hwinfo
      sysstat # sar, iostat, mpstat, pidstat, sadf
      ethtool
      pciutils
      usbutils
      lshw
    ];
  };
}
