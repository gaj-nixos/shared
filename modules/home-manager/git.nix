{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  signGitCommits = usr.signGitCommits;
  gitAuthorizedKeys = usr.gitAuthorizedKeys;
  allowedSigners = lib.concatStringsSep "\n" (map (key: "* ${key}") gitAuthorizedKeys);
in
{
  config = {
    home.file = lib.mkIf signGitCommits { ".ssh/allowed_signers".text = allowedSigners; };

    programs = {
      git = {
        enable = true;
        lfs.enable = true;
        userName = usr.name;
        userEmail = usr.email;
        signing = {
          format = "ssh";
        };

        extraConfig = {
          commit.gpgsign = signGitCommits;
          gpg.ssh.allowedSignersFile = lib.mkIf signGitCommits "~/.ssh/allowed_signers";
          user.signingkey = lib.mkIf signGitCommits "~/.ssh/id_rsa.pub";

          push = {
            autoSetupRemote = true;
            default = "simple";
          };

          init.defaultBranch = "master";

          credential.helper = "store --file ~/.git-credentials";

          sequence.editor = "${pkgs.git-interactive-rebase-tool}/bin/interactive-rebase-tool";

          # credential.helper = "${
          #   pkgs.git.override { withLibsecret = true; }
          # }/bin/git-credential-libsecret";

          diff.lockb = {
            textconv = "bun";
            binary = true;
          };

          core = {
            editor = usr.editor;
            autocrlf = false;
            quotePath = false;
          };

          pull.rebase = true;
          fetch.prune = true;
          branch.autosetuprebase = "always";
          rerere.enabled = true;
          color.ui = true;

          blame = {
            date = "relative";
          };

          "color \"diff-highlight\"" = {
            oldNormal = "red bold";
            oldHighlight = "red bold";
            newNormal = "green bold";
            newHighlight = "green bold ul";
          };

          "color \"diff\"" = {
            meta = "yellow";
            frag = "magenta bold";
            commit = "yellow bold";
            old = "red bold";
            new = "green bold";
            whitespace = "red reverse";
          };
        };

        diff-so-fancy.enable = true;
      };

      lazygit.enable = true;
    };
  };
}
