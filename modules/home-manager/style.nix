{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";

  # if file exists
  settings =
    if lib.pathExists (../../resources/themes/omp + "/${usr.ohMyPoshTheme}.omp.nix") then
      (import (../../resources/themes/omp + "/${usr.ohMyPoshTheme}.omp.nix") { inherit config; })
    else
      (import (../../resources/themes/omp + "/${usr.ohMyPoshTheme}.omp") { inherit config; });
in
{
  config = {
    stylix =
      (import ../stylix.nix { stylixSettings = usr.stylixUser; } { inherit config pkgs lib; }).stylix;

    programs = {
      oh-my-posh = {
        enable = true;
        enableZshIntegration = usr.software.zsh.enable;
        inherit settings;
      };
    };
  };
}
