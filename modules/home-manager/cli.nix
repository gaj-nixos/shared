{ pkgs, ... }:
{
  config = {
    home.packages = with pkgs; [
      btrfs-snapshots-diff # see https://github.com/sysnux/btrfs-snapshots-diff/blob/master/create-example.sh
      fdupes
      duperemove
      rsync
      atool # archive command line helper
      which # show the location of a program
      pydf # disk free utility
      television # fuzzy finder
      gping # ping graph
      scooter # search and replace
      wget # download files
      tldr # too long, didn't read the man page
      rainfrog # database management TUI
      superfile # file manager
    ];

    programs = {
      bat.enable = true; # better cat
      lsd.enable = true;
      btop.enable = true;
      #command-not-found.enable = true;
      ranger.enable = true; # console file manager

      zellij = {
        # terminal multiplexer
        enable = true;
        enableBashIntegration = false;
        enableZshIntegration = false;
      };

      jq.enable = true;

      fzf = {
        enable = true;
        enableBashIntegration = true;
        enableZshIntegration = true;
      };
    };
  };
}
