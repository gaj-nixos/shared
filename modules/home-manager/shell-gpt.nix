{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig.software.shellGpt;
  username = config.home.username;
  shellGptConfig = ''
    # API key, also it is possible to define OPENAI_API_KEY env.
    OPENAI_API_KEY="1234"

    # Base URL of the backend server. If "default" URL will be resolved based on --model.
    API_BASE_URL=${cfg.ollamaServerUrl}

    # Max amount of cached message per chat session.
    CHAT_CACHE_LENGTH=100

    # Chat cache folder.
    CHAT_CACHE_PATH=/tmp/shell_gpt/chat_cache

    # Request cache length (amount).
    CACHE_LENGTH=100

    # Request cache folder.
    CACHE_PATH=/tmp/shell_gpt/cache

    # Request timeout in seconds.
    REQUEST_TIMEOUT=60

    # Default OpenAI model to use.
    DEFAULT_MODEL=${cfg.model}

    # Default color for shell and code completions.
    DEFAULT_COLOR=magenta

    # When in --shell mode, default to "Y" for no input.
    DEFAULT_EXECUTE_SHELL_CMD=false

    # Disable streaming of responses
    DISABLE_STREAMING=false

    # The pygment theme to view markdown (default/describe role).
    CODE_THEME=default

    # Path to a directory with functions.
    OPENAI_FUNCTIONS_PATH=/home/${username}/.config/shell_gpt/functions

    # Print output of functions when LLM uses them.
    SHOW_FUNCTIONS_OUTPUT=false

    # Allows LLM to use functions.
    OPENAI_USE_FUNCTIONS=false

    # Enforce LiteLLM usage (for local LLMs).
    USE_LITELLM=true

    SHELL_INTERACTION=false
    PRETTIFY_MARKDOWN=false
    OS_NAME="NixOS"
    SHELL_NAME=auto
  '';
  shellGptConfigPath = "${config.xdg.configHome}/shell_gpt/.sgptrc";
in
{
  config = lib.mkIf cfg.enable {
    home.packages = with pkgs.gaelj; [
      shell-gpt
    ];

    # it overwrites its configuration file so we need to set it in an activation script
    home.activation = {
      createShellGptConfiguration = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
        run mkdir -p ${config.xdg.configHome}/shell_gpt
        run echo "${shellGptConfig}" > ${shellGptConfigPath}
      '';
    };
  };
}
