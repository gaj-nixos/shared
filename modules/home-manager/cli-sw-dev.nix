{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf usr.software.swDevPrograms.enable {
    home.packages = with pkgs; [
      nixd
      nil
      nix-doc # Nix/NixOS package documentation
      nix-script # write quick scripts in compiled languages, transparently compile and cache them, and pull in whatever dependencies you need from the Nix ecosystem https://github.com/BrianHicks/nix-script
      nixos-generators # generate nixos installation ISO etc
    ];

    programs.ripgrep.enable = true; # recursively search the current directory for a regex pattern
  };
}
