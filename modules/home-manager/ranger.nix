{
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  homeDir = if username == "root" then "/root" else "/home/${username}";
in
{
  config = {
    programs.ranger = {
      enable = true;
      settings.unicode_ellipsis = true;
      plugins = [
        {
          name = "ranger_devicons";
          src = pkgs.fetchFromGitHub {
            owner = "alexanderjeurissen";
            repo = "ranger_devicons";
            rev = "a8d626485ca83719e1d8d5e32289cd96a097c861";
            sha256 = "sha256-sijO9leDXgnjgcVlh5fKYalhjOupwFMRyH0xh2g/rEQ=";
          };
        }
      ];
      extraConfig =
        if (usr.software.guiPrograms.enable) then
          ''
            default_linemode devicons

            set preview_images true
            set preview_images_size 50%
            set preview_images_method kitty

            set preview_script ${homeDir}/.config/ranger/scope.sh
          ''
        else
          ''
            default_linemode devicons
          '';
    };

    xdg.configFile."ranger/scope.sh".source = ../../resources/scripts/ranger/scope.sh;
  };
}
