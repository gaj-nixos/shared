{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf usr.software.guiPrograms.enable {
    home = {
      packages = with pkgs; [
        ghostty.terminfo
      ];
    };

    programs.ghostty = {
      enable = true;
      #installBatSyntax
      #installVimSyntax
      enableZshIntegration = usr.software.zsh.enable;
      enableBashIntegration = true;
      #enableFishIntegration
      #clearDefaultKeybinds
      #package
      #settings
      #themes
    };
  };
}
