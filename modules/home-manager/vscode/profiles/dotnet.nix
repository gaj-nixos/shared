{ pkgs, ... }:

let
  netCore = import ../../../../pkgs/dotnet/dotnet.nix pkgs;
in
{
  extensions = [
    #(pkgs.master.vscode-utils.extensionFromVscodeMarketplace {
    #  name = "csdevkit-preview";
    #  publisher = "ms-dotnettools";
    #  version = "1.0.230602001";
    #  sha256 = "0000000000000000000000000000000000000000000000000000";
    #})
    pkgs.master.vscode-extensions.ms-dotnettools.csharp
  ];
  extraPackages = [ netCore ];
  icon128 = pkgs.fetchurl {
    url = "https://raw.githubusercontent.com/dotnet/brand/main/logo/dotnet-logo.png";
    sha256 = "sha256-j89vbNV1wPjGQ2kXZafbKks7EEv7/zRkZVX1zP/bKJU=";
  };
  extraCommandLineArguments = [ "--locale fr-FR" ];
}
