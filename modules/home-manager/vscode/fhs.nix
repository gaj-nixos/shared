{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";

  vscodeFhsPkg = pkgs.master.vscode.fhsWithPackages (
    ps: with ps; [
      git
      rustup
      zlib
      openssl.dev
      pkg-config
      playwright-driver.browsers
    ]
  );

in
{
  config = lib.mkIf (usr.software.guiPrograms.enable && usr.software.swDevPrograms.enable) {
    programs = {
      vscode = {
        enable = true;
        extensions = [ ];
        package = vscodeFhsPkg;
      };
    };

    stylix.targets.vscode.enable = false;
  };
}
