{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";

  vscode-with-extensions = pkgs.master.vscode-with-extensions;

  # Create isolated profiles
  mkProfile =
    name: extensions: extraPackages: extraCommandLineArguments:
    let
      vscode-pkg = vscode-with-extensions.override {
        vscodeExtensions = extensions;
      };

      concatExtraCommandLineArguments =
        if extraCommandLineArguments == null then
          ""
        else
          (lib.concatStringsSep " " extraCommandLineArguments);

      wrapped = pkgs.writeShellScriptBin "vscode-${name}" ''
        exec ${vscode-pkg}/bin/code \
          --user-data-dir "${config.xdg.configHome}/vscode/${name}-data" \
          --extensions-dir "${config.xdg.dataHome}/vscode/${name}-extensions" \
          ${concatExtraCommandLineArguments} \
          "$@"
      '';
    in
    pkgs.symlinkJoin {
      name = "vscode-${name}";
      paths = [ wrapped ] ++ extraPackages;
    };

  profiles = {
    dotnet = import ./profiles/dotnet.nix { inherit pkgs; };
  };

in
{
  config = lib.mkIf (usr.software.guiPrograms.enable && usr.software.swDevPrograms.enable) {
    home = {
      packages = (
        map (
          name:
          mkProfile name profiles.${name}.extensions profiles.${name}.extraPackages
            profiles.${name}.extraCommandLineArguments
        ) (builtins.attrNames profiles)
      );
    };

    xdg.desktopEntries = builtins.mapAttrs (name: profile: {
      name = "Visual Studio Code (${name})";
      exec = "vscode-${name} %F";
      icon = "code-${name}";
      comment = "VS Code profile for ${name} development";
      genericName = "Text Editor";
      categories = [
        "Utility"
        "TextEditor"
        "Development"
        "IDE"
      ];
      startupNotify = true;
      settings = {
        Keywords = "vscode;${name};";
        StartupWMClass = "Code-${name}";
      };
    }) profiles;

    # Icons
    xdg.dataFile = lib.mapAttrs' (name: profile: {
      name = "icons/hicolor/128x128/apps/code-${name}.png";
      value.source = profile.icon128;
    }) profiles;

  };
}
