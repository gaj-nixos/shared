# A module that automatically imports everything else in the parent folder.
{
  imports = [
    ./fhs.nix
    ./pure.nix
  ];
}
