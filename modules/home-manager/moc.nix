{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf usr.software.moc.enable {
    home = {
      packages = with pkgs; [
        moc
      ];

      file.".moc/config".text = ''
        # See https://github.com/jonsafari/mocp/blob/master/config.example.in

        # Show file titles (title, author, album) instead of file names?
        #ReadTags = yes

        #MusicDir =
        #StartInMusicDir = no

        # Values of the TERM environment variable which are deemed to be managed by
        # screen(1).  If you are setting a specific terminal using screen(1)'s
        # '-T <term>' option, then you will need to add 'screen.<term>' to this list.
        # Note that this is only a partial test; the value of the WINDOW environment
        # variable must also be a number (which screen(1) sets).
        #ScreenTerms = screen:screen-w:vt100

        # Values of the TERM environment variable which are deemed to be xterms.  If
        # you are using MOC within screen(1) under an xterm, then add screen(1)'s
        # TERM setting here as well to cause MOC to update the xterm's title.
        #XTerms = xterm
        #XTerms += xterm-colour:xterm-color
        #XTerms += xterm-256colour:xterm-256color
        #XTerms += rxvt:rxvt-unicode
        #XTerms += rxvt-unicode-256colour:rxvt-unicode-256color
        #XTerms += eterm

        # Theme file to use.  This can be absolute path or relative to
        # /usr/share/moc/themes/ (depends on installation prefix) or
        # ~/.moc/themes/ .
        #
        # Example:    Theme = laras_theme
        #
        Theme = ${usr.software.moc.theme}

        # The theme used when running on an xterm.
        #
        # Example:    XTermTheme = transparent-background
        #
        #XTermTheme =
      '';

      file.".moc/themes/nightly_theme".text = ''
        # Author: Wim Speekenbrink <w.speek@wanadoo.nl>

        background          = blue      black
        frame               = blue      black   bold
        window_title        = blue      black   bold
        directory           = blue      black   bold
        selected_directory  = black     magenta
        playlist            = blue      black   bold
        selected_playlist   = black     magenta
        file                = blue      black   bold
        selected_file       = black     magenta
        marked_file         = green     black   bold
        marked_selected_file = green    magenta bold
        info                = green     black   bold
        status              = blue      black   bold
        title               = green     black   bold
        state               = blue      black   bold
        current_time        = magenta   black   bold
        time_left           = magenta   black   bold
        total_time          = magenta   black   bold
        time_total_frames   = blue      black   bold
        sound_parameters    = magenta   black   bold
        legend              = blue      black   bold
        disabled            = black     black
        enabled             = blue      black   bold
        empty_mixer_bar     = blue      black   bold
        filled_mixer_bar    = black     magenta
        empty_time_bar      = blue      black   bold
        filled_time_bar     = black     magenta
        entry               = green     black   bold
        entry_title         = green     black   bold
        error               = red       black   bold
        message             = green     black   bold
        plist_time          = green	black	bold
      '';
    };
  };
}
