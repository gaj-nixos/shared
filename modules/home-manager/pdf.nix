{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf usr.software.guiPrograms.enable {
    programs.zathura = {
      enable = true;
      options = {
        scroll-page-aware = true;
      };
      extraConfig = "include theme";
    };

    xdg.configFile."zathura/theme".source =
      "${pkgs.vimPlugins.tokyonight-nvim}/extras/zathura/tokyonight_night.zathurarc";

    xdg.mimeApps = {
      enable = true;
      defaultApplications = {
        "application/pdf" = "org.pwmt.zathura.desktop";
      };
    };
  };
}
