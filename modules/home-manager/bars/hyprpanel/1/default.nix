{
  lib,
  osConfig,
  config,
  ...
}:

let
  location = osConfig.sharedConfig.localisation.weatherLocation;
  sensor = osConfig.sharedConfig.devices.CPU.sensor;
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;

  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;

  hyprEnabled = hyprGlobalEnabled && usrHyprEnabled;

  hyprpanelEnabled = usr.bar == "hyprpanel";

  enable = hyprpanelEnabled && hyprEnabled;
in
{
  config = lib.mkIf enable {
    programs.hyprpanel = {

      # Import a theme from './themes/*.json'.
      # Default: ""
      theme = usr.hyprPanelTheme;

      # Override the final config with an arbitrary set.
      # Useful for overriding colors in your selected theme.
      # Default: {}
      override = {
        #theme.bar.menus.text = "#123ABC";
      };

      # Configure bar layouts for monitors.
      # See 'https://hyprpanel.com/configuration/panel.html'.
      # Default: null
      layout = {
        "bar.layouts" = {
          "0" = {
            left = [
              "dashboard"
              "workspaces"
              "ram"
              "cpu"
              "cputemp"
              "storage"
              #"updates"
              #"weather"
            ];
            middle = [
              "windowtitle"
            ];
            right = [
              "netstat"
              #"hyprsunset"
              #"hypridle"
              #"battery"
              "kbinput"
              "submap"
              "network"
              "bluetooth"

              "systray"
              "volume"
              "clock"
              "notifications"
              "power"
            ];
          };

          "*" = {
            left = [
              "dashboard"
              "workspaces"
            ];
            middle = [
              "media"
              "cava"
              "windowtitle"
            ];
            right = [
              "volume"
              "clock"
              "notifications"
              "power"
            ];
          };
        };
      };

      # Configure and theme almost all options from the GUI.
      # Options that require '{}' or '[]' are not yet implemented,
      # except for the layout above.
      # See 'https://hyprpanel.com/configuration/settings.html'.
      # Default: <same as gui>
      settings = {
        bar.launcher.autoDetectIcon = true;
        bar.workspaces = {
          #show_icons = true;
          show_numbered = true;
        };
        bar.customModules.cpuTemp.sensor = sensor;
        bar.customModules.weather.unit = "metric";
        bar.clock.format = "%a %d %b %Y %H:%M:%S";
        bar.windowtitle.truncation_size = 80;

        menus.dashboard = {
          shortcuts = {
            left = {
              shortcut1.command = "uwsm app -- brave --ozone-platform-hint=auto";
              shortcut1.icon = "";
              shortcut1.tooltip = "Brave Browser";

              shortcut2.command = "uwsm app -- code --ozone-platform=wayland";
              shortcut2.icon = "";
              shortcut2.tooltip = "Visual Studio Code";

              shortcut3.command = "uwsm app -- thunar";
              shortcut3.icon = "";
              shortcut3.tooltip = "File Manager";

              shortcut4.command = "pkill wofi || uwsm app -- wofi --show drun -I";
              shortcut4.icon = "";
              shortcut4.tooltip = "Application Launcher";
            };
          };

          directories.enabled = false;
          stats.enable_gpu = true;
        };

        menus.clock = {
          time = {
            military = true;
            hideSeconds = true;
          };
          weather = {
            enabled = false;
            unit = "metric";
            inherit location;
          };
        };

        theme.bar.transparent = true;

        theme.font = {
          name = "CaskaydiaCove NF";
          size = "1.2rem";
        };
      };
    };
  };
}
