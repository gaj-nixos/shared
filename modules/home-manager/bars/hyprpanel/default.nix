{
  lib,
  config,
  osConfig,
  ...
}:

let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;

  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;

  hyprEnabled = hyprGlobalEnabled && usrHyprEnabled;

  hyprpanelEnabled = usr.bar == "hyprpanel";

  enable = hyprpanelEnabled && hyprEnabled;
in
{
  imports = [ ./1 ];

  config = lib.mkIf enable {
    programs.hyprpanel = {
      inherit enable;

      overlay.enable = false;

      # Add '/nix/store/.../hyprpanel' to your
      # Hyprland config 'exec-once'.
      # Default: false
      hyprland.enable = true;

      # Fix the overwrite issue with HyprPanel.
      # Default: false
      overwrite.enable = true;
    };
  };
}
