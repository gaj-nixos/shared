{
  lib,
  config,
  pkgs,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usri3Enabled = (usr ? i3.enable) && usr.i3.enable;
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;

  i3GlobalEnabled = (cfg ? windowManagers.i3) && cfg.windowManagers.i3;
  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;

  i3Enabled = i3GlobalEnabled && usri3Enabled;
  hyprEnabled = hyprGlobalEnabled && usrHyprEnabled;
in
{
  config = {
    programs.waybar.settings = {
      mainBar = {
        layer = "top";
        position = "top";
        reload_style_on_change = true;

        modules-left = [
          "custom/os"
          "idle_inhibitor"
          "privacy"
          "systemd-failed-units"
          "power-profiles-daemon"
          "cpu"
          "load"
          "memory"
          "disk"
          "custom/storage"
          "custom/storage2"
          "backlight"
        ];

        modules-center =
          [
            #"sway/window"
            "cava"
            "mpris"
          ]
          ++ (lib.optional hyprEnabled "hyprland/workspaces")
          ++ (lib.optional i3Enabled "sway/workspaces");

        modules-right = [
          "network"
          #"network#wl"
          "keyboard-state"
          "bluetooth"
          "pulseaudio"
          "upower"
          "mako"
          "battery"
          "tray"
          "clock"
        ];

        "custom/os" = {
          "format" = "{} ";
          "exec" = ''echo "" '';
          "interval" = "once";
          "on-click" = "pkill wofi || wofi --show drun -I";
        };
        battery = {
          "states" = {
            "good" = 95;
            "warning" = 30;
            "critical" = 15;
          };
          "format" = "{icon} {capacity}%";
          "format-charging" = " {capacity}%";
          "format-plugged" = " {capacity}%";
          #"format-good" = ""; # An empty format will hide the module
          #"format-full" = "";
          "format-icons" = [
            ""
            ""
            ""
            ""
            ""
          ];
        };
        "hyprland/workspaces" = {
          "on-click" = "activate";
          "on-scroll-up" = "hyprctl dispatch workspace e+1";
          "on-scroll-down" = "hyprctl dispatch workspace e-1";
          #"all-outputs" = true;
          #"active-only" = true;
          "ignore-workspaces" = [
            "scratch"
            "-"
          ];
          #"show-special" = false;
        };
        privacy = {
          icon-size = 16;
          modules = [
            {
              type = "screenshare";
              tooltip = true;
              tooltip-icon-size = 16;
            }
            {
              type = "audio-in";
              tooltip = true;
              tooltip-icon-size = 16;
            }
          ];
        };
        idle_inhibitor = {
          format = "{icon}";
          format-icons = {
            activated = "";
            deactivated = "";
          };
        };
        "keyboard-state" = {
          "numlock" = true;
          "format" = "{icon} ";
          "format-icons" = {
            "locked" = "󰎠";
            "unlocked" = "󱧓";
          };
        };
        cava = {
          framerate = 30;
          autosens = 1;
          bars = 14;
          lower_cutoff_freq = 50;
          higher_cutoff_freq = 10000;
          method = "pipewire";
          source = "auto";
          stereo = true;
          bar_delimiter = 0;
          noise_reduction = 0.77;
          input_delay = 2;
          hide_on_silence = true;
          format-icons = [
            "▁"
            "▂"
            "▃"
            "▄"
            "▅"
            "▆"
            "▇"
            "█"
          ];
          actions = {
            "on-click-right" = "mode";
          };
        };
        load = {
          format = "{load1} 󰣖";
          interval = 2;
        };
        memory = {
          format = "{percentage}%/{swapPercentage}% ";
          interval = 2;
        };
        disk = {
          format = "{free} {path}";
          path = "/";
        };
        cpu = {
          "format" = "  {usage}%";
        };
        "custom/storage" = {
          #format = "{}";
          format = "  {}";
          #tooltip-format = "{tooltip}";
          return-type = "json";
          interval = 60;
          exec = "${../../../../../resources/scripts/waybar/storage.sh} /mnt";
        };
        "custom/storage2" = {
          #format = "{}";
          format = "  {}";
          #tooltip-format = "{tooltip}";
          return-type = "json";
          interval = 60;
          exec = "${../../../../../resources/scripts/waybar/storage.sh} /srv";
        };
        clock = {
          timezone = osConfig.time.timeZone;
          format = "{:%H:%M %Z} ";
          format-alt = "{:%a %F} ";
          interval = 5;
          tooltip-format = "<big>{:%Y}</big>\n<tt><small>{calendar}</small></tt>";
          calendar = {
            mode = "year";
            mode-mon-col = 3;
            week-pos = "left";
          };
          actions = {
            on-click-left = "mode";
            on-scroll-up = "tz_up";
            on-scroll-down = "tz_down";
          };
        };
        backlight = {
          format = "{percent} {icon}";
          format-icons = [
            ""
            ""
            ""
            ""
            ""
            ""
            ""
            ""
            ""
          ];
          on-scroll-up = "${lib.getExe pkgs.light} -U 5";
          on-scroll-down = "${lib.getExe pkgs.light} -A 5";
        };
        power-profiles-daemon = {
          format-icons = {
            default = "";
            performance = "";
            balanced = "";
            power-saver = "";
          };
        };
        upower = {
          icon-size = 16;
        };
        network = {
          interface = osConfig.sharedConfig.network.interface;
          format-ethernet = "{bandwidthDownBits} {bandwidthUpBits} ";
          tooltip-format-ethernet = "{ifname} {ipaddr}/{cidr} via {gwaddr}";
          format-linked = "{ifname} (No IP) ";
          format-disconnected = "";
          format-alt = "{ifname} {ipaddr}/{cidr}";
          interval = 2;
          "format-icons" = {
            "wifi" = [
              ""
              ""
              ""
            ];
            "ethernet" = [ "" ];
            "disconnected" = [ "" ];
          };
          "on-click" = "${usr.terminal} -e nmtui";
        };
        "network#wl" = {
          interface = "wlp*";
          format-wifi = "{bandwidthDownBits} {bandwidthUpBits} {essid} {signalStrength}% ";
          tooltip-format-wifi = "{ipaddr}/{cidr} via {gwaddr} with {signaldBm}dBm";
          format-linked = "{ifname} (No IP) ";
          format-disconnected = "";
          format-alt = "{ifname} {ipaddr}/{cidr}";
          interval = 2;
        };
        pulseaudio = {
          scroll-step = 1;
          format = "{volume}% {icon} {format_source}";
          format-bluetooth = "{volume}% {icon} {format_source}";
          format-bluetooth-muted = " {icon} {format_source}";
          format-muted = " {format_source}";
          format-source = "{volume}% ";
          format-source-muted = "";
          format-icons = {
            headphone = "󰋋";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = [
              ""
              ""
              ""
            ];
          };
          on-click = "${usr.terminal} -e ${lib.getExe pkgs.pulsemixer}";
        };
        bluetooth = {
          format = " {status}";
          format-no-controller = "";
          format-on = "";
          format-off = "󰂲";
          format-connected = " {device_alias}";
          format-connected-battery = " {device_alias} {device_battery_percentage}%";
          tooltip-format = "{controller_alias}\t{controller_address}\n\n{num_connections} connected";
          tooltip-format-connected = "{controller_alias}\t{controller_address}\n\n{num_connections} connected\n\n{device_enumerate}";
          tooltip-format-enumerate-connected = "{device_alias}\t{device_address}";
          tooltip-format-enumerate-connected-battery = "{device_alias}\t{device_address}\t{device_battery_percentage}%";
        };
        mpris = {
          format = "{player_icon} {dynamic}";
          format-paused = "{status_icon} <i>{dynamic}</i>";
          dynamic-order = [
            "title"
            "artist"
          ];
          player-icons = {
            default = "▶";
            spotify = "";
            spotifyd = "";
            firefox = "";
            librewolf = "";
            chromium = "";
          };
          status-icons = {
            paused = "";
          };
        };
        mako = {
          "format" = "{body}";
          "max-history" = 5;
        };
        systemd-failed-units = {
          hide-on-ok = false;
          format = "✗ {nr_failed}";
          format-ok = "✓";
          system = true;
          user = true;
        };
      };
    };
  };
}
