{ ... }:
{
  imports = [
    ./waybar.nix
    ./waybar_style.nix
  ];
}
