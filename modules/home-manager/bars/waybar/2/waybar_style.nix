{ ... }:
let
  padding = "5px 25px";
  borderRadius = "0px";
  verticalSep = "0px";
  bgCol = "#24283b";
in
{
  config = {
    programs.waybar.style = ''
      * {
        border: none;
        border-radius: 0;
        font-family: "Departure Mono";
        font-size: 11pt;
        min-height: 0;
      }

      window#waybar {
        background-color: rgba(0,0,0,0.8);
        color: #A9B1D6;
      }

      #custom-os {
        background-color: ${bgCol};
        padding: ${padding};
        margin: 0px;
        margin-right: ${verticalSep};
        border-radius: ${borderRadius};
      }

      #workspaces {
        background-color: ${bgCol};
        padding: ${padding};
        margin: 0px;
        margin-left: 60px;
        margin-right: ${verticalSep};
        border-radius: ${borderRadius};
      }

      #workspaces button {
        padding: 2px 5px;
        color: #A9B1D6;
      }

      #workspaces button:first-child {
        margin-left: ${verticalSep};
      }

      #workspaces button:last-child {
        margin-right: ${verticalSep};
      }

      #workspaces button.focused {
        color: ${bgCol};
        background-color: #7aa2f7;
        border-radius: ${borderRadius};
      }

      #workspaces button:hover {
        background-color: #7dcfff;
        color: ${bgCol};
        border-radius: ${borderRadius};
      }

      #workspaces button.urgent {
        background-color: #b16286;
      }

      #cava {
        background-color: ${bgCol};
        padding: ${padding};
        margin: 0px;
        margin-left: ${verticalSep};
        margin-right: 60px;
        border-radius: ${borderRadius};
      }

      #idle_inhibitor,
      #privacy,
      #systemd-failed-units,
      #power-profiles-daemon,
      #cpu,
      #load,
      #memory,
      #disk,
      #custom-storage,
      #custom-storage2,
      #backlight,

      #cava
      #mpris,
      #mode,

      #network,
      #network-wl,
      #keyboard-state,
      #bluetooth,
      #pulseaudio,
      #upower,
      #mako,
      #battery,
      #tray {
        background-color: ${bgCol};
        padding: ${padding};
        margin: 0px ${verticalSep};
        border-radius: ${borderRadius};
      }

      #load, #bluetooth {
        color: #7dcfff;
      }

      #bluetooth.off {
        color: #a9b1d6;
      }

      #memory {
        color: #bb9af7;
      }

      #disk {
        color: #9ece6a;
      }

      #tray {
        margin: 0;
      }

      #custom-storage.warning {
        color:      rgba(255, 210, 4, 1);
      }

      #custom-storage.critical {
        color:      rgba(238, 46, 36, 1);
      }

      #mode,
      #idle_inhibitor.activated {
        color: ${bgCol};
        background-color: #7aa2f7;
      }

      #clock {
        background-color: ${bgCol};
        padding: ${padding};
        margin: 0px;
        margin-left: ${verticalSep};
        border-radius: ${borderRadius};
        color: #bb9af7;
      }

      #upower {
        color: #9ece6a;
      }

      #upower.charging {
        color: #9ece6a;
      }

      #systemd-failed-units.ok {
        color: #9ece6a;
      }

      #systemd-failed-units.degraded {
        color: #f7768e;
      }

      #upower.discharging {
        background-color: #f7768e;
        color: ${bgCol};
      }

      #network, #network-wl, #bluetooth.discoverable {
        color: #f7768e;
      }

      #pulseaudio {
        color: #e0af68;
      }

      /* If workspaces is the leftmost module, omit left margin */
      .modules-left > widget:first-child > #workspaces {
        margin-left: 0;
      }

      /* If workspaces is the rightmost module, omit right margin */
      .modules-right > widget:last-child > #workspaces {
        margin-right: 0;
      }

      #battery.charging, #battery.plugged {
        background-color: #98971a;
        color: ${bgCol};
      }

      @keyframes blink {
        to {
          background-color: ${bgCol};
          color: #ebdbb2;
        }
      }

      /* Using steps() instead of linear as a timing function to limit cpu usage */
      #battery.critical:not(.charging) {
        background-color: #cc241d;
        color: #ebdbb2;
        animation-name: blink;
        animation-duration: 0.5s;
        animation-timing-function: steps(12);
        animation-iteration-count: infinite;
        animation-direction: alternate;
      }

      #tray > .passive {
        -gtk-icon-effect: dim;
      }

      #tray > .needs-attention {
        -gtk-icon-effect: highlight;
      }

      #tray menu {
        font-family: sans-serif;
      }
    '';
  };
}
