{ config, ... }:
let
  colors = config.lib.stylix.colors;
in
{
  config = {
    programs.waybar.style =
      ''
        * {
          /* `otf-font-awesome` is required to be installed for icons */
          font-family: FontAwesome, JetBrainsMono Nerd Font Propo;
          font-size: 20px;
        }

        window#waybar {
          background-color: rgba(''
      + colors.base00-rgb-r
      + ","
      + colors.base00-rgb-g
      + ","
      + colors.base00-rgb-b
      + ","
      + ''
        0.55);
                border-radius: 12px;
                color: #''
      + colors.base07
      + ''
        ;
                transition-property: background-color;
                transition-duration: .2s;
              }

              window > box {
                border-radius: 12px;
                opacity: 0.94;
              }

              window#waybar.hidden {
                opacity: 0.2;
              }

              button {
                border: none;
              }

              #custom-hyprprofile {
                color: #''
      + colors.base0D
      + ''
        ;
              }

              /* https://github.com/Alexays/Waybar/wiki/FAQ#the-workspace-buttons-have-a-strange-hover-effect */
              button:hover {
                background: inherit;
              }

              #workspaces button {
                padding: 0 7px;
                background-color: transparent;
                color: #''
      + colors.base04
      + ''
        ;
              }

              #workspaces button:hover {
                color: #''
      + colors.base07
      + ''
        ;
              }

              #workspaces button.active {
                color: #''
      + colors.base08
      + ''
        ;
              }

              #workspaces button.focused {
                color: #''
      + colors.base0A
      + ''
        ;
              }

              #workspaces button.visible {
                color: #''
      + colors.base05
      + ''
        ;
              }

              #workspaces button.urgent {
                color: #''
      + colors.base09
      + ''
        ;
              }

              #cava,
              #clock,
              #battery,
              #cpu,
              #memory,
              #disk,
              #temperature,
              #backlight,
              #network,
              #pulseaudio,
              #wireplumber,
              #custom-media,
              #tray,
              #mode,
              #idle_inhibitor,
              #scratchpad,
              #mpd {
                padding: 0 10px;
                color: #''
      + colors.base07
      + ''
        ;
                border: none;
                border-radius: 12px;
              }

              #window,
              #workspaces {
                margin: 0 4px;
              }

              /* If workspaces is the leftmost module, omit left margin */
              .modules-left > widget:first-child > #workspaces {
                margin-left: 0;
              }

              /* If workspaces is the rightmost module, omit right margin */
              .modules-right > widget:last-child > #workspaces {
                margin-right: 0;
              }

              #clock {
                color: #''
      + colors.base0D
      + ''
        ;
              }

              #battery {
                color: #''
      + colors.base0B
      + ''
        ;
              }

              #battery.charging, #battery.plugged {
                color: #''
      + colors.base0C
      + ''
        ;
              }

              @keyframes blink {
                to {
                  background-color: #''
      + colors.base07
      + ''
        ;
                  color: #''
      + colors.base00
      + ''
        ;
                }
              }

              #battery.critical:not(.charging) {
                background-color: #''
      + colors.base08
      + ''
        ;
                color: #''
      + colors.base07
      + ''
        ;
                animation-name: blink;
                animation-duration: 0.5s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;
                animation-direction: alternate;
              }

              label:focus {
                background-color: #''
      + colors.base00
      + ''
        ;
              }

              #cpu {
                color: #''
      + colors.base0D
      + ''
        ;
              }

              #memory {
                color: #''
      + colors.base0E
      + ''
        ;
              }

              #disk {
                color: #''
      + colors.base0F
      + ''
        ;
              }

              #backlight {
                color: #''
      + colors.base0A
      + ''
        ;
              }

              label.numlock {
                color: #''
      + colors.base04
      + ''
        ;
              }

              label.numlock.locked {
                color: #''
      + colors.base0F
      + ''
        ;
              }

              #pulseaudio {
                color: #''
      + colors.base0C
      + ''
        ;
              }

              #pulseaudio.muted {
                color: #''
      + colors.base04
      + ''
        ;
              }

              #tray > .passive {
                -gtk-icon-effect: dim;
              }

              #tray > .needs-attention {
                -gtk-icon-effect: highlight;
              }

              #idle_inhibitor {
                color: #''
      + colors.base04
      + ''
        ;
              }

              #idle_inhibitor.activated {
                color: #''
      + colors.base0F
      + ''
        ;
              }
      '';
  };
}
