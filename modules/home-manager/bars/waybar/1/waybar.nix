{
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = {
    programs.waybar.settings = {
      mainBar = {
        layer = "top";
        position = "top";
        height = 37;
        margin = "1 7 1 7";
        spacing = 4;
        width = 1600;

        modules-left = [
          "custom/os"
          "battery"
          "backlight"
          "cava"
          "pulseaudio"
          "cpu"
          "memory"
          "custom/storage"
          "custom/storage2"
        ];
        modules-center = [ "hyprland/workspaces" ];
        modules-right = [
          "load"
          "network"
          "idle_inhibitor"
          "tray"
          "mako"
          "clock"
        ];

        "custom/os" = {
          "format" = "{} ";
          "exec" = ''echo "" '';
          "interval" = "once";
          "on-click" = "pkill wofi || wofi --show drun -I";
        };
        battery = {
          "states" = {
            "good" = 95;
            "warning" = 30;
            "critical" = 15;
          };
          "format" = "{icon} {capacity}%";
          "format-charging" = " {capacity}%";
          "format-plugged" = " {capacity}%";
          #"format-good" = ""; # An empty format will hide the module
          #"format-full" = "";
          "format-icons" = [
            ""
            ""
            ""
            ""
            ""
          ];
        };
        backlight = {
          "format" = "{icon} {percent}%";
          "format-icons" = [
            ""
            ""
            ""
            ""
            ""
            ""
            ""
            ""
            ""
          ];
        };
        "keyboard-state" = {
          "numlock" = true;
          "format" = "{icon} ";
          "format-icons" = {
            "locked" = "󰎠";
            "unlocked" = "󱧓";
          };
        };
        cava = {
          framerate = 30;
          autosens = 1;
          bars = 14;
          lower_cutoff_freq = 50;
          higher_cutoff_freq = 10000;
          method = "pipewire";
          source = "auto";
          stereo = true;
          bar_delimiter = 0;
          noise_reduction = 0.77;
          input_delay = 2;
          hide_on_silence = true;
          format-icons = [
            "▁"
            "▂"
            "▃"
            "▄"
            "▅"
            "▆"
            "▇"
            "█"
          ];
          actions = {
            "on-click-right" = "mode";
          };
        };
        pulseaudio = {
          "scroll-step" = 1;
          "format" = "{icon}   {volume}% {format_source} ";
          "format-bluetooth" = "{icon}  {volume}%  {format_source}";
          "format-bluetooth-muted" = "{icon}  󰸈  {format_source}";
          "format-muted" = "󰸈  {format_source}";
          "format-source" = "  {volume}%";
          "format-source-muted" = "";
          "format-icons" = {
            "headphone" = "";
            "hands-free" = "";
            "headset" = "";
            "phone" = "";
            "portable" = "";
            "car" = "";
            "default" = [
              ""
              ""
              ""
            ];
          };
          "on-click" = "pavucontrol && hyprctl dispatch bringactivetotop";
        };
        cpu = {
          "format" = "  {usage}%";
        };
        memory = {
          "format" = "  {}%";
        };
        "custom/storage" = {
          #format = "{}";
          format = "  {}";
          #tooltip-format = "{tooltip}";
          return-type = "json";
          interval = 60;
          exec = "${../../../../../resources/scripts/waybar/storage.sh} /mnt";
        };
        "custom/storage2" = {
          #format = "{}";
          format = "  {}";
          #tooltip-format = "{tooltip}";
          return-type = "json";
          interval = 60;
          exec = "${../../../../../resources/scripts/waybar/storage.sh} /srv";
        };
        "hyprland/workspaces" = {
          #"format" = "{icon}";
          #"format-icons" = {
          #  "1" = "󱚌";
          #  "2" = "󰖟";
          #  "3" = "";
          #  "4" = "󰎄";
          #  "5" = "󰋩";
          #  "6" = "";
          #  "7" = "󰄖";
          #  "8" = "󰑴";
          #  "9" = "󱎓";
          #  "scratch_term" = "_";
          #  "scratch_ranger" = "_󰴉";
          #  "scratch_musikcube" = "_";
          #  "scratch_btm" = "_";
          #  "scratch_pavucontrol" = "_󰍰";
          #};
          "on-click" = "activate";
          "on-scroll-up" = "hyprctl dispatch workspace e+1";
          "on-scroll-down" = "hyprctl dispatch workspace e-1";
          #"all-outputs" = true;
          #"active-only" = true;
          "ignore-workspaces" = [
            "scratch"
            "-"
          ];
          #"show-special" = false;
        };
        "load" = {
          "format" = "  {}";
        };
        network = {
          "format" = "{icon}";
          "format-alt" = "{ipaddr}/{cidr} {icon}";
          "format-alt-click" = "click-right";
          "format-icons" = {
            "wifi" = [
              ""
              ""
              ""
            ];
            "ethernet" = [ "" ];
            "disconnected" = [ "" ];
          };
          "on-click" = "${usr.terminal} -e nmtui";
          "tooltip" = false;
        };
        idle_inhibitor = {
          format = "{icon} ";
          format-icons = {
            activated = "";
            deactivated = "";
          };
        };
        tray = {
          "icon-size" = 16;
          "spacing" = 13;
        };
        mako = {
          "format" = "{body}";
          "max-history" = 5;
        };
        clock = {
          "interval" = 1;
          "format" = "{:%a %Y-%m-%d %H:%M:%S}";
          "timezone" = osConfig.time.timeZone;
          "tooltip-format" = ''
            <big>{:%Y %B}</big>
            <tt><small>{calendar}</small></tt>'';
        };
      };
    };
  };
}
