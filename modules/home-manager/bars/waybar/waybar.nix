{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usri3Enabled = (usr ? windowManagers.i3.enable) && usr.windowManagers.i3.enable;
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;

  i3GlobalEnabled = (cfg ? windowManagers.i3) && cfg.windowManagers.i3;
  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;

  i3Enabled = i3GlobalEnabled && usri3Enabled;
  hyprEnabled = hyprGlobalEnabled && usrHyprEnabled;

  waybarEnabled = usr.bar == "waybar";

  enabled = waybarEnabled && (i3Enabled || hyprEnabled);

  basePkg = pkgs.waybar;

  waylandPkg = basePkg.overrideAttrs (oldAttrs: {
    postPatch = ''
      # use hyprctl to switch workspaces
      sed -i 's/zext_workspace_handle_v1_activate(workspace_handle_);/const std::string command = "hyprctl dispatch focusworkspaceoncurrentmonitor " + std::to_string(id());\n\tsystem(command.c_str());/g' src/modules/wlr/workspace_manager.cpp
      sed -i 's/gIPC->getSocket1Reply("dispatch workspace " + std::to_string(id()));/gIPC->getSocket1Reply("dispatch focusworkspaceoncurrentmonitor " + std::to_string(id()));/g' src/modules/hyprland/workspaces.cpp
    '';
  });

  xPkg = basePkg.override { hyprlandSupport = false; };

  package = if hyprEnabled then waylandPkg else xPkg;
in
{
  imports = [ ./2 ];

  config = lib.mkIf enabled {
    programs.waybar = {
      enable = true;
      inherit package;
    };

    xsession.windowManager.i3 = lib.mkIf i3Enabled {
      config = {
        startup = [
          {
            command = "systemctl --user enable --now waybar.service";
            always = true;
            notification = false;
          }
        ];
      };
    };
  };
}
