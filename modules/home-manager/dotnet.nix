{
  osConfig,
  lib,
  pkgs,
  ...
}:
{
  config = lib.mkIf osConfig.sharedConfig.software.dotnet.enable {
    home.packages = with pkgs; [
      powershell
      sqlcmd
      nuget-to-json

      # dotnet tools
      dotnet-symbol
      nugethierarchy
      nugetupdater
      dotnet-outdated
    ];
  };
}
