{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;

  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;
  waybarEnabled = usr.bar == "waybar";
in
{
  config = lib.mkIf (hyprGlobalEnabled && usrHyprEnabled) {
    home.packages = with pkgs; [
      #hyprland-protocols
      hyprshot
      hyprutils
      hyprnotify
      hyprpicker
      hyprshade
      libnotify
      wl-clipboard
      slurp
      grim
      wofi-pass
      pass-wayland
      hyprland-qtutils
    ];

    home.pointerCursor.hyprcursor.enable = true;

    services = {
      network-manager-applet.enable = true;
      hyprpaper.enable = true;
      cliphist.enable = true;
      mako.enable = waybarEnabled; # notification daemon
    };

    programs = {
      foot = {
        enable = true;
        settings = {
          main = {
            font = "JetBrainsMono Nerd Font Propo:size=16";
            #dpi-aware = "yes";
            bold-text-in-bright = "yes";
          };

          mouse = {
            hide-when-typing = "yes";
          };
        };
      };
      wofi = {
        enable = true;
        settings = {
          insensitive = true;
          columns = 3;
          image_size = 64;
          line_wrap = "word";
          dynamic_lines = true;
        };
      };
      hyprlock = {
        enable = true;
      };
    };
  };
}
