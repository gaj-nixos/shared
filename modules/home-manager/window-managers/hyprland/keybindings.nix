{
  lib,
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;

  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;

  mod = "SUPER";
  gap = 7;
in
{
  config = lib.mkIf (hyprGlobalEnabled && usrHyprEnabled) {
    wayland.windowManager.hyprland = {
      settings = {
        misc = {
          key_press_enables_dpms = true;
        };

        binds = {
          allow_workspace_cycles = true;
        };

        bind = [
          "${mod},                Return, exec, uwsm app -- ${usr.terminal}"
          "${mod},                F1, exec, uwsm app -- brave --ozone-platform-hint=auto"
          "${mod}_SHIFT,          F1, exec, uwsm app -- brave --ozone-platform-hint=auto --incognito"
          "${mod},                F2, exec, uwsm app -- code --ozone-platform=wayland"
          "${mod},                F3, exec, uwsm app -- azuredatastudio"
          "${mod},                F7, exec, uwsm app -- teams-for-linux --ozone-platform-hint=auto"
          "${mod},                F8, exec, uwsm app -- thunar"
          "${mod},                F10, exec, uwsm app -- pavucontrol"

          "${mod},                W, killactive"
          "${mod}_SHIFT,          ESCAPE, exec, loginctl terminate-user \"\""
          "${mod}_SHIFT,          V, exec, uwsm app -- pavucontrol"
          "${mod}_CONTROL_ALT,    BACKSPACE, exit"
          "${mod}_SHIFT,          R, exec, hyprctl reload"
          "${mod},                F, fullscreen, 0"
          "${mod}_CONTROL_SHIFT,  F, fullscreen, 1"
          "${mod}_ALT,            F, fullscreenstate, 2"
          "${mod},                T, swapactiveworkspaces, 0 1"

          "${mod}_SHIFT,          F, togglefloating"
          "${mod}_CONTROL,        F, pseudo"
          "${mod},                B, exec, systemctl --user restart --now waybar.service"

          "${mod},                L, exec, hyprctl keyword general:layout master"
          "${mod}_SHIFT,          L, exec, hyprctl keyword general:layout dwindle"
          "${mod},                J, togglesplit" # dwindle layout
          "${mod},                J, layoutmsg, addmaster" # master layout
          "${mod}_SHIFT,          J, layoutmsg, removemaster" # master layout
          "${mod},                K, layoutmsg, orientationnext" # master layout
          "${mod}_SHIFT,          K, layoutmsg, orientationprev" # master layout

          "${mod},                left, movefocus, l"
          "${mod},                right, movefocus, r"
          "${mod},                up, movefocus, u"
          "${mod},                down, movefocus, d"

          "${mod},                PAGE_UP, workspace, r-1"
          "${mod},                PAGE_DOWN, workspace, r+1"

          "${mod}_SHIFT,          left, swapwindow, l"
          "${mod}_SHIFT,          right, swapwindow, r"
          "${mod}_SHIFT,          up, swapwindow, u"
          "${mod}_SHIFT,          down, swapwindow, d"

          "${mod},                1, workspace, 1"
          "${mod},                2, workspace, 2"
          "${mod},                3, workspace, 3"
          "${mod},                4, workspace, 4"
          "${mod},                5, workspace, 5"
          "${mod},                6, workspace, 6"
          "${mod},                7, workspace, 7"
          "${mod},                8, workspace, 8"
          "${mod},                9, workspace, 9"
          "${mod},                0, workspace, 10"

          "${mod}_SHIFT,          1, movetoworkspace, 1"
          "${mod}_SHIFT,          2, movetoworkspace, 2"
          "${mod}_SHIFT,          3, movetoworkspace, 3"
          "${mod}_SHIFT,          4, movetoworkspace, 4"
          "${mod}_SHIFT,          5, movetoworkspace, 5"
          "${mod}_SHIFT,          6, movetoworkspace, 6"
          "${mod}_SHIFT,          7, movetoworkspace, 7"
          "${mod}_SHIFT,          8, movetoworkspace, 8"
          "${mod}_SHIFT,          9, movetoworkspace, 9"
          "${mod}_SHIFT,          0, movetoworkspace, 10"

          "${mod}_ALT,            1, exec, hyprctl --batch \"keyword general:gaps_out ${toString (2 * gap)};keyword general:gaps_in ${toString gap}\""
          "${mod}_ALT,            2, exec, hyprctl --batch \"keyword general:gaps_out 150;keyword general:gaps_in 75\""
          "${mod}_ALT,            3, exec, hyprctl --batch \"keyword general:gaps_out 100;keyword general:gaps_in 50\""
          "${mod}_ALT,            4, exec, hyprctl --batch \"keyword general:gaps_out 50;keyword general:gaps_in 25\""
          "${mod}_ALT,            5, exec, hyprctl --batch \"keyword general:gaps_out 40;keyword general:gaps_in 20\""
          "${mod}_ALT,            6, exec, hyprctl --batch \"keyword general:gaps_out 30;keyword general:gaps_in 15\""
          "${mod}_ALT,            7, exec, hyprctl --batch \"keyword general:gaps_out 20;keyword general:gaps_in 10\""
          "${mod}_ALT,            8, exec, hyprctl --batch \"keyword general:gaps_out 10;keyword general:gaps_in 5\""
          "${mod}_ALT,            9, exec, hyprctl --batch \"keyword general:gaps_out 2;keyword general:gaps_in 1\""
          "${mod}_ALT,            0, exec, hyprctl --batch \"keyword general:gaps_out 0;keyword general:gaps_in 0\""

          "${mod},                mouse_down, workspace, e+1"
          "${mod},                mouse_up, workspace, e-1"

          #"${mod},               DELETE, submap, clean"

          "${mod},                V, exec, pkill wofi || cliphist list | uwsm app -- wofi -dmenu | cliphist decode | wl-copy"

          "${mod},                S, exec, slurp | grim -g - - | wl-copy"

          # Scan a document and send to Paperless by running scan-to-paperless
          "${mod}_SHIFT,          P, exec, uwsm app -- ${usr.terminal} -e scan-to-paperless"

          # Take a screenshot with slurp and grim and send to Paperless
          "${mod}_SHIFT,          O, exec, slurp | grim -g - - > /mnt/aeroserver/paperless/$(date +%Y-%m-%d-%H-%M-%S).png"

          "${mod},                SLASH, togglespecialworkspace"
          "${mod}_SHIFT,          SLASH, movetoworkspace, special"

          # turn screen off
          #bind = $mod SHIFT, S, exec, sleep 1 && hyprctl dispatch dpms toggle
          #bind = $mod, SPACE, exec, sleep 1 && hyprctl dispatch dpms on
          "${mod}_SHIFT,          S, exec, sleep 1 && hyprctl dispatch dpms toggle"
          "${mod},                SPACE, exec, sleep 1 && hyprctl dispatch dpms on"

          ",                      XF86AudioMute,     exec, uwsm app -- wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
          ",                      XF86AudioMicMute,  exec, uwsm app -- wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle"
          ",                      XF86AudioPlay,     exec, uwsm app -- playerctl play-pause"
          ",                      XF86AudioPrev,     exec, uwsm app -- playerctl previous"
          ",                      XF86AudioNext,     exec, uwsm app -- playerctl next"
        ];

        # bind on key release
        bindr = [
          "${mod},                D, exec, pkill wofi || uwsm app -- wofi --show drun -I"
          "${mod},                P, exec, wofi-pass"
          "${mod}_SHIFT,          D, exec, pkill wofi || uwsm app -- wofi --show run"
        ];

        # bind with repeat
        binde = [
          "${mod}_CONTROL,        left, resizeactive, -15 0"
          "${mod}_CONTROL,        right, resizeactive, 15 0"
          "${mod}_CONTROL,        up, resizeactive, 0 15"
          "${mod}_CONTROL,        down, resizeactive, 0 -15"

          ",                      XF86AudioLowerVolume,  exec, uwsm app -- wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
          ",                      XF86AudioRaiseVolume,  exec, uwsm app -- wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
          ",                      XF86MonBrightnessUp,   exec, uwsm app -- brightnessctl s 10%+"
          ",                      XF86MonBrightnessDown, exec, uwsm app -- brightnessctl s 10%-"
        ];

        bindm = [
          "${mod},                mouse:272, movewindow"
          "${mod},                mouse:273, resizewindow"
          #", mouse:274, exec, " # disable middle click paste
        ];
      };

      extraConfig = ''
        # Disabling keybindings with one master keybinding
        bind = ${mod},DELETE,submap,NoKeyBindings
        submap = NoKeyBindings
        bind = ${mod},HOME,submap,reset
        submap = reset
      '';
    };
  };
}
