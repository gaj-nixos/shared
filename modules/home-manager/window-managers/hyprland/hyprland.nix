{
  lib,
  pkgs,
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  usrHyprEnabled = (usr ? windowManagers.hyprland.enable) && usr.windowManagers.hyprland.enable;
  hyprGlobalEnabled = (cfg ? windowManagers.hyprland) && cfg.windowManagers.hyprland;
  waybarEnabled = usr.bar == "waybar";

  enableAnimations = !cfg.windowManagers.disableAnimations;
  enableShadows = !cfg.windowManagers.disableShadows;
  enableBlur = !cfg.windowManagers.disableBlur;

  gap = 7;
  cursorSize = "32";
in
{
  config = lib.mkIf (hyprGlobalEnabled && usrHyprEnabled) {
    wayland.windowManager.hyprland = {
      enable = true;
      systemd = {
        enable = false; # If you use the Home Manager module, make sure to disable the systemd integration, as it conflicts with uwsm
        variables = [ "--all" ];
      };
      xwayland.enable = true;

      #plugins = [
      #  inputs.hyprland-plugins.packages.${pkgs.system}.hyprtrails
      #  inputs.hycov.packages.${pkgs.system}.hycov
      #];

      settings = {
        env = [
          "WLR_NO_HARDWARE_CURSORS,1"
          "HYPRCURSOR_SIZE,${cursorSize}"
          "XCURSOR_SIZE,${cursorSize}"
        ] ++ (if cfg.virtualisation.isVmwareGuest then [ "WLR_RENDERER_ALLOW_SOFTWARE,1" ] else [ ]);
        # "QT_QPA_PLATFORMTHEME,qt5ct"

        envd = [
          "XCURSOR_SIZE,${cursorSize}"
        ];

        monitor = [
          # name,resolution,position,scale
          "HDMI-A-1,preferred,0x0,1"
          "DP-1,preferred,auto-right,1"
          "DP-2,preferred,auto-right,1"

          #"HDMI-A-1,addreserved, 0, 30, 2360, 2760"
          #"DP-1,addreserved, 0, 30, 2360, 2760"
          #"DP-2,addreserved, 0, 30, 2360, 2760"

          "Virtual-1,1920x1080,auto-left,1"
          "Virtual-2,3840x2160,auto-right,1"
          ",preferred,auto,1"
          #"eDP-1,preferred,auto-left,1"
          #"HDMI-A-1,highrr,auto-right,1"
        ];

        exec-once = [
          # "amixer -c 3 sset PCM 100%"
          # "wpctl set-volume @DEFAULT_AUDIO_SINK@ 20%"
          #"${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1"
          "systemctl --user enable --now hyprpolkitagent.service"
          "wl-paste --type text  --watch cliphist store" # Stores only text data
          "wl-paste --type image --watch cliphist store" # Stores only image data
          "dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP" # When some apps take a really long time to open
          "${pkgs.opensnitch-ui}/bin/opensnitch-ui"
        ] ++ (lib.optional waybarEnabled "systemctl --user enable --now waybar.service");

        input = {
          kb_layout = cfg.localisation.kbLayout;
          repeat_rate = 50;
          repeat_delay = 300;

          accel_profile = "flat";
          follow_mouse = 1;
          sensitivity = 0.5;
          #mouse_refocus = false;

          touchpad = {
            natural_scroll = true;
            disable_while_typing = false;
          };
        };

        general = {
          border_size = 1;
          no_border_on_floating = false;
          gaps_in = gap;
          gaps_out = 2 * gap;
          layout = "dwindle";
          resize_on_border = true;
        };

        misc = {
          new_window_takes_over_fullscreen = 2;
          disable_hyprland_logo = true;
          animate_manual_resizes = true;
          animate_mouse_windowdragging = false;
          disable_autoreload = false;
          initial_workspace_tracking = 0;
          focus_on_activate = true;
          vfr = true; # lower the amount of sent frames when nothing is happening on-screen
        };

        decoration = {
          rounding = 12;

          shadow = {
            enabled = enableShadows;
            render_power = 2;
            range = 40;
          };

          blur = {
            enabled = enableBlur;
            size = 5;
            passes = 4;
            brightness = 1;
            noise = 0.01;
            contrast = 1;
            xray = true;
            special = false;
            new_optimizations = "on";
          };
        };

        animations = {
          enabled = enableAnimations;
          bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
          animation = [
            "windows, 1, 7, myBezier"
            "windowsOut, 1, 7, default, popin 80%"
            "border, 1, 10, default"
            "borderangle, 1, 8, default"
            "fade, 1, 7, default"
            "workspaces, 1, 10, default, slide"
            "specialWorkspace, 1, 7, default, slidevert"
          ];
        };

        dwindle = {
          pseudotile = true;
          preserve_split = true;
        };

        master = {
          allow_small_split = true;
          orientation = "center";
          mfact = 0.66;
        };

        layerrule = [
          #"blur,waybar"
          "blur,wofi"
          "blur,notifications"
        ];

        windowrule = [
          "float,title:^(Open File)(.*)$"
          "float,title:^(Select a File)(.*)$"
          "float,title:^(Choose wallpaper)(.*)$"
          "float,title:^(Open Folder)(.*)$"
          "float,title:^(Save As)(.*)$"
          "float,title:^(Library)(.*)$"
          "float, ^(guifetch)$"
          "float, ^(gvncviewer)$"
          "pin, ^(showmethekey-gtk)$"
          "center,pavucontrol"
          "float,pavucontrol"
          "float,title:^(Opensnitch_ui)$"
        ];

        windowrulev2 = [
          "tile,class:(wpsoffice)"
          "opaque,class:^(kitty)$"
          "noblur,class:^(kitty)$"
          "opaque,class:^(ghostty)$"
          "noblur,class:^(ghostty)$"
          "nodim,title:^(Picture-in-Picture)$"
          "nodim,class:^(mpv)$"
          "tile,class:^(.qemu-system-x86_64-wrapped)$"

          "float, class:^(code|Code)$, windowtype:dialog"
          # Match OpenSnitch by class or title
          "float, class:^(opensnitch_ui)$"
          "float, title:^(Opensnitch_ui)$"

          "float, title:^(Save As)(.*)$"
          "float, title:^(Open Folder)(.*)$"
          "float, title:^(Open File)(.*)$"
          "float, title:^(Select a File)(.*)$"
          "float, title:^(Choose wallpaper)(.*)$"
        ];

        workspace = [
          "special: scratchpad, on-created-empty: ${usr.terminal}"
        ];
      };
    };
  };
}
