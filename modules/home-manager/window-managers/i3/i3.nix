{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}".windowManagers;
  usri3Enabled = (usr ? i3.enable) && usr.i3.enable;

  i3GlobalEnabled = (cfg ? windowManagers.i3) && cfg.windowManagers.i3;
in
{
  config = lib.mkIf (i3GlobalEnabled && usri3Enabled) {
    # bugfix for network-manager-applet: `Unit tray.target not found.`
    # see https://github.com/nix-community/home-manager/issues/2064
    systemd.user.targets.tray = {
      Unit = {
        Description = "Home Manager System Tray";
        Requires = [ "graphical-session-pre.target" ];
      };
    };

    programs.autorandr.enable = true;

    services = {
      network-manager-applet.enable = true;

      # picom --backend glx -f --corner-radius 40 --blur-method dual_kawase --blur-strength 10 --shadow --shadow-color "#000000" --crop-shadow-to-monitor
      picom = {
        enable = true;
        backend = "glx";
        fade = true;
        shadow = true;
        shadowOffsets = [
          3
          3
        ];
        settings = {
          #blur = {
          #  method = "dual_kawase";
          #  size = 10;
          #  deviation = 5.0;
          #  strength = 10;
          #};
          corner-radius = 20;
          shadow-radius = 15;
          shadow-opacity = 0.75;
          # shadow-color = "#${colors.base00}";
          shadow-color = "#000000";
        };
        extraArgs = [
          "--crop-shadow-to-monitor"
        ];
      };
    };

    xsession.windowManager.i3 = {
      enable = true;
      config = {
        terminal = usr.terminal;

        gaps.inner = 40;

        window = {
          hideEdgeBorders = "none";
          titlebar = false;
        };

        floating = {
          border = 4;
        };

        startup = [
          {
            command = "${pkgs.networkmanagerapplet}/bin/nm-applet";
            always = true;
          }
          {
            command = "${pkgs.opensnitch-ui}/bin/opensnitch-ui";
            always = true;
          }
        ];

        bars = [
          #{
          #  position = "top";
          #  statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ${./i3status-rust.toml}";
          #}
        ];

        window.commands = [
          {
            criteria = {
              window_type = "dialog";
            };
            command = "floating enable";
          }
          {
            criteria = {
              title = "~^(Open|Save)"; # use xprop to get the class name
            };
            command = "floating enable";
          }
          {
            criteria = {
              class = "Opensnitch_ui"; # use xprop to get the class name
            };
            command = "floating enable";
          }
        ];
      };

      extraConfig = ''
        set $ws1 "1"
        set $ws2 "2"
        set $ws3 "3"
        set $ws4 "4"
        set $ws5 "5"
        set $ws6 "6"
        set $ws7 "7"
        set $ws8 "8"
        set $ws9 "9"
        set $ws10 "10"

        for_window [class="^.*"] border pixel 2
      '';
    };
  };
}
