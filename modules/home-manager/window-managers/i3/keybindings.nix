{
  lib,
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  wm = cfg.users."${username}".windowManagers;
  usri3Enabled = (wm ? i3.enable) && wm.i3.enable;

  i3GlobalEnabled = (cfg ? windowManagers.i3) && cfg.windowManagers.i3;

  mod = "Mod4";
in
{
  config = lib.mkIf (i3GlobalEnabled && usri3Enabled) {
    xsession.windowManager.i3 = {
      config = {
        modifier = mod;
        keybindings = lib.mkOptionDefault {
          # "${mod}+p" =                  "exec ${pkgs.dmenu}/bin/dmenu_run";
          # "${mod}+x" =                  "exec sh -c '${pkgs.maim}/bin/maim -s | xclip -selection clipboard -t image/png'";
          # "${mod}+Shift+x" =            "exec sh -c '${pkgs.i3lock}/bin/i3lock -c 222222 & sleep 5 && xset dpms force of'";
          # # Focus
          # "${mod}+j" =                  "focus left";
          # "${mod}+k" =                  "focus down";
          # "${mod}+l" =                  "focus up";
          # "${mod}+semicolon" =          "focus right";
          # # Move
          # "${mod}+Shift+j" =            "move left";
          # "${mod}+Shift+k" =            "move down";
          # "${mod}+Shift+l" =            "move up";
          # "${mod}+Shift+semicolon" =    "move right";
          # # My multi monitor setup
          # "${mod}+m" =                  "move workspace to output DP-2";
          # "${mod}+Shift+m" =            "move workspace to output DP-5";
          "${mod}+Return" = "exec --no-startup-id ${usr.terminal}";
          XF86AudioRaiseVolume = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +1%";
          XF86AudioLowerVolume = "exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -1%";
          XF86AudioMute = "exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle";
          XF86AudioMicMute = "exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle";
          XF86MonBrightnessUp = "exec --no-startup-id xbacklight -inc 20";
          XF86MonBrightnessDown = "exec --no-startup-id xbacklight -dec 20";
          XF86AudioPlay = "exec --no-startup-id playerctl play-pause";
          XF86AudioPause = "exec --no-startup-id playerctl play-pause";
          XF86AudioStop = "exec --no-startup-id playerctl pause";
          XF86AudioNext = "exec --no-startup-id playerctl next";
          XF86AudioPrev = "exec --no-startup-id playerctl previous";
          floating_modifier = "${mod}";
          "${mod}+Shift+Return" = "exec --no-startup-id alacritty";
          "${mod}+slash" = "scratchpad show";
          "${mod}+Shift+slash" = "move scratchpad"; # move the currently focused window to the scratchpad
          "${mod}+w" = "kill";
          "${mod}+d" = "exec --no-startup-id \"rofi -modi drun,ssh,run -show drun\""; # calc,emoji,
          "${mod}+p" = "exec --no-startup-id rofi-pass";
          "${mod}+j" = "focus left";
          "${mod}+k" = "focus down";
          "${mod}+l" = "focus up";
          "${mod}+m" = "focus right";
          "${mod}+Shift+j" = "move left";
          "${mod}+Shift+k" = "move down";
          "${mod}+Shift+l" = "move up";
          "${mod}+Shift+m" = "move right";
          "${mod}+Left" = "focus left";
          "${mod}+Down" = "focus down";
          "${mod}+Up" = "focus up";
          "${mod}+Right" = "focus right";
          "${mod}+Shift+Left" = "move left";
          "${mod}+Shift+Down" = "move down";
          "${mod}+Shift+Up" = "move up";
          "${mod}+Shift+Right" = "move right";
          "${mod}+Shift+h" = "split h";
          "${mod}+Shift+v" = "split v";
          "${mod}+f" = "fullscreen toggle";
          "${mod}+Shift+space" = "floating toggle";
          "${mod}+Shift+f" = "floating toggle";
          "${mod}+space" = "focus mode_toggle";
          "${mod}+q" = "focus parent";
          "${mod}+v" = "exec --no-startup-id \"env CM_LAUNCHER=rofi clipmenu -i\"";
          "${mod}+s" = "exec --no-startup-id \"maim -s | xclip -selection clipboard -t image/png\"";
          "${mod}+Shift+s" =
            "exec --no-startup-id \"maim -st 9999999 | convert - \( +clone -background black -shadow 80x3+5+5 \) +swap -background none -layers merge +repage tmp.png && xclip -selection clipboard -t image/png tmp.png && rm tmp.png\"";
          "${mod}+F1" = "exec --no-startup-id \"brave\"";
          "${mod}+Shift+F1" = "exec --no-startup-id \"brave --incognito\"";
          "${mod}+F2" = "exec --no-startup-id \"code\"";
          "${mod}+F3" = "exec --no-startup-id \"azuredatastudio\"";
          "${mod}+F5" = "exec --no-startup-id \"remmina\"";
          "${mod}+F7" = "exec --no-startup-id \"teams-for-linux\"";
          "${mod}+Shift+F7" =
            "exec --no-startup-id \"xrandr --output DisplayPort-0 --mode 1440x900\"; gaps inner all set 10";
          "${mod}+Shift+Ctrl+F7" =
            "exec --no-startup-id \"xrandr --output DisplayPort-0 --mode 3840x2160\"; gaps inner all set 40";
          "${mod}+Shift+F8" = "gaps inner all set 10";
          "${mod}+Shift+Ctrl+F8" = "gaps inner all set 40";
          "${mod}+F8" = "exec --no-startup-id \"thunar\"";
          "${mod}+1" = "workspace number $ws1";
          "${mod}+2" = "workspace number $ws2";
          "${mod}+3" = "workspace number $ws3";
          "${mod}+4" = "workspace number $ws4";
          "${mod}+5" = "workspace number $ws5";
          "${mod}+6" = "workspace number $ws6";
          "${mod}+7" = "workspace number $ws7";
          "${mod}+8" = "workspace number $ws8";
          "${mod}+9" = "workspace number $ws9";
          "${mod}+0" = "workspace number $ws10";
          "${mod}+Next" = "workspace next";
          "${mod}+Prior" = "workspace prev";
          "${mod}+Ctrl+1" = "move container to workspace number $ws1";
          "${mod}+Ctrl+2" = "move container to workspace number $ws2";
          "${mod}+Ctrl+3" = "move container to workspace number $ws3";
          "${mod}+Ctrl+4" = "move container to workspace number $ws4";
          "${mod}+Ctrl+5" = "move container to workspace number $ws5";
          "${mod}+Ctrl+6" = "move container to workspace number $ws6";
          "${mod}+Ctrl+7" = "move container to workspace number $ws7";
          "${mod}+Ctrl+8" = "move container to workspace number $ws8";
          "${mod}+Ctrl+9" = "move container to workspace number $ws9";
          "${mod}+Ctrl+0" = "move container to workspace number $ws10";
          "${mod}+Shift+1" = "move container to workspace number $ws1; workspace $ws1";
          "${mod}+Shift+2" = "move container to workspace number $ws2; workspace $ws2";
          "${mod}+Shift+3" = "move container to workspace number $ws3; workspace $ws3";
          "${mod}+Shift+4" = "move container to workspace number $ws4; workspace $ws4";
          "${mod}+Shift+5" = "move container to workspace number $ws5; workspace $ws5";
          "${mod}+Shift+6" = "move container to workspace number $ws6; workspace $ws6";
          "${mod}+Shift+7" = "move container to workspace number $ws7; workspace $ws7";
          "${mod}+Shift+8" = "move container to workspace number $ws8; workspace $ws8";
          "${mod}+Shift+9" = "move container to workspace number $ws9; workspace $ws9";
          "${mod}+Shift+0" = "move container to workspace number $ws10; workspace $ws10";
          mouse_warping = "none";
          focus_wrapping = "no";
          "${mod}+Shift+c" = "reload";
          "${mod}+Shift+r" = "restart";
          "${mod}+Shift+Escape" =
            "exec --no-startup-id \"i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'\"";
          "${mod}+Shift+q" =
            "exec --no-startup-id \"i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'\"";
          "${mod}+Ctrl+Left" = "resize shrink width 1 px or 1 ppt";
          "${mod}+Ctrl+Right" = "resize grow width 1 px or 1 ppt";
          "${mod}+Ctrl+Down" = "resize shrink height 1 px or 1 ppt";
          "${mod}+Ctrl+Up" = "resize grow height 1 px or 1 ppt";
          "${mod}+g" = "gaps inner all plus 5";
          "${mod}+Shift+g" = "gaps inner all minus 5";
          "${mod}+b" = "exec --no-startup-id \"systemctl --user restart --now waybar.service\"";
        };
      };
    };
  };
}
