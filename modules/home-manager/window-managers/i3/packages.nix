{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}".windowManagers;
  usri3Enabled = (usr ? i3.enable) && usr.i3.enable;

  i3GlobalEnabled = (cfg ? windowManagers.i3) && cfg.windowManagers.i3;
in
{
  config = lib.mkIf (i3GlobalEnabled && usri3Enabled) {
    home.packages = with pkgs; [
      arandr
      dmenu # for application launcher
      i3 # for window manager
      i3lock # for screen lock
      maim # for screenshots
      xclip # for clipboard management
      variety # for wallpapers
      libmpdclient # for music player
      jsoncpp # for libmpdclient
      gnugrep
      gnused
      networkmanagerapplet
    ];

    programs = {
      kitty.enable = true;
      i3status = {
        enable = true;
        package = pkgs.i3status-rust;
      };
    };

    services.picom.enable = true;
  };
}
