{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  wm = cfg.users."${username}".windowManagers;
  usri3Enabled = (wm ? i3.enable) && wm.i3.enable;

  i3GlobalEnabled = (cfg ? windowManagers.i3) && cfg.windowManagers.i3;
in
{
  config = lib.mkIf (i3GlobalEnabled && usri3Enabled) {
    services = {
      clipmenu = {
        enable = true;
        launcher = "rofi";
      };
    };

    programs.rofi = {
      enable = true;
      extraConfig = {
        #modi = "drun,calc,emoji,ssh,run";
        modi = "drun,ssh,run";
        color-enabled = true;
        show-icons = true;
      };
      # theme = "fancy";
      pass = {
        enable = true; # for password store
        package = pkgs.rofi-pass;
        stores = [ "pass" ]; # Directory roots of your password-stores
      };
      plugins = with pkgs; [
        rofi-emoji
        rofi-calc
        rofi-pass
      ];
      terminal = usr.terminal;
    };

    xsession.windowManager.i3 = {
      config = {
        startup = [
          {
            command = "${pkgs.clipmenu}/bin/clipmenud";
            always = true;
          }
        ];
      };
    };
  };
}
