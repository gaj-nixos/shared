{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";

  chromeExtensions = {
    wakaTime = {
      id = "jnbbnacmeggbgdjgaoojpmhdlkkpblgi";
    };
    jsonFormatter = {
      id = "bcjindcccaagfpapjjmafapmmgkkhgoa";
    };
    adblockPlus = {
      id = "cfhdojbkjhnklbpkdaibdccddilifddb";
    };
    bitwarden = {
      id = "nngceckbapebfimnlniiiahkandclblb";
    };
    adblock = {
      id = "gighmmpiobklfepjocnamgkkbiglidom";
    };
    ublock = {
      id = "epcnnfbjfcgphgdmggkamkmgojdagdnn";
    };
    ublockOrigin = {
      id = "cjpalhdlnbpafiamejdnhcphjbkeiagm";
    };
    ghostery = {
      id = "mlomiejdfkolichcflejclcbmpeaniij";
    };
    privacyBadger = {
      id = "pkehgijcmpdhfbdbbnkijodmdjhbjlgp";
    };
    disconnect = {
      id = "jeoacafpbcihiomhlakheieifhpjdfeo";
    };
    httpsEverywhere = {
      id = "gcbommkclmclpchllfjekcdonpmejbdp";
    };
    vtchromizer = {
      id = "efbjojhplkelaegfbieplglfidafgoka";
    };
    drWebLinkChecker = {
      id = "aleggpabliehgbeagmfhnodcijcmbonb";
    };
    changedetectionio = {
      id = "kefcfmgmlhmankjmnbijimhofdjekbop";
    };
    wallabag = {
      id = "gbmgphmejlcoihgedabhgjdkcahacjlj";
    };
  };
in
{
  config = mkIf usr.software.brave.enable {
    home.packages = with pkgs; [
      brave
    ];

    programs.chromium = {
      enable = true;
      package = pkgs.brave;
      extensions = with chromeExtensions; [
        wakaTime
        jsonFormatter
        # adblockPlus
        bitwarden
        # adblock
        # ublock
        ublockOrigin
        # ghostery
        # privacyBadger
        # disconnect
        # httpsEverywhere
        # vtchromizer
        # drWebLinkChecker
        changedetectionio
        wallabag
      ];
      #commandLineArgs = [
      #  "--enable-features=WebRTCPipeWireCapturer,VaapiVideoDecoder"
      #  "--ignore-gpu-blocklist"
      #  "--enable-gpu-rasterization"
      #  "--disable-background-networking"
      #  "--enable-accelerated-video-decode"
      #  "--enable-zero-copy"
      #  "--no-default-browser-check"
      #  "--disable-sync"
      #  "--disable-features=MediaRouter"
      #  "--enable-features=UseOzonePlatform"
      #];
    };
  };
}
