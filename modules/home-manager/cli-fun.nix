{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf usr.software.funCliPrograms.enable {
    home.packages = with pkgs; [
      neofetch
      onefetch
      ipfetch
      cpufetch
      ramfetch
      #starfetch
      octofetch
      lolcat
      unimatrix
      cli-visualizer
      cbonsai
      pokeget-rs
    ];

    programs = {
      cava.enable = true;
    };
  };
}
