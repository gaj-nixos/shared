{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  gui = usr.software.guiPrograms;

  conf = lib.readFile ./qmmp.conf;

  # replace all instances of "nix\store\qmmp-2.1.8" in conf, with the actual path of the package

  # replace all / with \\ to get the path in the format "nix\\store\\qmmp-2.1.8"
  pkgPath = lib.replaceStrings [ "/" ] [ "\\" ] "${pkgs.qmmp}";

  # trim the first \\ in pkgPath
  pkgPath2 = lib.substring 1 (lib.stringLength pkgPath) pkgPath;

  conf2 = lib.replaceStrings [ "nix\\store\\qmmp-2.1.8" ] [ pkgPath2 ] conf;

  # create a file in the nix store with the contents of conf2
  confFile = pkgs.writeText "qmmp.conf" conf2;
in
{
  config = lib.mkIf gui.enable {

    home = {
      packages = with pkgs; [
        qmmp
      ];
      file = lib.mkIf gui.manageQmmpSettings {
        ".config/qmmp/qmmp.conf".source = confFile;
      };
    };

    services = {
      playerctld.enable = true;
    };
  };
}
