{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf (usr.software.guiPrograms.enable && usr.software.swDevPrograms.enable) {
    home = {
      packages = with pkgs; [
        azuredatastudio
        dbeaver-bin
        playwright-driver.browsers
      ];
    };
  };
}
