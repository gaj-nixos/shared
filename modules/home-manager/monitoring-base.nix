{
  pkgs,
  lib,
  config,
  osConfig,
  ...
}:
let
  inherit (lib) optional;
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = {
    home.packages =
      with pkgs;
      [
        zfxtop
        lm_sensors # for `sensors` command
        ncdu # disk usage analyzer
        iperf
        nvme-cli
        glxinfo
      ]
      ++ (optional (usr.software.guiPrograms.enable) gpu-viewer);

    programs = {
      btop.enable = true;
      htop.enable = true;
      bottom.enable = true;
    };
  };
}
