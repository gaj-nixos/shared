{
  lib,
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  homeStateVersion = cfg.homeStateVersion;
in
{
  config = {
    home.sessionVariables = {
      EDITOR = usr.editor;
      #SPAWNEDITOR=
      #TERM=
      #BROWSER=
    };

    pam.sessionVariables = {
      EDITOR = usr.editor;
    };

    dconf.settings = lib.mkIf cfg.virtualisation.isQemuGuest {
      "org/virt-manager/virt-manager/connections" = {
        autoconnect = [ "qemu:///system" ];
        uris = [ "qemu:///system" ];
      };
    };

    home.stateVersion = homeStateVersion;

    nixpkgs = {
      config = lib.mapAttrs (n: v: lib.mkDefault v) osConfig.nixpkgs.config;
      # mkOrder 900 is after mkBefore but before default order
      overlays = lib.mkOrder 900 osConfig.nixpkgs.overlays;
    };
  };
}
