{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  homeDir = if username == "root" then "/root" else "/home/${username}";
in
{
  config = lib.mkIf usr.software.guiPrograms.enable {
    xdg = {
      userDirs = {
        music = lib.mkDefault "${homeDir}/Media/Music";
        videos = lib.mkDefault "${homeDir}/Media/Videos";
        pictures = lib.mkDefault "${homeDir}/Media/Pictures";
        extraConfig = {
          #XDG_ORG_DIR = lib.mkDefault "${homeDir}/Org";
          #XDG_PODCAST_DIR = lib.mkDefault "${homeDir}/Media/Podcasts";
          #XDG_BOOK_DIR = lib.mkDefault "${homeDir}/Media/Books";
        };
      };
      mime.enable = true;
      mimeApps.enable = true;
    };

    home = {
      sessionVariables = {
        TERM = "kitty";
      };
      packages = with pkgs; [
        teams-for-linux
        google-chrome
        libreoffice
        playerctl
        pavucontrol
        w3m # to display images in Kitty terminal emulator
        vlc
        qmmp
        freerdp3
        immich-go
        element-desktop # matrix client
        xdragon
        alacritty.terminfo
        signal-desktop
      ];
    };

    programs = {
      firefox.enable = true;
      alacritty.enable = true;
      feh = {
        # image viewer
        enable = true;
        keybindings = {
          zoom_in = "plus";
          zoom_out = "minus";
        };
      };
    };

    services = {
      playerctld.enable = true;
      remmina.enable = true;
    };
  };
}
