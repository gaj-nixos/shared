{
  lib,
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  inherit (config.lib.stylix.colors.withHashtag) base00;
in
{
  config = lib.mkIf usr.software.guiPrograms.enable {
    programs = {
      kitty = {
        enable = true;
        settings = {
          enable_audio_bell = false;
          close_on_child_death = true;
          cursor_blink_interval = 0;

          enabled_layouts = "fat, tall, vertical";
          wayland_titlebar_color = "background";

          #allow_remote_control = true;
          #listen_on = "unix:/tmp/kitty";
          dynamic_background_opacity = true;

          window_padding_width = 5;
          tab_bar_margin_width = 5;

          scrollback_pager = "less --chop-long-lines --raw-control-chars +INPUT_LINE_NUMBER";
        };

        extraConfig = ''
          term xterm-256color
          tab_bar_background ${base00}
          inactive_tab_background ${base00}
        '';

        shellIntegration = {
          enableZshIntegration = usr.software.zsh.enable;
          enableBashIntegration = true;
        };
      };
    };

    xdg.configFile."kitty/diff.conf".text = ''
      map d scroll_to next-page
      map u scroll_to prev-page
      map g scroll_to start
      map G scroll_to end
    '';
  };
}
