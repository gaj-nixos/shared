{
  lib,
  config,
  osConfig,
  pkgs,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
  commonAliases = import ../../resources/users/common-aliases.nix;

  ohMyZshTheme = "agnoster";

  sgpt =
    if cfg.software.shellGpt.enable then
      ''
        # Shell-GPT integration ZSH v0.2
        _sgpt_zsh() {
        if [[ -n "$BUFFER" ]]; then
            _sgpt_prev_cmd=$BUFFER
            BUFFER+="⌛"
            zle -I && zle redisplay
            BUFFER=$(sgpt --shell <<< "$_sgpt_prev_cmd" --no-interaction)
            zle end-of-line
        fi
        }
        zle -N _sgpt_zsh
        bindkey '^ ' _sgpt_zsh
        # Shell-GPT integration ZSH v0.2
      ''
    else
      "";

  resizePrompt = ''
    # Function to redraw the prompt
    redraw_prompt() {
        #echo -ne "\r\033[K" # Clear the current line
        zle && { zle reset-prompt; zle -R } # zle clear-screen;
    }

    setopt prompt_subst

    precmd() {
      zle && zle clear-screen
    }

    # Trap the WINCH signal to call the redraw_prompt function
    TRAPWINCH() {
        redraw_prompt
    }
  '';

in
{
  config = lib.mkIf usr.software.zsh.enable {
    home.packages = with pkgs; [
      chroma
      zsh-history-substring-search
      oh-my-zsh
      zsh-completions
      #zsh-autosuggestions
      #zsh-syntax-highlighting
      zsh-nix-shell # use zsh in nix-shell shell
      #zsh-you-should-use
      #nix-zsh-completions
    ];

    programs.zsh = {
      enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      historySubstringSearch.enable = true;
      history = {
        save = 1000000;
        size = 1000000;
        ignoreDups = true;
      };
      oh-my-zsh = {
        enable = true;
        plugins = [
          # See https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/

          #"aliases"                  # `als`: list the shortcuts that are currently available based on the enabled plugins
          "colored-man-pages" # automatically colorize man pages
          "colorize" # syntax-highlight file contents of over 300 supported languages and other text formats. Depends on Chroma and Pygments
          "common-aliases" # a collection of aliases that are commonly used
          "copyfile" # copy the contents of a file to the clipboard
          "copypath" # copy the path of the current directory to the clipboard
          "dirhistory" # Alt + Left	Go to previous directory. Alt + Right	Go to next directory, Alt + Up	Move into the parent directory, Alt + Down	Move into the first child directory by alphabetical order
          #"docker"                   # Docker auto-completion and aliases
          "dotenv" # load environment variables from .env files
          "dotnet" # .NET Core auto-completion and aliases
          "extract" # extract archives with a single command
          "fancy-ctrl-z" # suspend a process with Ctrl + Z and resume it in the background with Ctrl + Z again, instead of `fg`
          "firewalld" # firewalld functions and aliases
          "gh" # GitHub CLI auto-completion
          "git-auto-fetch" # automatically fetch changes from the remote repository
          "git" # Git functions and aliases
          "history-substring-search" # type in any part of any command from history and then press chosen keys, such as the UP and DOWN arrows, to cycle through matches
          "magic-enter" # makes your enter key magical, by binding commonly used commands to it
          "nmap" # Nmap aliases
          "npm" # npm auto-completion and aliases
          "pass" # completion for the pass password manager
          "per-directory-history" # maintain a separate history for each directory. Press Ctrl + G to toggle between local and global histories
          "pip" # pip auto-completion and aliases
          "safe-paste" # paste text without executing it
          "sudo" # Easily prefix your current or previous commands with sudo by pressing esc twice
          "systemd" # systemd aliases
          #"themes" # change ZSH theme on the go. lstheme: Lists installed ZSH themes ; theme <theme_name> - Changes the ZSH theme
          #"tmux"                     # tmux aliases
          "ufw" # UFW completions and aliases
          #"you-should-use"           # suggest packages to install when a command is not found
          #"zsh-autosuggestions"
          #"zsh-syntax-highlighting"  # Keep last!
        ];
        theme = ohMyZshTheme;
      };
      completionInit = ''
        zstyle ':completion:*' completer _complete _ignored _correct _approximate
        zstyle ':completion:*' max-errors 2
        zstyle ':completion:*' menu select=2
        zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
        zstyle ':completion::complete:*' use-cache 1
        zstyle ':completion:*:descriptions' format '%U%F{cyan}%d%f%u'
        autoload -U zmv       # zmv -  a command for renaming files by means of shell patterns.
        autoload -U zargs     # zargs, as an alternative to find -exec and xargs.
        autoload -U compinit
        eval "$(bw completion --shell zsh); compdef _bw bw;"
        eval "$(nh completions --shell zsh)"
        eval "$(tv init zsh)"
        compinit
        fpath+=''${ZSH_CUSTOM:-''${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src
      '';
      shellAliases = commonAliases // usr.software.zsh.shellAliases;
      defaultKeymap = "emacs";
      initExtra = ''
        ${sgpt}
        ${resizePrompt}
        ${usr.software.zsh.welcomeMessage}
      '';
    };
  };
}
