{
  lib,
  config,
  ...
}:
let
  username = config.home.username;
  homeDir = if username == "root" then "/root" else "/home/${username}";
in
{
  config = {
    xdg = {
      enable = true;
      userDirs = {
        enable = true;
        createDirectories = true;
        #templates = lib.mkDefault "${homeDir}/Templates";
        download = lib.mkDefault "${homeDir}/Downloads";
        documents = lib.mkDefault "${homeDir}/Documents";
        desktop = null;
        publicShare = null;
        extraConfig = {
          #XDG_DOTFILES_DIR = lib.mkDefault "${homeDir}/.dotfiles";
          #XDG_ARCHIVE_DIR = lib.mkDefault "${homeDir}/Archive";
          #XDG_VM_DIR = lib.mkDefault "${homeDir}/Machines";
        };
      };
    };
  };
}
