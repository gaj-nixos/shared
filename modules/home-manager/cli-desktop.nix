{
  pkgs,
  lib,
  config,
  osConfig,
  ...
}:
let
  cfg = osConfig.sharedConfig;
  username = config.home.username;
  usr = cfg.users."${username}";
in
{
  config = lib.mkIf usr.software.guiPrograms.enable {
    home.packages = with pkgs; [
      openconnect # VPN client
      bc # arbitrary precision calculator language
    ];
  };
}
