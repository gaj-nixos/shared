{
  stylixSettings ? null,
}:
{
  config,
  pkgs,
  ...
}:
# theme gallery: https://tinted-theming.github.io/base16-gallery/
let
  cfg = if stylixSettings == null then config.sharedConfig.stylixGlobal else stylixSettings;
in
{
  stylix = {
    #image = pkgs.fetchurl {
    #  url = cfg.background.url;
    #  sha256 = cfg.background.sha256;
    #};
    image = cfg.background.image;
    enable = cfg.enable;
    autoEnable = cfg.enable;
    base16Scheme = "${pkgs.base16-schemes}/share/themes/${cfg.theme}.yaml";
    polarity = cfg.polarity;
    fonts = {
      monospace = {
        name = cfg.fonts.monospace.name;
        package = cfg.fonts.monospace.package;
      };
      #serif = {
      #  name = font;
      #  package = fontPkg;
      #};
      #sansSerif = {
      #  name = font;
      #  package = fontPkg;
      #};
      emoji = {
        name = cfg.fonts.emoji.name;
        package = cfg.fonts.emoji.package;
      };
      sizes = {
        terminal = cfg.fonts.sizes.terminal;
        applications = cfg.fonts.sizes.applications;
        popups = cfg.fonts.sizes.popups;
        desktop = cfg.fonts.sizes.desktop;
      };
    };
  };
}
