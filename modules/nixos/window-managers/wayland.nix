{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig.windowManagers;
  enabled = cfg ? wayland && cfg.wayland;
in
{
  config = lib.mkIf enabled {
    programs.uwsm = {
      enable = true;
    };

    xdg.portal = {
      enable = true;
      xdgOpenUsePortal = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-gtk
      ];
    };

    systemd.user.services.xdg-desktop-portal-gtk = {
      wantedBy = [ "xdg-desktop-portal.service" ];
      before = [ "xdg-desktop-portal.service" ];
    };

    environment.sessionVariables = {
      XDG_SESSION_TYPE = "wayland";
      NIXOS_OZONE_WL = "1"; # for vs code
    };

    security.polkit.enable = true;
    hardware.graphics.enable = true;
  };
}
