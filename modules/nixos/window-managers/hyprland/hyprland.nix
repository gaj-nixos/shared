{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig.windowManagers;
  hyprEnabled = cfg ? hyprland && cfg.hyprland;
in
{
  config = lib.mkIf hyprEnabled {
    programs.hyprland = {
      enable = true;
      xwayland.enable = true;
      withUWSM = true;
    };
    programs.uwsm = {
      enable = true;
      waylandCompositors.hyprland = {
        prettyName = "Hyprland";
        comment = "Hyprland compositor managed by UWSM";
        binPath = "/run/current-system/sw/bin/Hyprland";
      };
    };
    nix.settings = {
      substituters = [ "https://hyprland.cachix.org" ];
      trusted-public-keys = [ "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc=" ];
    };

    xdg.portal = {
      enable = true;
      xdgOpenUsePortal = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-hyprland
      ];
    };

    systemd.user.services.xdg-desktop-portal-hyprland = {
      wantedBy = [ "xdg-desktop-portal.service" ];
      before = [ "xdg-desktop-portal.service" ];
    };

    services.greetd = {
      enable = true;
      settings = {
        default_session.command = ''
          ${pkgs.greetd.tuigreet}/bin/tuigreet \
            --time \
            --asterisks \
            --user-menu \
            --remember \
            --remember-user-session \
            --cmd "${pkgs.uwsm}/bin/uwsm start -S -F /run/current-system/sw/bin/Hyprland"
        '';
      };
    };

    environment.etc."greetd/environments".text = ''
      hyprland
    '';

    environment.sessionVariables = {
      XDG_SESSION_DESKTOP = "hyprland";
    };

    security = {
      pam.services.swaylock = {
        text = ''
          auth include login
        '';
      };
    };
  };
}
