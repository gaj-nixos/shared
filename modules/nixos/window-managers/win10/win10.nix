{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig.windowManagers;
  win10Enabled = cfg ? win10 && cfg.win10;
in
{
  config = lib.mkIf win10Enabled {
    services.xserver = {
      displayManager.gdm = {
        enable = true;
        wayland = true;
      };
      #displayManager.defaultSession = "gnome-wayland";
      desktopManager.gnome.enable = true;
    };
    programs.xwayland.enable = true;
    programs.uwsm = {
      enable = true;
      waylandCompositors.gnome = {
        prettyName = "Gnome";
        comment = "Gnome managed by UWSM";
        binPath = "/run/current-system/sw/bin/gnome";
      };
    };

    environment.sessionVariables = {
      XDG_SESSION_DESKTOP = "gnome";
    };

    environment.systemPackages = with pkgs; [
      windows10-icons
      gnome.gvfs
      gnome-tweaks
      gnome-shell-extensions
      gnomeExtensions.dash-to-panel
      gnomeExtensions.arcmenu
    ];

    #qt.platformTheme = "qtct";
  };
}
