{
  config,
  lib,
  ...
}:
let
  cfg = config.sharedConfig.windowManagers;
  i3Enabled = cfg ? i3 && cfg.i3;
in
{
  config = lib.mkIf i3Enabled {
    services.xserver = {
      enable = true;
      xkb.layout = config.sharedConfig.localisation.kbLayout;
      displayManager.lightdm.enable = true;
      desktopManager.xterm.enable = false;
      windowManager.i3.enable = true;
    };

    #services.displayManager.sddm.enable = true;

    services.displayManager.defaultSession = "none+i3";

    programs.xfconf.enable = true; # Xfce configuration storage system
  };
}
