# OBSOLETE: use Disko
{
  lib,
  config,
  ...
}:
let
  defaults = [
    "compress=zstd"
    "space_cache=v2"
    "noatime"
    "ssd"
    "discard=async"
  ];
  noCow = defaults ++ [ "nodatacow" ];
  useLuks = config.sharedConfig.disks.encryptDataPartition;
  baseDataDevice = "/dev/disk/by-label/data";
  encryptedDataDevice = "/dev/mapper/data";

  nixosDevice = "/dev/disk/by-label/nixos";
  dataDevice = if useLuks then encryptedDataDevice else baseDataDevice;
in
{
  config = lib.mkIf (config.sharedConfig.disks.partitioning == "split") {
    boot = {
      initrd = {
        kernelModules = [
          "cryptd"
        ];
        luks.devices."data" = lib.mkIf useLuks {
          device = "/dev/disk/by-label/data";
          keyFile = "/root/luks-keyfile";
          preLVM = true;
          fallbackToPassword = true;
        };
      };
      #loader = {
      #  efi.efiSysMountPoint = "/efi";
      #  systemd-boot.xbootldrMountPoint = "/boot";
      #};
    };

    swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

    fileSystems = {
      # "/efi" = {
      #   device = "/dev/disk/by-label/ESP";
      #   fsType = "vfat";
      #   options = [ "fmask=0077" "dmask=0077" ];
      # };

      # "/boot" = {
      #   device = "/dev/disk/by-label/BOOT";
      #   fsType = "ext4";
      # };

      # "/efi/EFI/Linux" = {
      #   device = "/boot/EFI/Linux";
      #   options = [ "bind" ];
      # };

      # "/efi/EFI/nixos" = {
      #   device = "/boot/EFI/nixos";
      #   options = [ "bind" ];
      # };

      "/boot" = {
        device = "/dev/disk/by-label/BOOT";
        fsType = "vfat";
        options = [
          "fmask=0077"
          "dmask=0077"
        ];
      };

      "/" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [ "subvol=@" ] ++ defaults;
      };

      "/var" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [ "subvol=@var" ] ++ defaults;
      };

      "/var/log" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [
          "subvol=@var_log"
          "noexec"
        ] ++ defaults;
      };

      "/var/cache" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [ "subvol=@var_cache" ] ++ defaults;
      };

      "/var/tmp" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [ "subvol=@var_tmp" ] ++ defaults;
      };

      "/nix" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [ "subvol=@nix" ] ++ defaults;
      };

      "/tmp" = {
        device = nixosDevice;
        fsType = "btrfs";
        options = [ "subvol=@tmp" ] ++ defaults;
      };

      "/home" = {
        device = dataDevice;
        fsType = "btrfs";
        options = [
          "subvol=@home"
          "nodev"
          "autodefrag"
        ] ++ defaults;
      };

      "/srv" = {
        device = dataDevice;
        fsType = "btrfs";
        options = [
          "subvol=@srv"
          "autodefrag"
        ] ++ defaults;
      };

      "/srv/db" = {
        device = dataDevice;
        fsType = "btrfs";
        options = [
          "subvol=@db"
          "nodev"
          "autodefrag"
        ] ++ noCow;
      };

      "/.snapshots/home" = {
        device = dataDevice;
        fsType = "btrfs";
        options = [
          "subvol=@snapshots_home"
          "autodefrag"
        ] ++ defaults;
      };

      "/.snapshots/srv" = {
        device = dataDevice;
        fsType = "btrfs";
        options = [
          "subvol=@snapshots_srv"
          "autodefrag"
        ] ++ defaults;
      };

      "/.snapshots/db" = {
        device = dataDevice;
        fsType = "btrfs";
        options = [
          "subvol=@snapshots_db"
          "autodefrag"
        ] ++ defaults;
      };
    };
  };
}
