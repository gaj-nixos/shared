{
  lib,
  pkgs,
  config,
  ...
}:
{
  environment.systemPackages =
    (with pkgs.gaelj; [

    ])

    ++ (with pkgs.master; [

    ])

    ++ (with pkgs.stable; [

    ])
    ++ (lib.optional config.sharedConfig.software.dotnet.enable
      pkgs.stable.swagger-cli
    )

    # unstable channel
    ++ (with pkgs; [
    ]);
}
