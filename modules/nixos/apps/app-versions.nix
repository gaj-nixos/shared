{ ... }:
{
  sharedConfig.appVersions = {
    #cockpit-machines = { # https://github.com/cockpit-project/cockpit-machines/releases
    #  version = "326";
    #  sha = "sha256-0jP4tcg/cdOwviVlr+sPDXgfKfBQs3nTGlwqkhXbhAc=";
    #};
    #cockpit-podman = {
    #  version = "99"; # https://github.com/cockpit-project/cockpit-podman/releases
    #  sha = "sha256-rfkGurW2mrqIqtcXe657BWPon8o5wzEqFLr/egGXGpA=";
    #};

    airsonic-refix.version = "sha-3fe9216"; # https://hub.docker.com/r/tamland/airsonic-refix/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/airsonic-refix/sources
    # nix run github:aksiksi/compose2nix -- --runtime podman --project airsonic

    fail2rest.version = "latest"; # "088693696c9b" https://hub.docker.com/r/monogramm/docker-fail2rest/tags
    fail2web.version = "latest"; # "863b355062b5" https://hub.docker.com/r/monogramm/docker-fail2web/tags
    # Update procedure:
    # - to re-generate the base of this file
    # - `cd modules/nixos/apps/containers/fail2web/sources`

    # - `curl -o docker-compose.yml https://raw.githubusercontent.com/Monogramm/docker-fail2web/refs/heads/master/docker-compose.yml`
    # - run `nix run github:aksiksi/compose2nix -- --runtime podman --project fail2web`
    # - check for any modifications in the generated file and apply them here

    fireflyiii.version = "version-6.2.9"; # https://hub.docker.com/r/fireflyiii/core/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/fireflyiii/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/firefly-iii/docker/main/docker-compose-importer.yml
    # curl -o .env https://raw.githubusercontent.com/firefly-iii/firefly-iii/main/.env.example
    # curl -o .importer.env https://raw.githubusercontent.com/firefly-iii/data-importer/main/.env.example
    # curl -o .db.env https://raw.githubusercontent.com/firefly-iii/docker/main/database.env
    # nix run github:aksiksi/compose2nix -- --runtime podman --project fireflyiii

    # Doc: https://docs.fireflyiii.org/how-to/fireflyiii/installation/docker/

    gonic.version = "v0.16.4"; # https://github.com/sentriz/gonic/tags
    # Update procedure:
    # - follow the download instructions at https://github.com/sentriz/gonic/wiki/installation#with-docker

    immich.version = "v1.129.0"; # https://github.com/immich-app/immich/releases
    # Update procedure:
    # - to re-generate the base of this file
    # - check for breaking changes at https://github.com/immich-app/immich/discussions?discussions_q=label%3Achangelog%3Abreaking-change+sort%3Adate_created

    # - follow the download instructions at https://immich.app/docs/install/docker-compose:
    # - `cd modules/nixos/apps/containers/immich/sources`
    # - `curl -o docker-compose.yml https://raw.githubusercontent.com/immich-app/immich/refs/tags/v1.122.1/docker/docker-compose.yml`
    # - `curl -o .env https://raw.githubusercontent.com/immich-app/immich/refs/tags/v1.122.1/docker/example.env`
    # - `curl -o hwaccel.transcoding.yml https://raw.githubusercontent.com/immich-app/immich/refs/tags/v1.122.1/docker/hwaccel.transcoding.yml`
    # - `curl -o hwaccel.ml.yml https://raw.githubusercontent.com/immich-app/immich/refs/tags/v1.122.1/docker/hwaccel.ml.yml`
    # - `nix run github:aksiksi/compose2nix -- --runtime podman --project immich`
    # - check for any modifications in the generated file and apply them here

    navidrome.version = "0.54.5"; # https://hub.docker.com/r/deluan/navidrome/tags
    # Update procedure:
    # - to re-generate the base of this file
    # - `cd modules/nixos/apps/containers/navidrome/sources`

    # - follow the download instructions at
    # - `curl -o docker-compose.yml https://raw.githubusercontent.com/ ... /docker-compose.yml`
    # - run `nix run github:aksiksi/compose2nix -- --runtime podman --project navidrome`
    # - check for any modifications in the generated file and apply them here

    openWebUi.version = "0.5.20"; # https://github.com/open-webui/open-webui/tags   https://github.com/open-webui/open-webui/pkgs/container/open-webui/versions?filters%5Bversion_type%5D=tagged
    openWebUiPipelines.version = "git-ff41479"; # https://github.com/open-webui/pipelines/pkgs/container/pipelines/versions?filters%5Bversion_type%5D=tagged
    tika.version = "3.1.0.0-full"; # https://hub.docker.com/r/apache/tika/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/open-webui/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/open-webui/open-webui/refs/heads/main/docker-compose.yaml
    # nix run github:aksiksi/compose2nix -- --runtime podman --project open-webui

    paperless-ngx.version = "2.14.7"; # https://github.com/paperless-ngx/paperless-ngx/tags

    dashy.version = "3.1.0"; # https://hub.docker.com/r/lissy93/dashy/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/dashy/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/Lissy93/dashy/refs/heads/master/docker-compose.yml
    # nix run github:aksiksi/compose2nix -- --runtime podman --project dashy

    glances.version = "4.3.0.8-full"; # https://hub.docker.com/r/nicolargo/glances/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/glances/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/nicolargo/glances/refs/heads/develop/docker-compose/docker-compose.yml
    # nix run github:aksiksi/compose2nix -- --runtime podman --project glances

    homeAssistant.version = "2025.3.0"; # https://hub.docker.com/r/homeassistant/home-assistant/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/home-assistant/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/home-assistant/docker-home-assistant/refs/heads/homeassistant/Dockerfile
    # nix run github:aksiksi/compose2nix -- --runtime podman --project home-assistant

    zWaveJs.version = "9.31.0"; # https://hub.docker.com/r/zwavejs/zwave-js-ui/tags
    mosquitto.version = "2.0.21"; # https://hub.docker.com/_/eclipse-mosquitto/tags
    espHome.version = "2025.2.2"; # https://hub.docker.com/r/esphome/esphome/tags

    vaultwarden.version = "1.33.2"; # https://hub.docker.com/r/vaultwarden/server/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/vaultwarden/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/vineethmn/vaultwarden-docker-compose/refs/heads/main/docker-compose.yml
    # nix run github:aksiksi/compose2nix -- --runtime podman --project vaultwarden

    wallabag.version = "2.6.10"; # https://hub.docker.com/r/wallabag/wallabag/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/wallabag/sources
    # see https://github.com/wallabag/docker?tab=readme-ov-file#docker-compose
    # nix run github:aksiksi/compose2nix -- --runtime podman --project wallabag

    dendrite.version = "0.14.1"; # https://github.com/element-hq/dendrite/tags
    # Update procedure:
    # cd modules/nixos/apps/containers/dendrite/sources
    # curl -o docker-compose.yml https://raw.githubusercontent.com/element-hq/dendrite/refs/heads/main/build/docker/docker-compose.yml
    # curl -o nginx.conf https://raw.githubusercontent.com/element-hq/dendrite/refs/heads/main/docs/nginx/dendrite-sample.conf
    # nix run github:aksiksi/compose2nix -- --runtime podman --project dendrite

    sync-v3.version = "0.99.19"; # https://github.com/matrix-org/sliding-sync/tags

    dbBackup.version = "4.1.16"; # https://hub.docker.com/r/tiredofit/db-backup/tags

    synapse-admin.version = "0.10.3"; # https://github.com/Awesome-Technologies/synapse-admin/tags

  };
}
