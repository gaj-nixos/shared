{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    mkContainerUser
    ;
  cfg = config.sharedConfig.webApps.gonic;

  VERSION = config.sharedConfig.appVersions.gonic.version;
  musicLocation = cfg.musicLocation;
  podcastsLocation = cfg.podcastsLocation;
  playlistsLocation = cfg.playlistsLocation;
  dataLocation = cfg.dbDataLocation;
  cacheLocation = cfg.cacheLocation;

  name = "gonic";
  #logDir = "/var/log/${name}";
  #logFile = "${name}.log";

  backupPath = "/srv/backups/${name}/db";

  proxyPass = cfg.proxyPass;

  extraConfig = ''
    client_max_body_size 0;
    proxy_buffering off;                # Prevent buffering on large files
    proxy_request_buffering off;        # Pass request without waiting for full body
    proxy_connect_timeout 120s;
    proxy_send_timeout 600s;
    proxy_read_timeout 600s;
    send_timeout 600s;
  '';

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    users = mkContainerUser name;

    virtualisation.oci-containers.containers."${name}" =
      {
        image = "sentriz/gonic:${VERSION}";
        ports = [ "${toString cfg.port}:80" ];
      }
      // sharedContainerSettings {
        volumes = [
          "${dataLocation}:/data:rw"
          "${musicLocation}:/music:ro"
          "${podcastsLocation}:/podcasts:ro"
          "${playlistsLocation}:/playlists:ro"
          "${cacheLocation}:/cache:rw"
        ];
        environment = {
          GONIC_MUSIC_PATH = "/music";
        };
        extraOptions = [
          "--network-alias=${name}"
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createGonicDataDir.text = ''
        mkdir -p "${cfg.cacheLocation}"
        mkdir -p "${cfg.musicLocation}"
        mkdir -p "${cfg.podcastsLocation}"
        mkdir -p "${cfg.playlistsLocation}"
        mkdir -p "${cfg.dbDataLocation}"
        mkdir -p "${backupPath}"

        chown -R gonic:gonic "${cfg.cacheLocation}"
        chown -R gonic:gonic "${cfg.musicLocation}"
        chown -R gonic:gonic "${cfg.dbDataLocation}"
        chown -R gonic:gonic "${cfg.podcastsLocation}"
        chown -R gonic:gonic "${cfg.playlistsLocation}"
        chmod -R 700 "${cfg.dbDataLocation}"
        chown -R gonic:gonic "${backupPath}"
      '';
    };

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        extraConfig
        ;
    };
  };
}
