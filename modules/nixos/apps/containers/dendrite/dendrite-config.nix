{
  config,
  lib,
  sharedConfig,
  ...
}:
let
  wa = config.sharedConfig.webApps;
  publicHost = config.sharedConfig.network.publicHost;
  cfg = wa.dendrite;

  dbPasswordSecretName = "dendriteSecretDb";
  yamlSecretName = "dendriteConfigYaml";
  sharedSecretName = "dendriteSharedSecret";
  reCaptchaPublicKeySecretName = "dendriteReCaptchaPublicKey";
  reCaptchaPrivateKeySecretName = "dendriteReCaptchaPrivateKey";

  dummyDbPassword = "itsasecret";
  dummySharedSecret = "itsasharedsecret";
  dummyReCaptchaPublicKey = "itsarecaptchapublickey";
  dummyReCaptchaPrivateKey = "itsarecaptchaprivatekey";
  dendrite_yaml = ''
    version: 2
    global:
      server_name: ${cfg.externalHost}
      key_id: ed25519:auto
      private_key: matrix_key.pem
      old_private_keys: []
      key_validity_period: 168h0m0s
      database:
        connection_string: postgres://dendrite:${dummyDbPassword}@postgres/dendrite?sslmode=disable
        max_open_conns: 90
        max_idle_conns: 2
        conn_max_lifetime: -1
      well_known_server_name: "${publicHost}"
      well_known_client_name: ""
      well_known_sliding_sync_proxy: ""
      disable_federation: false
      presence:
        enable_inbound: true
        enable_outbound: true
      trusted_third_party_id_servers:
      - matrix.org
      - vector.im
      jetstream:
        storage_path: /var/dendrite/
        addresses: []
        topic_prefix: Dendrite
        in_memory: false
        disable_tls_validation: true
        credentials_path: ""
      metrics:
        enabled: false
        basic_auth:
          username: metrics
          password: metrics
      sentry:
        enabled: false
        dsn: ""
        environment: ""
      dns_cache:
        enabled: true
        cache_size: 256
        cache_lifetime: 5m0s
      server_notices:
        enabled: true
        local_part: _server
        display_name: Server Alert
        avatar_url: ""
        room_name: Server Alert
      report_stats:
        enabled: false
        endpoint: https://panopticon.matrix.org/push
      cache:
        max_size_estimated: 1073741824
        max_age: 1h0m0s
    app_service_api:
      disable_tls_validation: false
      legacy_auth: false
      legacy_paths: false
      config_files: []
    client_api:
      registration_disabled: ${!cfg.allowRegistration}
      registration_requires_token: false
      registration_shared_secret: "${dummySharedSecret}"
      guests_disabled: true
      enable_registration_captcha: true
      recaptcha_api_js_url: ""
      recaptcha_sitekey_class: ""
      recaptcha_form_field: ""
      recaptcha_public_key: "${dummyReCaptchaPublicKey}"
      recaptcha_private_key: "${dummyReCaptchaPrivateKey}"
      recaptcha_bypass_secret: ""
      recaptcha_siteverify_api: "https://www.google.com/recaptcha/api/siteverify"
      turn:
        turn_user_lifetime: ""
        turn_uris: []
        turn_shared_secret: ""
        turn_username: ""
        turn_password: ""
      rate_limiting:
        enabled: true
        threshold: 5
        cooloff_ms: 500
        exempt_user_ids: []
    federation_api:
      send_max_retries: 16
      enable_relays: false
      p2p_retries_until_assumed_offline: 1
      disable_tls_validation: false
      disable_http_keepalives: false
      key_perspectives:
      - server_name: matrix.org
        keys:
        - key_id: ed25519:auto
          public_key: Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw
        - key_id: ed25519:a_RXGa
          public_key: l8Hft5qXKn1vfHrg3p4+W8gELQVo8N13JkluMfmn2sQ
      prefer_direct_fetch: false
      deny_networks:
      - 127.0.0.1/8
      - 10.0.0.0/8
      - 172.16.0.0/12
      - 192.168.0.0/16
      - 100.64.0.0/10
      - 169.254.0.0/16
      - ::1/128
      - fe80::/64
      - fc00::/7
      allow_networks:
      - 0.0.0.0/0
    key_server: {}
    media_api:
      base_path: /var/dendrite/media
      max_file_size_bytes: 10485760
      dynamic_thumbnails: false
      max_thumbnail_generators: 10
      thumbnail_sizes:
      - width: 32
        height: 32
        method: crop
      - width: 96
        height: 96
        method: crop
      - width: 640
        height: 480
        method: scale
    room_server:
      default_room_version: "10"
    sync_api:
      real_ip_header: ""
      search:
        enabled: true
        index_path: /var/dendrite/searchindex
        in_memory: false
        language: en
    user_api:
      bcrypt_cost: 10
      openid_token_lifetime_ms: 3600000
      push_gateway_disable_tls_validation: false
      auto_join_rooms: []
      worker_count: 8
    relay_api: {}
    mscs:
      mscs: []
    tracing:
      enabled: false
      jaeger:
        serviceName: ""
        disabled: false
        rpc_metrics: false
        traceid_128bit: false
        tags: []
        sampler: null
        reporter: null
        headers: null
        baggage_restrictions: null
        throttler: null
    logging:
    - type: file
      level: info
      params:
        path: /var/dendrite/log
  '';

in
{
  secretsYaml = {
    "${yamlSecretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${yamlSecretName}.age";
      generator = {
        dependencies = [
          config.age.secrets."${dbPasswordSecretName}"
          config.age.secrets."${sharedSecretName}"
          config.age.secrets."${reCaptchaPublicKeySecretName}"
          config.age.secrets."${reCaptchaPrivateKeySecretName}"
        ];
        script =
          { decrypt, deps, ... }:
          ''
            yaml="${dendrite_yaml}"

            pwd=$(${decrypt} ${lib.escapeShellArg (builtins.elemAt deps 0).file})
            dummyDbPassword="${dummyDbPassword}"

            sharedSecret=$(${decrypt} ${lib.escapeShellArg (builtins.elemAt deps 1).file})
            dummySharedSecret=${dummySharedSecret}

            reCaptchaPublicKey=$(${decrypt} ${lib.escapeShellArg (builtins.elemAt deps 2).file})
            dummyReCaptchaPublicKey=${dummyReCaptchaPublicKey}

            reCaptchaPrivateKey=$(${decrypt} ${lib.escapeShellArg (builtins.elemAt deps 3).file})
            dummyReCaptchaPrivateKey=${dummyReCaptchaPrivateKey}

            # Escape special characters in dummyDbPassword and pwd
            escaped_search=$(printf '%s\n' "$dummyDbPassword" | sed -e 's/[\/&]/\\&/g')
            escaped_replace=$(printf '%s\n' "$pwd" | sed -e 's/[\/&]/\\&/g')

            # Replace using sed
            yaml=$(echo "$yaml" | sed "s/$escaped_search/$escaped_replace/g")

            # Escape special characters in dummySharedSecret and sharedSecret
            escaped_search=$(printf '%s\n' "$dummySharedSecret" | sed -e 's/[\/&]/\\&/g')
            escaped_replace=$(printf '%s\n' "$sharedSecret" | sed -e 's/[\/&]/\\&/g')

            # Replace using sed
            yaml=$(echo "$yaml" | sed "s/$escaped_search/$escaped_replace/g")

            # Escape special characters in dummyReCaptchaPublicKey and reCaptchaPublicKey
            escaped_search=$(printf '%s\n' "$dummyReCaptchaPublicKey" | sed -e 's/[\/&]/\\&/g')
            escaped_replace=$(printf '%s\n' "$reCaptchaPublicKey" | sed -e 's/[\/&]/\\&/g')

            # Replace using sed
            yaml=$(echo "$yaml" | sed "s/$escaped_search/$escaped_replace/g")

            # Escape special characters in dummyReCaptchaPrivateKey and reCaptchaPrivateKey
            escaped_search=$(printf '%s\n' "$dummyReCaptchaPrivateKey" | sed -e 's/[\/&]/\\&/g')
            escaped_replace=$(printf '%s\n' "$reCaptchaPrivateKey" | sed -e 's/[\/&]/\\&/g')

            # Replace using sed
            yaml=$(echo "$yaml" | sed "s/$escaped_search/$escaped_replace/g")

            echo "$yaml"
          '';
      };
    };

    "${sharedSecretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${sharedSecretName}.age";
      generator.script = (
        sharedConfig.lib.make-agenix-generator-prompt {
          prompts = [
            "Configuring Dendrite shared secret"
            "Please enter the shared secret for Dendrite:"
          ];
        }
      );
    };

    "${reCaptchaPublicKeySecretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${reCaptchaPublicKeySecretName}.age";
      generator.script = (
        sharedConfig.lib.make-agenix-generator-prompt {
          prompts = [
            "Please enter the the ReCaptcha public key:"
            "You can get this from https://www.google.com/recaptcha/admin"
          ];
        }
      );
    };

    "${reCaptchaPrivateKeySecretName}" = {
      rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${reCaptchaPrivateKeySecretName}.age";
      generator.script = (
        sharedConfig.lib.make-agenix-generator-prompt {
          prompts = [
            "Please enter the the ReCaptcha private key:"
            "You can get this from https://www.google.com/recaptcha/admin"
          ];
        }
      );
    };
  };

  yamlSecretPath = config.age.secrets."${yamlSecretName}".path;
}
