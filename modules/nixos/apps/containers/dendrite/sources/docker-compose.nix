# Auto-generated using compose2nix v0.3.2-pre.
{
  pkgs,
  lib,
  config,
  ...
}:

{
  # Runtime
  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    dockerCompat = true;
  };

  # Enable container name DNS for all Podman networks.
  networking.firewall.interfaces =
    let
      matchAll = if !config.networking.nftables.enable then "podman+" else "podman*";
    in
    {
      "${matchAll}".allowedUDPPorts = [ 53 ];
    };

  virtualisation.oci-containers.backend = "podman";

  # Containers
  virtualisation.oci-containers.containers."dendrite-monolith" = {
    image = "ghcr.io/element-hq/dendrite-monolith:latest";
    volumes = [
      "/home/gaj/gitlab.com/gaelj/shared-nixos/modules/nixos/apps/containers/dendrite/sources/config:/etc/dendrite:rw"
      "dendrite_dendrite_jetstream:/var/dendrite/jetstream:rw"
      "dendrite_dendrite_media:/var/dendrite/media:rw"
      "dendrite_dendrite_search_index:/var/dendrite/searchindex:rw"
    ];
    ports = [
      "8008:8008/tcp"
      "8448:8448/tcp"
    ];
    dependsOn = [
      "dendrite-postgres"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=monolith"
      "--network-alias=monolith"
      "--network=dendrite_internal"
    ];
  };
  systemd.services."podman-dendrite-monolith" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-dendrite_internal.service"
      "podman-volume-dendrite_dendrite_jetstream.service"
      "podman-volume-dendrite_dendrite_media.service"
      "podman-volume-dendrite_dendrite_search_index.service"
    ];
    requires = [
      "podman-network-dendrite_internal.service"
      "podman-volume-dendrite_dendrite_jetstream.service"
      "podman-volume-dendrite_dendrite_media.service"
      "podman-volume-dendrite_dendrite_search_index.service"
    ];
    partOf = [
      "podman-compose-dendrite-root.target"
    ];
    wantedBy = [
      "podman-compose-dendrite-root.target"
    ];
  };
  virtualisation.oci-containers.containers."dendrite-postgres" = {
    image = "postgres:15-alpine";
    environment = {
      "POSTGRES_DATABASE" = "dendrite";
      "POSTGRES_PASSWORD" = "itsasecret";
      "POSTGRES_USER" = "dendrite";
    };
    volumes = [
      "dendrite_dendrite_postgres_data:/var/lib/postgresql/data:rw"
    ];
    log-driver = "journald";
    extraOptions = [
      "--health-cmd=pg_isready -U dendrite"
      "--health-interval=5s"
      "--health-retries=5"
      "--health-timeout=5s"
      "--hostname=postgres"
      "--network-alias=postgres"
      "--network=dendrite_internal"
    ];
  };
  systemd.services."podman-dendrite-postgres" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-dendrite_internal.service"
      "podman-volume-dendrite_dendrite_postgres_data.service"
    ];
    requires = [
      "podman-network-dendrite_internal.service"
      "podman-volume-dendrite_dendrite_postgres_data.service"
    ];
    partOf = [
      "podman-compose-dendrite-root.target"
    ];
    wantedBy = [
      "podman-compose-dendrite-root.target"
    ];
  };

  # Networks
  systemd.services."podman-network-dendrite_internal" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStop = "podman network rm -f dendrite_internal";
    };
    script = ''
      podman network inspect dendrite_internal || podman network create dendrite_internal
    '';
    partOf = [ "podman-compose-dendrite-root.target" ];
    wantedBy = [ "podman-compose-dendrite-root.target" ];
  };

  # Volumes
  systemd.services."podman-volume-dendrite_dendrite_jetstream" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    script = ''
      podman volume inspect dendrite_dendrite_jetstream || podman volume create dendrite_dendrite_jetstream
    '';
    partOf = [ "podman-compose-dendrite-root.target" ];
    wantedBy = [ "podman-compose-dendrite-root.target" ];
  };
  systemd.services."podman-volume-dendrite_dendrite_media" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    script = ''
      podman volume inspect dendrite_dendrite_media || podman volume create dendrite_dendrite_media
    '';
    partOf = [ "podman-compose-dendrite-root.target" ];
    wantedBy = [ "podman-compose-dendrite-root.target" ];
  };
  systemd.services."podman-volume-dendrite_dendrite_postgres_data" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    script = ''
      podman volume inspect dendrite_dendrite_postgres_data || podman volume create dendrite_dendrite_postgres_data
    '';
    partOf = [ "podman-compose-dendrite-root.target" ];
    wantedBy = [ "podman-compose-dendrite-root.target" ];
  };
  systemd.services."podman-volume-dendrite_dendrite_search_index" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    script = ''
      podman volume inspect dendrite_dendrite_search_index || podman volume create dendrite_dendrite_search_index
    '';
    partOf = [ "podman-compose-dendrite-root.target" ];
    wantedBy = [ "podman-compose-dendrite-root.target" ];
  };

  # Root service
  # When started, this will automatically create all resources and start
  # the containers. When stopped, this will teardown all resources.
  systemd.targets."podman-compose-dendrite-root" = {
    unitConfig = {
      Description = "Root target generated by compose2nix.";
    };
    wantedBy = [ "multi-user.target" ];
  };
}
