{
  sharedConfig,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkDbDumperUnit
    mkPostGresDumperContainerEnv
    mkSharedContainerSettings
    mkContainerUser
    ;
  wa = config.sharedConfig.webApps;
  cfg = wa.dendrite;
  syncV3Cfg = wa.sync-v3;
  version = config.sharedConfig.appVersions.dendrite.version;
  inherit (cfg) port port2 proxyPass;

  name = "dendrite";
  backupPath = "/srv/backups/${name}/db";
  dataPath = "/srv/${name}";

  POSTGRES_DB = "dendrite";
  POSTGRES_USER = "dendrite";
  POSTGRES_HOST = "postgres";

  inherit
    (sharedConfig.lib.make-agenix-container-environment-file-matrix {
      inherit config;
      secretName = "dendriteSecretKey";
    })
    secrets
    secretPath
    ;

  sEnv = (
    sharedConfig.lib.make-agenix-container-environment-file {
      inherit config lib;
      secretName = "dendriteSecretDb";
      envSecretName = "dendriteSecretDbEnv";
      envKeyName = "POSTGRES_PASSWORD";
    }
  );

  secrets2 = sEnv.secrets;
  envSecretPath = sEnv.envSecretPath;

  sharedContainerSettings = mkSharedContainerSettings name cfg;

  inherit (import ./dendrite-config.nix { inherit config lib sharedConfig; })
    secretsYaml
    yamlSecretPath
    ;
  inherit (config.sharedConfig.network) publicHost;

  wellKnownLocations = {
    "/.well-known/matrix/server" = {
      extraConfig = ''
        return 200  ' { m.server": "${cfg.externalHost}:443" } ';
      '';
    };

    "/.well-known/matrix/client" =
    let
      syncV3CfgStr = '', org.matrix.msc3575.proxy": { url": "https://${syncV3Cfg.externalHost}" }'';
      syncV3CfgCond = if (wa ? sync-v3.enable && wa.sync-v3.enable) then syncV3CfgStr else "";
    in
    {
      extraConfig = ''
        add_header Access-Control-Allow-Origin *;
        return 200  ' { m.homeserver": { base_url": "https://${cfg.externalHost}" }${syncV3CfgCond} } ' ;
      '';
    };
  };
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = secrets // secrets2 // secretsYaml;

    users = mkContainerUser name;

    virtualisation.oci-containers.containers."dendrite-monolith" =
      {
        image = "ghcr.io/element-hq/dendrite-monolith:v${version}";
        #user = "${name}:${name}";
        ports = [
          "${toString port}:8008/tcp"
          "${toString port2}:8448/tcp"
        ];
        dependsOn = [
          "dendrite-postgres"
        ];
      }
      // sharedContainerSettings {
        volumes = [
          "${dataPath}/config:/etc/dendrite:rw"
          "${yamlSecretPath}:/etc/dendrite/dendrite.yaml:ro"
          "${secretPath}:/etc/dendrite/matrix_key.pem"
          "${dataPath}/jetstream:/var/dendrite/jetstream:rw"
          "${dataPath}/media:/var/dendrite/media:rw"
          "${dataPath}/search_index:/var/dendrite/searchindex:rw"
          "${dataPath}/log:/var/dendrite/log:rw"
        ];
        extraOptions = [
          "--hostname=monolith"
          "--network-alias=monolith"
        ];
      };

    systemd.services."podman-dendrite-monolith" = mkAppUnit name;

    virtualisation.oci-containers.containers."dendrite-postgres" =
      {
        image = "postgres:15-alpine";
        environmentFiles = [ envSecretPath ];
      }
      // sharedContainerSettings {
        environment = {
          "POSTGRES_DATABASE" = POSTGRES_DB;
          "POSTGRES_USER" = POSTGRES_USER;
        };
        volumes = [
          "${dataPath}/data:/var/lib/postgresql/data:rw"
        ];
        extraOptions = [
          "--health-cmd=pg_isready -U ${POSTGRES_USER}"
          "--health-interval=5s"
          "--health-retries=5"
          "--health-timeout=5s"
          "--hostname=postgres"
          "--network-alias=postgres"
        ];
      };

    systemd.services."podman-dendrite-postgres" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_db_dumper" =
      mkPostGresDumperContainerEnv name POSTGRES_HOST POSTGRES_USER envSecretPath POSTGRES_DB backupPath
        cfg;

    systemd.services."podman-${name}_db_dumper" = mkDbDumperUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createDendriteDataDir.text = ''
        mkdir -p "${dataPath}/config"
        mkdir -p "${dataPath}/jetstream"
        mkdir -p "${dataPath}/media"
        mkdir -p "${dataPath}/search_index"
        mkdir -p "${dataPath}/data"
        mkdir -p "${dataPath}/log"
        mkdir -p "${backupPath}"

        chown -R ${name}:${name} "${dataPath}"
        chown -R ${name}:${name} "${backupPath}"

        chmod -R 777 "${dataPath}"
        # chmod -R 777 "${backupPath}"
      '';
    };

    networking.firewall.allowedTCPPorts = lib.optionals (cfg.openFirewall) [
      cfg.port
      cfg.port2
      8448
    ];

    networking.firewall.allowedUDPPorts = lib.optionals (cfg.openFirewall) [
      cfg.port
      cfg.port2
      8448
    ];

    services.nginx = {
      streamConfig = ''
        server {
          listen 8448 ssl;
          ssl_certificate     /var/lib/acme/${cfg.externalHost}/fullchain.pem;
          ssl_certificate_key /var/lib/acme/${cfg.externalHost}/key.pem;

          proxy_timeout 20s;

          proxy_pass 127.0.0.1:${toString port2};
        }
      '';

      virtualHosts = {
        "${cfg.externalHost}" = lib.mkIf (cfg.externalHost != "") {
          forceSSL = true;
          enableACME = true;
          locations = wellKnownLocations // {
            "/.well-known/acme-challenge" = {
              root = "/var/lib/acme/acme-challenge";
            };

            "/" = {
              inherit proxyPass;
            };
          };
        };

        "${publicHost}" = lib.mkIf (cfg.externalHost != "") {
          locations = wellKnownLocations // {
            "/_matrix" = {
              inherit proxyPass;
            };
            # "~ ^(\/_matrix|\/_synapse\/client)" = {
            #  proxyPass = proxyPassWithoutTrailingSlash;
            #  extraConfig = ''
            #    proxy_set_header X-Forwarded-For $remote_addr;
            #    proxy_set_header X-Forwarded-Proto $scheme;
            #    proxy_set_header Host $host;
            #    ${geoIpConfig}
            #  '';
            #};
          };
        };
      };
    };
  };
}
