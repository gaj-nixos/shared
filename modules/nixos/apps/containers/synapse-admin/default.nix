{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:

let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    ;
  wa = config.sharedConfig.webApps;
  cfg = wa.synapse-admin;
  version = config.sharedConfig.appVersions.synapse-admin.version;
  port = cfg.port;
  proxyPass = cfg.proxyPass;

  name = "synapse-admin";

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {

    virtualisation.oci-containers.containers."${name}-webserver" = {
      image = "awesometechnologies/synapse-admin:${version}";
      ports = [ "${toString port}:80" ];
    } // sharedContainerSettings { };
    systemd.services."podman-${name}-webserver" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
