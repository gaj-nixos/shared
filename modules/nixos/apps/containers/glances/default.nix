{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkSharedContainerSettingsHostNetwork
    mkAppUnitNoNetwork
    mkRootUnit
    ;
  inherit (lib) mkIf;
  glancesVersion = config.sharedConfig.appVersions.glances.version;

  cfg = config.sharedConfig.webApps.glances;

  name = "glances";
  dataDir = "/srv/${name}";

  proxyPass = cfg.proxyPass;

  sharedContainerSettingsHostNetwork = mkSharedContainerSettingsHostNetwork cfg;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    virtualisation.oci-containers.containers."${name}" =
      {
        image = "nicolargo/glances:${glancesVersion}";
        hostname = "${name}-app";
        #ports = [ "${toString cfg.port}:61208" ];
      }
      // sharedContainerSettingsHostNetwork {
        volumes = [
          "/run/user/${toString cfg.podmanSockUserId}/podman/podman.sock:/run/user/1000/podman/podman.sock:ro"
          "/etc/os-release:/etc/os-release:ro"
          "${dataDir}/glances.conf:/glances/conf/glances.conf:rw"
          "/run/user/1000/podman/podman.sock:/run/user/1000/podman/podman.sock:ro"
          "/var/run/docker.sock:/var/run/docker.sock:ro"
        ];
        environment = {
          GLANCES_OPT = "-C /glances/conf/glances.conf -w";
          PUID = "1000";
          PGID = "1000";
        };
        extraOptions = [
          "--privileged"
          "--name=${name}-app"
          "--health-interval=30s" # Check every 30 seconds
          "--health-start-period=120s" # Wait 60 seconds after startup before checking
          "--health-retries=3" # Retry 3 times before marking as unhealthy
          "--health-timeout=10s" # Timeout for each health check attempt
        ]; # "--pid host"
      };

    systemd.services."podman-${name}" = mkAppUnitNoNetwork name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createGlancesDataDir.text = ''
        mkdir -p "${dataDir}"
        chown -R root:root "${dataDir}"
        chmod 700 "${dataDir}"
        touch "${dataDir}/glances.conf"
      '';
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
