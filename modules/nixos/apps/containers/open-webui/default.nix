{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf optional;
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    ;

  openWebUiVersion = config.sharedConfig.appVersions.openWebUi.version;
  openWebUiPipelinesVersion = config.sharedConfig.appVersions.openWebUiPipelines.version;
  tikaVersion = config.sharedConfig.appVersions.tika.version;

  name = "open-webui";
  openWebUiDataDir = "/srv/open-webui/data";
  openwebuiPipelinesDataDir = "/srv/open-webui/pipelines";
  docsDir = "${openWebUiDataDir}/docs";

  searxPort = toString config.sharedConfig.webApps.searx.port;
  ollamaPort = toString config.sharedConfig.services.ollama.port;

  entrypointScript = ''
    #!/bin/sh
    set -e

    # Copy CA certificates from the host to the container
    if [ -d /host-certs ]; then
      cp /host-certs/rootCA.pem /usr/local/share/ca-certificates/rootCA.pem
      if [ -f /host-certs/ca-bundle.crt ]; then
        cp /host-certs/ca-bundle.crt /usr/local/share/ca-certificates/ca-bundle.crt
      fi
      update-ca-certificates
    fi

    # Execute the original command
    ./start.sh
  '';

  entrypointScriptBin = pkgs.writeScriptBin "podman-ssl-entrypoint.sh" entrypointScript;

  cfg = config.sharedConfig.webApps.openWebUi;

  volumes = cfg.volumes;

  rootCACertPath = config.sharedConfig.network.ssl.rootCACertPath;

  proxyPass = cfg.proxyPass;

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    virtualisation.oci-containers.containers."${name}" =
      {
        image = "ghcr.io/open-webui/open-webui:${openWebUiVersion}";
        ports = [ "${toString cfg.port}:8080" ];
      }
      // sharedContainerSettings {
        environment = {
          "OLLAMA_API_BASE_URL" = "http://localhost:${ollamaPort}/api";
          "OLLAMA_BASE_URL" = "http://localhost:${ollamaPort}";
          "ENABLE_RAG_WEB_SEARCH" = "true";
          "ANONYMIZED_TELEMETRY" = "false";
          "RAG_WEB_SEARCH_ENGINE" = "searxng";
          "RAG_WEB_SEARCH_RESULT_COUNT" = "3";
          "RAG_WEB_SEARCH_CONCURRENT_REQUESTS" = "10";
          "SEARXNG_QUERY_URL" = "http://localhost:${searxPort}/search?q=<query>";
        };
        volumes =
          [
            "${openWebUiDataDir}:/app/backend/data"
            "${docsDir}:/app/backend/data/docs"
            "/srv/pki/rootCA:/host-certs:ro"
            "${entrypointScriptBin}/bin/podman-ssl-entrypoint.sh:/app/backend/podman-ssl-entrypoint.sh:ro" # Mount the entrypoint script from Nix store
          ]
          ++ volumes
          ++ optional (
            if rootCACertPath != null then true else false
          ) "${rootCACertPath}:/host-certs/ca-bundle.crt:ro";
        extraOptions = [
          "--name=${name}"
          "--network-alias=${name}"
          "--entrypoint=/app/backend/podman-ssl-entrypoint.sh" # Use the entrypoint script
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-pipelines" =
      {
        # docker run -d -p 9099:9099 --add-host=host.docker.internal:host-gateway -v pipelines:/app/pipelines --name pipelines --restart always ghcr.io/open-webui/pipelines:main
        # default API key is 0p3n-w3bu!.
        # See https://github.com/open-webui/pipelines
        image = "ghcr.io/open-webui/pipelines:${openWebUiPipelinesVersion}";
        ports = [ "${toString cfg.pipelines.port}:9099" ];
      }
      // sharedContainerSettings {
        volumes = [
          "${openwebuiPipelinesDataDir}:/app/pipelines"
        ];
        extraOptions = [
          "--name=pipelines"
          "--network-alias=pipelines"
        ];
      };

    systemd.services."podman-${name}-pipelines" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-tika" =
      {
        # Content extraction engine
        image = "apache/tika:${tikaVersion}";
        ports = [ "${toString cfg.tika.port}:9998" ];
      }
      // sharedContainerSettings {
        extraOptions = [
          "--name=tika"
          "--network-alias=tika"
        ];
      };

    systemd.services."podman-${name}-tika" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      openWebUIScript.text = ''
        mkdir -p ${openWebUiDataDir}
        install -d -m 755 ${openWebUiDataDir} -o root -g root
        mkdir -p ${openwebuiPipelinesDataDir}
        install -d -m 755 ${openwebuiPipelinesDataDir} -o root -g root
        chmod 777 ${docsDir}
      '';
    };

    services.samba = {
      settings = {
        OpenWebUiRagDocumentsShare = {
          path = "${docsDir}";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
        };
      };
    };

    networking.firewall.allowedTCPPorts = [
      cfg.port
      cfg.pipelines.port
    ];

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
