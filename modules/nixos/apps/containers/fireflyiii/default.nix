{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkBridgeNetworkUnit
    mkDbDumperUnit
    mkPostGresDumperContainer
    mkSharedContainerSettings
    ;
  wa = config.sharedConfig.webApps;
  cfg = wa.fireflyiii;
  version = config.sharedConfig.appVersions.fireflyiii.version;
  port = cfg.port;
  proxyPass = cfg.proxyPass;

  name = "fireflyiii";
  backupPath = "/srv/backups/${name}/db";
  uploadPath = "/srv/${name}/upload";
  dbPath = "/srv/${name}/db";

  POSTGRES_DB = "firefly";
  POSTGRES_PASSWORD = "firefly";
  POSTGRES_USER = "firefly";
  POSTGRES_HOST = "db";

  extraConfig = ''
    proxy_set_header X-Forwarded-Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  '';

  inherit
    (sharedConfig.lib.make-agenix-container-environment-file {
      inherit config lib;
      secretName = "fireflyiiiSecretKey";
      envSecretName = "fireflyiiiSecretEnv";
      envKeyName = "APP_KEY";
      maxLength = 32;
    })
    secrets
    envSecretPath
    ;

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = secrets;

    virtualisation.oci-containers.containers."${name}_core" =
      {
        image = "fireflyiii/core:${version}";
        environmentFiles = [ envSecretPath ];
        ports = [
          "${toString port}:8080/tcp"
        ];
        dependsOn = [
          "${name}-db"
        ];
      }
      // sharedContainerSettings {
        environment = {
          "ALLOW_WEBHOOKS" = "false";
          "APP_DEBUG" = "false";
          "APP_ENV" = "production";
          "APP_LOG_LEVEL" = "notice";
          "APP_NAME" = "FireflyIII";
          "APP_URL" =
            if cfg.externalHost != "" then "https://${cfg.externalHost}" else "https://${cfg.mainInternalHost}";
          "AUDIT_LOG_CHANNEL" = "";
          "AUDIT_LOG_LEVEL" = "emergency";
          "AUTHENTICATION_GUARD" = "web";
          "AUTHENTICATION_GUARD_EMAIL" = "";
          "AUTHENTICATION_GUARD_HEADER" = "REMOTE_USER";
          "BROADCAST_DRIVER" = "log";
          "CACHE_DRIVER" = "file";
          "CACHE_PREFIX" = "firefly";
          "COOKIE_DOMAIN" = "";
          "COOKIE_PATH" = "/";
          "COOKIE_SAMESITE" = "lax";
          "COOKIE_SECURE" = "false";
          "CUSTOM_LOGOUT_URL" = "";

          "DB_CONNECTION" = "pgsql";
          "DB_DATABASE" = POSTGRES_DB;
          "DB_HOST" = POSTGRES_HOST;
          "DB_PASSWORD" = POSTGRES_PASSWORD;
          "DB_PORT" = "5432";
          "DB_SOCKET" = "";
          "DB_USERNAME" = POSTGRES_USER;

          "DEFAULT_LANGUAGE" = "en_US";
          "DEFAULT_LOCALE" = "fr_FR";
          "DEMO_PASSWORD" = "";
          "DEMO_USERNAME" = "";
          "DISABLE_CSP_HEADER" = "false";
          "DISABLE_FRAME_HEADER" = "false";
          "DKR_BUILD_LOCALE" = "false";
          "DKR_CHECK_SQLITE" = "true";
          "ENABLE_EXCHANGE_RATES" = "true";
          "ENABLE_EXTERNAL_MAP" = "true";
          "ENABLE_EXTERNAL_RATES" = "true";
          "FIREFLY_III_LAYOUT" = "v1";
          "IPINFO_TOKEN" = "";
          "LOG_CHANNEL" = "stack";

          "MAILERSEND_API_KEY" = "";
          "MAILGUN_DOMAIN" = "";
          "MAILGUN_ENDPOINT" = "api.mailgun.net";
          "MAILGUN_SECRET" = "";
          "MAIL_ENCRYPTION" = "null";
          "MAIL_FROM" = "changeme@example.com";
          "MAIL_HOST" = "null";
          "MAIL_MAILER" = "log";
          "MAIL_PASSWORD" = "null";
          "MAIL_PORT" = "2525";
          "MAIL_SENDMAIL_COMMAND" = "";
          "MAIL_USERNAME" = "null";
          "MANDRILL_SECRET" = "";
          "MAP_DEFAULT_LAT" = "51.983333";
          "MAP_DEFAULT_LONG" = "5.916667";
          "MAP_DEFAULT_ZOOM" = "6";
          "MYSQL_SSL_CA" = "";
          "MYSQL_SSL_CAPATH" = "/etc/ssl/certs/";
          "MYSQL_SSL_CERT" = "";
          "MYSQL_SSL_CIPHER" = "";
          "MYSQL_SSL_KEY" = "";
          "MYSQL_SSL_VERIFY_SERVER_CERT" = "true";
          "MYSQL_USE_SSL" = "false";
          "PAPERTRAIL_HOST" = "";
          "PAPERTRAIL_PORT" = "";
          "PASSPORT_PRIVATE_KEY" = "";
          "PASSPORT_PUBLIC_KEY" = "";
          "PGSQL_SCHEMA" = "public";
          "PGSQL_SSL_CERT" = "null";
          "PGSQL_SSL_CRL_FILE" = "null";
          "PGSQL_SSL_KEY" = "null";
          "PGSQL_SSL_MODE" = "prefer";
          "PGSQL_SSL_ROOT_CERT" = "null";
          "PUSHER_ID" = "";
          "PUSHER_KEY" = "";
          "PUSHER_SECRET" = "";
          "QUEUE_DRIVER" = "sync";
          "REDIS_CACHE_DB" = "1";
          "REDIS_DB" = "0";
          "REDIS_HOST" = "127.0.0.1";
          "REDIS_PASSWORD" = "";
          "REDIS_PATH" = "";
          "REDIS_PORT" = "6379";
          "REDIS_SCHEME" = "tcp";
          "REDIS_USERNAME" = "";
          "SEND_ERROR_MESSAGE" = "true";
          "SEND_REPORT_JOURNALS" = "true";
          "SESSION_DRIVER" = "file";
          "SITE_OWNER" = "mail@example.com";
          "SPARKPOST_SECRET" = "";
          "STATIC_CRON_TOKEN" = "";
          "TRACKER_SITE_ID" = "";
          "TRACKER_URL" = "";
          "TRUSTED_PROXIES" = "**";
          "USE_RUNNING_BALANCE" = "false";
          "VALID_URL_PROTOCOLS" = "";
        };
        volumes = [
          "${uploadPath}:/var/www/html/storage/upload:rw"
        ];
        extraOptions = [
          "--hostname=app"
          "--network-alias=app"
        ];
      };

    systemd.services."podman-${name}_core" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_cron" =
      {
        image = "alpine";
        cmd = [
          "sh"
          "-c"
          "echo \"0 3 * * * wget -qO- http://app:8080/api/v1/cron/REPLACEME;echo\" | crontab - && crond -f -L /dev/stdout"
        ];
      }
      // sharedContainerSettings {
        extraOptions = [
          "--network-alias=cron"
        ];
      };

    systemd.services."podman-${name}_cron" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-db" =
      {
        image = "postgres:17.2";
        hostname = "${POSTGRES_HOST}";
      }
      // sharedContainerSettings {
        volumes = [
          "${dbPath}:/var/lib/postgresql/data:rw"
        ];
        environment = {
          POSTGRES_DB = POSTGRES_DB;
          POSTGRES_PASSWORD = POSTGRES_PASSWORD;
          POSTGRES_USER = POSTGRES_USER;
        };
        extraOptions = [
          "--health-cmd=pg_isready -U ${POSTGRES_USER}"
          "--health-interval=5s"
          "--health-retries=5"
          "--health-timeout=5s"
          "--network-alias=db"
        ];
      };

    systemd.services."podman-${name}-db" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_importer" =
      {
        image = "fireflyiii/data-importer:latest";
        ports = [ "${toString cfg.importerPort}:8080/tcp" ];
        dependsOn = [
          "${name}_core"
        ];
      }
      // sharedContainerSettings {
        environment = {
          "APP_DEBUG" = "false";
          "APP_ENV" = "local";
          "APP_NAME" = "DataImporter";
          "APP_URL" =
            if cfg.externalHost != "" then "https://${cfg.externalHost}" else "https://${cfg.mainInternalHost}";
          "ASSET_URL" = "";
          "AUTO_IMPORT_SECRET" = "";
          "BROADCAST_DRIVER" = "log";
          "CACHE_DRIVER" = "file";
          "CAN_POST_AUTOIMPORT" = "false";
          "CAN_POST_FILES" = "false";
          "CONNECTION_TIMEOUT" = "31.41";
          "ENABLE_MAIL_REPORT" = "false";
          "EXPECT_SECURE_URL" = "false";
          "FALLBACK_IN_DIR" = "false";
          "FIREFLY_III_ACCESS_TOKEN" = "";
          "FIREFLY_III_CLIENT_ID" = "";
          "FIREFLY_III_URL" =
            if cfg.externalHost != "" then "https://${cfg.externalHost}" else "https://${cfg.mainInternalHost}";
          "GOCARDLESS_GET_ACCOUNT_DETAILS" = "false";
          "GOCARDLESS_GET_BALANCE_DETAILS" = "false";
          "IGNORE_DUPLICATE_ERRORS" = "false";
          "IGNORE_NOT_FOUND_TRANSACTIONS" = "false";
          "IMPORT_DIR_ALLOWLIST" = "";
          "IS_EXTERNAL" = "false";
          "JSON_CONFIGURATION_DIR" = "";
          "LOG_CHANNEL" = "stack";
          "LOG_LEVEL" = "debug";
          "LOG_RETURN_JSON" = "false";
          "MAILGUN_DOMAIN" = "";
          "MAILGUN_ENDPOINT" = "";
          "MAILGUN_SECRET" = "";
          "MAIL_DESTINATION" = "noreply@example.com";
          "MAIL_ENCRYPTION" = "null";
          "MAIL_FROM_ADDRESS" = "noreply@example.com";
          "MAIL_HOST" = "smtp.mailtrap.io";
          "MAIL_MAILER" = "";
          "MAIL_PASSWORD" = "password";
          "MAIL_PORT" = "2525";
          "MAIL_USERNAME" = "username";
          "NORDIGEN_ID" = "";
          "NORDIGEN_KEY" = "";
          "NORDIGEN_SANDBOX" = "false";
          "POSTMARK_TOKEN" = "";
          "QUEUE_CONNECTION" = "sync";
          "REDIS_CACHE_DB" = "1";
          "REDIS_DB" = "0";
          "REDIS_HOST" = "127.0.0.1";
          "REDIS_PASSWORD" = "null";
          "REDIS_PORT" = "6379";
          "RESPOND_TO_GOCARDLESS_LIMIT" = "wait";
          "SESSION_DRIVER" = "file";
          "SESSION_LIFETIME" = "120";
          "SPECTRE_APP_ID" = "";
          "SPECTRE_SECRET" = "";
          "TRACKER_SITE_ID" = "";
          "TRACKER_URL" = "";
          "TRUSTED_PROXIES" = "**";
          "USE_CACHE" = "false";
          "VANITY_URL" = "";
          "VERIFY_TLS_SECURITY" = "true";
        };
        extraOptions = [
          "--hostname=importer"
          "--network-alias=importer"
        ];
      };

    systemd.services."podman-${name}_importer" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_db_dumper" =
      mkPostGresDumperContainer name POSTGRES_HOST POSTGRES_USER POSTGRES_PASSWORD POSTGRES_DB backupPath
        cfg;

    systemd.services."podman-${name}_db_dumper" = mkDbDumperUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkBridgeNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createFireflyDataDir.text = ''
        mkdir -p "${uploadPath}"
        mkdir -p "${dbPath}"
        mkdir -p "${backupPath}"
      '';
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        extraConfig
        ;
    };
  };
}
