# Auto-generated using compose2nix v0.3.2-pre.
{
  pkgs,
  lib,
  ...
}:
{
  # Runtime
  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    dockerCompat = true;
    defaultNetwork.settings = {
      # Required for container networking to be able to use names.
      dns_enabled = true;
    };
  };

  # Enable container name DNS for non-default Podman networks.
  # https://github.com/NixOS/nixpkgs/issues/226365
  networking.firewall.interfaces."podman+".allowedUDPPorts = [ 53 ];

  virtualisation.oci-containers.backend = "podman";

  # Containers
  virtualisation.oci-containers.containers."firefly_iii_core" = {
    image = "fireflyiii/core:latest";
    environment = {
      "ALLOW_WEBHOOKS" = "false";
      "APP_DEBUG" = "false";
      "APP_ENV" = "production";
      "APP_KEY" = "SomeRandomStringOf32CharsExactly";
      "APP_LOG_LEVEL" = "notice";
      "APP_NAME" = "FireflyIII";
      "APP_URL" = "http://localhost";
      "AUDIT_LOG_CHANNEL" = "";
      "AUDIT_LOG_LEVEL" = "emergency";
      "AUTHENTICATION_GUARD" = "web";
      "AUTHENTICATION_GUARD_EMAIL" = "";
      "AUTHENTICATION_GUARD_HEADER" = "REMOTE_USER";
      "BROADCAST_DRIVER" = "log";
      "CACHE_DRIVER" = "file";
      "CACHE_PREFIX" = "firefly";
      "COOKIE_DOMAIN" = "";
      "COOKIE_PATH" = "/";
      "COOKIE_SAMESITE" = "lax";
      "COOKIE_SECURE" = "false";
      "CUSTOM_LOGOUT_URL" = "";
      "DB_CONNECTION" = "mysql";
      "DB_DATABASE" = "firefly";
      "DB_HOST" = "db";
      "DB_PASSWORD" = "secret_firefly_password";
      "DB_PORT" = "3306";
      "DB_SOCKET" = "";
      "DB_USERNAME" = "firefly";
      "DEFAULT_LANGUAGE" = "en_US";
      "DEFAULT_LOCALE" = "equal";
      "DEMO_PASSWORD" = "";
      "DEMO_USERNAME" = "";
      "DISABLE_CSP_HEADER" = "false";
      "DISABLE_FRAME_HEADER" = "false";
      "DKR_BUILD_LOCALE" = "false";
      "DKR_CHECK_SQLITE" = "true";
      "ENABLE_EXCHANGE_RATES" = "false";
      "ENABLE_EXTERNAL_MAP" = "false";
      "ENABLE_EXTERNAL_RATES" = "false";
      "FIREFLY_III_LAYOUT" = "v1";
      "IPINFO_TOKEN" = "";
      "LOG_CHANNEL" = "stack";
      "MAILERSEND_API_KEY" = "";
      "MAILGUN_DOMAIN" = "";
      "MAILGUN_ENDPOINT" = "api.mailgun.net";
      "MAILGUN_SECRET" = "";
      "MAIL_ENCRYPTION" = "null";
      "MAIL_FROM" = "changeme@example.com";
      "MAIL_HOST" = "null";
      "MAIL_MAILER" = "log";
      "MAIL_PASSWORD" = "null";
      "MAIL_PORT" = "2525";
      "MAIL_SENDMAIL_COMMAND" = "";
      "MAIL_USERNAME" = "null";
      "MANDRILL_SECRET" = "";
      "MAP_DEFAULT_LAT" = "51.983333";
      "MAP_DEFAULT_LONG" = "5.916667";
      "MAP_DEFAULT_ZOOM" = "6";
      "MYSQL_SSL_CA" = "";
      "MYSQL_SSL_CAPATH" = "/etc/ssl/certs/";
      "MYSQL_SSL_CERT" = "";
      "MYSQL_SSL_CIPHER" = "";
      "MYSQL_SSL_KEY" = "";
      "MYSQL_SSL_VERIFY_SERVER_CERT" = "true";
      "MYSQL_USE_SSL" = "false";
      "PAPERTRAIL_HOST" = "";
      "PAPERTRAIL_PORT" = "";
      "PASSPORT_PRIVATE_KEY" = "";
      "PASSPORT_PUBLIC_KEY" = "";
      "PGSQL_SCHEMA" = "public";
      "PGSQL_SSL_CERT" = "null";
      "PGSQL_SSL_CRL_FILE" = "null";
      "PGSQL_SSL_KEY" = "null";
      "PGSQL_SSL_MODE" = "prefer";
      "PGSQL_SSL_ROOT_CERT" = "null";
      "PUSHER_ID" = "";
      "PUSHER_KEY" = "";
      "PUSHER_SECRET" = "";
      "QUEUE_DRIVER" = "sync";
      "REDIS_CACHE_DB" = "1";
      "REDIS_DB" = "0";
      "REDIS_HOST" = "127.0.0.1";
      "REDIS_PASSWORD" = "";
      "REDIS_PATH" = "";
      "REDIS_PORT" = "6379";
      "REDIS_SCHEME" = "tcp";
      "REDIS_USERNAME" = "";
      "SEND_ERROR_MESSAGE" = "true";
      "SEND_REPORT_JOURNALS" = "true";
      "SESSION_DRIVER" = "file";
      "SITE_OWNER" = "mail@example.com";
      "SPARKPOST_SECRET" = "";
      "STATIC_CRON_TOKEN" = "";
      "TRACKER_SITE_ID" = "";
      "TRACKER_URL" = "";
      "TRUSTED_PROXIES" = "";
      "TZ" = "Europe/Amsterdam";
      "USE_RUNNING_BALANCE" = "false";
      "VALID_URL_PROTOCOLS" = "";
    };
    volumes = [
      "firefly-iii_firefly_iii_upload:/var/www/html/storage/upload:rw"
    ];
    ports = [
      "80:8080/tcp"
    ];
    dependsOn = [
      "firefly_iii_db"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=app"
      "--network-alias=app"
      "--network=firefly-iii_firefly_iii"
    ];
  };
  systemd.services."podman-firefly_iii_core" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-firefly-iii_firefly_iii.service"
      "podman-volume-firefly-iii_firefly_iii_upload.service"
    ];
    requires = [
      "podman-network-firefly-iii_firefly_iii.service"
      "podman-volume-firefly-iii_firefly_iii_upload.service"
    ];
    partOf = [
      "podman-compose-firefly-iii-root.target"
    ];
    wantedBy = [
      "podman-compose-firefly-iii-root.target"
    ];
  };
  virtualisation.oci-containers.containers."firefly_iii_cron" = {
    image = "alpine";
    cmd = [
      "sh"
      "-c"
      "echo \"0 3 * * * wget -qO- http://app:8080/api/v1/cron/REPLACEME;echo\" | crontab - && crond -f -L /dev/stdout"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=cron"
      "--network=firefly-iii_firefly_iii"
    ];
  };
  systemd.services."podman-firefly_iii_cron" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-firefly-iii_firefly_iii.service"
    ];
    requires = [
      "podman-network-firefly-iii_firefly_iii.service"
    ];
    partOf = [
      "podman-compose-firefly-iii-root.target"
    ];
    wantedBy = [
      "podman-compose-firefly-iii-root.target"
    ];
  };
  virtualisation.oci-containers.containers."firefly_iii_db" = {
    image = "mariadb:lts";
    environment = {
      "MYSQL_DATABASE" = "firefly";
      "MYSQL_PASSWORD" = "secret_firefly_password";
      "MYSQL_RANDOM_ROOT_PASSWORD" = "yes";
      "MYSQL_USER" = "firefly";
    };
    volumes = [
      "firefly-iii_firefly_iii_db:/var/lib/mysql:rw"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=db"
      "--network-alias=db"
      "--network=firefly-iii_firefly_iii"
    ];
  };
  systemd.services."podman-firefly_iii_db" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-firefly-iii_firefly_iii.service"
      "podman-volume-firefly-iii_firefly_iii_db.service"
    ];
    requires = [
      "podman-network-firefly-iii_firefly_iii.service"
      "podman-volume-firefly-iii_firefly_iii_db.service"
    ];
    partOf = [
      "podman-compose-firefly-iii-root.target"
    ];
    wantedBy = [
      "podman-compose-firefly-iii-root.target"
    ];
  };
  virtualisation.oci-containers.containers."firefly_iii_importer" = {
    image = "fireflyiii/data-importer:latest";
    environment = {
      "APP_DEBUG" = "false";
      "APP_ENV" = "local";
      "APP_NAME" = "DataImporter";
      "APP_URL" = "http://localhost";
      "ASSET_URL" = "";
      "AUTO_IMPORT_SECRET" = "";
      "BROADCAST_DRIVER" = "log";
      "CACHE_DRIVER" = "file";
      "CAN_POST_AUTOIMPORT" = "false";
      "CAN_POST_FILES" = "false";
      "CONNECTION_TIMEOUT" = "31.41";
      "ENABLE_MAIL_REPORT" = "false";
      "EXPECT_SECURE_URL" = "false";
      "FALLBACK_IN_DIR" = "false";
      "FIREFLY_III_ACCESS_TOKEN" = "";
      "FIREFLY_III_CLIENT_ID" = "";
      "FIREFLY_III_URL" = "";
      "GOCARDLESS_GET_ACCOUNT_DETAILS" = "false";
      "GOCARDLESS_GET_BALANCE_DETAILS" = "false";
      "IGNORE_DUPLICATE_ERRORS" = "false";
      "IGNORE_NOT_FOUND_TRANSACTIONS" = "false";
      "IMPORT_DIR_ALLOWLIST" = "";
      "IS_EXTERNAL" = "false";
      "JSON_CONFIGURATION_DIR" = "";
      "LOG_CHANNEL" = "stack";
      "LOG_LEVEL" = "debug";
      "LOG_RETURN_JSON" = "false";
      "MAILGUN_DOMAIN" = "";
      "MAILGUN_ENDPOINT" = "";
      "MAILGUN_SECRET" = "";
      "MAIL_DESTINATION" = "noreply@example.com";
      "MAIL_ENCRYPTION" = "null";
      "MAIL_FROM_ADDRESS" = "noreply@example.com";
      "MAIL_HOST" = "smtp.mailtrap.io";
      "MAIL_MAILER" = "";
      "MAIL_PASSWORD" = "password";
      "MAIL_PORT" = "2525";
      "MAIL_USERNAME" = "username";
      "NORDIGEN_ID" = "";
      "NORDIGEN_KEY" = "";
      "NORDIGEN_SANDBOX" = "false";
      "POSTMARK_TOKEN" = "";
      "QUEUE_CONNECTION" = "sync";
      "REDIS_CACHE_DB" = "1";
      "REDIS_DB" = "0";
      "REDIS_HOST" = "127.0.0.1";
      "REDIS_PASSWORD" = "null";
      "REDIS_PORT" = "6379";
      "RESPOND_TO_GOCARDLESS_LIMIT" = "wait";
      "SESSION_DRIVER" = "file";
      "SESSION_LIFETIME" = "120";
      "SPECTRE_APP_ID" = "";
      "SPECTRE_SECRET" = "";
      "TRACKER_SITE_ID" = "";
      "TRACKER_URL" = "";
      "TRUSTED_PROXIES" = "";
      "TZ" = "Europe/Amsterdam";
      "USE_CACHE" = "false";
      "VANITY_URL" = "";
      "VERIFY_TLS_SECURITY" = "true";
    };
    ports = [
      "81:8080/tcp"
    ];
    dependsOn = [
      "firefly_iii_core"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=importer"
      "--network-alias=importer"
      "--network=firefly-iii_firefly_iii"
    ];
  };
  systemd.services."podman-firefly_iii_importer" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-firefly-iii_firefly_iii.service"
    ];
    requires = [
      "podman-network-firefly-iii_firefly_iii.service"
    ];
    partOf = [
      "podman-compose-firefly-iii-root.target"
    ];
    wantedBy = [
      "podman-compose-firefly-iii-root.target"
    ];
  };

  # Networks
  systemd.services."podman-network-firefly-iii_firefly_iii" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStop = "podman network rm -f firefly-iii_firefly_iii";
    };
    script = ''
      podman network inspect firefly-iii_firefly_iii || podman network create firefly-iii_firefly_iii --driver=bridge
    '';
    partOf = [ "podman-compose-firefly-iii-root.target" ];
    wantedBy = [ "podman-compose-firefly-iii-root.target" ];
  };

  # Volumes
  systemd.services."podman-volume-firefly-iii_firefly_iii_db" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    script = ''
      podman volume inspect firefly-iii_firefly_iii_db || podman volume create firefly-iii_firefly_iii_db
    '';
    partOf = [ "podman-compose-firefly-iii-root.target" ];
    wantedBy = [ "podman-compose-firefly-iii-root.target" ];
  };
  systemd.services."podman-volume-firefly-iii_firefly_iii_upload" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    script = ''
      podman volume inspect firefly-iii_firefly_iii_upload || podman volume create firefly-iii_firefly_iii_upload
    '';
    partOf = [ "podman-compose-firefly-iii-root.target" ];
    wantedBy = [ "podman-compose-firefly-iii-root.target" ];
  };

  # Root service
  # When started, this will automatically create all resources and start
  # the containers. When stopped, this will teardown all resources.
  systemd.targets."podman-compose-firefly-iii-root" = {
    unitConfig = {
      Description = "Root target generated by compose2nix.";
    };
    wantedBy = [ "multi-user.target" ];
  };
}
