{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    mkContainerUser
    ;
  cfg = config.sharedConfig.webApps.fail2web;

  FAIL2REST_VERSION = config.sharedConfig.appVersions.fail2rest.version;
  FAIL2WEB_VERSION = config.sharedConfig.appVersions.fail2web.version;

  name = "fail2web";
  nameRest = "fail2rest";
  srvDirWeb = "/srv/${name}";
  srvDirRest = "/srv/${nameRest}";
  password = "admin";
  port = cfg.port;
  restPort = cfg.restPort;

  proxyPass = cfg.proxyPass;

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {

    users = mkContainerUser name;

    virtualisation.oci-containers.containers."${nameRest}" =
      {
        image = "monogramm/docker-fail2rest:${FAIL2REST_VERSION}";
        hostname = "${nameRest}";
        ports = [ "${toString restPort}:5000/tcp" ];
      }
      // sharedContainerSettings {
        volumes = [
          "${srvDirRest}/:/srv/fail2rest:rw"
          # Mount directory containing fail2ban.sock
          "/var/run/fail2ban/:/var/run/fail2ban:rw"
        ];
        extraOptions = [
          "--privileged"
          "--network-alias=${nameRest}"
        ];
      };
    systemd.services."podman-${nameRest}" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}" =
      {
        image = "monogramm/docker-fail2web:${FAIL2WEB_VERSION}";
        hostname = "${name}";
        ports = [ "${toString port}:80/tcp" ];
      }
      // sharedContainerSettings {
        volumes = [
          "${srvDirWeb}/:/srv/fail2web:rw"
        ];
        environment = {
          "FAIL2REST_ADDR" = "http://${nameRest}:5000/";
          "FAIL2REST_PASSWD" = "${password}";
          "FAIL2REST_USER" = "admin";
        };
        extraOptions = [
          "--network-alias=${name}"
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createFail2webDataDir.text = ''
        mkdir -p "${srvDirWeb}"
        mkdir -p "${srvDirRest}"
        chown -R ${name}:${name} "${srvDirWeb}"
        chown -R ${name}:${name} "${srvDirRest}"
      '';
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
