# Auto-generated using compose2nix v0.3.2-pre.
{
  pkgs,
  lib,
  ...
}:
{
  # Runtime
  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    dockerCompat = true;
    defaultNetwork.settings = {
      # Required for container networking to be able to use names.
      dns_enabled = true;
    };
  };

  # Enable container name DNS for non-default Podman networks.
  # https://github.com/NixOS/nixpkgs/issues/226365
  networking.firewall.interfaces."podman+".allowedUDPPorts = [ 53 ];

  virtualisation.oci-containers.backend = "podman";

  # Containers
  virtualisation.oci-containers.containers."esphome" = {
    image = "esphome/esphome";
    environment = {
      "TZ" = "Europe/Paris";
    };
    volumes = [
      "/etc/localtime:/etc/localtime:ro"
      "./home-assistant/sources/esphome:/config:rw"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=esphome-app"
      "--network=host"
    ];
  };
  systemd.services."podman-esphome" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    partOf = [
      "podman-compose-home-assistant-root.target"
    ];
    wantedBy = [
      "podman-compose-home-assistant-root.target"
    ];
  };
  virtualisation.oci-containers.containers."homeassistant-app" = {
    image = "homeassistant/home-assistant:stable";
    environment = {
      "TZ" = "Europe/Paris";
    };
    volumes = [
      "/dev/rfkill:/dev/rfkill:rw"
      "/etc/localtime:/etc/localtime:ro"
      "./home-assistant/sources/config:/config:rw"
      "/run/dbus:/run/dbus:ro"
    ];
    dependsOn = [
      "zwave-js-app"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=homeassistant-app"
      "--network=host"
      "--privileged"
    ];
  };
  systemd.services."podman-homeassistant-app" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    partOf = [
      "podman-compose-home-assistant-root.target"
    ];
    wantedBy = [
      "podman-compose-home-assistant-root.target"
    ];
  };
  virtualisation.oci-containers.containers."homeassistant-db-backup" = {
    image = "tiredofit/db-backup";
    environment = {
      "COMPRESSION" = "XZ";
      "DB_CLEANUP_TIME" = "1400";
      "DB_DUMP_BEGIN" = "0000";
      "DB_DUMP_FREQ" = "1440";
      "DB_HOST" = "/data/home-assistant_v2.db";
      "DB_TYPE" = "sqlite";
      "MD5" = "TRUE";
      "SPLIT_DB" = "FALSE";
      "TZ" = "Europe/Paris";
    };
    volumes = [
      "./home-assistant/sources/config:/data:rw"
      "./home-assistant/sources/config/db-backup:/backup:rw"
      "./home-assistant/sources/post-script-db-backup.sh:/assets/custom-scripts/post-script.sh:ro"
    ];
    dependsOn = [
      "homeassistant-app"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=db-backup"
      "--network=home-assistant_default"
    ];
  };
  systemd.services."podman-homeassistant-db-backup" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-home-assistant_default.service"
    ];
    requires = [
      "podman-network-home-assistant_default.service"
    ];
    partOf = [
      "podman-compose-home-assistant-root.target"
    ];
    wantedBy = [
      "podman-compose-home-assistant-root.target"
    ];
  };
  virtualisation.oci-containers.containers."mosquitto_container" = {
    image = "eclipse-mosquitto:latest";
    volumes = [
      "./home-assistant/sources/mosquitto/config:/mosquitto/config:ro"
      "./home-assistant/sources/mosquitto/data:/mosquitto/data:rw"
      "./home-assistant/sources/mosquitto/log:/mosquitto/log:rw"
    ];
    log-driver = "journald";
    extraOptions = [
      "--hostname=mosquitto-app"
      "--network=host"
    ];
  };
  systemd.services."podman-mosquitto_container" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    partOf = [
      "podman-compose-home-assistant-root.target"
    ];
    wantedBy = [
      "podman-compose-home-assistant-root.target"
    ];
  };
  virtualisation.oci-containers.containers."zwave-js-app" = {
    image = "zwavejs/zwave-js-ui:latest";
    environment = {
      "TZ" = "Europe/Paris";
    };
    volumes = [
      "./home-assistant/sources/zwave-js:/usr/src/app/store:rw"
    ];
    dependsOn = [
      "mosquitto_container"
    ];
    log-driver = "journald";
    extraOptions = [
      "--device=/dev/ttyACM0:/dev/ttyACM0:rwm"
      "--hostname=zwave-js-app"
      "--network=host"
    ];
  };
  systemd.services."podman-zwave-js-app" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    partOf = [
      "podman-compose-home-assistant-root.target"
    ];
    wantedBy = [
      "podman-compose-home-assistant-root.target"
    ];
  };

  # Networks
  systemd.services."podman-network-home-assistant_default" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStop = "podman network rm -f home-assistant_default";
    };
    script = ''
      podman network inspect home-assistant_default || podman network create home-assistant_default
    '';
    partOf = [ "podman-compose-home-assistant-root.target" ];
    wantedBy = [ "podman-compose-home-assistant-root.target" ];
  };

  # Root service
  # When started, this will automatically create all resources and start
  # the containers. When stopped, this will teardown all resources.
  systemd.targets."podman-compose-home-assistant-root" = {
    unitConfig = {
      Description = "Root target generated by compose2nix.";
    };
    wantedBy = [ "multi-user.target" ];
  };
}
