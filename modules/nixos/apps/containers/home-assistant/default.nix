{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkSharedContainerSettingsHostNetwork
    mkAppUnitNoNetwork
    mkRootUnit
    ;
  inherit (lib) mkIf optional optionals;
  homeAssistantVersion = config.sharedConfig.appVersions.homeAssistant.version;
  zwaveJsVersion = config.sharedConfig.appVersions.zWaveJs.version;
  mosquittoVersion = config.sharedConfig.appVersions.mosquitto.version;
  espHomeVersion = config.sharedConfig.appVersions.espHome.version;

  cfg = config.sharedConfig.webApps.home-assistant;

  name = "home-assistant";
  srvDir = "/srv/${name}";
  dataDir = "config";
  logRoot = "/var/log";

  backupConfig = {
    inherit name dataDir;
    dbType = "sqlite3";
    dbHost = "home-assistant_v2.db";
  };

  dbBackup = sharedConfig.lib.container-db-backup { inherit backupConfig; };

  proxyPassHass = cfg.proxyPass;
  proxyPassZwaveJS = cfg.zWaveJs.proxyPass;
  proxyPassMosquitto = cfg.mosquitto.proxyPass;
  proxyPassEspHome = cfg.espHome.proxyPass;

  zwaveEnabled = if (cfg ? zWaveJs.enable) then cfg.zWaveJs.enable else false;
  mosquittoEnabled = if (cfg ? mosquitto.enable) then cfg.mosquitto.enable else false;
  espHomeEnabled = if (cfg ? espHome.enable) then cfg.espHome.enable else false;

  mkNginxHost =
    { cfg, proxyPass }:
    sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
in
{
  imports = [ dbBackup ];

  config = mkIf (if (cfg ? enable) then cfg.enable else false) {
    virtualisation.oci-containers.containers = {
      "${name}" =
        {
          image = "homeassistant/home-assistant:${homeAssistantVersion}";
          hostname = "${name}-app";
          # ports = [ "${toString cfg.port}:8123" ];
          dependsOn = optional zwaveEnabled "zwave-js" ++ [
            "mosquitto"
          ];
        }
        // (mkSharedContainerSettingsHostNetwork cfg {
          volumes = [
            "/dev/rfkill:/dev/rfkill"
            "/run/dbus:/run/dbus:ro"
            "${srvDir}/${dataDir}:/config:rw"
          ];
          extraOptions = [
            "--privileged=true"
          ];
        });

      "zwave-js" = mkIf zwaveEnabled (
        {
          image = "zwavejs/zwave-js-ui:${zwaveJsVersion}";
          hostname = "zwave-js-app";
          # ports = [ "8091:8091" "3000:3000" ];
          dependsOn = optional mosquittoEnabled "mosquitto";
        }
        // (mkSharedContainerSettingsHostNetwork cfg.zWaveJs {
          volumes = [
            "${srvDir}/zwave-js:/usr/src/app/store:rw"
          ];
          extraOptions = [
            "--device=/dev/ttyACM0:/dev/ttyACM0:rwm"
            "--privileged=true"
          ];
        })
      );

      "mosquitto" = mkIf mosquittoEnabled (
        {
          image = "eclipse-mosquitto:${mosquittoVersion}";
          hostname = "mosquitto-app";
          # ports = [ "1883:1883" "9001:9001" ];
          dependsOn = optional espHomeEnabled "esp-home";
        }
        // (mkSharedContainerSettingsHostNetwork cfg.mosquitto {
          volumes = [
            "${srvDir}/mosquitto/config/:/mosquitto/config/:rw"
            "${logRoot}/mosquitto/log/:/mosquitto/log/:rw"
            "${srvDir}/mosquitto/data:/mosquitto/data/:rw"
            "/etc${srvDir}/mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf:ro"
          ];
        })
      );

      "esp-home" = mkIf espHomeEnabled (
        {
          image = "esphome/esphome:${espHomeVersion}";
          hostname = "esp-home-app";
          # ports = [ "6052:6052" "6123:6123" ];
        }
        // (mkSharedContainerSettingsHostNetwork cfg.espHome {
          volumes = [
            "${srvDir}/esphome:/config:rw"
            "/etc${srvDir}/esphome/relay.yaml:/mosquitto/config/relay.yaml:ro"
          ];
          extraOptions = [
            "--device=/dev/ttyUSB0:/dev/ttyUSB0:rwm"
          ];
        })
      );
    };

    system.activationScripts = {
      createHomeAssistantDataDir.text = ''
        mkdir -p "${srvDir}/${dataDir}"
        mkdir -p "${srvDir}/mosquitto/config"
        mkdir -p "${logRoot}/mosquitto/log"
        mkdir -p "${srvDir}/mosquitto/data"
        mkdir -p "${srvDir}/zwave-js"
        mkdir -p "${srvDir}/esphome"

        chmod 700 "${srvDir}/${dataDir}"
        chmod 700 "${srvDir}/mosquitto"
        chmod 700 "${srvDir}/mosquitto/config"
        chmod 700 "${logRoot}/mosquitto/log"
        chmod 700 "${srvDir}/mosquitto/data"
        chmod 700 "${srvDir}/zwave-js"
        chmod 700 "${srvDir}/esphome"

        chown -R root:root "${srvDir}"
      '';
    };

    environment.etc = {
      "srv/${name}/mosquitto/config/mosquitto.conf" = lib.mkIf (
        cfg.mosquitto.mosquittoConfigFile != null
      ) { source = cfg.mosquitto.mosquittoConfigFile; };
      "srv/${name}/esphome/relay.yaml" = lib.mkIf (cfg.espHome.relayConfigFile != null) {
        source = cfg.espHome.relayConfigFile;
      };
    };

    systemd.services."podman-${name}" = mkAppUnitNoNetwork name;
    systemd.services."podman-zwave-js" = mkIf zwaveEnabled (mkAppUnitNoNetwork name);
    systemd.services."podman-mosquitto" = mkIf mosquittoEnabled (mkAppUnitNoNetwork name);
    systemd.services."podman-esphome" = mkIf espHomeEnabled (mkAppUnitNoNetwork name);

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    networking.firewall.allowedTCPPorts = optionals (cfg.openFirewall) [
      cfg.port
      40000
      54547
      cfg.zWaveJs.port
      cfg.zWaveJs.port2
      cfg.espHome.port
      cfg.espHome.port2
      cfg.mosquitto.port
      cfg.mosquitto.port2
    ];

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts =
      (mkNginxHost {
        inherit cfg;
        proxyPass = proxyPassHass;
      })
      // (mkNginxHost {
        cfg = cfg.espHome;
        proxyPass = proxyPassEspHome;
      })
      // (mkNginxHost {
        cfg = cfg.zWaveJs;
        proxyPass = proxyPassZwaveJS;
      })
      // (mkNginxHost {
        cfg = cfg.mosquitto;
        proxyPass = proxyPassMosquitto;
      });
  };
}
