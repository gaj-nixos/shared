{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    ;
  name = "airsonic";
  wa = config.sharedConfig.webApps;
  cfg = wa.airsonic;
  version = config.sharedConfig.appVersions.airsonic-refix.version;
  port = cfg.port;
  serverUrl = cfg.musicServerUrl;
  proxyPass = cfg.proxyPass;

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    virtualisation.oci-containers.containers."${name}" =
      {
        image = "tamland/airsonic-refix:${version}";
        ports = [ "${toString port}:80/tcp" ];
      }
      // sharedContainerSettings {
        environment = {
          "SERVER_URL" = serverUrl;
        };
        extraOptions = [
          "--network-alias=${name}"
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
