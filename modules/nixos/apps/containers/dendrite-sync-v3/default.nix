{
  sharedConfig,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkDbDumperUnit
    mkPostGresDumperContainerEnv
    mkSharedContainerSettings
    mkContainerUser
    ;
  wa = config.sharedConfig.webApps;
  cfg = wa.sync-v3;
  cfgDendrite = wa.dendrite;
  version = config.sharedConfig.appVersions.sync-v3.version;
  inherit (cfg) port proxyPass;

  proxyPassWithoutTrailingSlash = builtins.substring 0 (
    builtins.stringLength proxyPass - 1
  ) proxyPass;

  name = "syncV3";
  backupPath = "/srv/backups/${name}/db";
  dataPath = "/srv/${name}";
  dbPath = "${dataPath}/db";

  POSTGRES_DB = "sync-v3";
  POSTGRES_USER = "sync-v3";
  POSTGRES_HOST = "postgres";

  inherit
    (sharedConfig.lib.make-agenix-container-environment-file-sync-v3 {
      inherit config lib POSTGRES_USER;
    })
    secrets
    envSecretPath
    ;

  sEnv = (
    sharedConfig.lib.make-agenix-container-environment-file {
      inherit config lib;
      secretName = "syncV3SecretDb";
      envSecretName = "syncV3SecretDbEnv";
      envKeyName = "POSTGRES_PASSWORD";
    }
  );

  secrets2 = sEnv.secrets;
  envSecretPathDbPwd = sEnv.envSecretPath;

  sharedContainerSettings = mkSharedContainerSettings name cfg;

  inherit (config.sharedConfig.network) publicHost;

  geoIpConfig = sharedConfig.lib.geo-ip-config { inherit config; };
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = secrets // secrets2;

    users = mkContainerUser name;

    virtualisation.oci-containers.containers."${name}" =
      {
        image = "ghcr.io/matrix-org/sliding-sync:v${version}";
        #user = "${name}:${name}";
        ports = [
          "${toString port}:8008/tcp"
        ];
        dependsOn = [
          "${name}-postgres"
        ];
        environmentFiles = [ envSecretPath ];
      }
      // sharedContainerSettings {
        environment = {
          "SYNCV3_SERVER" = "https://${cfgDendrite.externalHost}";
        };
        extraOptions = [
          "--hostname=${name}"
          "--network-alias=${name}"
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-postgres" =
      {
        image = "postgres:15-alpine";
        #user = "${name}:${name}";
        environmentFiles = [ envSecretPathDbPwd ];
      }
      // sharedContainerSettings {
        environment = {
          "POSTGRES_DATABASE" = POSTGRES_DB;
          "POSTGRES_USER" = POSTGRES_USER;
        };
        volumes = [
          "${dbPath}:/var/lib/postgresql/data:rw"
        ];
        extraOptions = [
          "--health-cmd=pg_isready -U ${POSTGRES_USER}"
          "--health-interval=5s"
          "--health-retries=5"
          "--health-timeout=5s"
          "--hostname=${name}-postgres"
          "--network-alias=${name}-postgres"
        ];
      };

    systemd.services."podman-${name}-postgres" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_db_dumper" =
      mkPostGresDumperContainerEnv name POSTGRES_HOST POSTGRES_USER envSecretPathDbPwd POSTGRES_DB
        backupPath
        cfg;

    systemd.services."podman-${name}_db_dumper" = mkDbDumperUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createSyncV3DataDir.text = ''
        mkdir -p "${dbPath}"
        mkdir -p "${backupPath}"

        chown -R ${name}:${name} "${dbPath}"
        chown -R ${name}:${name} "${backupPath}"

        chmod -R 777 "${dbPath}"
        # chmod -R 777 "${backupPath}"
      '';
    };

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    services.nginx.virtualHosts =
      {
        "${cfg.externalHost}" = lib.mkIf (cfg.externalHost != "") {
          forceSSL = true;
          enableACME = true;
          locations = {
          };
        };
      #}
      #// {
      #  "${publicHost}" = lib.mkIf (cfg.externalHost != "") {
      #    locations = {
      #      "~ ^/(client/|_matrix/client/unstable/org.matrix.msc3575/sync)" = {
      #        proxyPass = proxyPassWithoutTrailingSlash;
      #        extraConfig = ''
      #          proxy_set_header X-Forwarded-For $remote_addr;
      #          proxy_set_header X-Forwarded-Proto $scheme;
      #          proxy_set_header Host $host;
      #           ${geoIpConfig}
      #        '';
      #      };
      #    };
      #  };
      };
  };
}
