{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    ;
  inherit (lib) mkIf optional;
  version = config.sharedConfig.appVersions.vaultwarden.version;

  cfg = config.sharedConfig.webApps.vaultwarden;
  smtpHost = config.sharedConfig.services.smtpHost;

  name = "vaultwarden";
  srvDir = "/srv/${name}";
  dataDir = "data";
  logDir = "/var/log/${name}";
  logFile = "${name}.log";

  backupConfig = {
    inherit name dataDir;
    dbType = "sqlite3";
    dbHost = "db.sqlite3";
  };

  dbBackup = sharedConfig.lib.container-db-backup { inherit backupConfig; };

  proxyPass = cfg.proxyPass;

  sharedContainerSettings = mkSharedContainerSettings name cfg;

  jail = filter: {
    settings = {
      inherit filter;
      logpath = "${logDir}/*.log";
      backend = "polling";
      journalmatch = "";
      port = "http,https";
      action = ''iptables-multiport[name=HTTP, port="http,https"]'';
    };
  };
in
{
  imports = [ dbBackup ];

  config = mkIf (if cfg ? enable then cfg.enable else false) {

    virtualisation.oci-containers.containers."${name}" =
      {
        image = "vaultwarden/server:${version}";
        hostname = "${name}-app";
        ports = [ "${toString cfg.port}:80" ];
      }
      // sharedContainerSettings {
        volumes = [
          "${srvDir}/${dataDir}:/data"
          "${srvDir}/${name}.env:/.env"
          "${logDir}/${logFile}:/${logFile}"
        ];
        environment = {
          # See https://github.com/dani-garcia/vaultwarden/blob/main/.env.template
          WEBSOCKET_ENABLED = "true";
          INVITATIONS_ALLOWED = "true";
          SENDS_ALLOWED = "true";
          SIGNUPS_ALLOWED = toString cfg.allowSignups;
          ROCKET_ADDRESS = "0.0.0.0";
          ROCKET_PORT = "80";
          #LOG_LEVEL = "info";
          #ROCKET_LOG = "critical";
          LOG_FILE = "/${logFile}";
          SMTP_HOST = smtpHost;
          SMTP_PORT = "587";
          SMTP_SECURITY = "starttls";
          SMTP_FROM_NAME = cfg.orgName;
          SMTP_ACCEPT_INVALID_CERTS = "true";
          INVITATION_ORG_NAME = cfg.orgName;
        };
        extraOptions = [
          "--name=${name}-app"
          "--network-alias=${name}"
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createVaultwardenDataDir.text = ''
        mkdir -p "${srvDir}/${dataDir}"
        chown -R root:root "${srvDir}"
        mkdir -p "${logDir}"
        chown -R root:root "${logDir}"
        touch "${logDir}/${logFile}"
      '';
    };

    networking.firewall.allowedTCPPorts = optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };

    services.fail2ban.jails = lib.mkIf config.sharedConfig.network.fail2ban.enable {
      bitwarden = jail "bitwarden";
    };
  };
}
