{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:
# Update procedure:
# cd modules/nixos/apps/containers/paperless-ngx/sources
# nix run github:aksiksi/compose2nix -- --runtime podman --project paperless-ngx
# Doc: https://docs.paperless-ngx.com/setup/#docker
# First time setup:
# sudo podman exec -it paperless-ngx-webserver /bin/bash
# python3 manage.py createsuperuser
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkDbDumperUnit
    mkPostGresDumperContainer
    mkSharedContainerSettings
    ;
  wa = config.sharedConfig.webApps;
  cfg = wa.paperless-ngx;
  version = config.sharedConfig.appVersions.paperless-ngx.version;
  tikaVersion = config.sharedConfig.appVersions.tika.version;
  port = cfg.port;
  proxyPass = cfg.proxyPass;
  subUrl = cfg.subUrl;

  name = "paperless-ngx";
  backupPath = "/srv/backups/${name}/db";

  POSTGRES_DB = "paperless";
  POSTGRES_PASSWORD = "paperless";
  POSTGRES_USER = "paperless";
  POSTGRES_HOST = "${name}-db";

  inherit
    (sharedConfig.lib.make-agenix-container-environment-file {
      inherit config lib;
      secretName = "paperlessSecretKey";
      envSecretName = "paperlessSecretEnv";
      envKeyName = "PAPERLESS_SECRET_KEY";
    })
    secrets
    envSecretPath
    ;

  sharedContainerSettings = mkSharedContainerSettings name cfg;

in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = secrets;

    virtualisation.oci-containers.containers."${name}-broker" =
      {
        image = "docker.io/library/redis:7";
      }
      // sharedContainerSettings {
        volumes = [
          "/srv/${name}/redisdata:/data:rw"
        ];
        extraOptions = [
          "--network-alias=broker"
        ];
      };
    systemd.services."podman-${name}-broker" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-db" =
      {
        image = "docker.io/library/postgres:16";
        hostname = "${POSTGRES_HOST}";
      }
      // sharedContainerSettings {
        volumes = [
          "/srv/${name}/pgdata:/var/lib/postgresql/data:rw"
        ];
        environment = {
          "POSTGRES_DB" = POSTGRES_DB;
          "POSTGRES_PASSWORD" = POSTGRES_PASSWORD;
          "POSTGRES_USER" = POSTGRES_USER;
        };
        extraOptions = [
          "--health-cmd=pg_isready -U ${POSTGRES_USER}"
          "--health-interval=5s"
          "--health-retries=5"
          "--health-timeout=5s"
          "--network-alias=db"
        ];
      };
    systemd.services."podman-${name}-db" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-gotenberg" =
      {
        image = "docker.io/gotenberg/gotenberg:8.7";
        cmd = [
          "gotenberg"
          "--chromium-disable-javascript=true"
          "--chromium-allow-list=file:///tmp/.*"
        ];
      }
      // sharedContainerSettings {
        extraOptions = [
          "--network-alias=gotenberg"
        ];
      };
    systemd.services."podman-${name}-gotenberg" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-tika" =
      {
        image = "docker.io/apache/tika:${tikaVersion}";
      }
      // sharedContainerSettings {
        extraOptions = [
          "--network-alias=tika"
        ];
      };
    systemd.services."podman-${name}-tika" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-webserver" =
      {
        image = "ghcr.io/paperless-ngx/paperless-ngx:${version}";
        ports = [ "${toString port}:8000/tcp" ];
        user = "1000:1000";
        environmentFiles = [ envSecretPath ];
        dependsOn = [
          "${name}-broker"
          "${name}-db"
          "${name}-gotenberg"
          "${name}-tika"
        ];
      }
      // sharedContainerSettings {
        volumes = [
          "/srv/${name}/consume:/usr/src/paperless/consume:rw"
          "/srv/${name}/data:/usr/src/paperless/data:rw"
          "/srv/${name}/export:/usr/src/paperless/export:rw"
          "/srv/${name}/media:/usr/src/paperless/media:rw"
        ];
        environment = {
          "PAPERLESS_DBHOST" = "${POSTGRES_HOST}";
          "PAPERLESS_OCR_LANGUAGE" = "fra";
          "PAPERLESS_OCR_LANGUAGES" = "fra eng";
          "PAPERLESS_REDIS" = "redis://broker:6379";
          "PAPERLESS_TIKA_ENABLED" = "1";
          "PAPERLESS_TIKA_ENDPOINT" = "http://tika:9998";
          "PAPERLESS_TIKA_GOTENBERG_ENDPOINT" = "http://gotenberg:3000";
          "PAPERLESS_TIME_ZONE" = config.time.timeZone;
          "PAPERLESS_URL" =
            if cfg.externalHost != "" then "https://${cfg.externalHost}" else "https://${cfg.mainInternalHost}";
          "PAPERLESS_ALLOWED_HOSTS" =
            if cfg.externalHost != "" then
              "https://${cfg.externalHost},https://${cfg.mainInternalHost}"
            else
              "https://${cfg.mainInternalHost}";
          "USERMAP_GID" = "1000";
          "USERMAP_UID" = "1000";
          "PAPERLESS_FORCE_SCRIPT_NAME" = subUrl;
          "PAPERLESS_STATIC_URL" = "${subUrl}/static/";
        };
        extraOptions = [
          "--network-alias=webserver"
        ];
      };
    systemd.services."podman-${name}-webserver" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_db_dumper" =
      mkPostGresDumperContainer name POSTGRES_HOST POSTGRES_USER POSTGRES_PASSWORD POSTGRES_DB backupPath
        cfg;

    systemd.services."podman-${name}_db_dumper" = mkDbDumperUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createPaperlessDataDir.text = ''
        mkdir -p "/srv/${name}/redisdata"
        mkdir -p "/srv/${name}/pgdata"
        mkdir -p "/srv/${name}/consume"
        mkdir -p "/srv/${name}/data"
        mkdir -p "/srv/${name}/export"
        mkdir -p "/srv/${name}/media"

        chown -R 1000:1000 "/srv/${name}"
        chown -R 999:0 "/srv/${name}/redisdata"
        chown -R 999:0 "/srv/${name}/pgdata"
        mkdir -p "${backupPath}"
      '';
    };

    services.samba = {
      settings = {
        Paperless = {
          path = "/srv/${name}/consume";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
        };
      };
    };

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        subUrl
        ;
    };
  };
}
