# Auto-generated using compose2nix v0.3.2-pre.
{
  pkgs,
  lib,
  ...
}:
{
  virtualisation.oci-containers.containers."paperless-ngx-broker" = {
    image = "docker.io/library/redis:7";
    volumes = [
      "/srv/paperless/redisdata:/data:rw"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=broker"
      "--network=paperless-ngx_default"
    ];
  };
  systemd.services."podman-paperless-ngx-broker" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-paperless-ngx_default.service"
    ];
    requires = [
      "podman-network-paperless-ngx_default.service"
    ];
    partOf = [
      "podman-compose-paperless-ngx-root.target"
    ];
    wantedBy = [
      "podman-compose-paperless-ngx-root.target"
    ];
  };
  virtualisation.oci-containers.containers."paperless-ngx-db" = {
    image = "docker.io/library/postgres:16";
    environment = {
      "POSTGRES_DB" = "paperless";
      "POSTGRES_PASSWORD" = "paperless";
      "POSTGRES_USER" = "paperless";
    };
    volumes = [
      "/srv/paperless/pgdata:/var/lib/postgresql/data:rw"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=db"
      "--network=paperless-ngx_default"
    ];
  };
  systemd.services."podman-paperless-ngx-db" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-paperless-ngx_default.service"
    ];
    requires = [
      "podman-network-paperless-ngx_default.service"
    ];
    partOf = [
      "podman-compose-paperless-ngx-root.target"
    ];
    wantedBy = [
      "podman-compose-paperless-ngx-root.target"
    ];
  };
  virtualisation.oci-containers.containers."paperless-ngx-gotenberg" = {
    image = "docker.io/gotenberg/gotenberg:8.7";
    cmd = [
      "gotenberg"
      "--chromium-disable-javascript=true"
      "--chromium-allow-list=file:///tmp/.*"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=gotenberg"
      "--network=paperless-ngx_default"
    ];
  };
  systemd.services."podman-paperless-ngx-gotenberg" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-paperless-ngx_default.service"
    ];
    requires = [
      "podman-network-paperless-ngx_default.service"
    ];
    partOf = [
      "podman-compose-paperless-ngx-root.target"
    ];
    wantedBy = [
      "podman-compose-paperless-ngx-root.target"
    ];
  };
  virtualisation.oci-containers.containers."paperless-ngx-tika" = {
    image = "docker.io/apache/tika:latest";
    log-driver = "journald";
    extraOptions = [
      "--network-alias=tika"
      "--network=paperless-ngx_default"
    ];
  };
  systemd.services."podman-paperless-ngx-tika" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-paperless-ngx_default.service"
    ];
    requires = [
      "podman-network-paperless-ngx_default.service"
    ];
    partOf = [
      "podman-compose-paperless-ngx-root.target"
    ];
    wantedBy = [
      "podman-compose-paperless-ngx-root.target"
    ];
  };
  virtualisation.oci-containers.containers."paperless-ngx-webserver" = {
    image = "ghcr.io/paperless-ngx/paperless-ngx:latest";
    environment = {
      "PAPERLESS_DBHOST" = "db";
      "PAPERLESS_OCR_LANGUAGE" = "fra";
      "PAPERLESS_OCR_LANGUAGES" = "fra eng";
      "PAPERLESS_REDIS" = "redis://broker:6379";
      "PAPERLESS_SECRET_KEY" = "change-me";
      "PAPERLESS_TIKA_ENABLED" = "1";
      "PAPERLESS_TIKA_ENDPOINT" = "http://tika:9998";
      "PAPERLESS_TIKA_GOTENBERG_ENDPOINT" = "http://gotenberg:3000";
      "PAPERLESS_TIME_ZONE" = "Europe/Paris";
      "PAPERLESS_URL" = "https://paperless.example.com";
      "USERMAP_GID" = "1000";
      "USERMAP_UID" = "1000";
    };
    volumes = [
      "/srv/paperless/consume:/usr/src/paperless/consume:rw"
      "/srv/paperless/data:/usr/src/paperless/data:rw"
      "/srv/paperless/export:/usr/src/paperless/export:rw"
      "/srv/paperless/media:/usr/src/paperless/media:rw"
    ];
    ports = [
      "8089:8000/tcp"
    ];
    dependsOn = [
      "paperless-ngx-broker"
      "paperless-ngx-db"
      "paperless-ngx-gotenberg"
      "paperless-ngx-tika"
    ];
    log-driver = "journald";
    extraOptions = [
      "--network-alias=webserver"
      "--network=paperless-ngx_default"
    ];
  };
  systemd.services."podman-paperless-ngx-webserver" = {
    serviceConfig = {
      Restart = lib.mkOverride 90 "always";
    };
    after = [
      "podman-network-paperless-ngx_default.service"
    ];
    requires = [
      "podman-network-paperless-ngx_default.service"
    ];
    partOf = [
      "podman-compose-paperless-ngx-root.target"
    ];
    wantedBy = [
      "podman-compose-paperless-ngx-root.target"
    ];
  };

  # Networks
  systemd.services."podman-network-paperless-ngx_default" = {
    path = [ pkgs.podman ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStop = "podman network rm -f paperless-ngx_default";
    };
    script = ''
      podman network inspect paperless-ngx_default || podman network create paperless-ngx_default
    '';
    partOf = [ "podman-compose-paperless-ngx-root.target" ];
    wantedBy = [ "podman-compose-paperless-ngx-root.target" ];
  };

  # Root service
  # When started, this will automatically create all resources and start
  # the containers. When stopped, this will teardown all resources.
  systemd.targets."podman-compose-paperless-ngx-root" = {
    unitConfig = {
      Description = "Root target generated by compose2nix.";
    };
    wantedBy = [ "multi-user.target" ];
  };
}
