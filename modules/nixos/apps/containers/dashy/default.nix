{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  inherit (lib) mkIf optional;
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    ;
  dashyVersion = config.sharedConfig.appVersions.dashy.version;

  cfg = config.sharedConfig.webApps.dashy;

  name = "dashy";
  dataDir = "/srv/${name}";
  backupsSubDir = "config-backups";
  configFile = "config.yml";

  proxyPass = cfg.proxyPass;

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    virtualisation.oci-containers.containers."${name}" =
      {
        image = "lissy93/dashy:${dashyVersion}";
        hostname = "${name}-app";
        ports = [ "${toString cfg.port}:8080" ];
      }
      // sharedContainerSettings {
        volumes = [
          "/etc${dataDir}/${configFile}:/app/user-data/conf.yml:ro"
          "${dataDir}/${backupsSubDir}:/app/user-data/config-backups:rw"
        ];
        environment = {
          NODE_ENV = "production";
        };
        extraOptions = [
          "--name=${name}-app"
          "--network-alias=${name}"
          "--health-cmd=[\"node\", \"/app/services/healthcheck\"]"
          "--health-start-period=5m0s"
          "--health-startup-interval=1m30s"
          "--health-interval=1m30s" # Check every 1m30 seconds
          "--health-retries=3" # Retry 3 times before marking as unhealthy
          "--health-timeout=10s" # Timeout for each health check attempt
        ];
      };

    systemd.services."podman-${name}" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    environment.etc."srv/${name}/${configFile}".source = (pkgs.formats.yaml { }).generate "${
      name
    }" cfg.configuration;

    system.activationScripts = {
      createDashyDataDir.text = ''
        mkdir -p "${dataDir}/${backupsSubDir}"
        chown -R root:root "${dataDir}"
        chmod 700 "${dataDir}"
        chmod 700 "${dataDir}/${backupsSubDir}"
        #chmod 600 "${dataDir}/${configFile}"
      '';
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    networking.firewall.allowedTCPPorts = optional (cfg.openFirewall) cfg.port;

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
