{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:

let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkSharedContainerSettings
    ;
  wa = config.sharedConfig.webApps;
  cfg = wa.wallabag;
  version = config.sharedConfig.appVersions.wallabag.version;
  port = cfg.port;
  proxyPass = cfg.proxyPass;
  subUrl = cfg.subUrl;

  name = "wallabag";
  backupPath = "/srv/backups/${name}/db";
  domainName =
    if cfg.externalHost != "" then "https://${cfg.externalHost}" else "https://${cfg.mainInternalHost}";
  smtpHost = config.sharedConfig.services.smtpHost;

  sharedContainerSettings = mkSharedContainerSettings name cfg;

in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    virtualisation.oci-containers.containers."${name}-${name}" =
      {
        image = "wallabag/wallabag:${version}";
        ports = [ "${toString port}:80/tcp" ];
        #user = "1000:1000";
        dependsOn = [
          "${name}-db"
          "${name}-redis"
        ];
      }
      // sharedContainerSettings {
        volumes = [
          "/srv/${name}/images:/var/www/wallabag/web/assets/images:rw"
        ];
        environment = {
          "MYSQL_ROOT_PASSWORD" = "wallaroot";
          "SYMFONY__ENV__DATABASE_CHARSET" = "utf8mb4";
          "SYMFONY__ENV__DATABASE_DRIVER" = "pdo_mysql";
          "SYMFONY__ENV__DATABASE_HOST" = "db";
          "SYMFONY__ENV__DATABASE_NAME" = "wallabag";
          "SYMFONY__ENV__DATABASE_PASSWORD" = "wallapass";
          "SYMFONY__ENV__DATABASE_PORT" = "3306";
          "SYMFONY__ENV__DATABASE_TABLE_PREFIX" = "\"wallabag_\"";
          "SYMFONY__ENV__DATABASE_USER" = "wallabag";
          "SYMFONY__ENV__DOMAIN_NAME" = domainName;
          "SYMFONY__ENV__FROM_EMAIL" = "wallabag@${domainName}";
          "SYMFONY__ENV__MAILER_DSN" = smtpHost;
          "SYMFONY__ENV__SERVER_NAME" = "\"Your ${name} instance\"";
        };
        extraOptions = [
          "--health-cmd=[\"wget\", \"--no-verbose\", \"--tries=1\", \"--spider\", \"http://localhost/api/info\"]"
          "--health-interval=1m0s"
          "--health-timeout=3s"
          "--network-alias=${name}"
        ];
      };
    systemd.services."podman-${name}-${name}" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-db" =
      {
        image = "mariadb";
      }
      // sharedContainerSettings {
        volumes = [
          "/srv/${name}/data:/var/lib/mysql:rw"
        ];
        environment = {
          "MYSQL_ROOT_PASSWORD" = "wallaroot";
        };
        extraOptions = [
          #"--health-cmd=[\"mysqladmin\", \"ping\", \"-h\", \"localhost\"]"
          #"--health-interval=20s"
          #"--health-timeout=3s"
          "--network-alias=db"
        ];
      };
    systemd.services."podman-${name}-db" = mkAppUnit name;
    virtualisation.oci-containers.containers."${name}-redis" =
      {
        image = "redis:alpine";
      }
      // sharedContainerSettings {
        extraOptions = [
          "--health-cmd=[\"redis-cli\", \"ping\"]"
          "--health-interval=20s"
          "--health-timeout=3s"
          "--network-alias=redis"
        ];
      };
    systemd.services."podman-${name}-redis" = mkAppUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createWallabagDataDir.text = ''
        mkdir -p "/srv/${name}/images"
        mkdir -p "/srv/${name}/data"

        #chown -R 1000:1000 "/srv/${name}"
        mkdir -p "${backupPath}"
      '';
    };

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        subUrl
        ;
    };
  };
}
