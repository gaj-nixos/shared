{
  sharedConfig,
  pkgs,
  config,
  lib,
  ...
}:
let
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkAppUnit
    mkRootUnit
    mkNetworkUnit
    mkDbDumperUnit
    mkPostGresDumperContainer
    mkSharedContainerSettings
    mkContainerUser
    ;
  cfg = config.sharedConfig.webApps.immich;

  IMMICH_VERSION = config.sharedConfig.appVersions.immich.version;

  # see environment variables at https://immich.app/docs/install/environment-variables
  CACHE_LOCATION = cfg.cacheLocation;
  UPLOAD_LOCATION = cfg.libraryLocation;
  DB_DATA_LOCATION = cfg.dbDataLocation;
  POSTGRES_DB = cfg.dbDatabaseName;
  POSTGRES_USER = cfg.dbUserName;
  POSTGRES_PASSWORD = cfg.dbPassword;
  IMMICH_CONFIG_FILE = cfg.configFile;
  POSTGRES_HOST = "database";

  name = "immich";
  #logDir = "/var/log/${name}";
  #logFile = "${name}.log";

  backupPath = "/srv/backups/${name}/db";

  proxyPass = cfg.proxyPass;

  extraConfig = ''
    client_max_body_size 0;
    proxy_buffering off;                # Prevent buffering on large files
    proxy_request_buffering off;        # Pass request without waiting for full body
    proxy_connect_timeout 120s;
    proxy_send_timeout 600s;
    proxy_read_timeout 600s;
    send_timeout 600s;
    chunked_transfer_encoding on;
    gzip_buffers 32 4k;                 # try to fix gzip errors in nginx logs
  '';

  sharedContainerSettings = mkSharedContainerSettings name cfg;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {

    users = mkContainerUser name;

    virtualisation.oci-containers.containers."${name}_server" =
      {
        image = "ghcr.io/immich-app/immich-server:${IMMICH_VERSION}";
        hostname = "${name}-server";
        ports = [ "${toString cfg.port}:2283/tcp" ];
        dependsOn = [
          "${name}-db"
          "${name}_redis"
        ];
      }
      // sharedContainerSettings {
        volumes = [
          "${UPLOAD_LOCATION}:/usr/src/app/upload:rw"
        ];
        environment = {
          DB_DATABASE_NAME = "${POSTGRES_DB}";
          DB_USERNAME = "${POSTGRES_USER}";
          DB_PASSWORD = "${POSTGRES_PASSWORD}";
          IMMICH_CONFIG_FILE = "${IMMICH_CONFIG_FILE}";
        };
        extraOptions = [
          "--network-alias=${name}-server"
        ];
      };
    systemd.services."podman-${name}_server" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_machine_learning" =
      {
        image = "ghcr.io/immich-app/immich-machine-learning:${IMMICH_VERSION}";
        hostname = "${name}-machine-learning";
      }
      // sharedContainerSettings {
        volumes = [
          "${CACHE_LOCATION}:/cache:rw"
        ];
        environment = {
          DB_DATABASE_NAME = "${POSTGRES_DB}";
          DB_USERNAME = "${POSTGRES_USER}";
          DB_PASSWORD = "${POSTGRES_PASSWORD}";
          IMMICH_CONFIG_FILE = "${IMMICH_CONFIG_FILE}";
        };
        extraOptions = [
          "--network-alias=${name}-machine-learning"
        ];
      };
    systemd.services."podman-${name}_machine_learning" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}-db" =
      {
        image = "docker.io/tensorchord/pgvecto-rs:pg14-v0.2.0@sha256:90724186f0a3517cf6914295b5ab410db9ce23190a2d9d0b9dd6463e3fa298f0";
        hostname = "${POSTGRES_HOST}";
        cmd = [
          "postgres"
          "-c"
          "shared_preload_libraries=vectors.so"
          "-c"
          "search_path=\"$user\", public, vectors"
          "-c"
          "logging_collector=on"
          "-c"
          "max_wal_size=2GB"
          "-c"
          "shared_buffers=512MB"
          "-c"
          "wal_compression=on"
        ];
      }
      // sharedContainerSettings {
        volumes = [
          "${DB_DATA_LOCATION}:/var/lib/postgresql/data:rw"
        ];
        environment = {
          POSTGRES_DB = "${POSTGRES_DB}";
          POSTGRES_INITDB_ARGS = "--data-checksums";
          POSTGRES_PASSWORD = "${POSTGRES_PASSWORD}";
          POSTGRES_USER = "${POSTGRES_USER}";
        };
        extraOptions = [
          "--health-cmd=pg_isready --dbname='${POSTGRES_DB}' --username='${POSTGRES_USER}' || exit 1; Chksum=\"$(psql --dbname='${POSTGRES_DB}' --username='${POSTGRES_USER}' --tuples-only --no-align --command='SELECT COALESCE(SUM(checksum_failures), 0) FROM pg_stat_database')\"; echo \"checksum failure count is $Chksum\"; [ \"$Chksum\" = '0' ] || exit 1"
          "--health-interval=5m0s"
          "--health-start-period=5m0s"
          "--health-startup-interval=30s"
          "--network-alias=database"
        ];
      };
    systemd.services."podman-${name}-db" = mkAppUnit name;

    virtualisation.oci-containers.containers."${name}_redis" =
      {
        image = "docker.io/redis:6.2-alpine@sha256:eaba718fecd1196d88533de7ba49bf903ad33664a92debb24660a922ecd9cac8";
        hostname = "redis";
      }
      // sharedContainerSettings {
        environment = {
          DB_DATABASE_NAME = "${POSTGRES_DB}";
          DB_USERNAME = "${POSTGRES_USER}";
          DB_PASSWORD = "${POSTGRES_PASSWORD}";
        };
        extraOptions = [
          "--health-cmd=redis-cli ping || exit 1"
          "--network-alias=redis"
        ];
      };
    systemd.services."podman-${name}_redis" = mkAppUnit name;

    # see https://immich.app/docs/administration/backup-and-restore
    virtualisation.oci-containers.containers."${name}_db_dumper" =
      mkPostGresDumperContainer name POSTGRES_HOST POSTGRES_USER POSTGRES_PASSWORD POSTGRES_DB backupPath
        cfg;
    systemd.services."podman-${name}_db_dumper" = mkDbDumperUnit name;

    # Networks
    systemd.services."podman-network-${name}_default" = mkNetworkUnit name;

    systemd.targets."podman-compose-${name}-root" = mkRootUnit name;

    system.activationScripts = {
      createImmichDataDir.text = ''
        mkdir -p "${cfg.cacheLocation}"
        mkdir -p "${cfg.libraryLocation}"
        mkdir -p "${cfg.dbDataLocation}"
        mkdir -p "${backupPath}"

        chown -R immich:immich "${cfg.cacheLocation}"
        chown -R immich:immich "${cfg.libraryLocation}"
        chown -R 999:999 "${cfg.dbDataLocation}"
        chmod -R 700 "${cfg.dbDataLocation}"
        chown -R immich:immich "${backupPath}"
      '';
    };

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        extraConfig
        ;
    };
  };
}
