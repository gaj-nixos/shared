{
  lib,
  config,
  pkgs,
  ...
}:

let
  cfg = config.sharedConfig.webApps.jupyter;
in
{
  config = lib.mkIf cfg.enable {
    services.jupyterhub = {
      enable = true;
      port = cfg.port;
      jupyterhubEnv = pkgs.python3.withPackages (
        p: with p; [
          jupyterhub
          jupyterhub-systemdspawner
        ]
      );
      jupyterlabEnv = pkgs.python3.withPackages (
        p: with p; [
          jupyterhub
          jupyterlab
        ]
      );
      kernels = {
        python3 =
          let
            env = pkgs.python3.withPackages (
              p: with p; [
                ipykernel
                pandas
                scikit-learn
              ]
            );
          in
          {
            displayName = "Python 3 for machine learning";
            argv = [
              "${env.interpreter}"
              "-m"
              "ipykernel_launcher"
              "-f"
              "{connection_file}"
            ];
            language = "python";
            logo32 = "${env}/${env.sitePackages}/ipykernel/resources/logo-32x32.png";
            logo64 = "${env}/${env.sitePackages}/ipykernel/resources/logo-64x64.png";
          };
        #python3-2 = let
        #  env = pkgs.python3.withPackages (p: with p; [
        #    ipykernel
        #    pandas
        #    scikit-learn
        #  ]);
        #in {
        #  displayName = "Python 3 for machine learning (2)";
        #  argv = [
        #    "${env.interpreter}"
        #    "-m"
        #    "ipykernel_launcher"
        #    "-f"
        #    "{connection_file}"
        #  ];
        #  language = "python";
        #  logo32 = "${env}/${env.sitePackages}/ipykernel/resources/logo-32x32.png";
        #  logo64 = "${env}/${env.sitePackages}/ipykernel/resources/logo-64x64.png";
        #};
      };
    };

    environment.systemPackages = with pkgs; [
      python312Packages.jupyterhub
      python312Packages.jupyterhub-systemdspawner
      # nginx
    ];

    networking.firewall.allowedTCPPorts = lib.optional cfg.openFirewall cfg.port;
  };
}
