{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:

let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.webApps.crowdsec;
  proxyPass = cfg.proxyPass;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = {
      "crowdsec-enroll-key" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/crowdsec-enroll-key.age";
        owner = "crowdsec";
        group = "crowdsec";
        generator.script = (
          sharedConfig.lib.make-agenix-generator-prompt {
            prompts = [
              "Configuring crowdsec"
              "Please enter the crowdsec enrollment key"
            ];
          }
        );
      };
    };

    services.crowdsec-firewall-bouncer = {
      enable = true;
      settings = {
        #api_key = "<api-key>";
        api_url = "http://localhost:${toString cfg.port}";
      };
    };

    services.crowdsec =
      let
        yaml = (pkgs.formats.yaml { }).generate;
        acquisitions_file = yaml "acquisitions.yaml" {
          source = "journalctl";
          journalctl_filter = [ "_SYSTEMD_UNIT=sshd.service" ];
          labels.type = "syslog";
        };
      in
      {
        enable = true;
        enrollKeyFile = config.age.secrets.crowdsec-enroll-key.path;
        allowLocalJournalAccess = true;
        settings = {
          crowdsec_service.acquisition_path = acquisitions_file;
          api.server = {
            listen_uri = "127.0.0.1:${toString cfg.port}";
          };
        };
      };

    systemd.services.crowdsec.serviceConfig = {
      ExecStartPre =
        let
          script = pkgs.writeScriptBin "register-bouncer" ''
            #!${pkgs.runtimeShell}
            set -eu
            set -o pipefail

            if ! cscli bouncers list | grep -q "my-bouncer"; then
              cscli bouncers add "my-bouncer" --key "<api-key>"
            fi
          '';
        in
        [ "${script}/bin/register-bouncer" ];
    };

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
