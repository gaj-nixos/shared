{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.services.dockerRegistry;
  proxyPass = cfg.proxyPass;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    services.dockerRegistry = {
      enable = true;
      storagePath = "/var/lib/docker-registry";
      port = cfg.port;
      openFirewall = cfg.openFirewall;
      #  https://registry-1.docker.io
      extraConfig = {
        proxy = {
          remoteurl = config.sharedConfig.defaultRegistrySettings.registry;
          ttl = "168h";
          # username = config.sharedConfig.defaultRegistrySettings.username;
          # password = config.sharedConfig.defaultRegistrySettings.passwordFile;
        };
      };
      enableGarbageCollect = true;
      enableDelete = true;
    };

    virtualisation = {
      containers.registries.search = [
        "${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}"
      ];
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
