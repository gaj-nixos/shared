{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.webApps.gotify;
  proxyPass = cfg.proxyPass;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    environment.systemPackages = with pkgs; [
      gotify-server
      gotify-cli
    ];

    services.gotify = {
      enable = true;
      package = pkgs.gotify-server;
      environment = {
        GOTIFY_SERVER_PORT = cfg.port;
        GOTIFY_DATABASE_DIALECT = "sqlite3";
        GOTIFY_UPLOADEDIMAGESDIR = "data/images";
        GOTIFY_PLUGINSDIR = "data/plugins";
        GOTIFY_REGISTRATION = "${toString cfg.allowSignups}";
      };
    };

    networking.firewall.allowedTCPPorts = lib.optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
