{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.services.ollama;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    environment.systemPackages = with pkgs; [
      ollama
      oterm
    ];

    services.ollama = {
      package = pkgs.ollama;
      enable = true;
      acceleration = cfg.acceleration;
      host = "0.0.0.0";
      port = cfg.port;
      openFirewall = cfg.openFirewall;
    };
  };
}
