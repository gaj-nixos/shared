{
  #sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  cfg = config.sharedConfig.webApps.tabby;
in
#proxyPass = cfg.proxyPass;
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    environment.systemPackages = [ pkgs.cmake ];

    services.tabby = {
      enable = true;
      model = cfg.model;
      port = cfg.port;
      acceleration = cfg.acceleration;
      usageCollection = false;
      host = "0.0.0.0";
      package = pkgs.gaelj.tabby;
    };

    networking.firewall.allowedTCPPorts = [ cfg.port ];

    #services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {inherit sharedConfig config lib pkgs cfg proxyPass;};
  };
}
