{
  sharedConfig,
  config,
  pkgs,
  lib,
  ...
}:
let
  inherit (lib) mkIf;
  inherit (sharedConfig.lib.make-podman-services { inherit config; })
    mkRootUnit
    ;
  name = "changedetection-io";
  cfg = config.sharedConfig.webApps.changeDetectionIo;
  dataDir = "/var/lib/changedetection-io";
  proxyPass = cfg.proxyPass;
  baseUrl = if cfg.externalHost != "" then cfg.externalHost else cfg.mainInternalHost;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    services."${name}" = {
      enable = true;
      port = cfg.port;
      webDriverSupport = false; # Enable support for fetching web pages using WebDriver and Chromium. This starts a headless chromium controlled by puppeteer in an oci container. Playwright can currently leak memory
      playwrightSupport = false; # Enable support for fetching web pages using playwright and Chromium. This starts a headless Chromium controlled by puppeteer in an oci container. Playwright can currently leak memory
      #environmentFile = "/run/secrets/changedetection-io.env";
      datastorePath = dataDir;
      chromePort = cfg.chromePort;
      behindProxy = true;
      baseURL = "https://${baseUrl}";
    };

    # Root service
    # When started, this will automatically create all resources and start
    # the containers. When stopped, this will teardown all resources.
    systemd.services."${name}" = {
      after = [ "gotify-server.service" ];
    } // (mkRootUnit name);

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    networking.firewall.allowedTCPPorts = lib.optionals cfg.openFirewall [
      cfg.port
      cfg.chromePort
    ];

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
