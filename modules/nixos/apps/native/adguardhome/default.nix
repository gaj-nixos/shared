{
  sharedConfig,
  config,
  lib,
  pkgs,
  ...
}:
let
  # Docs: https://github.com/AdguardTeam/AdGuardHome/wiki/Configuration#configuration-file
  inherit (lib) mkIf;
  netCfg = config.sharedConfig.network;
  domain = netCfg.domain;
  hostName = config.networking.hostName;
  interface = netCfg.interface;

  dataDir = "/var/lib/AdGuardHome";

  cfg = netCfg.adGuardHome;

  proxyPass = cfg.proxyPass;
  isPublic = cfg.isPublic;
  tz = config.time.timeZone;

  udpPorts =
    [
      53 # DNS
      5353 # mDNS  (Multicast DNS)
    ]
    ++ (
      if cfg.dhcpServerEnabled then
        [
          67 # DHCP bootstrap Server
          68 # DHCP bootstrap Client
          546 # DHCP6 client
          547 # DHCP6 server
        ]
      else
        [ ]
    )
    ++ (
      if cfg.dnsOverHttps then
        [
          853
          784
        ]
      else
        [ ]
    );

  tcpPorts =
    [
      53 # DNS
      5353 # mDNS  (Multicast DNS)
    ]
    ++ (
      if cfg.dnsOverHttps then
        [
          853
          784
        ]
      else
        [ ]
    );

  filters = [
    {
      id = 1;
      enabled = true;
      name = "AdGuard DNS filter";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_1.txt";
    }
    {
      id = 2;
      enabled = true;
      name = "AdAway Default Blocklist";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_2.txt";
    }
    {
      id = 3;
      enabled = true;
      name = "Peter Lowe's Blocklist";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_3.txt";
    }
    {
      id = 4;
      enabled = true;
      name = "Dan Pollock's List";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_4.txt";
    }
    {
      id = 5;
      enabled = true;
      name = "OISD Blocklist Small";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_5.txt";
    }
    {
      id = 7;
      enabled = true;
      name = "Perflyst and Dandelion Sprout's Smart-TV Blocklist";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_7.txt";
    }
    {
      id = 9;
      enabled = true;
      name = "The Big List of Hacked Malware Web Sites";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_9.txt";
    }
    {
      id = 10;
      enabled = true;
      name = "Scam Blocklist by DurableNapkin";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_10.txt";
    }
    {
      id = 11;
      enabled = true;
      name = "Malicious URL Blocklist (URLHaus)";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_11.txt";
    }
    {
      id = 12;
      enabled = true;
      name = "Dandelion Sprout's Anti-Malware List";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_12.txt";
    }
    {
      id = 18;
      enabled = true;
      name = "Phishing Army";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_18.txt";
    }
    {
      id = 23;
      enabled = true;
      name = "WindowsSpyBlocker - Hosts spy rules";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_23.txt";
    }
    {
      id = 24;
      enabled = true;
      name = "1Hosts (Lite)";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_24.txt";
    }
    {
      id = 27;
      enabled = true;
      name = "OISD Blocklist Full";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_27.txt";
    }
    {
      id = 30;
      enabled = true;
      name = "Phishing URL Blocklist (PhishTank and OpenPhish)";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_30.txt";
    }
    {
      id = 31;
      enabled = true;
      name = "Stalkerware Indicators List";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_31.txt";
    }
    {
      id = 32;
      enabled = true;
      name = "The NoTracking blocklist";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_32.txt";
    }
    {
      id = 33;
      enabled = true;
      name = "Steven Black's List";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_33.txt";
    }
    {
      id = 34;
      enabled = true;
      name = "HaGeZi Multi NORMAL";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_34.txt";
    }
    {
      id = 38;
      enabled = true;
      name = "1Hosts (mini)";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_38.txt";
    }
    {
      id = 42;
      enabled = true;
      name = "ShadowWhisperer's Malware List";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_42.txt";
    }
    {
      id = 44;
      enabled = true;
      name = "HaGeZi's Threat Intelligence Feeds";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_44.txt";
    }
    {
      id = 50;
      enabled = true;
      name = "uBlock₀ filters – Badware risks";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_50.txt";
    }
    {
      id = 56;
      enabled = true;
      name = "HaGeZi's The World's Most Abused TLDs";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_56.txt";
    }
    {
      id = 60;
      enabled = true;
      name = "HaGeZi's Xiaomi Tracker Blocklist";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_60.txt";
    }
    {
      id = 61;
      enabled = true;
      name = "HaGeZi's Samsung Tracker Blocklist";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_61.txt";
    }
    {
      id = 62;
      enabled = true;
      name = "Ukrainian Security Filter";
      url = "https://adguardteam.github.io/HostlistsRegistry/assets/filter_62.txt";
    }
  ];
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    services.adguardhome = {
      enable = true;
      openFirewall = cfg.openFirewall;
      package = pkgs.adguardhome;
      port = cfg.port;
      allowDHCP = cfg.dhcpServerEnabled;
      mutableSettings = false;
      settings = {
        schema_version = 29;

        http = {
          address = "0.0.0.0:${toString cfg.port}";
          session_ttl = "720h"; # 30 days
          pprof = {
            enabled = false; # get profiling information with your browser, for example http://localhost:[PORT]/debug/pprof/goroutine?debug=2 will show the call trace of each running goroutine
            port = 6060;
          };
        };

        users = [
          {
            name = cfg.userName;
            password = cfg.userPassword;
          }
        ];

        auth_attempts = 5;
        block_auth_min = 15;
        http_proxy = "";
        #language = "en";
        #theme = "dark";
        #debug_pprof = false;

        dns = {
          bind_hosts = [
            "0.0.0.0"
          ];
          port = 53;
          anonymize_client_ip = false;

          # Anti-DNS amplification features
          ratelimit = if isPublic then 200 else 0;
          #ratelimit_subnet_len_ipv4 = 24;
          #ratelimit_subnet_len_ipv6 = 56;
          ratelimit_whitelist = [ ];
          refuse_any = isPublic;

          # Upstream DNS servers settings
          upstream_dns = cfg.upstreamDnsServers;
          upstream_dns_file = "";
          bootstrap_dns = cfg.externalDnsServerIps;
          bootstrap_prefer_ipv6 = false;
          fallback_dns = cfg.externalDnsServerIps;
          private_networks = [ ];
          use_private_ptr_resolvers = true;
          local_ptr_upstreams = [ ];
          upstream_mode = "parallel";
          fastest_timeout = "1s";
          use_http3_upstreams = cfg.dnsOverHttps;
          use_dns64 = false;
          dns64_prefixes = [ ];

          # ECS settings
          edns_client_subnet = {
            custom_ip = "";
            enabled = false;
            use_custom = false;
          };

          # Access settings
          allowed_clients = [ ];
          disallowed_clients = [ ];
          blocked_hosts = [
            "version.bind"
            "id.server"
            "hostname.bind"
          ] ++ cfg.blockedHosts;
          trusted_proxies = [
            "127.0.0.0/8"
            "::1/128"
          ] ++ cfg.trustedProxies;

          # DNS cache settings
          cache_size = 4194304;
          cache_ttl_min = 0;
          cache_ttl_max = 0;
          cache_optimistic = false;

          # Other settings
          bogus_nxdomain = [ ];
          enable_dnssec = false;
          aaaa_disabled = false;
          cache_time = "30"; # in seconds
          max_goroutines = 300;
          handle_ddr = true;
          ipset = [ ];
          ipset_file = "";
          upstream_timeout = "10s";
          resolve_clients = true;
          serve_http3 = cfg.dnsOverHttps;
          serve_plain_dns = !cfg.dnsOverHttps;
          hostsfile_enabled = false;
        };

        filtering = {
          protection_enabled = true;
          filtering_enabled = true;
          blocking_mode = "default";
          blocking_ipv4 = "";
          blocking_ipv6 = "";
          blocked_response_ttl = 10;
          protection_disabled_until = null;
          parental_block_host = "family-block.dns.adguard.com";
          safebrowsing_block_host = "standard-block.dns.adguard.com";
          parental_enabled = cfg.parentalEnabled;
          safe_search = {
            enabled = cfg.safeSearchEnabled;
            bing = true;
            duckduckgo = true;
            google = true;
            pixabay = true;
            yandex = true;
            youtube = true;
          };
          safebrowsing_enabled = cfg.safeBrowsingEnabled;
          safebrowsing_cache_size = 1048576;
          safesearch_cache_size = 1048576;
          parental_cache_size = 1048576;
          rewrites = cfg.rewrites;
          cache_time = 30; # in minutes
          filters_update_interval = 24; # in hours
          blocked_services = {
            ids = cfg.blockedServiceIds;
            schedule = {
              time_zone = tz;
            };
          };
        };

        querylog = {
          enabled = true;
          file_enabled = true;
          interval = "720h"; # 30 days
          size_memory = 1000;
          ignored = [ ];
          dir_path = "${dataDir}/querylog";
        };

        statistics = {
          enabled = true;
          interval = "24h"; # 1 day
          ignored = [ ];
          dir_path = "${dataDir}/statistics";
        };

        filters = filters;

        dhcp = {
          enabled = cfg.dhcpServerEnabled;
          interface_name = netCfg.interface;
          local_domain_name = domain;
          dhcpv4 = cfg.dhcpv4;
        };

        dhcpv6 = {
          range_start = cfg.dhcpv6RangeStart;
          lease_duration = 86400; # 1 day
          ra_slaac_only = false; # sends RA packets forcing the clients to use SLAAC. The DHCPv6 server won't be started in this case
          ra_allow_slaac = true; # sends RA packets allowing the clients to choose between SLAAC and DHCPv6
        };

        tls = {
          enabled = cfg.dnsOverHttps;
          server_name = "${hostName}.${domain}";
          force_https = false;
          port_https = 0; # cfg.port;
          port_dns_over_tls = 853;
          port_dns_over_quic = 784;
          port_dnscrypt = 0;
          # dnscrypt_config_file = "";
          allow_unencrypted_doh = true;
          certificate_chain = "";
          strict_sni_check = false;
          private_key = "";
          icmp_timeout_msec = 1000;
          # override_tls_ciphers = [];
        };

        user_rules = cfg.userRules;

        clients = {
          runtime_sources = {
            whois = true;
            arp = true;
            rdns = true;
            dhcp = true;
            hosts = true;
          };
          persistent = cfg.persistentClients;
        };

        os = {
          group = "";
          user = "";
          rlimit_nofile = 0;
        };

        log = {
          file = "${dataDir}/logs";
          compress = false;
          local_time = true;
          max_backups = 0;
          max_size = 100; # in MiB
          max_age = 3; # in days
          verbose = false;
        };
      };
    };

    system.activationScripts = {
      createAdGuardDataDir.text = ''
        mkdir -p "${dataDir}/querylog"
        mkdir -p "${dataDir}/statistics"
        mkdir -p "${dataDir}/data"
        chown -R 62939:62939 "${dataDir}/querylog"
        chown -R 62939:62939 "${dataDir}/statistics"
        chown -R 62939:62939 "${dataDir}/data"
        chmod 700 -R "${dataDir}"
        chmod 600 -R "${dataDir}/data/sessions.db" || true
        chmod 600 -R "${dataDir}/querylog/querylog.json" || true
        chmod 600 -R "${dataDir}/statistics/stats.db" || true
      '';
    };
    #    touch "${dataDir}/logs"
    #    chown -R root:root "${dataDir}"
    #    chmod 700 -R /var/lib/private/AdGuardHome

    networking.firewall.interfaces."${interface}" = {
      allowedUDPPorts = udpPorts;
      allowedTCPPorts = tcpPorts;
    };

    networking.firewall.interfaces."lo" = {
      allowedUDPPorts = udpPorts;
      allowedTCPPorts = tcpPorts;
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
