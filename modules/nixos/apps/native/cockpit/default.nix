{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  cfg = config.sharedConfig.webApps.cockpit;
  hostName = config.networking.hostName;
  fqdn = config.sharedConfig.network.fullyQualifiedDomainName;
  subUrl = cfg.subUrl; # cockpit is reserved and should not be used
  cockpitPort = cfg.port;

  extraConfig = ''
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;

    proxy_buffering off;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";

    # Pass ETag header from Cockpit to clients.
    # See: https://github.com/cockpit-project/cockpit/issues/5239
    gzip off;
  '';

  proxyPass = cfg.proxyPass;

  cockpit-machines = pkgs.callPackage ../../../../../pkgs/cockpit-machines.nix { };
  cockpit-podman = pkgs.callPackage ../../../../../pkgs/cockpit-podman.nix { };

  isPodmanEnabled = config.virtualisation.podman.enable;

  package = pkgs.cockpit;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages =
      [
        cockpit-machines
      ]
      ++ lib.optional isPodmanEnabled cockpit-podman
      ++ (with pkgs; [
        libvirt-dbus # needed for cockpit-machines
        cockpit
        kexec-tools
      ]);

    security.sudo.extraConfig = ''
      %wheel ALL=(ALL) NOPASSWD: /run/current-system/sw/bin/cockpit-bridge
    '';

    services = {
      cockpit = {
        enable = true;
        port = cockpitPort;
        openFirewall = cfg.openFirewall;
        inherit package;
        settings = {
          WebService = {
            UrlRoot = subUrl;
            Origins = builtins.concatStringsSep " " (
              map (domain: "https://${domain} wss://${domain}") (
                [
                  cfg.mainInternalHost
                  "${hostName}:${toString cockpitPort}"
                  "${fqdn}:${toString cockpitPort}"
                  "localhost:${toString cockpitPort}"
                  "127.0.0.1:${toString cockpitPort}"
                  "10.88.0.1:${toString cockpitPort}" # docker network
                  "::1:${toString cockpitPort}"
                ]
                ++ (if cfg.externalHost != "" then [ cfg.externalHost ] else [ ])
              )
            );
            ProtocolHeader = "X-Forwarded-Proto";
            ForwardedForHeader = "X-Forwarded-For";
          };
        };
      };

      nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
        inherit
          sharedConfig
          config
          lib
          pkgs
          cfg
          proxyPass
          extraConfig
          subUrl
          ;
      };
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';
  };
}
