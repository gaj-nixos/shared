{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  cfg = config.sharedConfig;
  inherit (cfg.network) publicHost fullyQualifiedDomainName;
  enableACME = cfg.services.letsEncrypt.enable;
  sslCertificate = if !enableACME then config.sharedConfig.services.sslServer.pkiCert else "";
  sslCertificateKey = if !enableACME then config.sharedConfig.services.sslServer.pkiKey else "";
  jail = filter: {
    settings = {
      inherit filter;
      logpath = "/var/log/nginx/*.log";
      backend = "polling";
      journalmatch = "";
      port = "http,https";
      action = ''iptables-multiport[name=HTTP, port="http,https"]'';
    };
  };
  interface = config.sharedConfig.network.interface;
  tcpPorts = [
    80
    443
  ];
  geoCfg = config.sharedConfig.services.nginx.geoBlocking;
  enableGeoBlocking = geoCfg.enable;
  allowedCountries = geoCfg.allowedCountries;
  geoDbCountryPath = "${geoCfg.databaseDir}/GeoLite2-Country.mmdb";
  nginxPkg = pkgs.nginx;
  pkgWithGeo = nginxPkg.override {
    modules = with pkgs.nginxModules; [ geoip2 ];
  };
  package = if enableGeoBlocking then pkgWithGeo else nginxPkg;
  cf = sharedConfig.lib.cloudflare;
  allTrustedProxies = cf.cloudflareIPsV4 ++ cf.cloudflareIPsV6;
in
{
  config = lib.mkIf cfg.services.nginx.enable {
    services.nginx = {
      enable = lib.mkDefault true;
      inherit package;
      enableReload = true;
      clientMaxBodySize = "40M";
      mapHashMaxSize = 4096;
      resolver.ipv6 = false;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedGzipSettings = true;
      appendHttpConfig =
        ''
          proxy_headers_hash_max_size 2048;
          proxy_headers_hash_bucket_size 256;
        ''
        + (
          if enableGeoBlocking then
            ''
              geoip2 ${geoDbCountryPath} {
                auto_reload 5m;
                $geoip2_country_code country iso_code;
              }

              map $geoip2_country_code $allowed_country {
                default 0;
                ${builtins.concatStringsSep "\n  " (map (c: "${c} 1;") allowedCountries)}
              }

              map $realip_remote_addr $is_trusted_proxy {
                default 0;
                ${builtins.concatStringsSep "\n  " (map (ip: "${ip} 1;") allTrustedProxies)}
              }

              map $remote_addr $is_trusted_proxy {
                default 0;
                ${builtins.concatStringsSep "\n  " (map (ip: "${ip} 1;") allTrustedProxies)}
              }

              real_ip_header X-Forwarded-For;
              real_ip_recursive on;
              ${builtins.concatStringsSep "\n" (map (ip: "set_real_ip_from ${ip};") allTrustedProxies)}

            ''
          else
            ""
        );

      virtualHosts = {
        #default = {
        #  serverName = "_"; # Matches any hostname not explicitly defined
        #  locations."/" = {
        #    extraConfig = ''
        #      return 403 "Access denied";
        #    '';
        #  };
        #};

        #"${fullyQualifiedDomainName}" = {
        #  serverName = fullyQualifiedDomainName;
        #  forceSSL = true;
        #  inherit sslCertificate sslCertificateKey;
        #};

        "${publicHost}" = lib.mkIf (publicHost != "") {
          forceSSL = true;
          default = true;
          inherit sslCertificate sslCertificateKey enableACME;

          locations = {
            "/.well-known/acme-challenge" = lib.mkIf enableACME {
              root = "/var/lib/acme/acme-challenge";
            };

            "/robots.txt" = lib.mkIf enableACME {
              extraConfig = ''
                rewrite ^/(.*)  $1;
                return 200 "User-agent: *\nDisallow: /";
              '';
            };

            # show a blank page on /
            #"/" = {
            #    extraConfig = ''
            #      return 200 "<html><body></body></html>";
            #    '';
            #};
          };
        };
      };
    };

    networking.extraHosts = ''
      127.0.0.1  ${fullyQualifiedDomainName}
    '';

    networking.firewall.interfaces."${interface}".allowedTCPPorts = tcpPorts;
    networking.firewall.interfaces."lo".allowedTCPPorts = tcpPorts;

    services.fail2ban.jails = lib.mkIf cfg.network.fail2ban.enable {
      # see https://www.digitalocean.com/community/tutorials/how-to-protect-an-nginx-server-with-fail2ban-on-ubuntu-20-04

      nginx-http-auth = jail "nginx-http-auth";
      nginx-botsearch = jail "nginx-botsearch";
      nginx-bad-request = jail "nginx-bad-request";
      nginx-forbidden = jail "nginx-forbidden";
      php-url-fopen = jail "php-url-fopen";
    };
  };
}
