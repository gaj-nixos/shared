{
  sharedConfig,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.services.gitlab-runner;
  configFile = config.age.secrets."${cfg.name}-config".path;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = {
      "${cfg.name}-token" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${cfg.name}-token.age";
        generator.script = (
          sharedConfig.lib.make-agenix-generator-prompt {
            prompts = [
              "Configuring GitLab Runner '${cfg.name}'"
              "Please enter the GitLab Runner token."
            ];
          }
        );
      };
      "${cfg.name}-tokenObtainedAt" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${cfg.name}-tokenObtainedAt.age";
        generator.script = (
          sharedConfig.lib.make-agenix-generator-prompt {
            prompts = [
              "Configuring GitLab Runner '${cfg.name}'"
              "Please enter the date and time when the GitLab Runner token was obtained."
              "Example format: 2024-10-04T16:03:55Z"
            ];
          }
        );
      };
      "${cfg.name}-config" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${cfg.name}-config.age";
        generator = {
          dependencies = [
            config.age.secrets."${cfg.name}-token"
            config.age.secrets."${cfg.name}-tokenObtainedAt"
          ];
          script =
            { decrypt, deps, ... }:
            ''
              printf 'concurrent=%s\n' ${toString cfg.concurrent}
              printf 'check_interval=0\n'
              printf 'shutdown_timeout=0\n'
              printf '\n'
              printf '[session_server]\n'
              printf '  session_timeout=1800\n'
              printf '\n'
              printf '[[runners]]\n'
              printf '  name="%s"\n' ${cfg.name}
              printf '  url="%s"\n' ${cfg.url}
              printf '  id=%s\n' ${toString cfg.id}
              printf '  token="%s"\n' $(${decrypt} ${lib.escapeShellArg (builtins.elemAt deps 0).file})
              printf '  token_obtained_at=%s\n' $(${decrypt} ${lib.escapeShellArg (builtins.elemAt deps 1).file})
              printf '  token_expires_at=0001-01-01T00:00:00Z\n'
              printf '  executor="%s"\n' ${cfg.executor}
              printf '\n'
              printf '[runners.custom_build_dir]\n'
              printf '\n'
              printf '[runners.cache]\n'
              printf '  MaxUploadedArchiveSize=0\n'
              printf '\n'
              printf '[runners.cache.s3]\n'
              printf '[runners.cache.gcs]\n'
              printf '[runners.cache.azure]\n'
            '';
        };
      };
    };

    environment.systemPackages = with pkgs; [
      gitlab-runner
    ];

    services = {
      gitlab-runner = {
        enable = true;
        package = pkgs.gitlab-runner;
        inherit configFile;
        gracefulTermination = true;
      };
    };

    users = {
      users."gitlab-runner" = {
        isSystemUser = true;
        extraGroups = [
          "wheel" # enable sudo
          "docker"
          "samba"
        ];
        shell = pkgs.bashInteractive;
        group = "gitlab-runner";
      };
      groups.gitlab-runner = { };
    };

    systemd.services.gitlab-runner.serviceConfig = {
      DynamicUser = lib.mkForce false;
      User = "gitlab-runner";
    };

    security.sudo = {
      extraConfig = ''
        gitlab-runner ALL=(ALL) NOPASSWD: ALL
      '';
    };
  };
}
