{
  sharedConfig,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.services.binary-cache;
  proxyPass = cfg.proxyPass;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    services.nix-serve = {
      enable = true;
      secretKeyFile = "/var/cache-priv-key.pem"; # generation: see https://nixos.wiki/wiki/Binary_Cache
      port = cfg.port;
      openFirewall = cfg.openFirewall;
    };

    users = {
      users."nix-serve" = {
        isSystemUser = true;
        extraGroups = [ ];
        shell = pkgs.bashInteractive;
        group = "nix-serve";
      };
      groups.nix-serve = { };
    };

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
