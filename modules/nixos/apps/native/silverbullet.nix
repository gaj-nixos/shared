{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.webApps.silverbullet;
  proxyPass = cfg.proxyPass;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    environment.systemPackages = with pkgs; [
      silverbullet
    ];

    services.silverbullet = {
      enable = true;
      package = pkgs.silverbullet;
      spaceDir = "/srv/silverbullet/space";
      extraArgs = [
        "--db /srv/silverbullet/silverbullet.db"
      ];
      openFirewall = true;
      listenPort = cfg.port;
      listenAddress = "0.0.0.0";
      #envFile = "/srv/silverbullet/silverbullet.env";
    };

    system.activationScripts = {
      silverbulletScript.text = ''
        mkdir -p /srv/silverbullet/space
        chown -R silverbullet:users /srv/silverbullet
      '';
    };

    users = {
      users."silverbullet" = {
        isSystemUser = true;
        extraGroups = [ ];
        shell = pkgs.bashInteractive;
        group = "silverbullet";
      };
      groups."silverbullet" = { };
    };

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
