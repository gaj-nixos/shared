{
  #sharedConfig,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.services.elastic-search;
in
#proxyPass = cfg.proxyPass;
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    services.elasticsearch = {
      inherit (cfg) enable port;
      listenAddress = "0.0.0.0";
      dataDir = "/var/lib/elasticsearch";
      plugins = with pkgs.elasticsearchPlugins; [ ];
      single_node = true;
      extraCmdLineOptions = [
        "xpack.security.enabled=false"
      ];
    };

    networking.firewall.allowedTCPPorts = lib.optional cfg.openFirewall cfg.port;

    #networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
    #  127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
    #  127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    #'';

    #services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts { inherit sharedConfig config lib pkgs cfg proxyPass; };
  };
}
