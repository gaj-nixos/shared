{
  sharedConfig,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkIf optional;
  cfg = config.sharedConfig.webApps.searx;
  proxyPass = cfg.proxyPass;
  secretName = "searxSecretKey";
  configSecretName = "searxConfig";
  configSecretPath = config.age.secrets."${configSecretName}".path;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    age.secrets = {
      "${secretName}" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${secretName}.age";
        owner = "searx";
        generator.script = "alnum"; # Generates a 48 character alphanumeric password
      };
      "${configSecretName}" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${configSecretName}.age";
        owner = "searx";
        generator = {
          dependencies = [ config.age.secrets."${secretName}" ];
          script =
            { decrypt, deps, ... }:
            ''
              printf 'use_default_settings: true\n'
              printf 'server:\n'
              printf '  bind_address: "0.0.0.0"\n'
              printf '  port: ${builtins.toString cfg.port}\n'
              printf '  secret_key: "%s"\n' $(${decrypt} ${lib.escapeShellArg (builtins.head deps).file})
              printf 'search:\n'
              printf '  formats:\n'
              printf '    - json\n'
              printf '    - html\n'
            '';
        };
      };
    };
    environment.systemPackages = with pkgs; [
      searxng
    ];

    services.searx = {
      enable = true;
      package = pkgs.searxng;
      #redisCreateLocally = true;
      settingsFile = configSecretPath;
    };

    networking.firewall.allowedTCPPorts = optional (cfg.openFirewall) cfg.port;

    networking.extraHosts = lib.mkIf (cfg.serverName != "") ''
      127.0.0.1  ${cfg.serverName}.${config.networking.hostName}
      127.0.0.1  ${cfg.serverName}.${config.sharedConfig.network.fullyQualifiedDomainName}
    '';

    services.nginx.virtualHosts = sharedConfig.lib.make-nginx-hosts {
      inherit
        sharedConfig
        config
        lib
        pkgs
        cfg
        proxyPass
        ;
    };
  };
}
