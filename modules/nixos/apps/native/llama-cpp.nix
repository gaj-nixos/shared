{
  lib,
  config,
  pkgs,
  ...
}:

let
  llamaCppPath = "/srv/llama.cpp/models";

  cfg = config.sharedConfig.webApps.llama-cpp;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      llama-cpp
    ];

    environment.variables = {
      LLAMA_MODEL_PATH = llamaCppPath;
    };

    system.activationScripts = {
      script.text = ''
        mkdir -p ${llamaCppPath}
      '';
    };
  };
}
