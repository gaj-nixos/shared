{
  pkgs,
  config,
  lib,
  ...
}:
let
  cfg = config.sharedConfig.network.adGuardHome;
  adGuardHomeEnabled = if cfg ? enable then cfg.enable else false;
in
{
  config = {
    networking.networkmanager.enable = !config.sharedConfig.network.networkd.enable;

    networking.search = [ config.sharedConfig.network.domain ];

    systemd.network = lib.mkIf config.sharedConfig.network.networkd.enable {
      enable = true;
    };
    services.resolved = {
      enable = !adGuardHomeEnabled;
      # dnssec = "false";
      # llmnr = "false";
    };
    environment.systemPackages = with pkgs; [
      dnsutils
    ];
  };
}
