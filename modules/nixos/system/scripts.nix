{
  sharedConfig,
  self,
  ...
}:
let
  inherit (sharedConfig.lib) generateScriptPackages;
  scriptPackages = generateScriptPackages "${self}/scripts";
  libScriptPackages = generateScriptPackages ../../../resources/scripts;
in
{
  environment.systemPackages = scriptPackages ++ libScriptPackages;
}
