{
  lib,
  pkgs,
  config,
  ...
}:
let
  cfg = config.sharedConfig.devices.kmscon;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      pango # list fonts in the TTY
    ];

    services.kmscon = {
      enable = true;
      hwRender = true;
      autologinUser = config.sharedConfig.boot.autologinUser;

      # Extra flags to pass to kmscon
      extraOptions = '''';
      # --sb-size 5000
      # --font-engine pango
      # --term xterm-256color
      # font-name

      # Extra contents of the kmscon.conf file
      extraConfig = ''
        xkb-layout=${config.sharedConfig.localisation.kbLayout}
        font-size=10
        font-dpi=163
      '';
      # font-name=Hack Nerd Font
      #  font-dpi=96
      #  font-size=16

      fonts = [
        #{ name = "Terminess Nerd Font Mono"; package = pkgs.terminus-nerdfont; }
        #{ name = "Source Code Pro"; package = pkgs.source-code-pro; }
        #{ name = "JetBrainsMono Nerd Font Propo"; package = pkgs.jetbrains-mono; }
        {
          name = "Inconsolata";
          package = pkgs.nerd-fonts.inconsolata;
        }
      ];

      # font_unifont
      # font_pango
      # --font-engine unifont
      # --font-name xxxx
    };
  };
}
