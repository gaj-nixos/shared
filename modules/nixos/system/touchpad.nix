{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.devices.touchpad.enable {
    services.libinput.enable = true;
  };
}
