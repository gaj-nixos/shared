{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.software.guiPrograms.enable {
    services.dbus = {
      enable = true;
      packages = with pkgs; [
        dconf
        usbguard-notifier
        #gaelj.creditagricole_particuliers
      ];
      implementation = "broker";
    };

    xdg = {
      autostart.enable = true;
      icons.enable = true;
      menus.enable = true;
      mime.enable = true;
      sounds.enable = true;
      terminal-exec = {
        enable = true;
        settings = {
          default = [
            "kitty.desktop"
          ];
        };
      };
    };

    programs = {
      dconf.enable = true;
      thunar = {
        enable = true;
        plugins = with pkgs.xfce; [
          thunar-archive-plugin
          thunar-volman
          thunar-media-tags-plugin
        ];
      };
      file-roller.enable = true; # thunar zip support
      xfconf.enable = true;
    };
    services.gvfs.enable = true; # Mount, trash, and other functionalities
    services.tumbler.enable = true; # Thumbnail support for images

    # needed to store VS Code auth token
    services.gnome.gnome-keyring.enable = true;
  };
}
