{ pkgs, ... }:
{
  config = {
    environment.systemPackages = with pkgs; [
      lsof # list open files and ports
      whois
      smartmontools
      nix-output-monitor
      radeontop
    ];

    programs = {
      iotop.enable = true;
      iftop.enable = true;
      traceroute.enable = true;
    };
  };
}
