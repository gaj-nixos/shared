{
  config,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig.services.binary-cache-client;
  substituter = cfg.substituter;
  trustedPublicKey = cfg.trustedPublicKey;

  defaultSubstituters = [
    "https://nix-community.cachix.org"
    "https://devenv.cachix.org"
  ];
  defaultKeys = [
    "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw="
  ];
  substituters = defaultSubstituters ++ (if substituter != null then [ substituter ] else [ ]);
  trusted-public-keys =
    defaultKeys ++ (if trustedPublicKey != null then [ trustedPublicKey ] else [ ]);
in
{
  config = {
    # Retrieve sha256 hash of a git repository
    environment.systemPackages = with pkgs; [
      nix-prefetch-git
      nixfmt-rfc-style
      nvd # Nix Visual Difference. Nix/NixOS package version diff tool
    ];

    # yet-another-nix-helper
    programs.nh = {
      enable = true;
      clean.enable = true;
      clean.extraArgs = "--keep-since 4d --keep 5";
      clean.dates = "weekly";
      flake = "/etc/nixos";
    };

    programs.nix-ld.enable = true;

    nix = {
      extraOptions = ''
        experimental-features = nix-command flakes recursive-nix
      '';
      settings.system-features = [ "recursive-nix" ];

      # Periodically remove old, unreferenced packages (runs nix-collect-garbage)
      #gc = {
      #  automatic  = true;
      #  dates  = "12:30";
      #  #options = "-d"; # delete old roots, removing the ability to roll back to them
      #};

      optimise = {
        automatic = true;
        dates = [ "05:30" ];
      };

      settings = {
        inherit substituters trusted-public-keys;
      };
    };
  };
}
