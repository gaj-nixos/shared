{
  lib,
  pkgs,
  config,
  ...
}:
let
  paPkg = pkgs.pulseaudioFull;
  q = config.sharedConfig.devices.sound.pipewireMinQuantum;
in
{
  config = lib.mkIf config.sharedConfig.devices.sound.enable {

    security.rtkit.enable = true;

    services.pulseaudio = {
      package = paPkg;
      configFile = lib.mkIf config.sharedConfig.devices.sound.fixAudioStutterInVmWare1 (
        pkgs.runCommand "default.pa" { } ''
          sed 's/module-udev-detect$/module-udev-detect tsched=0/' \
            ${paPkg}/etc/pulse/default.pa > $out
        ''
      );
    };

    services.pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
      jack.enable = true;
      extraConfig.pipewire = lib.mkIf config.sharedConfig.devices.sound.fixAudioStutterInVmWare2 {
        "10-min-quantum" = {
          "context.properties" = {
            "default.clock.rate" = 48000;
            "default.clock.min-quantum" = q;
            "default.clock.max-quantum" = q;
            "default.clock.quantum" = q;
            "vm.overrides" = {
              "default.clock.min-quantum" = q;
              "default.clock.max-quantum" = q;
              "default.clock.quantum" = q;
            };
          };
        };
      };
    };
  };
}
#Found "settings" metadata 33
#update: id:0 key:'log.level' value:'2' type:''
#update: id:0 key:'clock.rate' value:'48000' type:''
#update: id:0 key:'clock.allowed-rates' value:'[ 48000 ]' type:''
#update: id:0 key:'clock.quantum' value:'8192' type:''
#update: id:0 key:'clock.min-quantum' value:'8192' type:''
#update: id:0 key:'clock.max-quantum' value:'8192' type:''
#update: id:0 key:'clock.force-quantum' value:'0' type:''
#update: id:0 key:'clock.force-rate' value:'0' type:''
