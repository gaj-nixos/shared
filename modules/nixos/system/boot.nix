{
  config,
  pkgs,
  lib,
  ...
}:
let
  cfg = config.sharedConfig.boot;
  amd = config.sharedConfig.devices.amdCPU.enable;
  wm = config.sharedConfig.windowManagers;
  win10Enabled = wm ? win10 && wm.win10;
in
{
  config = {
    boot = {
      loader = {
        grub.enable = false;
        systemd-boot = lib.mkIf (cfg.loader == "systemd-boot") {
          enable = true;
          consoleMode = "auto";
          configurationLimit = 120;
          memtest86.enable = true;
        };
        generic-extlinux-compatible = lib.mkIf (cfg.loader == "extlinux") {
          # Raspberry Pi
          enable = true;
        };
        efi.canTouchEfiVariables = true;
      };

      initrd = {
        systemd.enable = true;
        #network.ssh.enable = true;
      };

      kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;

      plymouth.enable = cfg.enablePlymouth;

      kernelParams = [
        "video=${cfg.videoResolution}"
      ];
    };

    hardware.enableAllFirmware = true;

    systemd = {
      enableEmergencyMode = true;
      extraConfig = ''
        DefaultTimeoutStopSec=20s
      '';
    };

    services = {
      upower.enable = !amd;
      auto-cpufreq.enable = !amd && !win10Enabled;
      fwupd.enable = true; # firmware updating service
      fstrim.enable = true; # ssd maintenance service
      #thermald.enable = true;               # thermal regulation service
    };
  };
}
