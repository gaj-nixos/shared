{
  sharedConfig,
  pkgs,
  lib,
  config,
  ...
}:
let
  cfg = config.sharedConfig.services.smbServer;
  name = builtins.replaceStrings [ "/" ] [ "-" ] cfg.sharedPath;
  secretName = "smbServer-${name}-password";
  secretPath = config.age.secrets."${secretName}".path;
in
{
  config = lib.mkIf cfg.enable {
    age.secrets = {
      "${secretName}" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/${secretName}.age";
        mode = "440"; # R
        group = "samba";
        generator.script = (
          sharedConfig.lib.make-agenix-generator-prompt {
            prompts = [
              "Configuring Samba Server '${cfg.sharedPath}'"
              "Please enter the Samba Server password for user ${cfg.userName}."
            ];
          }
        );
      };
    };

    users = {
      users."samba" = {
        isSystemUser = true;
        extraGroups = [ ];
        shell = pkgs.bashInteractive;
        group = "samba";
      };
      groups."samba" = {
        gid = 982;
      };
    };

    services.samba = {
      enable = true;
      package = pkgs.samba4Full;
      openFirewall = cfg.openFirewall;
      nsswins = true;
      settings = {
        global.security = "user";
        Share = {
          path = "${cfg.sharedPath}";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
        };
      };
    };

    services.avahi = {
      publish.enable = true;
      publish.userServices = true;
      publish.workstation = true;
      publish.domain = true;
      publish.hinfo = true;
      enable = true;
      openFirewall = cfg.openFirewall;
      domainName = config.sharedConfig.network.domain;
      ipv4 = true;
      ipv6 = false;
      nssmdns4 = true;
    };

    # Advertise the shares to Windows hosts
    services.samba-wsdd = {
      enable = true;
      openFirewall = cfg.openFirewall;
      workgroup = cfg.workgroup;
      interface = config.sharedConfig.network.interface;
      discovery = true;
      extraOptions = [
        #"--verbose"
        #"--no-http"
        "--ipv4only"
        #"--no-host"
      ];
    };

    # create shared directory
    system.activationScripts = {
      sambaDirectoryOwnershipScript.text = ''
        mkdir -p ${cfg.sharedPath}
        chown -R ${cfg.userName} ${cfg.sharedPath}
        chgrp -R users ${cfg.sharedPath}
      '';
      sambaPasswordScript =
        let
          password = ''$(printf "%s" "$(cat "${secretPath}")")'';
        in
        ''
          ${pkgs.samba}/bin/smbpasswd -a ${cfg.userName} -s <<EOF
          ${password}
          ${password}
          EOF
        '';
    };
  };
}
