{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.software.docker.enable {
    virtualisation = {
      docker = {
        enable = true;
        autoPrune.enable = true;
        daemon.settings = {
          data-root = "/srv/docker";
        };
      };
      oci-containers.backend = "docker";
    };

    environment.systemPackages = with pkgs; [
      docker
      #docker-compose
    ];

    system.activationScripts = {
      createDockerNetworksScript.text = ''
        if ! (/run/current-system/sw/bin/docker network list | grep -q ' services_bridge '); then
          /run/current-system/sw/bin/docker network create -d bridge services_bridge
        fi
      '';
    };
  };
}
