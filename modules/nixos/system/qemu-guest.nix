{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.virtualisation.isQemuGuest {
    services.qemuGuest.enable = true;
    services.spice-vdagentd.enable = true; # enable copy and paste between host and guest
  };
}
