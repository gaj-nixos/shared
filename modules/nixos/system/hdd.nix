{ ... }:
{
  config = {
    services = {
      # Auto-mount
      devmon.enable = true;
      gvfs.enable = true;
      udisks2 = {
        enable = true;
        mountOnMedia = true;
      };
      btrfs.autoScrub = {
        enable = true;
        fileSystems = [ "/" ];
      };
    };
  };
}
