{
  pkgs,
  lib,
  config,
  ...
}:
let
  netCore = import ../../../pkgs/dotnet/dotnet.nix pkgs;
in
{
  config = lib.mkIf config.sharedConfig.software.dotnet.enable {
    environment = {
      systemPackages = with pkgs; [
        netCore
        dotnetPackages.Nuget
        omnisharp-roslyn
        mono
        msbuild
        swagger-codegen3
        #swagger-cli # FIXME fails to build ERROR: noBrokenSymlinks: the symlink /nix/store/r0dia80awly4v9rh2wk6qb1s4jzlqxys-swagger-cli-4.0.4/lib/node_modules/@apidevtools/swagger-cli/node_modules/.bin/semver points to a missing target /nix/store/r0dia80awly4v9rh2wk6qb1s4jzlqxys-swagger-cli-4.0.4/lib/node_modules/@apidevtools/swagger-cli/node_modules/semver/bin/semver.j
        semver-tool

        openapi-generator-cli
      ];
      sessionVariables = {
        DOTNET_ROOT = "${netCore}";
        DOTNET_CLI_TELEMETRY_OPTOUT = "true";
      };
    };
    #system.activationScripts = {
    #  linkLibldap.text = ''
    #    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${netCore}/lib";
    #  '';
    #};
  };
}
