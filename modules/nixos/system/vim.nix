{ ... }:
{
  #programs.nvf = {
  #  enable = true;
  #  settings = {
  #    vim = {
  #      viAlias = false;
  #      vimAlias = true;
  #    };
  #  };
  #};

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
  };
}
