{
  lib,
  inputs,
  ...
}:
let
  hasHyprpanel = inputs ? hyprpanel;
  inherit (inputs) hyprpanel;
in
{
  home-manager = {
    useGlobalPkgs = false;
    useUserPackages = true;
    backupFileExtension = "backup2";
    sharedModules =
      [
        # Extra modules added to all users
        #hyprpanel.homeManagerModules.hyprpanel
      ]
      ++ (lib.optional hasHyprpanel hyprpanel.homeManagerModules.hyprpanel)
      ++ (lib.optional (!hasHyprpanel) (
        { ... }:
        {
          options = {
            programs = {
              hyprpanel = lib.mkOption {
                type = lib.types.attrs;
              };
            };
          };
        }
      ));
    extraSpecialArgs = {
      # pass additional arguments to all modules
    };
  };
}
