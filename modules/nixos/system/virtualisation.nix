{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.virtualisation.isHost {
    virtualisation.libvirtd.enable = true;
    programs.virt-manager.enable = true;
  };
}
