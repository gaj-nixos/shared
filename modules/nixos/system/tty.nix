{ pkgs, ... }:
{
  config = {
    console = {
      font = "${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
      packages = with pkgs; [
        terminus_font
      ];
      useXkbConfig = true; # use xkb.options in tty.
    };

    environment.systemPackages = with pkgs; [
      fastfetch-with-logo
    ];
  };
}
