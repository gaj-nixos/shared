{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.virtualisation.isVmwareGuest {
    virtualisation.vmware.guest = {
      enable = true;
      headless = config.sharedConfig.virtualisation.installOpenVMToolsHeadless;
    };
  };
}
