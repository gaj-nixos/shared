{
  config,
  lib,
  pkgs,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.devices.bluetooth.enable {
    hardware.bluetooth.enable = true;
    services.blueman.enable = true;
    environment.systemPackages = with pkgs; [
      overskride
    ];
  };
}
