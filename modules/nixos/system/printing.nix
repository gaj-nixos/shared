{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.devices.printing.enable {
    services.printing = {
      enable = true;
      drivers = with pkgs; [
        cups-pk-helper
        gutenprint
      ];
    };

    environment.systemPackages = with pkgs; [
      cups-filters
    ];
  };
}
