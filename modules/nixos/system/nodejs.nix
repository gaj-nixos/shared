{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.software.nodejs.enable {
    environment = {
      systemPackages = with pkgs; [
        nodejs
      ];
    };
  };
}
