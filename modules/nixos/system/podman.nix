{
  pkgs,
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.software;
in
{
  config = mkIf (if cfg ? podman then cfg.podman.enable else false) {
    virtualisation = {
      podman = {
        enable = true;
        autoPrune.enable = true;
        dockerCompat = true;
        dockerSocket.enable = true;
        defaultNetwork.settings = {
          # Required for container networking to be able to use names.
          dns_enabled = true;
        };
        networkSocket.server = "ghostunnel";
      };
      oci-containers.backend = "podman";
      containers.registries.search = [
        "quay.io"
        "ghcr.io"
        "docker.io"
      ];
    };

    networking.firewall.interfaces."podman+".allowedUDPPorts = [
      53
      5353
    ];

    # Root service
    # When started, this will automatically create all resources and start
    # the containers. When stopped, this will teardown all resources.
    systemd.targets."podman-compose-all-root" = {
      unitConfig = {
        Description = "Root target for all podman-compose services";
      };
      wantedBy = [ "multi-user.target" ];
    };

    environment.systemPackages = with pkgs; [
      podman-tui # podman terminal UI
      podman-compose
    ];
  };
}
