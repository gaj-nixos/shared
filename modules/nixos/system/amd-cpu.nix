{
  config,
  lib,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.devices.amdCPU.enable {
    boot = {
      kernelModules = [ "kvm-amd" ];
      kernelParams = [ "amd_pstate=active" ];
    };

    hardware.cpu.amd = {
      ryzen-smu.enable = true;
      updateMicrocode = true;
    };

    services.power-profiles-daemon.enable = true;

    powerManagement.cpuFreqGovernor = "performance";
  };
}
