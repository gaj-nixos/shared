{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.software.cliImageTools.enable {
    environment.systemPackages = with pkgs; [
      poppler_utils # PDF rendering library
      ffmpegthumbnailer
      librsvg
      imagemagickBig
      mediainfo
      odt2txt
      libtiff
      ffmpeg # multimedia processing
    ];
  };
}
