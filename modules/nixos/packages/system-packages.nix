{
  pkgs,
  config,
  ...
}:
{
  config = {
    # List packages installed in system profile. To search, run:
    # $ nix search wget

    programs = {
      direnv = {
        enable = true;
        nix-direnv = {
          enable = true;
        };
      };
      bandwhich.enable = true;
    };

    environment.systemPackages =
      with pkgs;
      [
        fastfetch # system info
        unzip
        jq # json processor (like sed for json)
        yq

        # partitioning
        parted # disk partitioning
        cryptsetup # for encrypted partitions

        # style
        base16-schemes
      ]
      ++ lib.optional (config.sharedConfig.software.swDevPrograms.enable) glab;
  };
}
