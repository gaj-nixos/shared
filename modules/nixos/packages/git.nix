{ pkgs, ... }:
{
  config = {
    environment.systemPackages = with pkgs; [
      git
      git-lfs
      git-interactive-rebase-tool
    ];

    programs = {
      git.enable = true;
      git.lfs.enable = true;
    };
  };
}
