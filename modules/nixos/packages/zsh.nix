{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.software.zsh.enable {
    programs.zsh = {
      enable = true;
      autosuggestions.enable = true;
    };

    environment.pathsToLink = [ "/share/zsh" ];
  };
}
