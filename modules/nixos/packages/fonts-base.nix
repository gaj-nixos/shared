{
  config,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig.stylixGlobal;
in
{
  config = {
    fonts = {
      packages = with pkgs; [
        cfg.fonts.emoji.package
        nerd-fonts.terminess-ttf
        terminus_font
        source-code-pro
        nerd-fonts.inconsolata
        jetbrains-mono
        noto-fonts-emoji-blob-bin
      ];
      fontDir.enable = true;
    };

    environment.systemPackages = with pkgs; [
      fontforge
    ];
  };
}
