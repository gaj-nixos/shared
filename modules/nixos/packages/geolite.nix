{
  sharedConfig,
  lib,
  pkgs,
  config,
  ...
}:
let
  geoCfg = config.sharedConfig.services.nginx.geoBlocking;
  interval = "weekly";
  update-geoip-database = pkgs.writeScriptBin "update-geoip-database" ''
    #!${pkgs.runtimeShell}
    mkdir -p ${geoCfg.databaseDir}
    chmod 755 ${geoCfg.databaseDir}
    chown geoip:srv "${geoCfg.databaseDir}"
    skipExisting=0
    for arg in "$@"; do
      case "$arg" in
        --skip-existing)
          skipExisting=1
          echo "Option --skip-existing is set: not updating existing databases"
          ;;
        *)
          echo "Unknown argument: $arg";;
      esac
    done
    key=$(cat ${config.age.secrets."geoLiteLicenseKey".path})
    if [ -z "$key" ]; then
      echo "No GeoLite license key found"
      exit 1
    fi
    cd ${geoCfg.databaseDir}
    for dbName in "GeoLite2-City" "GeoLite2-Country" "GeoLite2-ASN"; do
      if [ "$skipExisting" -eq 1 -a -f $dbName.mmdb ]; then
        echo "Skipping existing file: $dbName.mmdb"
        continue
      fi
      ${pkgs.curl}/bin/curl -L -o "${geoCfg.databaseDir}/$dbName.tar.gz" "https://download.maxmind.com/app/geoip_download?edition_id=$dbName&license_key=$key&suffix=tar.gz"
      ${pkgs.gzip}/bin/gzip -cd "${geoCfg.databaseDir}/$dbName.tar.gz" | ${pkgs.gnutar}/bin/tar xvf -
      mv $dbName\_*/$dbName.mmdb .
      rm -rfv $dbName\_*/ $dbName.tar.gz
    done
  '';
in
{
  config = lib.mkIf config.sharedConfig.services.nginx.geoBlocking.enable {
    age.secrets = {
      "geoLiteLicenseKey" = {
        rekeyFile = config.sharedConfig.flakeRoot + "/secrets/geoLiteLicenseKey.age";
        mode = "440";
        owner = "geoip";
        group = "srv";
        generator.script = (
          sharedConfig.lib.make-agenix-generator-prompt {
            prompts = [
              "Please enter your MaxMind GeoLite2 license key."
              "You can get one by registering at https://www.maxmind.com/en/geolite2/signup"
              "Or manage your existing license keys at https://www.maxmind.com/en/accounts/current/license-key"
            ];
          }
        );
      };
    };

    environment.systemPackages = [
      pkgs.libmaxminddb
      # update-geoip-database
    ];

    users = {
      users."geoip" = {
        isSystemUser = true;
        extraGroups = [ ];
        shell = pkgs.bashInteractive;
        group = "srv";
        description = "GeoIP database updater";
      };
      groups."srv" = { };
    };

    systemd.timers.update-geoip-database = {
      description = "GeoIP Updater Timer";
      partOf = [ "update-geoip-database.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = interval;
      timerConfig.Persistent = "true";
      timerConfig.RandomizedDelaySec = "3600";
    };

    systemd.services.update-geoip-database = {
      description = "GeoIP Updater";
      after = [
        "network-online.target"
        "nss-lookup.target"
      ];
      wants = [ "network-online.target" ];
      preStart = with geoCfg; ''
        mkdir -p "${databaseDir}"
        chmod 755 "${databaseDir}"
        chown geoip:srv "${databaseDir}"
      '';
      serviceConfig = {
        ExecStart = "${update-geoip-database}/bin/update-geoip-database";
        User = "geoip";
        PermissionsStartOnly = true;
      };
    };

    systemd.services.update-geoip-database-setup = {
      description = "GeoIP Updater Setup";
      after = [
        "network-online.target"
        "nss-lookup.target"
      ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      conflicts = [ "update-geoip-database.service" ];
      preStart = with geoCfg; ''
        mkdir -p "${databaseDir}"
        chmod 644 "${databaseDir}"
        chown geoip:srv "${databaseDir}"
      '';
      serviceConfig = {
        ExecStart = "${update-geoip-database}/bin/update-geoip-database --skip-existing";
        User = "geoip";
        PermissionsStartOnly = true;
        # So it won't be (needlessly) restarted:
        RemainAfterExit = true;
      };
    };
  };
}
