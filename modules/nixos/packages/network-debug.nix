{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.software.wireshark.enable {
    programs = {
      wireshark.enable = true;
      tcpdump.enable = true;
    };

    environment.systemPackages = with pkgs; [
      nmap
    ];
  };
}
