{
  config,
  lib,
  pkgs,
  ...
}:

{
  config = lib.mkIf config.sharedConfig.software.guiPrograms.enable {
    fonts = {
      packages =
        with pkgs;
        [
          # font-awesome_5
          # font-awesome_6
          # b612
          # ubuntu_font_family
          # noto-fonts
          # fira-code
          # fira-mono
          # fira-sans
          # inter
          # commit-mono
          # departure-mono
          # cantarell-fonts
          # caladea # Cambria replacement
          # carlito # Calibri replacement
          # comic-relief # Comic Sans replacement
          # cm_unicode
          # crimson
          # dejavu_fonts
          # fira
          # gentium
          # google-fonts
          # (input-fonts.override { acceptLicense = true; })
          # ipafont
          # ipaexfont
          # league-of-moveable-type
          # libertine
          # (joypixels.override { acceptLicense = true; })
          # liberation_ttf_v2 # Arial, Times New Roman & Courier New replacement
          # # libre-baskerville
          # libre-bodoni
          # libre-caslon
          # lmmath
          # lmodern
          # # source-sans-pro
          # # source-serif-pro
          # ubuntu-classic
        ]
        ++ (with pkgs.nerd-fonts; [
          # fira-code
          # fira-mono
        ]);
      #fontDir.enable = true;
      #fontconfig = {
      #  enable = true;
      #  defaultFonts = {
      #    emoji = [ cfg.fonts.emoji.name ];
      #    monospace = [
      #      "CommitMono"
      #      cfg.fonts.emoji.name
      #      "Font Awesome 6 Free"
      #      "Font Awesome 5 Free"
      #    ];
      #    sansSerif = [
      #      "Inter"
      #      cfg.fonts.emoji.name
      #      "Font Awesome 6 Free"
      #      "Font Awesome 5 Free"
      #    ];
      #    serif = [
      #      "Noto Serif"
      #      cfg.fonts.emoji.name
      #      "Font Awesome 6 Free"
      #      "Font Awesome 5 Free"
      #    ];
      #  };
      #  subpixel = {
      #    rgba = mkDefault "rgb";
      #  };
      #};
      #enableDefaultPackages = true;
    };
  };
}
