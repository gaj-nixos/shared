{ pkgs, ... }:
{
  config = {
    environment.systemPackages = with pkgs; [
      pass
      #pass2csv
      #passExtensions.pass-tomb
      #passExtensions.pass-update
      #passExtensions.pass-otp
      #passExtensions.pass-import
      #passExtensions.pass-audit
      #tomb
      pwgen
      pwgen-secure
      bitwarden-cli
      libsecret # for gnome-keyring
      openldap
      openssh
      killall
    ];
  };
}
