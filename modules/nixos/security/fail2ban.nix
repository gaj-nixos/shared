{
  config,
  lib,
  ...
}:
let
  cfg = config.sharedConfig.network.fail2ban;
in
{
  config = lib.mkIf cfg.enable {
    services.fail2ban = {
      enable = true;
      maxretry = 3;
      bantime-increment.enable = true;
      ignoreIP = cfg.ignoreIP;
    };
  };
}
