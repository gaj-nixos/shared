# A module that automatically imports everything else in the parent folder.
let
  inherit (builtins) filter attrNames readDir;
in
{
  imports = map (fn: ./${fn}) (filter (fn: fn != "default.nix") (attrNames (readDir ./.)));
}
