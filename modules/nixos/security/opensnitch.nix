{
  config,
  lib,
  pkgs,
  ...
}:

{
  config = lib.mkIf config.sharedConfig.network.firewall.enableOpenSnitch {
    services.opensnitch = {
      enable = true;
      settings = {
        Rules.Path = "/var/lib/opensnitch/rules";
        InterceptUnknown = true;
        Firewall = "iptables";
        DefaultAction = "deny";
      };
    };

    environment.systemPackages = with pkgs; [
      opensnitch-ui
    ];
  };
}
