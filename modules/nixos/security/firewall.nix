{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.network.firewall.enable {
    networking.firewall = {
      enable = true;
      allowPing = true;
      logRefusedConnections = true;
      logReversePathDrops = true;
      logRefusedPackets = true;
      logRefusedUnicastsOnly = true;
      #filterForward = true;
      rejectPackets = true; # Less secure, but more informative
    };
  };
}
