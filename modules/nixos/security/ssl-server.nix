{
  pkgs,
  lib,
  config,
  ...
}:
let
  cfg = config.sharedConfig;
  selfSignedSSLCerts = cfg.services.sslServer.selfSignedSSLCerts;
  pkiPath = cfg.services.sslServer.pkiPath;
  pkiCertsSubPath = cfg.services.sslServer.pkiCertsSubPath;
  pkiKeysSubPath = cfg.services.sslServer.pkiKeysSubPath;
  hostName = config.networking.hostName;
  fullyQualifiedDomainName = cfg.network.fullyQualifiedDomainName;
  smbUserName = cfg.services.smbServer.userName;

  rootCAPath = "${pkiPath}/rootCA"; # /srv/pki/rootCA
  pkiCertsPath = "${pkiPath}/${pkiCertsSubPath}"; # /srv/pki/certs
  pkiKeyPath = "${pkiPath}/${pkiKeysSubPath}"; # /srv/pki/keys

  caRootCertPath = "${rootCAPath}/rootCA.pem";
  publicKeyPath = "${pkiCertsPath}/${hostName}-cert.pem";
  encryptedPrivateKeyPath = "${pkiKeyPath}/${hostName}-encrypted-key.pem";
  privateKeyPath = "${pkiKeyPath}/${hostName}-key.pem";
  password = "${fullyQualifiedDomainName}";

  updateSelfSignedCertScript = ''
    export CAROOT=${rootCAPath}
    mkdir -p ${rootCAPath}
    ${pkgs.mkcert}/bin/mkcert -install
    ${pkgs.mkcert}/bin/mkcert \
      -cert-file ${publicKeyPath} \
      -key-file ${privateKeyPath} \
      ${hostName} ${fullyQualifiedDomainName} *.${hostName} *.${fullyQualifiedDomainName} localhost
    ln -sf ${caRootCertPath} /etc/ssl/certs/rootCA.pem
    ln -sf ${caRootCertPath} ${pkiCertsPath}/rootCA.pem
    ln -sf ${rootCAPath}/rootCA-key.pem ${pkiKeyPath}/rootCA-key.pem
  '';
  updateSelfSignedCert = pkgs.writeShellScriptBin "update-self-signed-ssl-cert" ''${
    updateSelfSignedCertScript
  }'';
  makeSelfSignedCertIfNotExisting = ''
    if [ ! -f ${publicKeyPath} ]; then
      ${updateSelfSignedCertScript}
    fi
  '';

  updateProvidedCertScript = ''
    if [ -f ${encryptedPrivateKeyPath} ]; then
      ${pkgs.openssl}/bin/openssl rsa -in ${encryptedPrivateKeyPath} -out ${privateKeyPath} -passin pass:${password}
      chown root:nginx ${smbUserName} ${privateKeyPath}
      chmod 640 ${privateKeyPath}
    fi
    ln -sf ${caRootCertPath} /etc/ssl/certs/rootCA.pem
    ln -sf ${caRootCertPath} ${pkiCertsPath}/rootCA.pem
  '';
  updateProvidedCert = pkgs.writeShellScriptBin "update-provided-ssl-cert" ''${
    updateProvidedCertScript
  }'';
  makeProvidedCertIfNotExisting = ''
    if [ ! -f ${privateKeyPath} ]; then
      ${updateProvidedCertScript}
    fi
  '';
in
{
  config = {
    sharedConfig.services.sslServer.pkiCert = "${pkiPath}/${pkiCertsSubPath}/${hostName}-cert.pem";
    sharedConfig.services.sslServer.pkiKey = "${pkiPath}/${pkiKeysSubPath}/${hostName}-key.pem";

    environment.systemPackages =
      with pkgs;
      lib.optional selfSignedSSLCerts mkcert
      ++ lib.optional selfSignedSSLCerts updateSelfSignedCert
      ++ lib.optional (!selfSignedSSLCerts) updateProvidedCert
      ++ [
        cacert
        openssl
      ];

    services.samba = {
      settings = {
        PKI = {
          path = "${pkiCertsPath}";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
        };
      };
    };

    system.activationScripts = {
      # Conditionally add the certificate generation script
      makeSelfSignedCertIfNotExistingScript = lib.mkIf selfSignedSSLCerts {
        text = makeSelfSignedCertIfNotExisting;
      };

      decryptPrivateKey = lib.mkIf (!selfSignedSSLCerts) {
        text = makeProvidedCertIfNotExisting;
      };

      # Always add the directory creation script
      createPkiShareDir.text = ''
        mkdir -p ${pkiPath}
        mkdir -p ${pkiCertsPath}
        mkdir -p ${pkiKeyPath}
        chown -R ${smbUserName} ${pkiCertsPath}
        chgrp -R users ${pkiCertsPath}
        chmod a+wr -R /srv/pki
      '';
    };
  };
}
