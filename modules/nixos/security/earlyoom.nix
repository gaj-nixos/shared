{ ... }:

{
  services.earlyoom = {
    enable = true;
    enableNotifications = true;
    freeMemThreshold = 5; # %
    freeSwapThreshold = 10; # %
  };
}
