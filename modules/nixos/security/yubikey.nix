{
  pkgs,
  lib,
  config,
  ...
}:
let
  inherit (lib) mkIf;
  cfg = config.sharedConfig.devices.yubiKey;
in
{
  config = mkIf (if cfg ? enable then cfg.enable else false) {
    environment.systemPackages = with pkgs; [
      yubikey-manager
      yubikey-personalization
      yubikey-personalization-gui
      gnupg
      gnupg-pkcs11-scd
      pcsc-tools
      openssh
      pam_u2f
      yubico-piv-tool
      yubioath-flutter
    ];

    programs = {
      yubikey-touch-detector.enable = true;
      gnupg.agent = {
        enable = true;
        enableSSHSupport = true;
        enableBrowserSocket = true;
      };
    };

    services = {
      udev.packages = [ pkgs.yubikey-personalization ];
      yubikey-agent.enable = true;
      # pcscd.enable = true; # use the smart card mode (CCID) of the YubiKey
    };

    security.pam = {
      # TODO Don't forget to create an authorization mapping file for your user (https://nixos.wiki/wiki/Yubikey#pam_u2f)
      #u2f = {
      #  enable = true;
      #  settings.cue = true;
      #  control = "sufficient";
      #};
      services = {
        greetd.u2fAuth = cfg.enableForLogin;
        login.u2fAuth = cfg.enableForLogin;
        hyprlock.u2fAuth = cfg.enableForLogin;
        sudo.u2fAuth = cfg.enableForSudo;
      };
    };

    boot.initrd.luks.yubikeySupport = false;
  };
}
