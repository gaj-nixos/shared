{
  config,
  lib,
  ...
}:
let
  cfg = config.sharedConfig.network.adGuardHome;
in
{
  config = lib.mkIf (if cfg ? enable then cfg.enable else false) {
    # Disable DNS services
    networking = {
      nameservers = [
        "127.0.0.1"
        "[::1]"
      ];
      # If using dhcpcd:
      dhcpcd.enable = false; # disable, because enabled by default
      dhcpcd.extraConfig = "nohook resolv.conf";

      # If using NetworkManager:
      networkmanager.dns = "none";

      # If using resolv.conf:
      resolvconf = {
        enable = false; # TODO remember to delete /etc/resolv.conf after applying this config
        useLocalResolver = true;
      };

      # If using iwd:
      wireless.iwd.settings.Network.NameResolvingService = "none";
    };
  };
}
