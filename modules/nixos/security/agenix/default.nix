{
  config,
  pkgs,
  ...
}:
{
  imports = [
    (import ./agenix.nix {
      inherit config pkgs;
      flakeRoot = config.sharedConfig.flakeRoot;
    })
  ];
}
