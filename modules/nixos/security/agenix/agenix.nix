{
  config,
  pkgs,
  flakeRoot,
}:
let
  ageDir = "/etc/age";
  keyPath = "${ageDir}/key.txt";
  keygen = "${pkgs.age}/bin/age-keygen";

  keySetupScript = pkgs.writeScript "age-key-setup" ''
    #!/bin/sh

    if [ ! -d "${ageDir}" ]; then
      mkdir -p "${ageDir}"
      chmod 700 "${ageDir}"
    fi

    if [ ! -f "${keyPath}" ]; then
      echo "Generating new Age keypair for this machine..."
      ${keygen} -o "${keyPath}"
      chmod 600 "${keyPath}"
      echo ""
      echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      echo "  IMPORTANT: New Age key generated at ${keyPath}"
      echo "  Public key: $(grep 'public key' "${keyPath}" | cut -d' ' -f4)"
      echo ""
      echo "  Add this public key to your:"
      echo "    - flake.nix (in secrets/secrets.nix)"
      echo "    - age.reKey.masterIdentities"
      echo "  before deploying secrets to this machine!"
      echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    fi
  '';

in
{
  config = {
    environment.systemPackages = [
      pkgs.agenix-rekey
    ];

    age = {
      identityPaths = [ keyPath ];
      rekey = {
        # Obtain this using `ssh-keyscan` or by looking it up in your ~/.ssh/known_hosts
        #hostPubkey = ;
        # The path to the master identity used for decryption. See the option's description for more information.
        #masterIdentities = [ ./your-yubikey-identity.pub ];
        #masterIdentities = [ "/home/my-user/master-key" ]; # External master key
        masterIdentities = [
          # It is possible to specify an identity using the following alternate syntax,
          # this can be used to avoid unnecessary prompts during encryption.
          {
            identity = keyPath; # Password protected external master key
            #pubkey = mjolnirAgePublicKey; # Specify the public key explicitly
          }
        ];
        extraEncryptionPubkeys = [
          ../../../../resources/age/iso.age.pub
          ../../../../resources/age/mjolnir.age.pub
          ../../../../resources/age/vmware-workstation.age.pub
        ];
        storageMode = "local";
        # Choose a directory to store the rekeyed secrets for this host.
        # This cannot be shared with other hosts. Please refer to this path
        # from your flake's root directory and not by a direct path literal like ./secrets
        localStorageDir = flakeRoot + "/secrets/rekeyed/${config.networking.hostName}";
      };

      generators = { };
    };

    system.activationScripts.ageKeySetup = {
      text = ''
        if ! [ -e ${keyPath} ]; then
          ${keySetupScript}
        fi
      '';
    };
  };
}
