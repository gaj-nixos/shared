{
  lib,
  config,
  ...
}:
let
  # Remember to disable host proxying on Cloudflare when performing the ACME challenge
  enableACME = config.sharedConfig.services.letsEncrypt.enable;
  email = config.sharedConfig.services.letsEncrypt.email;
in
{
  config = lib.mkIf enableACME {
    security.acme = {
      acceptTerms = true;
      defaults = {
        inherit email;
        dnsProvider = "cloudflare";
        environmentFile = "/srv/cloudflare-dns-creds.env";
      };
    };
  };
}
