{
  pkgs,
  ...
}:
let
  inherit (builtins)
    attrNames
    filter
    readDir
    stringLength
    ;

  certificatesDir = ../../../resources/certificates;
  allFiles = attrNames (readDir certificatesDir);
  getExtension = path: (builtins.substring ((stringLength path) - 4) (-1) path);
  crtFiles = filter (path: (getExtension path) == ".crt") allFiles;
  certificateFiles = map (fn: ../../../resources/certificates/${fn}) crtFiles;

in
{
  config = {
    security.pki.certificateFiles = certificateFiles ++ [
      "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
    ];
  };
}
