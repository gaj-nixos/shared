{
  lib,
  config,
  ...
}:
let
  opensslCnf = builtins.readFile ../../../resources/configs/openssl_tls10.cnf;
in
{
  config = {
    security.pki.certificateFiles = config.sharedConfig.network.ssl.additionalCertificates;

    environment = lib.mkIf config.sharedConfig.network.ssl.allowSsl11 {
      etc."ssl/openssl.cnf".text = opensslCnf;

      sessionVariables = {
        OPENSSL_CONF = "/etc/ssl/openssl.cnf";
        CLR_OPENSSL_VERSION_OVERRIDE = "1.1";
      };
    };
  };
}
