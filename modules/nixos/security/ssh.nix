{
  pkgs,
  lib,
  ...
}:
{
  config = {
    environment.systemPackages = with pkgs; [
      sshfs
    ];

    # Enable the OpenSSH daemon.
    services.openssh = {
      enable = true;
      settings = {
        PermitRootLogin = lib.mkDefault "no";
        # KbdInteractiveAuthentication = false;
      };
      # passwordAuthentication = false;
      extraConfig = ''
        AcceptEnv TERM
      '';
    };

    programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      enableBrowserSocket = true;
      enableExtraSocket = true;
    };
  };
}
