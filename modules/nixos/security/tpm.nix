{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig.devices.tpm2;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      tpm2-tss
      tpm2-tools
    ];
    security.tpm2 = {
      enable = true;
      pkcs11.enable = true; # expose /run/current-system/sw/lib/libtpm2_pkcs11.so
      tctiEnvironment.enable = true; # TPM2TOOLS_TCTI and TPM2_PKCS11_TCTI env variables
    };
  };
}
# sudo systemd-cryptenroll --tpm2-device=auto --tpm2-pcrs=0+7 /dev/nvme0n1p3 or /dev/sda3
