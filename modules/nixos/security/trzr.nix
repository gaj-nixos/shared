{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.sharedConfig.devices.trezor.enable {
    environment.systemPackages = with pkgs; [
      trezor-suite # Trezor Suite for managing cryptocurrencies
      trezor-agent # SSH and GPG key management with Trezor
      python312Packages.trezor # Python libraries for advanced users/scripts
      gnupg # GPG for OpenPGP key management
      openssh # SSH for key authentication with Trezor
    ];

    services.trezord.enable = true;

    programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
}
