# Common NixOS user config, located under each `users.users."${username}"`
{
  username,
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.sharedConfig;
  useZsh = cfg.software.zsh.enable;

  userConfig = cfg.users."${username}";
  isRoot = username == "root";
  isNormalUser = !isRoot;
  description =
    if isNormalUser then "${userConfig.name},${userConfig.email}" else "System administrator";
in
{
  inherit isNormalUser;
  isSystemUser = isRoot;
  initialPassword = userConfig.initialPassword;
  extraGroups =
    [
      "wheel" # enable sudo
      "networkmanager"
      "docker"
      "samba"
      "input"
      "libvirtd"
      "audio"
      "pipewire"
      "lp" # printer
      "video"
      "tss" # TPM
    ]
    ++ lib.optional (config.sharedConfig.software.wireshark.enable) "wireshark"
    ++ lib.optional (!isRoot) "users";
  openssh.authorizedKeys.keyFiles = userConfig.sshAuthorizedKeyFiles;
  openssh.authorizedKeys.keys = userConfig.sshAuthorizedKeys;
  shell = if useZsh then pkgs.zsh else pkgs.bashInteractive;
  createHome = isNormalUser;
  inherit description;
}
