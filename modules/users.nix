{
  config,
  pkgs,
  lib,
  ...
}:
# import user and home manager modules for each user listed in config.sharedConfig.users
let
  inherit (lib) foldl' mkMerge attrNames;

  combinedHomeManagerConfig = foldl' (
    acc: username:
    let
      hmConfig = {
        users."${username}".imports = [ ./home-manager ];
      };
    in
    # Merge the Home Manager config with the accumulated configuration
    mkMerge [
      acc
      hmConfig
    ]
  ) { } (attrNames config.sharedConfig.users);

  combinedUserConfig = foldl' (
    acc: username:
    # users."${username}".[...]
    let
      usr = import ./userDefaults.nix {
        inherit
          username
          config
          pkgs
          lib
          ;
      };
      userConfig = {
        users."${username}" = usr;
      };
    in
    # Merge the user's config with the accumulated configuration
    mkMerge [
      acc
      userConfig
    ]
  ) { } (attrNames config.sharedConfig.users);
in
{
  config = {
    users = combinedUserConfig;
    home-manager = combinedHomeManagerConfig;
  };
}
