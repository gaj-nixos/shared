{
  sharedConfig,
  config,
  pkgs,
  ...
}:
let
  inherit (sharedConfig.lib) generatePythonPackages;
  swDevProgramsEnabled = config.sharedConfig.software.swDevPrograms.enable;
  btcRiskMetricEnabled = config.sharedConfig.software.btcRiskMetric.enable;

  gitlabDl = import ./gitlab-dl/default.nix { inherit pkgs; };

  gitlabDlPkgs = if swDevProgramsEnabled then [ (generatePythonPackages gitlabDl).pkg ] else [ ]; # pkgs.gitlab-dl

  riskMetric = import ./risk-metric/default.nix { inherit pkgs; };

  riskMetricPkgs = if btcRiskMetricEnabled then [ (generatePythonPackages riskMetric).pkg ] else [ ];
in
{
  config = {
    environment.systemPackages = gitlabDlPkgs ++ riskMetricPkgs;
  };
}
