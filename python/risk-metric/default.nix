{ pkgs, ... }:
let
  riskMetric = {
    scriptDir = ./.;
    scriptName = "BtcRiskMetricV2";
    requirements = with pkgs.python3Packages; [
      numpy
      pandas
      plotly
      #Quandl
      yfinance
    ];
  };
in
riskMetric
