#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import requests
from pathlib import Path

def get_headers(private_token):
    return {'Private-Token': private_token}

def get_data(base_url, headers):
    projects = []
    page = 1
    per_page = 100  # API allows up to 100 items per page

    while True:
        response = requests.get(f'https://{base_url}/api/v4/projects?per_page={per_page}&page={page}', headers=headers, verify=False)
        if response.status_code != 200:
            print(f"Error {response.status_code}: {response.content.decode('utf-8')}")
            break

        current_page_projects = response.json()
        if not current_page_projects:
            break

        projects.extend(current_page_projects)
        page += 1

    return projects

def get_projects(base_url, projects, home):
    for p in projects:
        print(p['http_url_to_repo'])
        print(p['description'])
        path_with_namespace = p['path_with_namespace']
        os.chdir(str(home / base_url))

        if not os.path.exists(home / base_url / path_with_namespace):
            print(f"mkdir -p {home / base_url / path_with_namespace}")
            os.system(f"mkdir -p {home / base_url / path_with_namespace}")
            print(f"git clone {p['http_url_to_repo']} \"{home / base_url / path_with_namespace}\"")
            os.system(f"git clone {p['http_url_to_repo']} \"{home / base_url / path_with_namespace}\"")
        else:
            git_folder = home / base_url / path_with_namespace / ".git"
            if os.path.exists(git_folder):
                os.chdir(str(home / base_url / path_with_namespace))
                print(f"git fetch --prune \"{home / base_url / path_with_namespace}\"")
                os.system(f"git fetch --prune")
                os.system(f"git pull")
            else:
                print(f"Ignoring path \"{home / base_url / path_with_namespace}\"")
        print()
    os.chdir(str(home / base_url))

def main():
    base_url = sys.argv[1]
    private_token = sys.argv[2]

    home = Path(sys.argv[3]) if len(sys.argv) > 3 and sys.argv[3] is not None else Path.home()

    if not os.path.exists(home / base_url):
        os.makedirs(home / base_url)
    os.chdir(str(home / base_url))

    headers = get_headers(private_token)
    projects = get_data(base_url, headers)

    get_projects(base_url, projects, home)

if __name__ == "__main__":
    main()
