{ pkgs, ... }:
let
  gitlabDl = {
    scriptDir = ./.;
    scriptName = "gitlab-dl";
    requirements = with pkgs.python3Packages; [
      requests
    ];
  };
in
gitlabDl
