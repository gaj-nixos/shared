{ pkgs, ... }:
let
  gitlabDl = {
    scriptDir = ./.;
    scriptName = "coin-info";
    requirements = with pkgs.python3Packages; [
      python-binance
      progress
      art
      termgraph
      python-coinmarketcap
      twitter
    ];
  };
in
gitlabDl
